VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} PutModelToServerForm 
   Caption         =   "����� �����"
   ClientHeight    =   4425
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   7305
   OleObjectBlob   =   "PutModelToServerForm.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "PutModelToServerForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Private Sub ChooseFolderButton_Click()
  For i = 0 To FolderListBox.ListCount - 1
    If FolderListBox.Selected(i) = True Then
      PutModelToServer.PutModel (i)
      Exit For
    End If
  Next i
End Sub

Private Sub LoadFoldersFoldersButton_Click()
  For i = 0 To FolderListBox.ListCount - 1
    If FolderListBox.Selected(i) = True Then
      PutModelToServer.FillFolderListBoxFromFolder (i)
      Exit For
    End If
  Next i
End Sub

Private Sub LoadProjectsFoldersButton_Click()
  FolderListBox.Clear
  For i = 0 To ProjectListBox.ListCount - 1
    If ProjectListBox.Selected(i) = True Then
      PutModelToServer.FillFolderListBoxFromProject (i)
      Exit For
    End If
  Next i
End Sub
