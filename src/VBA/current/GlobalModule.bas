Attribute VB_Name = "GlobalModule"
Option Private Module

Public IsAuthorized As Boolean
Public UserId As String
Public SessionId As String
Public URL As String
Public Projects As New Collection
Public Folders As New Collection
Public Products As New Collection

Public Const LoginURL As String = "/login"
Public Const GetProductsFromFolderURL As String = "/productsByFolder"
Public Const GetCadModelByIdURL As String = "/getCadModel"
Public Const GetCadModelNameByIdURL As String = "/getCadModelName"
Public Const GetFoldersByProjectURL As String = "/foldersByProject"
Public Const GetFoldersByFolderURL As String = "/foldersByFolder"
Public Const UploadModelURL As String = "/postProduct"

' ������� ����� � ����� ������
Sub ClearAll()
  GetModelInWorkForm.ProjectListBox.Clear
  GetModelInWorkForm.FolderListBox.Clear
  GetModelInWorkForm.ProductListBox.Clear
  FolderChooseForm.ProjectListBox.Clear
  FolderChooseForm.FolderListBox.Clear
  Set Projects = New Collection
  Set Folders = New Collection
  Set Products = New Collection
End Sub

' �������� ������ �������� � �������
Function GetAllProjects() As Collection
  Dim projectURL As String
  ' ����� �����
  projectURL = URL & "/getProjects"
      
  Dim xmlDoc As New MSXML2.DOMDocument60
  Set xmlDoc = GetXmlFromServer(projectURL)
      
  ' ���� ���� � ���������
  Dim xnodelist As MSXML2.IXMLDOMNodeList
  Set xnodelist = xmlDoc.getElementsByTagName("ProjectViewModel")
  
  Dim xnode As MSXML2.IXMLDOMNode
  
  Dim mProjects As New Collection
  
  ' �������� �� ���� �����, ���� - ������
  For i = 0 To xnodelist.Length - 1
    ' ���������� � ������� ���� ��������� �������
    Set xnode = xnodelist.Item(i)
    Dim curP As New Collection
    ' ������ ������
    Set curP = ParseProject(xnode)
    mProjects.Add curP, CStr(i)
  Next i
  
  Set GetAllProjects = mProjects
End Function

' ��������� � ������� (get), ��������� ������ � ���� xml
Function GetXmlFromServer(getUrl As String) As MSXML2.DOMDocument60
  Dim httpReq As New WinHttpRequest
  httpReq.Open "GET", getUrl, False
  httpReq.SetRequestHeader "accept", "application/xml"
  
  ' �������� ������
  httpReq.Send
  
  ' �������� �����
  Dim strResp As String
  strResp = httpReq.ResponseText
    
  ' ������ ����� � xml
  Dim xmlDoc As New MSXML2.DOMDocument60
  If Not xmlDoc.LoadXML(strResp) Then
    MsgBox "Load error"
    Exit Function
  End If
      
  Set GetXmlFromServer = xmlDoc
End Function

' ������� �����-�������
Function ParseProject(pNode As MSXML2.IXMLDOMNode) As Collection
 
  Dim project As New Collection
  project.Add pNode.ChildNodes(0).Text, "Id"
  project.Add pNode.ChildNodes(1).Text, "Sign"
  project.Add pNode.ChildNodes(2).Text, "Name"
  
  Set ParseProject = project

End Function

' ���������� ������ �������� �� �����
Sub FillProjectListBox(coll As Collection)
  For i = 0 To coll.Count - 1
    Dim curColl As New Collection
    Set curColl = coll(CStr(i))
    FolderChooseForm.ProjectListBox.AddItem curColl("Sign")
    GetModelInWorkForm.ProjectListBox.AddItem curColl("Sign")
  Next i
End Sub

' �������� � ���������� ������ ����� �� ����� �� ������� �������
Sub FillFolderListBoxFromProject(projectIndex As Integer)
  Dim projectId As String
  Dim curProj As Collection
  Set curProj = Projects(CStr(projectIndex))
  projectId = curProj("Id")
  
  Set Folders = LoadFoldersFromProject(projectId, GetFoldersByProjectURL)
    
  For i = 0 To Folders.Count - 1
    Dim curColl As New Collection
    Set curColl = Folders(CStr(i))
    FolderChooseForm.FolderListBox.AddItem curColl("Sign")
    GetModelInWorkForm.FolderListBox.AddItem curColl("Sign")
  Next i
  
End Sub

' �������� ����� �� ������� �����
Sub FillFolderListBoxFromFolder(folderIndex As Integer)
  Dim folderId As String
  Dim curFold As Collection
  Set curFold = Folders(CStr(folderIndex))
  folderId = curFold("Id")
  
  Dim newFolders As New Collection
  
  Set newFolders = LoadFoldersFromProject(folderId, GetFoldersByFolderURL)
  
  If (newFolders.Count > 0) Then
    Set Folders = newFolders
  Else
    MsgBox "� ���� ����� ��� ��������"
    Exit Sub
  End If
    
  FolderChooseForm.FolderListBox.Clear
  GetModelInWorkForm.FolderListBox.Clear
    
  For i = 0 To Folders.Count - 1
    Dim curColl As New Collection
    Set curColl = Folders(CStr(i))
    FolderChooseForm.FolderListBox.AddItem curColl("Sign")
    GetModelInWorkForm.FolderListBox.AddItem curColl("Sign")
  Next i
  
End Sub

' �������� ����� �� id �������
Function LoadFoldersFromProject(id As String, getUrl As String) As Collection
  Dim mUrl As String
  mUrl = URL & getUrl & "/" & id
  Dim xmlDoc As New MSXML2.DOMDocument60
  Set xmlDoc = GetXmlFromServer(mUrl)
        
  ' ���� ���� � ���������
  Dim xnodelist As MSXML2.IXMLDOMNodeList
  Set xnodelist = xmlDoc.getElementsByTagName("FolderViewModel")
  
  Dim xnode As MSXML2.IXMLDOMNode
  
  Dim mfolders As New Collection
  
  ' �������� �� ���� �����
  For i = 0 To xnodelist.Length - 1
    ' ���������� � ������� ���� ��������� �������
    Set xnode = xnodelist.Item(i)
    Dim curF As New Collection
    Set curF = ParseProject(xnode)
    mfolders.Add curF, CStr(i)
  Next i
  
  Set LoadFoldersFromProject = mfolders
End Function
