Attribute VB_Name = "GetModelInWork"
Sub CATMain()
  ' ������ ��������������
  If GlobalModule.IsAuthorized <> True Or GlobalModule.SessionId = "" Or GlobalModule.UserId = "" Then
    MsgBox "����������, �������������"
    Exit Sub
  End If

  ' ������� �����
  ClearAll
  ' ��������� url
  URL = "https://localhost:44344/api/v1/cad"
  
  ' �������� �������
  Dim projets As New Collection
  Set GlobalModule.Projects = GetAllProjects()
    
  ' ��������� ������ �������� �� �����
  FillProjectListBox GlobalModule.Projects
  
  ' ���������� �����
  GetModelInWorkForm.Show
End Sub

' ������� ����� � ����� ������
Sub ClearAll()
  GlobalModule.ClearAll
End Sub

' �������� ������ �������� � �������
Function GetAllProjects() As Collection
  Set GlobalModule.Projects = GlobalModule.GetAllProjects
End Function

' ���������� ������ �������� �� �����
Sub FillProjectListBox(coll As Collection)
  GlobalModule.FillProjectListBox col
End Sub

' ������ ����� �� ������� �������
Sub FillFolderListBoxFromProject(projectIndex As Integer)
  GlobalModule.FillFolderListBoxFromProject projectIndex
End Sub

Sub FillProductListBoxFromFolder(folderIndex As Integer)
  Dim folderId As String
  Dim curFold As Collection
  Set curFold = Folders(CStr(folderIndex))
  folderId = curFold("Id")
  
  Dim mProducts As New Collection
  Set mProducts = LoadProductsFromFolder(folderId, GlobalModule.GetProductsFromFolderURL)
  
  Set Products = mProducts
  GetModelInWorkForm.FolderListBox.Clear
  
  For i = 0 To Products.Count - 1
    Dim curColl As New Collection
    Set curColl = Products(CStr(i))
    Dim sign As String
    sign = curColl("PartNumber") & " - " & curColl("State")
    GetModelInWorkForm.ProductListBox.AddItem sign
  Next i
  
End Sub

Sub FillFolderListBoxFromFolder(folderIndex As Integer)
  GlobalModule.FillFolderListBoxFromFolder folderIndex
End Sub

Function LoadFoldersFromProject(id As String, getUrl As String) As Collection
  Set LoadFoldersFromProject = GlobalModule.LoadFoldersFromProject(id, getUrl)
End Function

Function LoadProductsFromFolder(id As String, getUrl As String) As Collection
  Dim mUrl As String
  mUrl = GlobalModule.URL & getUrl & "/" & id
  Dim xmlDoc As New MSXML2.DOMDocument60
  Set xmlDoc = GetXmlFromServer(mUrl)
        
  ' ���� ���� � ���������
  Dim xnodelist As MSXML2.IXMLDOMNodeList
  Set xnodelist = xmlDoc.getElementsByTagName("Product")
  
  Dim mProducts As New Collection
  
  Dim xnode As MSXML2.IXMLDOMNode
  
  For i = 0 To xnodelist.Length - 1
    ' ���������� � ������� ���� ��������� �������
    Set xnode = xnodelist.Item(i)
    Dim curP As New Collection
    Set curP = ParseProduct(xnode)
    mProducts.Add curP, CStr(i)
  Next i
  
  Set LoadProductsFromFolder = mProducts
  
End Function

Function ParseProduct(pNode As MSXML2.IXMLDOMNode) As Collection
 
  Dim curP As New Collection
  curP.Add pNode.ChildNodes(0).Text, "Id"
  curP.Add pNode.ChildNodes(1).Text, "PartNumber"
  curP.Add pNode.ChildNodes(2).Text, "Description"
  curP.Add pNode.ChildNodes(3).Text, "WeightKg"
  curP.Add pNode.ChildNodes(4).Text, "State"
  
  Set ParseProduct = curP

End Function

Sub TryGetProduct()

  Dim browseFolder As Object

  Set browseFolder = CreateObject("Shell.Application"). _
  BrowseForFolder(0, "����������, �������� ����������, � ������� ����� �������� ����", 0, ".\")
  
  Dim pathToFolder As String
  On Error GoTo SubEnd
  pathToFolder = browseFolder.self.Path
  MsgBox pathToFolder
  
SubEnd:
  MsgBox "����� �� ���� �������"
End Sub

Function LoadProductWithFilesById(id As String, getUrl As String) As Collection
  Dim mUrl As String
  mUrl = GlobalModule.URL & getUrl & "/" & id
  Dim xmlDoc As New MSXML2.DOMDocument60
  Set xmlDoc = GetXmlFromServer(mUrl)
        
  ' ���� ���� � ���������
  Dim xnodelist As MSXML2.IXMLDOMNodeList
  Set xnodelist = xmlDoc.getElementsByTagName("Product")
  
  Dim mProducts As New Collection
  
  Dim xnode As MSXML2.IXMLDOMNode
  
  For i = 0 To xnodelist.Length - 1
    ' ���������� � ������� ���� ��������� �������
    Set xnode = xnodelist.Item(i)
    Dim curP As New Collection
    Set curP = ParseProduct(xnode)
    mProducts.Add curP, CStr(i)
  Next i
  
  Set LoadProductsFromFolder = mProducts
  
End Function

Sub Test()
  DownloadProduct 1
End Sub

Sub DownloadProduct(productIndex As Integer)
  Dim productId As String
  Dim curProd As Collection
  Set curProd = GlobalModule.Products(CStr(productIndex))
  productId = curProd("Id")
  
  ' debug
  'productId = "test"
  
  ' url ��� ���������� ������
  Dim downloadURL As String
  downloadURL = GlobalModule.URL & GlobalModule.GetCadModelByIdURL & "/" & productId
  
  ' url ��� ������������ ��� ������
  Dim nameURL As String
  nameURL = GlobalModule.URL & GlobalModule.GetCadModelNameByIdURL & "/" & productId
  
  ' debug
  'downloadURL = "https://localhost:44344/api/v1/cad" + GlobalModule.GetCadModelByIdURL + "/" + productId
  'nameURL = "https://localhost:44344/api/v1/cad" + GlobalModule.GetCadModelNameByIdURL + "/" + productId
  
  ' �������� ����� ��� ����������
  Dim browseFolder As Object
  Set browseFolder = CreateObject("Shell.Application"). _
  BrowseForFolder(0, "����������, �������� ���������� ��� ���������� CAD-������", 0, ".\")
  
  Dim pathToFolder As String
  On Error GoTo FolderNotSelected
  pathToFolder = browseFolder.self.Path
  
  ' ���������� ��� ������
  Dim WinHttpReq As New WinHttpRequest
  On Error GoTo HttpEx
  WinHttpReq.Open "GET", nameURL, False
  WinHttpReq.Send
  
  Dim fileName As String
  
  If WinHttpReq.Status = 200 Then
    fileName = WinHttpReq.ResponseText
  Else
    GoTo Not_200
  End If
   
  
  
  ' ��������� ������
  On Error GoTo HttpEx
  WinHttpReq.Open "GET", downloadURL, False
  WinHttpReq.Send
  
  ' ���� �� ��, ���������
  If WinHttpReq.Status = 200 Then
    Set oStream = CreateObject("ADODB.Stream")
    oStream.Open
    oStream.Type = 1
    oStream.Write WinHttpReq.ResponseBody
    
    On Error GoTo SaveEx
    oStream.SaveToFile pathToFolder & "\" & fileName, 2 ' 1 = no overwrite, 2 = overwrite
    oStream.Close
    MsgBox "���� �������� � " & pathToFolder & " ��� ������ " & fileName
  Else
    GoTo Not_200
  End If
  
  Exit Sub
  
FolderNotSelected:
  MsgBox "����� �� ���� �������"
  Exit Sub
HttpEx:
  MsgBox "������ �� ����� ���������� � ��������"
  Exit Sub
Not_200:
  MsgBox "������ �� �������"
  Exit Sub
SaveEx:
  MsgBox "������ �� ����� ���������� �� ���������"
  Exit Sub
End Sub
