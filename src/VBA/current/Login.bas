Attribute VB_Name = "Login"
Sub CATMain()
  LoginForm.Show
End Sub

Sub Login(sUrl As String, sLogin As String, sPassword As String)
  Dim oFields As Object
  Set oFields = CreateObject("Scripting.Dictionary")
  With oFields
    .Add "Login", sLogin
    .Add "Password", sPassword
  End With
  
  GlobalModule.URL = sUrl
  Dim LoginURL As String
  LoginURL = GlobalModule.URL & GlobalModule.LoginURL
  
  UploadMultipart oFields, LoginURL
  
End Sub

Private Sub UploadMultipart(oFields As Object, sUrl As String)
  Dim sBoundary As String
  Dim sPayLoad As String
  
  sBoundary = String(6, "-") & Replace(Mid(CreateObject("Scriptlet.TypeLib").GUID, 2, 36), "-", "")
  sPayLoad = ""
  For Each sName In oFields
    sPayLoad = sPayLoad & "--" & sBoundary & vbCrLf
    sPayLoad = sPayLoad & "Content-Disposition: form-data; name=""" & sName & """" & vbCrLf & vbCrLf
    sPayLoad = sPayLoad & oFields(sName) & vbCrLf
  Next
  
  sPayLoad = sPayLoad & "--" & sBoundary & "--"
  Set httpReq = CreateObject("MSXML2.XMLHTTP")
  httpReq.Open "Post", sUrl, False
  httpReq.SetRequestHeader "Content-Type", "multipart/form-data; boundary=" & sBoundary
  httpReq.SetRequestHeader "accept", "application/xml"

  httpReq.Send (sPayLoad)
    
  Dim strResp As String
  strResp = httpReq.ResponseText
  
  ' ������ ����� � xml
  Dim xmlDoc As New MSXML2.DOMDocument60
  If Not xmlDoc.LoadXML(strResp) Then
    GoTo ErrorSub
    Exit Sub
  End If
  
  Dim xnodelist As MSXML2.IXMLDOMNodeList
  Set xnodelist = xmlDoc.getElementsByTagName("UserData")
  
  On Error GoTo ErrorSub
  Set xnode = xnodelist.Item(0)
  GlobalModule.UserId = xnode.ChildNodes(0).Text
  GlobalModule.SessionId = xnode.ChildNodes(1).Text
   
  If GlobalModule.UserId = "" Or GlobalModule.SessionId = "" Then
    GoTo ErrorSub
  End If
  
  MsgBox "�� ������� ����������������"
  
  Unload LoginForm
  GetModelInWorkForm.Show
  
  Exit Sub


ErrorSub:
  MsgBox "������"
  Exit Sub
End Sub
