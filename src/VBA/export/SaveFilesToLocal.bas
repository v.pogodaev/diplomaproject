Attribute VB_Name = "SaveFilesToLocal"
Sub SaveFiles()

  Const CATProduct As String = ".CATProduct"
  Const WRL As String = "wrl"
  
  Dim browseFolder As Object

  Set browseFolder = CreateObject("Shell.Application"). _
  BrowseForFolder(0, "����������, �������� ����������, � ������� ������������� ������", 0, ".\")
  
  Dim pathToFolder As String
  On Error GoTo SubEnd
  pathToFolder = browseFolder.self.Path
  
  Set browseFolder = CreateObject("Shell.Application"). _
  BrowseForFolder(0, "����������, �������� ����������, � ������� ����� ��������� VRML �����", 0, ".\")
  On Error GoTo SubEnd
  pathToFolderToSave = browseFolder.self.Path
  
  Set fso = CreateObject("Scripting.FileSystemObject")
  Set curFold = fso.GetFolder(pathToFolder)
  
  On Error Resume Next
  fso.DeleteFile pathToFolderToSave & "\*." & WRL
  
  Dim ThisProduct As ProductDocument
  Dim ProjectName As String
  
  For Each file In curFold.Files
    If file.Name Like "*" & CATProduct Then
      Set ThisProduct = CATIA.Documents.Open(file.Path)
      ProjectName = ThisProduct.Name
      ProjectName = Replace(ProjectName, CATProduct, "")
      ThisProduct.ExportData pathToFolderToSave & "\" & ProjectName & "." & WRL, WRL
      Exit For
    End If
  Next
  
  Dim ThisPart As PartDocument
  Dim PartName As String
  For Each file In curFold.Files
    If file.Name Like "*.CATPart" Then
      Set ThisPart = CATIA.Documents.Open(file.Path)
      PartName = ThisPart.Name
      ThisPart.ExportData pathToFolderToSave & "\" & PartName & "." & WRL, WRL
      ThisPart.Close
    End If
  Next
  
SubEnd:
  MsgBox "����� �� ���� �������"
End Sub
