Attribute VB_Name = "UploadFiles"
Sub PostFile()

    Dim objHttp As New WinHttpRequest
    Dim URL As String
    Dim file As Variant
    
    file = "Step4_complete.wrl"
        
    URL = "https://localhost:44344/api/v1/cad/upload"
    
    objHttp.Open "POST", URL, False
    objHttp.SetRequestHeader "Content-type", "multipart/form-data"
    objHttp.Send file
    
    Dim strResp As String
    strResp = objHttp.ResponseText
    
    Dim fso As Object
    Set fso = CreateObject("Scripting.FileSystemObject")

    Dim Fileout As Object
    Set Fileout = fso.CreateTextFile("response.html", True, True)
    Fileout.Write strResp
    Fileout.Close
    
    MsgBox "log done"

End Sub

Sub PostTest()

    Dim objHttp As Object
    Dim URL As String
    
    Dim message As String
    message = "Hello, service"
    
    Set objHttp = CreateObject("MSXML2.ServerXMLHTTP")
    URL = "https://localhost:44344/api/v1/cad/post"
    
    objHttp.Open "POST", URL, False
    objHttp.SetRequestHeader "Content-type", "application/json"
    objHttp.Send message
    
    Dim strResp As String
    strResp = objHttp.ResponseText
    
    MsgBox strResp

End Sub


'''''''''''''''''''''''''''''''''
Sub NewUpload()
    UploadFile "https://localhost:44344/api/v1/cad/upload", "D:\Other\Study\_08_sem\Diploma\diplomaproject\src\VBA\Step4_complete.wrl"
End Sub

Sub UploadFile(DestURL As String, FileName As String, Optional ByVal FieldName As String = "File")
  
  Dim sFormData As String, d As String

  'Boundary of fields.
  'Be sure this string is Not In the source file
  Const Boundary As String = "---------------------------0123456789012sdsdssdsdsd"

  'Get source file As a string.
  sFormData = GetFile(FileName)

  'Build source form with file contents
  d = "--" + Boundary + vbCrLf
  d = d + "Content-Disposition: form-data; name=""" + FieldName + """;"
  d = d + " filename=""" + FileName + """" + vbCrLf
  d = d + "Content-Type: application/upload" + vbCrLf + vbCrLf
  d = d + sFormData
  sFormData = d + vbCrLf + "--" + Boundary + "--" + vbCrLf

  'Post the data To the destination URL
  WinHTTPPostRequest DestURL, sFormData, Boundary
End Sub

'read binary file As a string value
Function GetFile(FileName As String) As String
  Dim FileContents() As Byte, FileNumber As Integer
  ReDim FileContents(FileLen(FileName) - 1)
  FileNumber = FreeFile
  Open FileName For Binary As FileNumber
    Get FileNumber, , FileContents
  Close FileNumber
  GetFile = StrConv(FileContents, vbUnicode)
End Function

'sends multipart/form-data To the URL using WinHttprequest/XMLHTTP
'FormData - binary (VT_UI1 | VT_ARRAY) multipart form data
Function WinHTTPPostRequest(URL, FormData, Boundary)
  'Dim http As New MSXML2.XMLHTTP

  'Create XMLHTTP/ServerXMLHTTP/WinHttprequest object
  'You can use any of these three objects.
  'Set http = CreateObject("WinHttp.WinHttprequest.5")
  Set http = CreateObject("MSXML2.XMLHTTP")
  'Set http = CreateObject("MSXML2.ServerXMLHTTP")

  'Open URL As POST request
  http.Open "POST", URL, False

  'Set Content-Type header
  http.SetRequestHeader "Content-Type", "multipart/form-data; boundary=" + Boundary
  'http.SetRequestHeader "Content-Type", " multipart/form-data"
  'Send the form data To URL As POST binary request
  http.Send (FormData)

  'Get a result of the script which has received upload
  Dim fso As Object
    Set fso = CreateObject("Scripting.FileSystemObject")
    
    Dim Fileout As Object
    Set Fileout = fso.CreateTextFile("response.html", True, True)
    Fileout.Write http.ResponseText
    Fileout.Close
    
    MsgBox "log done"
  WinHTTPPostRequest = http.ResponseText
End Function
'''''''''''''''''''''''''''''''''''

Sub UploadMiltipart()

  Dim oFields As Object
  Dim oFiles As Object
  Dim oFileNames As Object
  Dim sBoundary As String
  Dim sPayLoad As String
  Dim vName As Variant
  
  Set oFields = CreateObject("Scripting.Dictionary")
  With oFields
    .Add "ProductId", "TestId"
    .Add "PartNumber", "TestPartNumber"
    .Add "Description", "TestDescription"
    .Add "WeightKg", 2
    .Add "State", "TestState"
  End With
  Set oFiles = CreateObject("Scripting.Dictionary")
  With oFiles
    .Add "CADModel", GetFile("D:\Other\Study\_08_sem\Diploma\diplomaproject\src\VBA\readme.txt")
    .Add "ViewerModel", GetFile("D:\Other\Study\_08_sem\Diploma\diplomaproject\src\VBA\vba code.txt")
  End With
  Set oFileNames = CreateObject("Scripting.Dictionary")
  With oFileNames
    .Add "CADModel", "readme.txt"
    .Add "ViewerModel", "vba code.txt"
  End With
  
  sBoundary = String(6, "-") & Replace(Mid(CreateObject("Scriptlet.TypeLib").GUID, 2, 36), "-", "")
  sPayLoad = ""
  For Each sName In oFields
    sPayLoad = sPayLoad & "--" & sBoundary & vbCrLf
    sPayLoad = sPayLoad & "Content-Disposition: form-data; name=""" & sName & """" & vbCrLf & vbCrLf
    sPayLoad = sPayLoad & oFields(sName) & vbCrLf
  Next
  For Each sName In oFiles
    sPayLoad = sPayLoad & "--" & sBoundary & vbCrLf
    sPayLoad = sPayLoad & "Content-Disposition: form-data; name=""" & sName & """;"
    sPayLoad = sPayLoad & " filename=""" & oFileNames(sName) & """" + vbCrLf
    sPayLoad = sPayLoad & "Content-Type: application/upload" & vbCrLf & vbCrLf
    sPayLoad = sPayLoad & oFiles(sName) & vbCrLf
  Next
  
  sPayLoad = sPayLoad & "--" & sBoundary & "--"
  With CreateObject("MSXML2.XMLHTTP")
    .Open "POST", "https://localhost:44344/api/v1/CAD/uploadProduct", False
    .SetRequestHeader "Content-Type", "multipart/form-data; boundary=" & sBoundary
    '.SetRequestHeader "Content-Length", LenB(sPayLoad)
    .Send (sPayLoad)
    
    Dim fso As Object
    Set fso = CreateObject("Scripting.FileSystemObject")
    
    Dim Fileout As Object
    Set Fileout = fso.CreateTextFile("D:\Other\Study\_08_sem\Diploma\diplomaproject\src\VBA\response.html", True, True)
    Fileout.Write .ResponseText
    Fileout.Close
    
    MsgBox "log done"
    'MsgBox .ResponseText ' Erreur de login : Verifier vos identifiants !
  End With

End Sub

