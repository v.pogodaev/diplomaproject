Attribute VB_Name = "GetModelInWorkModule"
Public URL As String
Public Projects As New Collection
Public Folders As New Collection
Public Products As New Collection

Sub CATMain()

    ClearAll
    ChooseFileFromServer
  
End Sub

Sub ClearAll()
  FolderChooseForm.ProjectListBox.Clear
  GetModelInWorkForm.ProjectListBox.Clear
  FolderChooseForm.FolderListBox.Clear
  GetModelInWorkForm.FolderListBox.Clear
  GetModelInWorkForm.ProductListBox.Clear
  Set Projects = New Collection
  Set Folders = New Collection
  Set Products = New Collection
End Sub

Sub ChooseFileFromServer()
  ClearAll
  URL = "https://localhost:44344/api/v1/cad"
  
  ' �������� �������
  Dim projets As New Collection
  Set Projects = GetAllProjects()
    
  FillProjectListBox Projects
  
  GetModelInWorkForm.Show

End Sub

Function GetAllProjects() As Collection
  Dim projectURL As String
  projectURL = URL & "/getProjects"
      
  Dim xmlDoc As New MSXML2.DOMDocument60
  Set xmlDoc = GetXmlFromServer(projectURL)
      
  ' ���� ���� � ���������
  Dim xnodelist As MSXML2.IXMLDOMNodeList
  Set xnodelist = xmlDoc.getElementsByTagName("ProjectViewModel")
  
  Dim xnode As MSXML2.IXMLDOMNode
  
  Dim Projects As New Collection
  
  ' �������� �� ���� �����
  For i = 0 To xnodelist.Length - 1
    ' ���������� � ������� ���� ��������� �������
    Set xnode = xnodelist.Item(i)
    Dim curP As New Collection
    Set curP = ParseProject(xnode)
    Projects.Add curP, CStr(i)
  Next i
  
  Set GetAllProjects = Projects

End Function

Function GetXmlFromServer(getUrl As String) As MSXML2.DOMDocument60
  Dim httpReq As New WinHttpRequest
  httpReq.Open "GET", getUrl, False
  httpReq.SetRequestHeader "accept", "application/xml"
  
  ' �������� ������
  httpReq.Send
  
  ' �������� �����
  Dim strResp As String
  strResp = httpReq.ResponseText
    
  ' ������ ����� � xml
  Dim xmlDoc As New MSXML2.DOMDocument60
  If Not xmlDoc.LoadXML(strResp) Then
    MsgBox "Load error"
    Exit Function
  End If
      
  Set GetXmlFromServer = xmlDoc
End Function

Function ParseProject(pNode As MSXML2.IXMLDOMNode) As Collection
 
  Dim project As New Collection
  project.Add pNode.ChildNodes(0).Text, "Id"
  project.Add pNode.ChildNodes(1).Text, "Sign"
  project.Add pNode.ChildNodes(2).Text, "Name"
  
  Set ParseProject = project

End Function

Sub FillProjectListBox(coll As Collection)

  For i = 0 To coll.Count - 1
    Dim curColl As New Collection
    Set curColl = coll(CStr(i))
    FolderChooseForm.ProjectListBox.AddItem curColl("Sign")
    GetModelInWorkForm.ProjectListBox.AddItem curColl("Sign")
  Next i
  
End Sub

Sub FillFolderListBoxFromProject(projectIndex As Integer)
  Dim projectId As String
  Dim curProj As Collection
  Set curProj = Projects(CStr(projectIndex))
  projectId = curProj("Id")
  
  Set Folders = LoadFoldersFromProject(projectId, "/foldersByProject")
    
  For i = 0 To Folders.Count - 1
    Dim curColl As New Collection
    Set curColl = Folders(CStr(i))
    FolderChooseForm.FolderListBox.AddItem curColl("Sign")
    GetModelInWorkForm.FolderListBox.AddItem curColl("Sign")
  Next i
  
End Sub

Sub FillProductListBoxFromFolder(folderIndex As Integer)
  Dim folderId As String
  Dim curFold As Collection
  Set curFold = Folders(CStr(folderIndex))
  folderId = curFold("Id")
  
  Dim mProducts As New Collection
  Set mProducts = LoadProductsFromFolder(folderId, "/productsByFolder")
  
  Set Products = mProducts
  GetModelInWorkForm.FolderListBox.Clear
  
  For i = 0 To Products.Count - 1
    Dim curColl As New Collection
    Set curColl = Products(CStr(i))
    Dim sign As String
    sign = curColl("PartNumber") & " - " & curColl("State")
    GetModelInWorkForm.ProductListBox.AddItem sign
  Next i
  
End Sub

Sub FillFolderListBoxFromFolder(folderIndex As Integer)
  Dim folderId As String
  Dim curFold As Collection
  Set curFold = Folders(CStr(folderIndex))
  folderId = curFold("Id")
  
  Dim newFolders As New Collection
  
  Set newFolders = LoadFoldersFromProject(folderId, "/foldersByFolder")
  
  If (newFolders.Count > 0) Then
    Set Folders = newFolders
  Else
    MsgBox "� ���� ����� ��� ��������"
    Exit Sub
  End If
    
  FolderChooseForm.FolderListBox.Clear
  GetModelInWorkForm.FolderListBox.Clear
    
  For i = 0 To Folders.Count - 1
    Dim curColl As New Collection
    Set curColl = Folders(CStr(i))
    FolderChooseForm.FolderListBox.AddItem curColl("Sign")
    GetModelInWorkForm.FolderListBox.AddItem curColl("Sign")
  Next i
  
End Sub

Function LoadFoldersFromProject(id As String, getUrl As String) As Collection
  Dim mUrl As String
  mUrl = URL & getUrl & "/" & id
  Dim xmlDoc As New MSXML2.DOMDocument60
  Set xmlDoc = GetXmlFromServer(mUrl)
        
  ' ���� ���� � ���������
  Dim xnodelist As MSXML2.IXMLDOMNodeList
  Set xnodelist = xmlDoc.getElementsByTagName("FolderViewModel")
  
  Dim xnode As MSXML2.IXMLDOMNode
  
  Dim mfolders As New Collection
  
  ' �������� �� ���� �����
  For i = 0 To xnodelist.Length - 1
    ' ���������� � ������� ���� ��������� �������
    Set xnode = xnodelist.Item(i)
    Dim curF As New Collection
    Set curF = ParseProject(xnode)
    mfolders.Add curF, CStr(i)
  Next i
  
  Set LoadFoldersFromProject = mfolders
End Function

Function LoadProductsFromFolder(id As String, getUrl As String) As Collection
  Dim mUrl As String
  mUrl = URL & getUrl & "/" & id
  Dim xmlDoc As New MSXML2.DOMDocument60
  Set xmlDoc = GetXmlFromServer(mUrl)
        
  ' ���� ���� � ���������
  Dim xnodelist As MSXML2.IXMLDOMNodeList
  Set xnodelist = xmlDoc.getElementsByTagName("Product")
  
  Dim mProducts As New Collection
  
  Dim xnode As MSXML2.IXMLDOMNode
  
  For i = 0 To xnodelist.Length - 1
    ' ���������� � ������� ���� ��������� �������
    Set xnode = xnodelist.Item(i)
    Dim curP As New Collection
    Set curP = ParseProduct(xnode)
    mProducts.Add curP, CStr(i)
  Next i
  
  Set LoadProductsFromFolder = mProducts
  
End Function

Function ParseProduct(pNode As MSXML2.IXMLDOMNode) As Collection
 
  Dim curP As New Collection
  curP.Add pNode.ChildNodes(0).Text, "Id"
  curP.Add pNode.ChildNodes(1).Text, "PartNumber"
  curP.Add pNode.ChildNodes(2).Text, "Description"
  curP.Add pNode.ChildNodes(3).Text, "WeightKg"
  curP.Add pNode.ChildNodes(4).Text, "State"
  
  Set ParseProduct = curP

End Function

Sub TryGetProduct()

  Dim browseFolder As Object

  Set browseFolder = CreateObject("Shell.Application"). _
  BrowseForFolder(0, "����������, �������� ����������, � ������� ����� �������� ����", 0, ".\")
  
  Dim pathToFolder As String
  On Error GoTo SubEnd
  pathToFolder = browseFolder.self.Path
  MsgBox pathToFolder
  
SubEnd:
  MsgBox "����� �� ���� �������"
End Sub

Function LoadProductWithFilesById(id As String, getUrl As String) As Collection
  Dim mUrl As String
  mUrl = URL & getUrl & "/" & id
  Dim xmlDoc As New MSXML2.DOMDocument60
  Set xmlDoc = GetXmlFromServer(mUrl)
        
  ' ���� ���� � ���������
  Dim xnodelist As MSXML2.IXMLDOMNodeList
  Set xnodelist = xmlDoc.getElementsByTagName("Product")
  
  Dim mProducts As New Collection
  
  Dim xnode As MSXML2.IXMLDOMNode
  
  For i = 0 To xnodelist.Length - 1
    ' ���������� � ������� ���� ��������� �������
    Set xnode = xnodelist.Item(i)
    Dim curP As New Collection
    Set curP = ParseProduct(xnode)
    mProducts.Add curP, CStr(i)
  Next i
  
  Set LoadProductsFromFolder = mProducts
  
End Function



'''''''''''''''''''''''''''''''''''''''''''''''
Sub ChoosePlaceToSave()
  ClearAll
  URL = "https://localhost:44344/api/v1/cad"
  
  ' �������� �������
  Dim projets As New Collection
  Set Projects = GetAllProjects()
    
  FillProjectListBox Projects
  
  FolderChooseForm.Show

End Sub

Sub FillFolderListBox(coll As Collection)

  For i = 0 To coll.Count - 1
    Dim curColl As New Collection
    Set curColl = coll(CStr(i))
    FolderChooseForm.ProjectListBox.AddItem curColl("Sign")
  Next i
  
End Sub

Sub Test()
  FolderChooseForm.Show
End Sub

Sub FormTest()
  Dim Projects As New Collection
  Set Projects = GetProjects
  
  For i = 0 To Projects.Count - 1
    Dim curProject As New Collection
    Set curProject = Projects(CStr(i))
    UserForm1.ProjectList.AddItem curProject("Sign")
  Next i
  
  UserForm1.Show
End Sub

Function GetProjects() As Collection
  ' ����� �������
  'Dim URL As String
  'URL = "https://localhost:44344/api/v1/cad/getProjects"
  
  ' ����������� ������ ��� ������ � ���������
  Dim httpReq As New WinHttpRequest
  httpReq.Open "GET", URL, False
  httpReq.SetRequestHeader "accept", "application/xml"
  
  ' �������� ������
  httpReq.Send
  
  ' �������� �����
  Dim strResp As String
  strResp = httpReq.ResponseText
    
  ' ������ ����� � xml
  Dim xmlDoc As New MSXML2.DOMDocument60
  If Not xmlDoc.LoadXML(strResp) Then
    MsgBox "Load error"
    Exit Function
  End If
      
  ' ���� ���� � ���������
  Dim xnodelist As MSXML2.IXMLDOMNodeList
  Set xnodelist = xmlDoc.getElementsByTagName("Project")
  
  Dim xnode As MSXML2.IXMLDOMNode
  
  Dim Projects As New Collection
  
  ' �������� �� ���� �����
  For i = 0 To xnodelist.Length - 1
    ' ���������� � ������� ���� ��������� �������
    Set xnode = xnodelist.Item(i)
    Dim curP As New Collection
    Set curP = ParseProject(xnode)
    Projects.Add curP, CStr(i)
    
  Next i
  
  Set GetProjects = Projects
  
End Function

Function old_ParseProject(pNode As MSXML2.IXMLDOMNode) As Collection
 
  Dim project As New Collection
  project.Add pNode.ChildNodes(0).Text, "Id"
  project.Add pNode.ChildNodes(1).Text, "Sign"
  project.Add pNode.ChildNodes(2).Text, "Name"
  
  For i = 0 To pNode.ChildNodes(3).ChildNodes.Length - 1
    Dim folder As New Collection
    Dim curNode As MSXML2.IXMLDOMNode
    Set curNode = pNode.ChildNodes(3).ChildNodes(i)
    Set folder = ParseFolder(curNode)
    project.Add folder, CStr(i)
  Next i
  
  Set ParseProject = project

End Function

Function ParseFolder(fNode As MSXML2.IXMLDOMNode) As Collection
  Dim folder As New Collection
  folder.Add fNode.ChildNodes(0).Text, "Id"
  folder.Add fNode.ChildNodes(1).Text, "Sign"
  folder.Add fNode.ChildNodes(2).Text, "Name"
  
  Set ParseFolder = folder
End Function
