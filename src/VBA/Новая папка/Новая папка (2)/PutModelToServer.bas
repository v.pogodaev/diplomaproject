Attribute VB_Name = "PutModelToServer"
Private ModelPath As String
Private WrlPath As String

Sub CATMain()
' ������ ��������������
  If GlobalModule.IsAuthorized <> True Or GlobalModule.SessionId = "" Or GlobalModule.UserId = "" Then
    MsgBox "����������, �������������"
    Exit Sub
  End If

  ' ������� �����
  ClearAll
  
  ' �������� �������
  Dim projets As New Collection
  Set Projects = GetAllProjects()
    
  ' ��������� ������ �������� �� �����
  FillProjectListBox GlobalModule.Projects
  
  ' ���������� �����
  PutModelToServerForm.Show
End Sub

' ������� ����� � ����� ������
Sub ClearAll()
  GlobalModule.ClearAll
End Sub

' �������� ������ �������� � �������
Function GetAllProjects() As Collection
  Set GlobalModule.Projects = GlobalModule.GetAllProjects
End Function

' ���������� ������ �������� �� �����
Sub FillProjectListBox(coll As Collection)
  GlobalModule.FillProjectListBox col
End Sub

' ������ ����� �� ������� �������
Sub FillFolderListBoxFromProject(projectIndex As Integer)
  GlobalModule.FillFolderListBoxFromProject projectIndex
End Sub


Sub PutModel(productIndex As Integer)
  
  Dim product As Collection
  Set product = Products.Item(CStr(productIndex))
  
  ' ��������� �����
  If SaveFiles = False Then
    GoTo Ex
  End If
     
  ' ������� � �������� ������
  If UploadMiltipart(product.Item("Id")) = False Then
    GoTo Ex
  End If
  
  MsgBox "������ ������� ���������"
  
  Exit Sub
  
Ex:
  Exit Sub
End Sub

' ��������� cad � wrl ������
Private Function SaveFiles() As Boolean
  Const CATProduct As String = ".CATProduct"
  Const WRL As String = "wrl"
  
  Dim browseFolder As Object

  ' �������� �����
  Set browseFolder = CreateObject("Shell.Application"). _
  BrowseForFolder(0, "����������, �������� ����������, � ������� ����� �������� VRML ����", 0, ".\")
  On Error GoTo SubEnd
  pathToFolderToSave = browseFolder.self.Path
    
  Dim fileName As String
  fileName = Replace(CATIA.ActiveDocument.Name, "CATPart", "")
  
  WrlPath = pathToFolderToSave & "\" & fileName & ".wrl"
  ModelPath = CATIA.ActiveDocument.Path
  CATIA.ActiveDocument.ExportData WrlPath, "wrl"
  CATIA.ActiveDocument.Save
  SaveFiles = True
  
  Exit Function
SubEnd:
  MsgBox "����� �� ���������"
  SaveFiles = False
End Function

' �������� ����� � ������
Private Function UploadMiltipart(productId As String) As Boolean
  Dim oFields As Object
  Dim oFiles As Object
  Dim oFileNames As Object
  Dim sBoundary As String
  Dim sPayLoad As String
  Dim vName As Variant
  
  ' ��������� ������ ������
  On Error GoTo DataEx
  Set oFields = CreateObject("Scripting.Dictionary")
  With oFields
    .Add "ProductId", productId
    .Add "PartNumber", GetPartNumber
    .Add "Description", GetDescription
    .Add "WeightKg", GetWeight
    .Add "UserId", GlobalModule.UserId
    .Add "SessionId", GlobalModule.SessionId
  End With
  
  ' ��������� �����
  On Error GoTo FileEx
  Set oFiles = CreateObject("Scripting.Dictionary")
  With oFiles
    .Add "CADModel", GetFile(ModelPath)
    .Add "ViewerModel", GetFile(WrlPath)
  End With
  Set oFileNames = CreateObject("Scripting.Dictionary")
  With oFileNames
    .Add "CADModel", GetFilenameFromPath(ModelPath)
    .Add "ViewerModel", GetFilenameFromPath(WrlPath)
  End With
  
  ' ������� ������ � ���������
  On Error GoTo PackEx
  sBoundary = String(6, "-") & Replace(Mid(CreateObject("Scriptlet.TypeLib").GUID, 2, 36), "-", "")
  sPayLoad = ""
  For Each sName In oFields
    sPayLoad = sPayLoad & "--" & sBoundary & vbCrLf
    sPayLoad = sPayLoad & "Content-Disposition: form-data; name=""" & sName & """" & vbCrLf & vbCrLf
    sPayLoad = sPayLoad & oFields(sName) & vbCrLf
  Next
  For Each sName In oFiles
    sPayLoad = sPayLoad & "--" & sBoundary & vbCrLf
    sPayLoad = sPayLoad & "Content-Disposition: form-data; name=""" & sName & """;"
    sPayLoad = sPayLoad & " filename=""" & oFileNames(sName) & """" + vbCrLf
    sPayLoad = sPayLoad & "Content-Type: application/upload" & vbCrLf & vbCrLf
    sPayLoad = sPayLoad & oFiles(sName) & vbCrLf
  Next
  
  sPayLoad = sPayLoad & "--" & sBoundary & "--"
  
  ' ��������
  On Error GoTo UploadEx
  With CreateObject("MSXML2.XMLHTTP")
    .Open "POST", GlobalModule.URL & GlobalModule.UploadModelURL, False
    .SetRequestHeader "Content-Type", "multipart/form-data; boundary=" & sBoundary
    .Send (sPayLoad)
  End With

  UploadMiltipart = True

DataEx:
  MsgBox "���������� �������� ������ ������"
  UploadMiltipart = False
FileEx:
  MsgBox "���������� �������� ������ ������"
  UploadMiltipart = False
PackEx:
  MsgBox "���������� ����������� ������ � ��������"
  UploadMiltipart = False
UploadEx:
  MsgBox "������ ��� �������� ������"
  UploadMiltipart = False
End Function

' �������� ���� �� ��� ������� ����
Private Function GetFile(fileName As String) As String
  Dim FileContents() As Byte
  Dim FileNumber As Integer
  ReDim FileContents(FileLen(fileName) - 1)
  FileNumber = FreeFile
  Open fileName For Binary As FileNumber
    Get FileNumber, , FileContents
  Close FileNumber
  GetFile = StrConv(FileContents, vbUnicode)
End Function

' �������� ��� ����� �� ��� ������� ����
Private Function GetFilenameFromPath(ByVal strPath As String) As String
  If Right$(strPath, 1) <> "\" And Len(strPath) > 0 Then
    GetFilenameFromPath = GetFilenameFromPath(Left$(strPath, Len(strPath) - 1)) + Right$(strPath, 1)
  End If
End Function

' �������� part number ������
Private Function GetPartNumber() As String
  ' debug
  GetPartNumber = CATIA.ActiveDocument.product.PartNumber
End Function

' �������� �������� ������
Private Function GetDescription() As String
  GetDescription = CATIA.ActiveDocument.product.DescriptionRef
End Function

' �������� ��� ������
Private Function GetWeight() As Double
  GetWeight = CATIA.ActiveDocument.product.Analyze.Mass
End Function

