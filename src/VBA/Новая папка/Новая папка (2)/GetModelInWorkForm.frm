VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} GetModelInWorkForm 
   Caption         =   "����� ������ � ������"
   ClientHeight    =   4140
   ClientLeft      =   120
   ClientTop       =   465
   ClientWidth     =   9615.001
   OleObjectBlob   =   "GetModelInWorkForm.frx":0000
   StartUpPosition =   1  'CenterOwner
End
Attribute VB_Name = "GetModelInWorkForm"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Private Sub ChooseProductButton_Click()
  For i = 0 To ProductListBox.ListCount - 1
    If ProductListBox.Selected(i) = True Then
      GetModelInWork.DownloadProduct (i)
      Exit For
    End If
  Next i
End Sub

Private Sub LoadFoldersFoldersButton_Click()
  For i = 0 To FolderListBox.ListCount - 1
    If FolderListBox.Selected(i) = True Then
      GetModelInWork.FillProductListBoxFromFolder (i)
      GetModelInWork.FillFolderListBoxFromFolder (i)
      Exit For
    End If
  Next i
End Sub

Private Sub LoadProjectsFoldersButton_Click()
  FolderListBox.Clear
  For i = 0 To ProjectListBox.ListCount - 1
    If ProjectListBox.Selected(i) = True Then
      GetModelInWork.FillFolderListBoxFromProject (i)
      Exit For
    End If
  Next i
End Sub


Private Sub UserForm_Initialize()
  GetModelInWork.ChooseFileFromServer
End Sub
