package com.diploma.DB;

import com.diploma.Transactions.TransactionAction;
import com.diploma.TransactionsViewModels.TransactionDetailsViewModel;
import com.mongodb.*;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.types.ObjectId;
import sun.security.krb5.internal.crypto.Des;

import javax.print.Doc;
import java.net.UnknownHostException;
import java.util.ArrayList;
import java.util.Properties;
import java.util.UUID;
import java.util.function.Consumer;


public class WorkWithMongo {
    // это клиент который обеспечит подключение к БД
    private MongoClient mongoClient;

    // В нашем случае, этот класс дает
// возможность аутентифицироваться в MongoDB
    private MongoDatabase db;

    // тут мы будем хранить состояние подключения к БД
    private boolean authenticate;

    // И класс который обеспечит возможность работать
// с коллекциями / таблицами MongoDB
    private MongoCollection<Document> table;

    public WorkWithMongo(Properties prop) {
        // Создаем подключение
        System.out.println("trying to connect to " + prop.getProperty("connectionString"));
        char[] pass = prop.getProperty("password").toCharArray();
        MongoCredential credential = MongoCredential.createCredential(prop.getProperty("login"), "admin", pass);
        int connectTimeout = 1000 * 60;
        MongoClientOptions options = new MongoClientOptions.Builder().connectTimeout(connectTimeout).build();
        mongoClient = new MongoClient(new ServerAddress(prop.getProperty("connectionString")), credential, options);

        // Выбираем БД для дальнейшей работы
        db = mongoClient.getDatabase(prop.getProperty("dbname"));

        // Выбираем коллекцию/таблицу для дальнейшей работы
        table = db.getCollection(prop.getProperty("table"));

    }

    public void add(DesignDocument dis_Doc){
        Document document = new Document();
        // это поле будет записываться в коллекцию/таблицу
        document.put("designation", dis_Doc.getDesignation());
        document.put("revision", dis_Doc.getRevision());
        document.put("state", dis_Doc.getState());
        document.put("name", dis_Doc.getName());
        document.put("transactionId", dis_Doc.getTransactionId());
        document.put("sessionId", dis_Doc.getSessionId());
        document.put("userId", dis_Doc.getUserId());
        document.put("transactionAction", dis_Doc.getTransactionAction().toString());
        document.put("originalId", dis_Doc.getOriginalId());
        document.put("isOriginal", dis_Doc.getIsOriginal());
        // записываем данные в коллекцию/таблицу
        table.insertOne(document);
    }

    public DesignDocument getByName(String name){
        Document query = new Document();

        // задаем поле и значение поля по которому будем искать
        query.put("name", name);


        // осуществляем поиск

        Document result = table.find(query).first();

        // Заполняем сущность полученными данными с коллекции
        DesignDocument dis_Doc = new DesignDocument();
        dis_Doc.setDesignation(String.valueOf(result.get("designation")));
        dis_Doc.setRevision(Integer.parseInt(result.get("revision").toString()));
        dis_Doc.setState(String.valueOf(result.get("state")));
        dis_Doc.setName(String.valueOf(result.get("name")));
        // возвращаем полученного пользователя
        return dis_Doc;
    }

    public boolean findDoc(String designation){
        Document query = new Document();

        query.put("designation", designation);

        Document result = table.find(query).first();
        if(result != null){
            return true;
        }
        else {
            return false;
        }
    }

    public String getObjectId(String designation){
        Document query = new Document();

        query.put("designation", designation);

        Document result = table.find(query).first();
        return result.get("_id").toString();
    }

    public Document findObjectById(String id){
        Document query = new Document();

        query.put("_id", new ObjectId(id));
        return table.find(query).first();

    }
    /*String id,String designation, Integer revision, String name, String state,
                          String transactionId, String sessionId, String userId, TransactionAction transactionAction*/
    public DesignDocument getObjectById(String id){
        Document doc = table.find(new Document().append("_id", new ObjectId(id))).first();
       DesignDocument retDoc = new DesignDocument(
               doc.get("_id").toString(),
               doc.get("designation").toString(),
               Integer.parseInt(doc.get("revision").toString()),
               doc.get("name").toString(),
               doc.get("state").toString(),
               doc.get("transactionId").toString(),
               doc.get("sessionId").toString(),
               doc.get("userId").toString(),
               ToAction(doc.get("transactionAction").toString())
       );
        return retDoc;
    }

    public void updateDocument(Document document, String newDesignation, String newName){
        Document newDocument = new Document();

        newDocument.append("$set", new Document().append("designation", newDesignation).append("name", newName));
        //Document searchQuery = new Document().append("_id", new ObjectId(document.get("id").toString()));

        table.updateOne(document, newDocument);
    }

    public void updateAllDocumentAttributes(DesignDocument doc)
    {
        Document newDocument = new Document();
        newDocument.append("$set",
                new Document()
                        .append("isOriginal", doc.getIsOriginal())
                        .append("name", doc.getName() == null ? "" : doc.getName())
                        .append("state", doc.getState() == null ? "" : doc.getState())
                        .append("userId", doc.getUserId() == null ? "" : doc.getUserId())
                        .append("revision", doc.getRevision() == null ? "" : doc.getRevision())
                        .append("sessionId", doc.getSessionId() == null ? "" : doc.getSessionId())
                        .append("designation", doc.getDesignation() == null ? "" : doc.getDesignation())
                        .append("transactionId", doc.getTransactionId() == null ? "" : doc.getTransactionId())
                        .append("transactionAction", doc.getTransactionAction() == null ? "" : doc.getTransactionAction().toString()));
        Document searchQuery = new Document().append("_id", new ObjectId(doc.getId()));
        table.updateOne(searchQuery, newDocument);
    }

    public Document getObjectByDesignation(String designation){
        Document query = new Document();

        query.put("designation", designation);

        return table.find(query).first();
    }

    public void deleteByObjectId(String objectId){
        table.findOneAndDelete(findObjectById(objectId));
    }

    public void updateStateById(String id, String state){
        Document newDocument = new Document();

        newDocument.append("$set", new Document().append("state", state));
        Document searchQuery = new Document().append("_id", id);

        table.findOneAndUpdate(findObjectById(id), newDocument);
    }

    public Boolean checkLastRevision(String documentId){
        Document object = findObjectById(documentId);

        Document query = new Document();
        Integer rev = Integer.parseInt(object.get("revision").toString()) + 1;
        query.append("designation", object.get("designation")).append("name", object.get("name")).append("revision", rev);

        if(table.find(query).first() == null){
            return true;
        }
        else{
            return false;
        }
    }

    public Boolean checkState(String documentId){
        if(findObjectById(documentId).get("state").equals("Approved")){
            return true;
        }
        else{
            return false;
        }
    }

    public Document createNewRevDocument(String documentId){
        Document object = findObjectById(documentId);
        add(new DesignDocument(object.get("designation").toString(),Integer.parseInt(object.get("revision").toString())+1,object.get("name").toString(),"In Work"));

        Document query = new Document();

        query.put("designation", object.get("designation"));
        query.put("revision", Integer.parseInt(object.get("revision").toString())+1);

        return table.find(query).first();
    }

    public DesignDocument getAllDocumentAttributes(String documentId){
        Document object = findObjectById(documentId);

        DesignDocument document = new DesignDocument();

        document.setId(object.get("_id").toString());
        document.setDesignation(object.get("designation").toString());
        document.setName(object.get("name") == null ? "" : object.get("name").toString());
        document.setRevision(Integer.parseInt(object.get("revision").toString()));
        document.setState(object.get("state").toString());
        document.setTransactionId(object.get("transactionId") == null ? null : object.get("transactionId").toString());
        document.setSessionId(object.get("sessionId") == null ? null : object.get("sessionId").toString());
        document.setUserId(object.get("userId") == null ? null : object.get("userId").toString());
        document.setTransactionAction(object.get("transactionAction") == null ? ToAction(object.get("transactionAction").toString()) : TransactionAction.Empty);
        return document;
    }

    public ArrayList<DesignDocument> getAllDocuments(){
        ArrayList<DesignDocument> result = new ArrayList<>();
        ArrayList<Document> queryDB = new ArrayList<Document>();
        table.find().into(queryDB);
        for(Document doc : queryDB) {
//            String designation, Integer revision, String name, String state,
//                UUID transactionId, String sessionId, String userId, TransactionAction transactionAction
            //String id, String designation, Integer revision,String name, String state
            result.add(new DesignDocument(
                    doc.get("_id").toString(),
                    doc.get("designation") == null ? null : doc.get("designation").toString(),
                    doc.get("revision") == null ? null :  Integer.parseInt(doc.get("revision").toString()),
                    doc.get("name") == null ? null : doc.get("name").toString(),
                    doc.get("state") == null ? null : doc.get("state").toString(),
                    doc.get("transactionId") == null ? null : doc.get("transactionId").toString(),
                    doc.get("sessionId") == null ? null : doc.get("sessionId").toString(),
                    doc.get("userId") == null ? null : doc.get("userId").toString(),
                    doc.get("transactionAction") != null ? ToAction(doc.get("transactionAction").toString()) : TransactionAction.Empty,
                    doc.get("originalId") == null ? null : doc.get("originalId").toString(),
                    (Boolean)doc.get("isOriginal")));
        }
        return result;
    }

    public TransactionAction ToAction(String str)
    {
        TransactionAction resAction;
        switch (str){
            case "Adding":
                resAction = TransactionAction.Adding;
                break;
            case "Modifying" :
                resAction = TransactionAction.Modifying;
                break;
            case "Removing":
                resAction = TransactionAction.Removing;
                break;
            default:
                resAction = TransactionAction.Empty;
                break;
        }
        return resAction;
    }
    public ArrayList<DesignDocument> getTObjects(TransactionDetailsViewModel tmodel) {
        ArrayList<DesignDocument> documents = getAllDocuments();
        ArrayList<DesignDocument> tDocs = new ArrayList<>();

        for (DesignDocument doc : documents) {
            if (doc.getTransactionId() != null &&
                    doc.getTransactionId().toLowerCase().equals(tmodel.getTransactionId().toLowerCase()) &&
                    doc.getUserId().toLowerCase().equals(tmodel.getUserId().toLowerCase()) &&
                    doc.getSessionId().toLowerCase().equals(tmodel.getSessionId().toLowerCase())) {
                tDocs.add(doc);
            }
        }
        return tDocs;
    }
//        switch(str){
//            case "Adding" :
//                return TransactionAction.Adding;
//                break;
//            case "Modifying" :
//                return TransactionAction.Modifying;
//                break;
//            case  "Removing" :
//                return TransactionAction.Removing;
//                break;
//            default:
//                return null;
//        }
}

