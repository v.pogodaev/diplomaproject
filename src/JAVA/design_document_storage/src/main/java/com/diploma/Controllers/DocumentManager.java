package com.diploma.Controllers;

import au.com.bytecode.opencsv.CSVReader;
import com.diploma.DB.DesignDocument;
import com.diploma.DB.WorkWithMongo;
import com.diploma.Transactions.TransactionAction;
import com.diploma.TransactionsViewModels.TransactionDetailsViewModel;
import com.diploma.ViewModels.DesignDocumentChangeViewModel;
import com.diploma.ViewModels.DesignDocumentStateViewModel;
import com.diploma.ViewModels.DesignDocumentViewModel;
import com.diploma.ViewModels.ReturnModels.DesignDocumentReturnModel;
import io.swagger.annotations.ApiOperation;
import org.mapstruct.BeforeMapping;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Map;
import java.util.Properties;

@RestController
@Controller
@RequestMapping("/design_storage")
public class DocumentManager implements WebMvcConfigurer {
    WorkWithMongo mongo;

    @BeforeMapping
    public void setUp() throws Exception {
        Properties prop = new Properties();

        // получил переменные среды
        Map<String, String> envs = System.getenv();

        // тут используются переменные среды, прописанные в docker-compose
        // возможно (скорее всего надо) лучше перенести в другое место
        prop.setProperty("connectionString", envs.get("db_connectionString"));
        prop.setProperty("dbname", envs.get("db_dbname"));
        prop.setProperty("login", envs.get("db_login"));
        prop.setProperty("password", envs.get("db_password"));
        prop.setProperty("table", envs.get("db_table"));

//        prop.setProperty("connectionString", "localhost:27017");
//        prop.setProperty("dbname", "DesignDocumentsData");
//        prop.setProperty("login", "root");
//        prop.setProperty("password", "root12345");
//        prop.setProperty("table", "DesignDocuments");
        mongo = new WorkWithMongo(prop);
    }


    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String Index() {
        return "Design Document Storage MS";
    }
    @ApiOperation(value = "Создание нового конструкторского документа",notes = "Designation - обязательно, остальное нет")
    @RequestMapping(value = "/design_document/create", method = RequestMethod.POST)
    public String createNewDesignDocument(@Valid @RequestBody DesignDocumentViewModel object,HttpServletResponse response) throws Exception {
        setUp();

        DesignDocument item = object.takeModel();
        item.setTransactionAction(TransactionAction.Adding);
        if(mongo.findDoc(object.getDesignation())){
            response.setStatus(409);
            return "Документ с таким обозначением уже существует";}
        else {
            mongo.add(new DesignDocument(object.getDesignation(), 1, object.getName(), "On Check",
                    item.getTransactionId(),item.getSessionId(), item.getUserId(), item.getTransactionAction()));
            response.setStatus(200);
            return mongo.getObjectId(object.getDesignation());
        }
    }



//    @RequestMapping(value = "/designDocument/CancelTransaction", method = RequestMethod.PUT)
//    public void CancelTransaction(@Valid TransactionDetailsViewModel tmodel, HttpServletResponse response) throws Exception{
//        ArrayList<DesignDocument> documents = mongo.getAllDocuments();
//        ArrayList<DesignDocument> tDocs = new ArrayList<>();
//        for (DesignDocument doc : documents) {
//            if (doc.getTransactionId().toString().equals(tmodel.getTransactionId()) &&
//                    doc.getUserId().equals(tmodel.getUserId()) &&
//                    doc.getSessionId().equals(tmodel.getSessionId())){
//                tDocs.add(doc);
//            }
//        }
//
//        if (tDocs.size() == 0) {
//            return; //notfound
//        }
//
//        ArrayList<DesignDocument> docsToRemove = new ArrayList<>();
//        for (DesignDocument d : tDocs) {
//            if (d.getTransactionAction().equals(TransactionAction.Adding)) {
//                d.ClearCommitData();
//                docsToRemove.add(d);
//            }
//        }
//
//        for (DesignDocument d : docsToRemove) {
//            mongo.deleteByObjectId(d.getId());
//        }
//    }


    @ApiOperation(value = "Изменение существующего документа.", notes = "требуется id документа и новые данные designation и name")
    @RequestMapping(value = "/design_document/change/{documentId}", method = RequestMethod.POST)
    public void changeDesignDocument(@PathVariable("documentId") String documentId, @Valid @RequestBody DesignDocumentChangeViewModel object, HttpServletResponse response) throws Exception {
            setUp();
            DesignDocument item = mongo.getObjectById(documentId);
        if (item == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return;
        }

        //Если данные о транзакции существуют и они не равны пришедшим данным, ловим бедреквест
        if(!item.getTransactionId().isEmpty() && !item.getSessionId().isEmpty() && !item.getUserId().isEmpty() &&
                !item.getTransactionId().toLowerCase().equals(object.getTransactionId().toLowerCase()) &&
                !item.getSessionId().toLowerCase().equals(object.getSessionId().toLowerCase()) &&
                !item.getUserId().toLowerCase().equals(object.getUserId().toLowerCase()))
        {
            response.setStatus(HttpStatus.BAD_REQUEST.value());

            return;
        }
        //Если данные о транзакции пусты, значит мы моем создать новую транзакцию и без проблем работать с документом
        if(item.getTransactionId().isEmpty() && item.getSessionId().isEmpty())
        {
            //Создание копии изменяемого документа с данными о транзакции
            mongo.add(
                    new DesignDocument(
                            item.getDesignation(),
                            item.getRevision(),
                            item.getName(),
                            item.getState(),
                            object.getTransactionId(),
                            object.getSessionId(),
                            object.getUserId(),
                            TransactionAction.Modifying ,
                            item.getId(),
                            false));
            //Обновление документа оригинального документа
            mongo.updateAllDocumentAttributes(
                    new DesignDocument(
                    item.getId(),
                    object.getNewDesignation(),
                    item.getRevision(),
                    object.getNewName(),
                    item.getState(),
                    object.getTransactionId(),
                    object.getSessionId(),
                    object.getUserId(),
                    TransactionAction.Modifying,
            "",
            true));
            response.setStatus(HttpStatus.OK.value());
            return;
        }

        //Если данные о транзакции есть и они равны пришедшим данным, то мы тоже можем изменять документ
        if(!item.getTransactionId().isEmpty() &&
            item.getTransactionId().toLowerCase().equals(object.getTransactionId().toLowerCase()) &&
            item.getSessionId().toLowerCase().equals(object.getSessionId().toLowerCase())){
            mongo.updateAllDocumentAttributes(
                    new DesignDocument(
                            item.getId(),
                            object.getNewDesignation(),
                            item.getRevision(),
                            object.getNewName(),
                            item.getState(),
                            object.getTransactionId(),
                            object.getSessionId(),
                            object.getUserId(),
                            TransactionAction.Modifying,
                            "",
                            true));
            response.setStatus(HttpStatus.OK.value());
            response.setContentType("THREE");
            return;
        }
        response.setContentType("OVER");
        response.setStatus(HttpStatus.BAD_REQUEST.value());
    }

    @ApiOperation(value = "Удаление конструкторского документа.", notes = " Требуется id")
    @RequestMapping(value = "/design_document/removeDocument/{documentId}", method = RequestMethod.POST)
    public @ResponseBody
    void removeDocument(@PathVariable("documentId") String documentId,@Valid @RequestBody TransactionDetailsViewModel transactionDetailsViewModel, HttpServletResponse response) throws Exception{
        setUp();
        DesignDocument remDocument = mongo.getObjectById(documentId);

        if(remDocument.getTransactionId().isEmpty() && remDocument.getSessionId().isEmpty() && remDocument.getUserId().isEmpty()){
            remDocument.setTransactionId(transactionDetailsViewModel.getTransactionId());
            remDocument.setSessionId(transactionDetailsViewModel.getSessionId());
            remDocument.setUserId(transactionDetailsViewModel.getUserId());
            remDocument.setTransactionAction(TransactionAction.Removing);
            mongo.updateAllDocumentAttributes(remDocument);
        }
    }

    @ApiOperation(value = "Изменение состояния документа.", notes = "  Требуется id и state")
    @RequestMapping(value = "/design_document/changeState/{documentId}", method = RequestMethod.POST)
    public @ResponseBody
    void changeDocumentState(@PathVariable("documentId") String documentId, @Valid @RequestBody DesignDocumentStateViewModel object, HttpServletResponse response)throws Exception{
        setUp();

        DesignDocument item = mongo.getObjectById(documentId);
        if (item == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return;
        }
        //Если данные о транзакции существуют и они не равны пришедшим данным, ловим бедреквест
        if(!item.getTransactionId().isEmpty() && !item.getSessionId().isEmpty() && !item.getUserId().isEmpty() &&
                !item.getTransactionId().toLowerCase().equals(object.getTransactionId().toLowerCase()) &&
                !item.getSessionId().toLowerCase().equals(object.getSessionId().toLowerCase()) &&
                !item.getUserId().toLowerCase().equals(object.getUserId().toLowerCase()))
        {
            response.setStatus(HttpStatus.BAD_REQUEST.value());

            return;
        }
        //Если данные о транзакции пусты, значит мы моем создать новую транзакцию и без проблем работать с документом
        if(item.getTransactionId().isEmpty() && item.getSessionId().isEmpty())
        {
            //Создание копии изменяемого документа с данными о транзакции
            mongo.add(
                    new DesignDocument(
                            item.getDesignation(),
                            item.getRevision(),
                            item.getName(),
                            item.getState(),
                            object.getTransactionId(),
                            object.getSessionId(),
                            object.getUserId(),
                            TransactionAction.Modifying ,
                            item.getId(),
                            false));
            //Обновление документа оригинального документа
            mongo.updateAllDocumentAttributes(
                    new DesignDocument(
                            item.getId(),
                            item.getDesignation(),
                            item.getRevision(),
                            item.getName(),
                            object.getState(),
                            object.getTransactionId(),
                            object.getSessionId(),
                            object.getUserId(),
                            TransactionAction.Modifying,
                            "",
                            true));
            response.setStatus(HttpStatus.OK.value());
            return;
        }

        //Если данные о транзакции есть и они равны пришедшим данным, то мы тоже можем изменять документ
        if(!item.getTransactionId().isEmpty() &&
                item.getTransactionId().toLowerCase().equals(object.getTransactionId().toLowerCase()) &&
                item.getSessionId().toLowerCase().equals(object.getSessionId().toLowerCase())){
            mongo.updateAllDocumentAttributes(
                    new DesignDocument(
                            item.getId(),
                            item.getDesignation(),
                            item.getRevision(),
                            item.getName(),
                            object.getState(),
                            object.getTransactionId(),
                            object.getSessionId(),
                            object.getUserId(),
                            TransactionAction.Modifying,
                            "",
                            true));
            response.setStatus(HttpStatus.OK.value());
            return;
        }
        response.setStatus(HttpStatus.BAD_REQUEST.value());
    }

    @ApiOperation(value = "Создание новой версии существуещего документа.", notes = "  Требуется id документа")
    @RequestMapping(value = "/design_document/createNewRev", method = RequestMethod.GET)
    public @ResponseBody
    String createNewRevisionForDocument(@RequestParam("documentId") String documentId) throws Exception{
        setUp();
        if(!documentId.isEmpty())
        {
            if(mongo.checkLastRevision(documentId) && mongo.checkState(documentId))
            {
               return mongo.createNewRevDocument(documentId).get("_id").toString();
            }
            else{
                return "Atention";
            }
        }
        else{
            return "Input ID!";
        }
    }

    @ApiOperation(value = "Проверка на возможность создания новой версии(ревизии) у документа.", notes = "требуется Id документа")
    @RequestMapping(value = "/design_document/checkForCreateNewRev", method = RequestMethod.GET)
    public @ResponseBody
    String CheckDocumentForCreateNewRevision(@RequestParam("documentId") String documentId) throws Exception{
        setUp();
        if(mongo.checkLastRevision(documentId) && mongo.checkState(documentId)){return "true";}
        return "false";
    }

    @ApiOperation(value = "Вывод всех данных по документу.", notes = "  Требуется id документа")
    @RequestMapping(value = "/design_document/GetDocument", method = RequestMethod.GET)
    public @ResponseBody
    DesignDocumentReturnModel getDocumentAttributes(@RequestParam("documentId") String documentId) throws Exception{
        setUp();
        DesignDocument document = mongo.getAllDocumentAttributes(documentId);
        return new DesignDocumentReturnModel(document.getId(),
                                            document.getDesignation(),
                                            document.getRevision(),
                                            document.getName(),
                                            document.getState());
    }


    @ApiOperation(value = "Вывод всех данных по документам.", notes = "  Требуется id документов")
    @RequestMapping(value = "/design_document/getDocumentsByIds", method = RequestMethod.POST)
    public @ResponseBody
    ArrayList<DesignDocumentReturnModel> getDocumentsByIds(@RequestParam("documentsId") String[] documentsId) throws Exception{
        setUp();
        ArrayList<DesignDocumentReturnModel> documents = new ArrayList<>();
        for (String id : documentsId) {
            DesignDocument document = mongo.getAllDocumentAttributes(id);
            if(document != null) {
                documents.add(new DesignDocumentReturnModel(document.getId(),
                                                            document.getDesignation(),
                                                            document.getRevision(),
                                                            document.getName(),
                                                            document.getState()));
            }
        }

        return documents;
    }

    @RequestMapping(value="/design_document/getAllDocuments", method = RequestMethod.GET)
    public @ResponseBody
        ArrayList<DesignDocumentReturnModel> getAllDocuments() throws Exception{
        setUp();

    ArrayList<DesignDocument> allDocuments = mongo.getAllDocuments();
    ArrayList<DesignDocumentReturnModel> listToView = new ArrayList<>();
        for (DesignDocument doc: allDocuments) {
            if((doc.getTransactionId().isEmpty() || doc.getTransactionId() == null)  && (!doc.getTransactionAction().equals(TransactionAction.Adding) ||
                    !doc.getTransactionAction().equals(TransactionAction.Modifying ))){

                listToView.add(new DesignDocumentReturnModel(doc.getId(),
                                                            doc.getDesignation(),
                                                            doc.getRevision(),
                                                            doc.getName(),
                                                            doc.getState()));
            }
        }
        return listToView;
    }

    @RequestMapping(value="/design_document/getAllDocumentsWithTransact", method = RequestMethod.GET)
    public @ResponseBody
    ArrayList<DesignDocumentReturnModel> getAllDocumentsWithTransact() throws Exception {
        setUp();
        ArrayList<DesignDocument> allDocuments = mongo.getAllDocuments();
        ArrayList<DesignDocumentReturnModel> listToView = new ArrayList<>();
        for (DesignDocument doc : allDocuments) {
            listToView.add(new DesignDocumentReturnModel(doc.getId(),
                    doc.getDesignation(),
                    doc.getRevision(),
                    doc.getName(),
                    doc.getState()));
        }
        return listToView;
    }

    @RequestMapping(value = "/designDocument/CommitTransaction", method = RequestMethod.PUT)
    public void CommitTransaction(@Valid @RequestBody TransactionDetailsViewModel tmodel, HttpServletResponse response) throws Exception{
        setUp();
        ArrayList<DesignDocument> tDocs =  mongo.getTObjects(tmodel);

        if (tDocs.size() == 0) {
            response.setStatus(404);
            return;
        }

        ArrayList<DesignDocument> docsToUpdate = new ArrayList<>();
        ArrayList<DesignDocument> docsToRemove = new ArrayList<>();
        for (DesignDocument d : tDocs) {
            if (d.getTransactionAction() == TransactionAction.Adding ) {
                d.ClearCommitData();
                docsToUpdate.add(d);
            }
            if(d.getTransactionAction() == TransactionAction.Modifying && !d.getIsOriginal()){
                docsToRemove.add(d);
            }
            if(d.getTransactionAction() == TransactionAction.Modifying && d.getIsOriginal()){
                d.ClearCommitData();
                docsToUpdate.add(d);
            }
            if(d.getTransactionAction() == TransactionAction.Removing)
            {
                docsToRemove.add(d);
            }
        }

        for (DesignDocument d : docsToUpdate) {
            mongo.updateAllDocumentAttributes(d);
        }

        for (DesignDocument d : docsToRemove){
            mongo.deleteByObjectId(d.getId());
        }

        response.setStatus(200);
    }

    @RequestMapping(value = "/designDocument/CancelTransaction", method = RequestMethod.PUT)
    public void CancelTransaction(@Valid @RequestBody TransactionDetailsViewModel tmodel, HttpServletResponse response) throws Exception {
        setUp();
        ArrayList<DesignDocument> tDocs =  mongo.getTObjects(tmodel),
                                fakeDocs = new ArrayList<>(),
                                docsToCancel = new ArrayList<>();

        if (tDocs.size() == 0) {
            response.setStatus(404);
            return;
        }

        for(DesignDocument d : tDocs){
            switch (d.getTransactionAction()){
                case Adding:
                    mongo.deleteByObjectId(d.getId());
                    break;
                case Removing:
                    d.ClearCommitData();
                    docsToCancel.add(d);
                    break;
                case Modifying:
                    if (!d.getIsOriginal()) {
                        fakeDocs.add(d);
                    }
                    break;
            }
        }
        for(DesignDocument d : fakeDocs) {
            DesignDocument docToCancel = new DesignDocument(
                    d.getOriginalId(),
                    d.getDesignation(),
                    d.getRevision(),
                    d.getName(),
                    d.getState(),
                    d.getTransactionId(),
                    d.getSessionId(),
                    d.getUserId(),
                    d.getTransactionAction(),
                    "",
                    true
            );
            docToCancel.ClearCommitData();
            docsToCancel.add(docToCancel);
        }
        for (DesignDocument d : docsToCancel) {
            mongo.updateAllDocumentAttributes(d);
        }

        for (DesignDocument d : fakeDocs){
            mongo.deleteByObjectId(d.getId());
        }
    }

    @RequestMapping(value = "/UploadProducts", method = RequestMethod.POST)
    public void uploadProducts() throws Exception {
        setUp();
        CSVReader reader = new CSVReader(new FileReader("C:/Users/Nkravchenko/Desktop/items2.csv"), ';', '"', 0);
        String[] nextLine;
        while((nextLine = reader.readNext()) != null) {
            if (nextLine != null) {
                if(!mongo.findDoc(nextLine[1])){
                    mongo.add(new DesignDocument(nextLine[0],
                            1,
                            nextLine[1],
                            "On Check",
                            "112233",
                            "112233",
                            "112233",
                            TransactionAction.Adding
                    ));
                }
            }
        }
    }
}
