package com.diploma.Transactions;


public enum TransactionAction {
    Adding,
    Modifying,
    Removing,
    Empty

}
