package com.diploma.ViewModels;

import com.diploma.DB.DesignDocument;
import com.diploma.TransactionsViewModels.TransactionDetailsViewModel;

import javax.validation.constraints.NotNull;

public class DesignDocumentViewModel extends TransactionDetailsViewModel {

    @NotNull
    private String designation;

    private String name;


    public String getDesignation(){
        return designation;
    }


    public String getName(){
        return name;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setDesignation(String designation){
        this.designation = designation;
    }

    public DesignDocument takeModel(){return new DesignDocument(
        designation, name, transactionId, sessionId, userId);
    }


}
