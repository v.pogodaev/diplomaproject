package com.diploma.ViewModels;

import com.diploma.TransactionsViewModels.TransactionDetailsViewModel;

import javax.validation.constraints.NotNull;

public class DesignDocumentRemoveViewModel extends TransactionDetailsViewModel {
    @NotNull
    private String id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }
}
