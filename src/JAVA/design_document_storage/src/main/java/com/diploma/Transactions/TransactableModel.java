package com.diploma.Transactions;

import java.util.UUID;

public abstract class TransactableModel {

    protected String transactionId;
    protected TransactionAction transactionAction;
    protected String userId;
    protected String sessionId;
    protected boolean IsOriginal;
    protected String OriginalId;



    public void ClearCommitData(){
        transactionId = null;
        transactionAction = null;
        userId = null;
        sessionId = null;
        IsOriginal = true;
        OriginalId = null;
    }

    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public TransactionAction getTransactionAction() {
        return transactionAction;
    }

    public void setTransactionAction(TransactionAction transactionAction) {
        this.transactionAction = transactionAction;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public boolean getIsOriginal() {
        return IsOriginal;
    }

    public void setIsOriginal(boolean isOriginal) {
        IsOriginal = isOriginal;
    }

    public String getOriginalId() {
        return OriginalId;
    }

    public void setOriginalId(String originalId) {
        OriginalId = originalId;
    }
}
