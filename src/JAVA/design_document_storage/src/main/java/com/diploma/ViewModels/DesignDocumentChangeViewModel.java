package com.diploma.ViewModels;

import com.diploma.TransactionsViewModels.TransactionDetailsViewModel;

import javax.validation.constraints.NotNull;

public class DesignDocumentChangeViewModel extends TransactionDetailsViewModel {

    private String newDesignation;

    private String newName;

    public String getNewDesignation() {
        return newDesignation;
    }

    public void setNewDesignation(String newDesignation) {
        this.newDesignation = newDesignation;
    }

    public String getNewName() {
        return newName;
    }

    public void setNewName(String newName) {
        this.newName = newName;
    }
}
