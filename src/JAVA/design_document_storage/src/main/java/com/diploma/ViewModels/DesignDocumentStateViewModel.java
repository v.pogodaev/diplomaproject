package com.diploma.ViewModels;

import com.diploma.TransactionsViewModels.TransactionDetailsViewModel;

import javax.validation.constraints.NotNull;

public class DesignDocumentStateViewModel extends TransactionDetailsViewModel {

    private String state;

    public String getState(){
        return state;
    }

    public void setState(String state){
        this.state = state;
    }
}
