package com.diploma.DB;

import com.diploma.Transactions.TransactableModel;
import com.diploma.Transactions.TransactionAction;
import com.diploma.TransactionsViewModels.TransactionDetailsViewModel;

import java.util.ArrayList;
import java.util.UUID;

public class DesignDocument extends TransactableModel {
    private String id;
    private String designation;
    private Integer revision;
    private String name;
    private String state;
    private ArrayList<String> connectFiles;

    public DesignDocument(){

    }

    public DesignDocument(
            String designation,
            Integer revision,
            String name,
            String state) {
        this.designation = designation;
        this.revision = revision;
        this.name = name;
        this.state = state;
    }

    public DesignDocument(String designation,
                          String name,
                          String transactionId,
                          String sessionId,
                          String userId){
        this.designation = designation;
        this.name = name;
        this.transactionId = transactionId;
        this.sessionId = sessionId;
        this.userId = userId;
    }
    public DesignDocument(String designation,
                          Integer revision,
                          String name,
                          String state,
                          String transactionId,
                          String sessionId,
                          String userId,
                          TransactionAction transactionAction) {
        this.designation = designation;
        this.revision = revision;
        this.name = name;
        this.state = state;
        this.transactionId = transactionId;
        this.sessionId = sessionId;
        this.userId = userId;
        this.transactionAction = transactionAction;
    }
    public DesignDocument(String id,
                          String designation,
                          Integer revision,
                          String name,
                          String state,
                          String transactionId,
                          String sessionId,
                          String userId,
                          TransactionAction
                          transactionAction){
        this.id = id;
        this.designation = designation;
        this.revision = revision;
        this.name = name;
        this.state = state;
        this.transactionId = transactionId;
        this.sessionId = sessionId;
        this.userId = userId;
        this.transactionAction = transactionAction;
    }
    public DesignDocument(String id,
                          String designation,
                          Integer revision,
                          String name,
                          String state,
                          String transactionId,
                          String sessionId,
                          String userId,
                          TransactionAction transactionAction,
                          String originalId,
                          boolean isOriginal){
        this.id = id;
        this.designation = designation;
        this.revision = revision;
        this.name = name;
        this.state = state;
        this.transactionId = transactionId;
        this.sessionId = sessionId;
        this.userId = userId;
        this.transactionAction = transactionAction;
        this.OriginalId = originalId;
        this.IsOriginal = isOriginal;
    }

    public DesignDocument(String designation,
                          Integer revision,
                          String name,
                          String state,
                          String transactionId,
                          String sessionId,
                          String userId,
                          TransactionAction transactionAction,
                          String originalId,
                          boolean isOriginal){
        this.designation = designation;
        this.revision = revision;
        this.name = name;
        this.state = state;
        this.transactionId = transactionId;
        this.sessionId = sessionId;
        this.userId = userId;
        this.transactionAction = transactionAction;
        this.OriginalId = originalId;
        this.setIsOriginal(isOriginal);
    }



    public String getId(){
        return id;
    }

    public String getDesignation(){
        return designation;
    }

    public Integer getRevision(){
        return revision;
    }

    public String getName(){
        return name;
    }

    public String getState(){
        return state;
    }

    public ArrayList<String> getConnectFiles() {return connectFiles;}

    public void setId(String id){
        this.id = id;
    }

    public void setDesignation(String designation){
        this.designation = designation;
    }

    public void setRevision(Integer revision){
        this.revision = revision;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setState(String state){
        this.state = state;
    }

    public void addFile(String fileId){this.connectFiles.add(fileId);}

    public void deleteFile(String fileId) {this.connectFiles.remove(fileId);}
}
