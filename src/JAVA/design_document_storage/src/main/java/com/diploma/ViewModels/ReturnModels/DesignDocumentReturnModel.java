package com.diploma.ViewModels.ReturnModels;

public class DesignDocumentReturnModel {
    private String id;
    private String designation;
    private Integer revision;
    private String name;
    private String state;

    public DesignDocumentReturnModel(
            String id,
            String designation,
            Integer revision,
            String name,
            String state) {
        this.id = id;
        this.designation = designation;
        this.revision = revision;
        this.name = name;
        this.state = state;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public Integer getRevision() {
        return revision;
    }

    public void setRevision(Integer revision) {
        this.revision = revision;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
