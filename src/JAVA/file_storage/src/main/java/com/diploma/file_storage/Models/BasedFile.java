package com.diploma.file_storage.Models;

public class BasedFile {
    private Byte fileData;
    private String id;
    private String name;
    private String size;
    private String type;
    private String comment;
    private String path;
    public BasedFile(){

    }

    public BasedFile (String name, String size, String type, String comment, String path){
        this.name = name;
        this.size = size;
        this.type = type;
        this.comment = comment;
        this.path = path;
    }
    public BasedFile (String id,String name, String size, String type, String comment, String path){
        this.id = id;
        this.name = name;
        this.size = size;
        this.type = type;
        this.comment = comment;
        this.path = path;
    }

    public String getName(){
        return name;
    }

    public String getSize(){
        return size;
    }

    public String getType(){
        return type;
    }

    public String getComment(){
        return comment;
    }

    public String getPath(){
        return path;
    }

    public void setName(String name){
        this.name = name;
    }

    public void setSize(String size){
        this.size = size;
    }

    public void setType(String type){
        this.type = type;
    }

    public void setComment(String comment){
        this.comment = comment;
    }

    public void setPath(String path){
        this.path = path;
    }

    @Override
    public String toString()
    {
        return "File{" + "name='" + name + '\'' +
                ", type='" + type + '\'' +
                ", size='" + size + '\'' +
                ", comment='" + comment + '\'' +
                ", path='" + path + '\'' + '}';
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Byte getFileData() {
        return fileData;
    }

    public void setFileData(Byte fileData) {
        this.fileData = fileData;
    }
}
