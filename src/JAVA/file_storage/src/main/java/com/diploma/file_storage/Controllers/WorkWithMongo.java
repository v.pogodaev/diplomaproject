package com.diploma.file_storage.Controllers;

import com.diploma.file_storage.Models.BasedFile;
import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.io.File;
import java.util.ArrayList;
import java.util.Properties;

public class WorkWithMongo {
    // это клиент который обеспечит подключение к БД
    private MongoClient mongoClient;

    // В нашем случае, этот класс дает
// возможность аутентифицироваться в MongoDB
    private MongoDatabase db;

    // тут мы будем хранить состояние подключения к БД
    private boolean authenticate;

    // И класс который обеспечит возможность работать
// с коллекциями / таблицами MongoDB
    private MongoCollection<Document> table;

    public WorkWithMongo(Properties prop) {
        // Создаем подключение
        System.out.println("trying to connect to " + prop.getProperty("connectionString"));
        char[] pass = prop.getProperty("password").toCharArray();
        MongoCredential credential = MongoCredential.createCredential(prop.getProperty("login"),"admin", pass);
        int connectTimeout = 1000 * 60;
        MongoClientOptions options = new MongoClientOptions.Builder().connectTimeout(connectTimeout).build();
        mongoClient = new MongoClient(new ServerAddress(prop.getProperty("connectionString")), credential, options);

        // Выбираем БД для дальнейшей работы
        db = mongoClient.getDatabase(prop.getProperty("dbname"));

        // Выбираем коллекцию/таблицу для дальнейшей работы
        table = db.getCollection(prop.getProperty("table"));
    }

    public void add(BasedFile file){
        Document document = new Document();
        // это поле будет записываться в коллекцию/таблицу
        document.put("name", file.getName());
        document.put("size", file.getSize());
        document.put("type", file.getType());
        document.put("comment", file.getComment());
        document.put("path", file.getPath());
        // записываем данные в коллекцию/таблицу
        table.insertOne(document);
    }

    public void removeByObjectId(String objectId){
        Document remFile = findObjectById(objectId);
        if(new File(remFile.get("path").toString()).delete()){
            table.findOneAndDelete(remFile);
        }

    }
    public BasedFile getById(String Id){
        Document query = new Document();

        // задаем поле и значение поля по которому будем искать
        query.put("_id", new ObjectId(Id));

        // осуществляем поиск
        Document result = table.find(query).first();

        // Заполняем сущность полученными данными с коллекции
        BasedFile file = new BasedFile(result.get("id") == null ? "" : result.get("id").toString(),
                                        result.get("name") == null ? "" : result.get("name").toString(),
                                        result.get("size") == null ? "" : result.get("size").toString(),
                                        result.get("type") == null ? "" : result.get("type").toString(),
                                        result.get("comment") == null ? "" : result.get("comment").toString(),
                                        result.get("path") == null ? "" : result.get("path").toString());
        return file;
    }

    public String getObjectId(String filepath){
        Document query = new Document();
        query.append("path", filepath);
        String result =  table.find(query).first().get("_id").toString();
        return result == null ? "" : result;
    }

    public Document findObjectById(String id){
        Document query = new Document();

        query.put("_id", new ObjectId(id));
        return table.find(query).first();
    }

    public BasedFile getAllFileAttributes(String documentId){
        Document object = findObjectById(documentId);

        BasedFile file = new BasedFile();

        file.setId(object.get("_id").toString());
        file.setName(object.get("name") == null ? "" : object.get("name").toString());
        file.setSize(object.get("size").toString());
        file.setType(object.get("type").toString());
        file.setComment(object.get("comment").toString());
        file.setPath(object.get("path").toString());

        return file;
    }

    public ArrayList<BasedFile> getAllFilesData(){
        ArrayList<BasedFile> result = new ArrayList<>();
        ArrayList<Document> queryDB = new ArrayList<Document>();
        table.find().into(queryDB);
        for(Document file : queryDB)
        {
            result.add(new BasedFile(file.get("_id").toString(),file.get("name").toString(),file.get("size").toString(),
                    file.get("type").toString(), file.get("comment").toString(), file.get("path").toString()));
        }
        return result;
    }

}
