package com.diploma.file_storage.Controllers;

import com.diploma.file_storage.Models.BasedFile;
import com.diploma.file_storage.Models.ViewModels.UploadFileViewModel;
import io.swagger.annotations.ApiOperation;
import org.mapstruct.BeforeMapping;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.ui.Model;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Map;
import java.util.Properties;

@RestController
@Controller
@RequestMapping("/storage")
public class FilesStorageController implements InitializingBean {
    WorkWithMongo mongo;


    @BeforeMapping
    public void setUp() throws Exception {
        Properties prop = new Properties();
        // получил переменные среды
        Map<String, String> envs = System.getenv();
        // тут используются переменные среды, прописанные в docker-compose
        // возможно (скорее всего надо) лучше перенести в другое место

        prop.setProperty("connectionString", envs.get("db_connectionString"));
        prop.setProperty("dbname", envs.get("db_dbname"));
        prop.setProperty("login", envs.get("db_login"));
        prop.setProperty("password", envs.get("db_password"));
        prop.setProperty("table", envs.get("db_table"));
        mongo = new WorkWithMongo(prop);
    }


    @RequestMapping(value = "/uploadOneFile", method = RequestMethod.POST)
    public String uploadOneFileHandler(@RequestParam("file") MultipartFile file, @RequestParam("comment") String Comment, HttpServletResponse responce) throws Exception {
setUp();
//        MyUploadForm myUploadForm = new MyUploadForm();
//        model.addAttribute("myUploadForm", myUploadForm);
        if(Comment.isEmpty()){Comment = "comment";}
        if (file != null) {
            try {
                byte[] bytes = file.getBytes();
                new File("./DB").mkdir();
                String path = "./DB/" + file.getOriginalFilename();

                System.out.println(new File(".").getAbsolutePath());
                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(path)));
                stream.write(bytes);
                stream.close();
                String[] fileName = file.getOriginalFilename().split("\\.");

                mongo.add(new BasedFile(
                        fileName[0],
                        String.valueOf(bytes.length),
                        fileName[1],
                        Comment, path));

                responce.setStatus(200);
                return mongo.getObjectId(path);
            } catch (Exception e) {
                responce.setStatus(409);
                return "";
            }
        } else {
          responce.setStatus(409);
          return "";
        }
    }

//    @ApiOperation(value = "Загрузка нового файла на сервер.", notes = "Требуется файл с ПК")
//    @RequestMapping(value="/file/upload", method=RequestMethod.POST)
//    public @ResponseBody void handleFileUpload(@Valid @RequestBody UploadFileViewModel object, HttpServletResponse httpServletResponse) throws Exception{
//        setUp();
//
//        if(object != null){
//            httpServletResponse.setStatus(200);
//        }
//        else{
//            httpServletResponse.setContentType("BLYAD");
//            httpServletResponse.setStatus(400);
//        }
//        /*if(object.getComment().isEmpty()){object.setComment("comment");}
//        if (object.getFileData() != null) {
//            try {
//                byte[] bytes = object.getFileData();
//                String path = "DB\\" + object.getName();
//                BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(new File(path)));
//                stream.write(bytes);
//                stream.close();
//                mongo.add(new BasedFile(object.getName(),String.valueOf(bytes.length),object.getType(),object.getComment(), path));
//
//                return "Вы удачно загрузили " + object.getName() + " в базу. name = " + object.getName()  + " type = "
//                        + object.getType() + " size = " + bytes.length;
//            } catch (Exception e) {
//                return "Вам не удалось загрузить файл => " + e.getMessage();
//            }
//        } else {
//            return "Вам не удалось загрузить " + object.getName() + " потому что файл пустой.";
//        }*/
//    }

    @ApiOperation(value = "Вернет ссылку на файл, для скачивания.", notes = "Требуется name файла")
    @RequestMapping(value="/download/{fileId}", method=RequestMethod.GET)
    public @ResponseBody
    ResponseEntity<InputStreamResource> handleFileDownload(@PathVariable("fileId") String fileId) throws Exception{

            setUp();
            File file = new File(mongo.getById(fileId).getPath());
            InputStreamResource resource = new InputStreamResource(new FileInputStream(file));

            return ResponseEntity.ok()
                    // Content-Disposition
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment;filename=" + file.getName())
                    // Content-Type
                    .contentType(MediaType.APPLICATION_OCTET_STREAM)
                    // Contet-Length
                    .contentLength(file.length()) //
                    .body(resource);
    }

    @ApiOperation(value = "Удалит файл с сервера.", notes = "Требуется name файла")
    @RequestMapping(value="/removeFile", method=RequestMethod.DELETE)
    public @ResponseBody
    void deleteFile(@RequestParam("fileId") String fileId) throws Exception{
        setUp();
        mongo.removeByObjectId(fileId);

    }

    @RequestMapping(value="/getAllFilesData", method = RequestMethod.GET)
    public @ResponseBody
    BasedFile[] getAllFilesData() throws Exception{
        setUp();
        ArrayList<BasedFile> tmp = mongo.getAllFilesData();
        BasedFile[] result = new BasedFile[tmp.size()];

        for(int i = 0; i < tmp.size(); i++){
            result[i] = tmp.get(i);
        }
        return result;
    }

    @ApiOperation(value = "Вывод всех данных по файлу.", notes = "  Требуется id файла")
    @RequestMapping(value = "/getData", method = RequestMethod.GET)
    public @ResponseBody
    BasedFile getDocumentAttributes(@RequestParam("documentId") String fileId) throws Exception{
        setUp();
        return mongo.getAllFileAttributes(fileId);
    }


    @Override
    public void afterPropertiesSet() throws Exception {

    }
}
