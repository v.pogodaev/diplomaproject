package com.diploma.file_storage.Models.ViewModels;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;

public class UploadFileViewModel {

    private MultipartFile fileData;
    private String comment;


    public MultipartFile getFileData() { return fileData; }

    public void setFileData(MultipartFile fileData) {
        this.fileData = fileData;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }
}
