package diploma.relationship_service.ViewModels.ReturnModels;

public class GetRelationshipReturnModel {
    private String documentId;
    private String productId;

    public GetRelationshipReturnModel(String documentId, String productId, String type) {
        this.documentId = documentId;
        this.productId = productId;
        this.type = type;
    }

    private String type;

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


}
