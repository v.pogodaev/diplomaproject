package diploma.relationship_service.ViewModels;

import diploma.relationship_service.Model.Relationship;
import diploma.relationship_service.Model.RelationshipModel;

import javax.validation.constraints.NotNull;

public class CreateRelationshipViewModel extends TransactionDetailsViewModel{
    @NotNull
    private String productId;

    @NotNull
    private String documentId;

    private String type;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Relationship takeModel() {return new Relationship(
            productId, documentId, type,transactionId, sessionId, userId);}
}
