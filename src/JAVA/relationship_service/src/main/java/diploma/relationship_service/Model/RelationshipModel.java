package diploma.relationship_service.Model;

import diploma.relationship_service.Transactions.TransactableModel;
import diploma.relationship_service.Transactions.TransactionAction;

public class RelationshipModel extends TransactableModel {


    private String productId;

    private String documentId;

    private String type;

    public RelationshipModel(){}
    public RelationshipModel(String productId,
                             String documentId,
                             String type,
                             String transactionId,
                             String sessionId,
                             String userId ){
        this.productId = productId;
        this.documentId = documentId;
        this.type = type;
        this.setTransactionId(transactionId);
        this.setSessionId(sessionId);
        this.setUserId(userId);
    }
    public RelationshipModel(String productId, String documentId, String type)
    {
        this.productId = productId;
        this.documentId = documentId;
        this.type = type;
    }
    public RelationshipModel(String productId, String documentId)
    {
        this.productId = productId;
        this.documentId = documentId;
    }
    public RelationshipModel(String productId,
                             String documentId,
                             String type,
                             String transactionId,
                             String sessionId,
                             String userId,
                             TransactionAction transactionAction){
        this(productId, documentId, type, transactionId, sessionId, userId);
        this.setTransactionAction(transactionAction);
    }


    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }


}
