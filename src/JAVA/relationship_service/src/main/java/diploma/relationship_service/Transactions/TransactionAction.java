package diploma.relationship_service.Transactions;


public enum TransactionAction {
    Adding,

    Modifying,

    Removing,

    Empty

}
