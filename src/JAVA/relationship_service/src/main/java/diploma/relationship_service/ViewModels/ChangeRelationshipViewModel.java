package diploma.relationship_service.ViewModels;

import javax.validation.constraints.NotNull;

public class ChangeRelationshipViewModel extends TransactionDetailsViewModel{

    @NotNull
    private String newType;

    public String getNewType() {
        return newType;
    }

    public void setNewType(String newType) {
        this.newType = newType;
    }

}
