package diploma.relationship_service.Controllers;

import diploma.relationship_service.DB.WorkWithMongo;
import diploma.relationship_service.Model.Relationship;
import diploma.relationship_service.Model.RelationshipModel;
import diploma.relationship_service.Transactions.TransactionAction;
import diploma.relationship_service.ViewModels.ChangeRelationshipViewModel;
import diploma.relationship_service.ViewModels.CreateRelationshipViewModel;
import diploma.relationship_service.ViewModels.RemoveRelationshipViewModel;
import diploma.relationship_service.ViewModels.ReturnModels.GetRelationshipReturnModel;
import diploma.relationship_service.ViewModels.TransactionDetailsViewModel;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.ArrayList;
import java.util.Map;
import java.util.Properties;

import static diploma.relationship_service.Transactions.TransactionAction.*;

@RestController
@Controller
@RequestMapping("/relationship_storage")
public class RelationshipController {
    WorkWithMongo mongo;

    @ConfigurationProperties
    public void setUp() throws Exception {
        Properties prop = new Properties();

        // получил переменные среды
        Map<String, String> envs = System.getenv();

        prop.setProperty("connectionString", envs.get("db_connectionString"));
        prop.setProperty("dbname", envs.get("db_dbname"));
        prop.setProperty("login", envs.get("db_login"));
        prop.setProperty("password", envs.get("db_password"));
        prop.setProperty("table", envs.get("db_table"));
        mongo = new WorkWithMongo(prop);
    }

    @ApiOperation(
        value = "Создание связи между изделием и документом",
        notes = "На входе: id документа, id изделия и тип связи(необязательно). На выходе: id связи",
        response = String.class)
    @ApiResponses(value = {
    @ApiResponse(code = 500, message = "Error")})
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String createRelationship(@Valid @RequestBody CreateRelationshipViewModel object, HttpServletResponse response) throws Exception {
        setUp();

        Relationship item = object.takeModel();
        item.setTransactionAction(Adding);
        if(mongo.findDoc(object.getDocumentId(), object.getProductId(), object.getType())){
            response.setStatus(409);
            return "Документ с таким обозначением уже существует";}
        else {
            mongo.add(new Relationship(object.getDocumentId(),
                    object.getProductId(),
                    object.getType(),
                    item.getTransactionId(),
                    item.getSessionId(),
                    item.getUserId(),
                    item.getTransactionAction()));

            response.setStatus(200);
            return mongo.getObjectId(object.getDocumentId(), object.getProductId(), object.getType());
        }
    }


    @ApiOperation(value = "Удаление связи между изделием и документом",
            notes = "на входе: id документа, id изделия и тип связи")
    @ApiResponses(value = {
            @ApiResponse(code = 500 /*HttpStatus.BAD_REQUEST*/, message = "Что то пошло не так. Попробуйте снова.")})

    @RequestMapping(value = "/removeRelationship/{relationshipId}", method = RequestMethod.POST)
    public void removeRelationship(@PathVariable("relationshipId") String relationshipId, @RequestBody TransactionDetailsViewModel transactionDetailsViewModel, HttpServletResponse response) throws Exception {

        setUp();
        Relationship remDocument = mongo.getDataById(relationshipId);

        if(remDocument.getTransactionId().isEmpty() && remDocument.getSessionId().isEmpty() && remDocument.getUserId().isEmpty()){
            remDocument.setTransactionId(transactionDetailsViewModel.getTransactionId());
            remDocument.setSessionId(transactionDetailsViewModel.getSessionId());
            remDocument.setUserId(transactionDetailsViewModel.getUserId());
            remDocument.setTransactionAction(TransactionAction.Removing);
            mongo.updateAllProductAttributes(remDocument);
        }
    }

    @ApiOperation(value = "Корректировка типа связи", notes = "на входе: id документа, id изделия и старый тип связи и новый тип связи")
    @RequestMapping(value = "/update/{relationshipId}", method = RequestMethod.PUT)
    public void ChangeRelationshipType(@PathVariable("relationshipId") String relationshipId, @RequestBody @Valid ChangeRelationshipViewModel object, HttpServletResponse response) throws Exception {
        /*setUp();
        mongo.changeRelationship(data.getRelationshipId(), data.getNewType());
        return "Excepted";*/

        setUp();
        Relationship item = mongo.getDataById(relationshipId);
        if (item == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return;
        }

        //Если данные о транзакции существуют и они не равны пришедшим данным, ловим бедреквест
        if(!item.getTransactionId().isEmpty() && !item.getSessionId().isEmpty() && !item.getUserId().isEmpty() &&
                !item.getTransactionId().toLowerCase().equals(object.getTransactionId().toLowerCase()) &&
                !item.getSessionId().toLowerCase().equals(object.getSessionId().toLowerCase()) &&
                !item.getUserId().toLowerCase().equals(object.getUserId().toLowerCase()))
        {
            response.setStatus(HttpStatus.BAD_REQUEST.value());

            return;
        }
        //Если данные о транзакции пусты, значит мы моем создать новую транзакцию и без проблем работать с документом
        if(item.getTransactionId().isEmpty() && item.getSessionId().isEmpty())
        {
            //Создание копии изменяемого документа с данными о транзакции
            mongo.add(
                    new Relationship(
                            item.getRelationshipId(),
                            item.getDocumentId(),
                            item.getProductId(),
                            item.getType(),
                            object.getTransactionId(),
                            object.getSessionId(),
                            object.getUserId(),
                            TransactionAction.Modifying,
                            item.getRelationshipId(),
                            false));
            //Обновление документа оригинального продукта
            mongo.updateAllProductAttributes(
                    new Relationship(
                            item.getRelationshipId(),
                            item.getDocumentId(),
                            item.getProductId(),
                            object.getNewType(),
                            object.getTransactionId(),
                            object.getSessionId(),
                            object.getUserId(),
                            TransactionAction.Modifying ,
                            item.getRelationshipId(),
                            true));
            response.setStatus(HttpStatus.OK.value());
            return;
        }

        //Если данные о транзакции есть и они равны пришедшим данным, то мы тоже можем изменять документ
        if(!item.getTransactionId().isEmpty() &&
                item.getTransactionId().toLowerCase().equals(object.getTransactionId().toLowerCase()) &&
                item.getSessionId().toLowerCase().equals(object.getSessionId().toLowerCase())){
            mongo.updateAllProductAttributes(
                    new Relationship(
                            item.getRelationshipId(),
                            item.getDocumentId(),
                            item.getProductId(),
                            item.getType(),
                            object.getTransactionId(),
                            object.getSessionId(),
                            object.getUserId(),
                            TransactionAction.Modifying ,
                            item.getRelationshipId(),
                            true));
            response.setStatus(HttpStatus.OK.value());
            return;
        }
        response.setStatus(HttpStatus.BAD_REQUEST.value());
    }

    @ApiOperation(value = "Вывод всех связанных документов", notes = "на входе: id документа")
    @RequestMapping(value = "/getRelatedDocuments", method = RequestMethod.GET)
    public String[] GetRelatedDocuments(@RequestParam("productId") String productId) throws Exception {
        setUp();

        return mongo.GetRelatedObjects(productId, "documentId");
    }

    @ApiOperation(value = "Вывод всех связанных изделий", notes = "на входе: id изделия")
    @RequestMapping(value = "/getRelatedProducts", method = RequestMethod.GET)
    public String[] getRelatedProducts(@RequestParam("documentId") String documentId) throws Exception {
        setUp();
        return mongo.GetRelatedObjects(documentId, "productId");
    }

    @RequestMapping(value = "/getRelationship", method = RequestMethod.GET)
    public @ResponseBody
    GetRelationshipReturnModel getRelationshipData(@RequestParam("relationshipId") String Id) throws Exception {
        setUp();

        return mongo.getAllDocumentAttributes(Id);
    }

    @RequestMapping(value = "/CommitTransaction", method = RequestMethod.PUT)
    public void CommitTransaction(@RequestBody @Valid TransactionDetailsViewModel tmodel, HttpServletResponse response) throws Exception{
        setUp();
        ArrayList<Relationship> tDocs =  mongo.getTObjects(tmodel);

        if (tDocs.size() == 0) {
            response.setStatus(404);
            return;
        }

        ArrayList<Relationship> docsToUpdate = new ArrayList<>();
        ArrayList<Relationship> docsToRemove = new ArrayList<>();
        for (Relationship d : tDocs) {
            if (d.getTransactionAction() == TransactionAction.Adding ) {
                d.ClearCommitData();
                docsToUpdate.add(d);
            }
            if(d.getTransactionAction() == TransactionAction.Modifying && !d.getIsOriginal()){
                docsToRemove.add(d);
            }
            if(d.getTransactionAction() == TransactionAction.Modifying && d.getIsOriginal()){
                d.ClearCommitData();
                docsToUpdate.add(d);
            }
            if(d.getTransactionAction() == TransactionAction.Removing)
            {
                docsToRemove.add(d);
            }
        }

        for (Relationship d : docsToUpdate) {
            mongo.updateAllProductAttributes(d);
        }

        for (Relationship d : docsToRemove){
            mongo.remove(d.getRelationshipId());
        }

        response.setStatus(200);
    }

    @RequestMapping(value = "/CancelTransaction", method = RequestMethod.PUT)
    public void CancelTransaction(@RequestBody @Valid TransactionDetailsViewModel tmodel, HttpServletResponse response) throws Exception {
        setUp();
        ArrayList<Relationship> tDocs =  mongo.getTObjects(tmodel),
                fakeDocs = new ArrayList<>(),
                docsToCancel = new ArrayList<>();

        if (tDocs.size() == 0) {
            response.setStatus(404);
            return;
        }

        for(Relationship d : tDocs){
            switch (d.getTransactionAction()){
                case Adding:
                    mongo.remove(d.getRelationshipId());
                    break;
                case Removing:
                    d.ClearCommitData();
                    docsToCancel.add(d);
                    break;
                case Modifying:
                    if (!d.getIsOriginal()) {
                        fakeDocs.add(d);
                    }
                    break;
            }
        }
        for(Relationship d : fakeDocs) {
            Relationship docToCancel = new Relationship(
                    d.getRelationshipId(),
                    d.getDocumentId(),
                    d.getProductId(),
                    d.getType(),
                    d.getTransactionId(),
                    d.getSessionId(),
                    d.getUserId(),
                    d.getTransactionAction(),
                    d.getOriginalId(),
                    true
            );
            docToCancel.ClearCommitData();
            docsToCancel.add(docToCancel);
        }
        for (Relationship d : docsToCancel) {
            mongo.updateAllProductAttributes(d);
        }

        for (Relationship d : fakeDocs){
            mongo.remove(d.getRelationshipId());
        }
    }

}
