package diploma.relationship_service.Model;

import diploma.relationship_service.Transactions.TransactableModel;
import diploma.relationship_service.Transactions.TransactionAction;

public class Relationship extends TransactableModel {
   private String relationshipId;
   private String documentId;
   private String productId;
   private String type;

    public Relationship(String relationshipId,
                        String documentId,
                        String productId,
                        String type){
        this.relationshipId = relationshipId;
        this.documentId = documentId;
        this.productId = productId;
        this.type = type;
    }

    public Relationship(String documentId,
                        String productId,
                        String type,
                        String transactionId,
                        String sessionId,
                        String userId,
                        TransactionAction transactionAction){
        this.documentId = documentId;
        this.productId = productId;
        this.type = type;
        this.transactionId = transactionId;
        this.userId = userId;
        this.sessionId = sessionId;
        this.transactionAction = transactionAction;
    }

    public Relationship(String documentId,
                        String productId,
                        String type,
                        String transactionId,
                        String sessionId,
                        String userId){
        this.documentId = documentId;
        this.productId = productId;
        this.type = type;
        this.transactionId = transactionId;
        this.userId = userId;
        this.sessionId = sessionId;
    }

    public Relationship(String relationshipId,
                        String documentId,
                        String productId,
                        String type,
                        String transactionId,
                        String sessionId,
                        String userId,
                        TransactionAction transactionAction) {
        this(relationshipId, documentId, productId, type);
        this.setTransactionId(transactionId);
        this.setSessionId(sessionId);
        this.setUserId(userId);
        this.setTransactionAction(transactionAction);
    }

    public Relationship(String relationshipId,
                        String documentId,
                        String productId,
                        String type,
                        String transactionId,
                        String sessionId,
                        String userId,
                        TransactionAction transactionAction,
                        String originalId,
                        boolean isOriginal) {
        this(relationshipId, documentId, productId, type);
        this.setTransactionId(transactionId);
        this.setSessionId(sessionId);
        this.setUserId(userId);
        this.setTransactionAction(transactionAction);
        this.setOriginalId(originalId);
        this.setIsOriginal(isOriginal);
    }

    public String getRelationshipId() {
        return relationshipId;
    }

    public void setRelationshipId(String relationshipId) {
        this.relationshipId = relationshipId;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
