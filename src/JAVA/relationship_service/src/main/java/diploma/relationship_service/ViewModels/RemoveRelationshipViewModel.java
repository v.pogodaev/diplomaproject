package diploma.relationship_service.ViewModels;

import javax.validation.constraints.NotNull;

public class RemoveRelationshipViewModel {
    @NotNull
    private String productId;

    @NotNull
    private String documentId;

    @NotNull
    private String type;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getDocumentId() {
        return documentId;
    }

    public void setDocumentId(String documentId) {
        this.documentId = documentId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
