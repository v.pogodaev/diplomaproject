package diploma.relationship_service.DB;

import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import diploma.relationship_service.Model.Relationship;
import diploma.relationship_service.Model.RelationshipModel;
import diploma.relationship_service.Transactions.TransactionAction;
import diploma.relationship_service.ViewModels.ReturnModels.GetRelationshipReturnModel;
import diploma.relationship_service.ViewModels.TransactionDetailsViewModel;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.Properties;

public class WorkWithMongo {
    // это клиент который обеспечит подключение к БД
    private MongoClient mongoClient;

    // В нашем случае, этот класс дает
// возможность аутентифицироваться в MongoDB
    private MongoDatabase db;

    // тут мы будем хранить состояние подключения к БД
    private boolean authenticate;

    // И класс который обеспечит возможность работать
// с коллекциями / таблицами MongoDB
    private MongoCollection<Document> table;

    public WorkWithMongo(Properties prop) {
        // Создаем подключение
        System.out.println("trying to connect to " + prop.getProperty("connectionString"));
        char[] pass = prop.getProperty("password").toCharArray();
        MongoCredential credential = MongoCredential.createCredential(prop.getProperty("login"),"admin", pass);
        int connectTimeout = 1000 * 60;
        MongoClientOptions options = new MongoClientOptions.Builder().connectTimeout(connectTimeout).build();
        mongoClient = new MongoClient(new ServerAddress(prop.getProperty("connectionString")), credential, options);

        // Выбираем БД для дальнейшей работы
        db = mongoClient.getDatabase(prop.getProperty("dbname"));

        // Выбираем коллекцию/таблицу для дальнейшей работы
        table = db.getCollection(prop.getProperty("table"));
    }

    public void add(Relationship relationship){
        Document document = new Document();
        // это поле будет записываться в коллекцию/таблицу
        document.append("documentId", relationship.getDocumentId())
                .append("productId", relationship.getProductId())
                .append("type", relationship.getType())
                .append("transactionId", relationship.getTransactionId())
                .append("sessionId", relationship.getSessionId())
                .append("userId", relationship.getUserId())
                .append("transactionAction", relationship.getTransactionAction().toString())
                .append("isOriginal", relationship.getIsOriginal())
                .append("originalId", relationship.getOriginalId());
        // записываем данные в коллекцию/таблицу
        table.insertOne(document);
    }

    public String getRelationshipId(RelationshipModel relationshipModel)
    {
        Document document = new Document();
        document.put("productId", relationshipModel.getProductId());
        document.put("documentId", relationshipModel.getDocumentId());
        document.put("type", relationshipModel.getType());

        return table.find(document).first().get("_id").toString();
    }

    public void remove(String relationshipId)
    {
        table.findOneAndDelete(findObjectById(relationshipId));
    }

    public Document findObjectById(String id){
        Document query = new Document();

        query.put("_id", new ObjectId(id));
        return table.find(query).first();

    }

    public GetRelationshipReturnModel getAllDocumentAttributes(String Id){
        Document object = findObjectById(Id);
        return new GetRelationshipReturnModel(object.get("productId").toString(),
                                     object.get("documentId").toString(),
                                     object.get("type").toString());
    }

    public void changeRelationship (String id, String newType){
        table.findOneAndUpdate(findObjectById(id), new Document().append("$set", new Document().append("type", newType)));
    }

    public String[] GetRelatedObjects(String objectId, String objectType)
    {
        ArrayList<Document> dbQuery = new ArrayList<>();
        Document query = new Document();

        query.put(objectType.equals("productId") ? "documentId" : "productId", objectId );
        table.find(query).into(dbQuery);
        String[] documents = new String[dbQuery.size()];
        Integer i = 0;
        for (Document doc: dbQuery) {
            documents[i] =  doc.get(objectType).toString();
            i++;
        }
        return documents;
    }

    public boolean findDoc(String documentId, String productId, String RelType) {
        Document query = new Document();

        query.append("documentId", documentId)
                .append("productId", productId)
                .append("type", RelType);

        Document result = table.find(query).first();
        if (result != null) {
            return true;
        } else {
            return false;
        }
    }

    public void updateAllProductAttributes(Relationship doc)
    {
        Document newDocument = new Document();
        newDocument.append("$set",
                new Document()
                        .append("documentId", doc.getDocumentId() == null ? "" : doc.getDocumentId())
                        .append("productId", doc.getProductId() == null ? "" : doc.getProductId())
                        .append("type", doc.getType() == null ? "" : doc.getType())
                        .append("transactionId", doc.getTransactionId() == null ? "" : doc.getTransactionId())
                        .append("sessionId", doc.getSessionId() == null ? "" : doc.getSessionId())
                        .append("userId", doc.getUserId() == null ? "" : doc.getUserId())
                        .append("transactionAction", doc.getTransactionAction() == null ? "" : doc.getTransactionAction().toString())
                        .append("originalId", doc.getOriginalId() == null ? "" : doc.getOriginalId())
                        .append("isOriginal", doc.getIsOriginal()));
        Document searchQuery = new Document().append("_id", new ObjectId(doc.getRelationshipId()));
        table.updateOne(searchQuery, newDocument);
    }

    public ArrayList<Relationship> getTObjects(TransactionDetailsViewModel tmodel) {
        ArrayList<Relationship> relationships = getAllProducts();
        ArrayList<Relationship> tDocs = new ArrayList<>();

        for (Relationship doc : relationships) {
            if (doc.getTransactionId() != null &&
                    doc.getTransactionId().toLowerCase().equals(tmodel.getTransactionId().toLowerCase()) &&
                    doc.getUserId().toLowerCase().equals(tmodel.getUserId().toLowerCase()) &&
                    doc.getSessionId().toLowerCase().equals(tmodel.getSessionId().toLowerCase())) {
                tDocs.add(doc);
            }
        }
        return tDocs;
    }

    public Relationship getDataById(String objectId){
        Document object = findObjectById(objectId);
        Relationship resultObject = new Relationship(object.get("_id") != null ? object.get("_id").toString() : "",
                object.get("documentId") != null ? object.get("documentId").toString() : "",
                object.get("productId") != null ? object.get("productId").toString() : "",
                object.get("type") != null ? object.get("type").toString() : "",
                object.get("transactionId") != null ? object.get("transactionId").toString() : "",
                object.get("sessionId") != null ? object.get("sessionId").toString() : "",
                object.get("userId") != null ? object.get("userId").toString() : "",
                object.get("transactionAction") != null ? ToAction(object.get("transactionAction").toString()) : TransactionAction.Empty,
                object.get("originalId") != null ? object.get("originalId").toString() : "",
                object.get("isOriginal") == null || (boolean) object.get("isOriginal"));
        return resultObject;
    }

    public ArrayList<Relationship> getAllProducts(){
        ArrayList<Relationship> result = new ArrayList<>();
        ArrayList<Document> queryDB = new ArrayList<Document>();
        table.find().into(queryDB);
        for(Document doc : queryDB) {
            result.add(new Relationship(
                    doc.get("_id") != null ? doc.get("_id").toString() : "",
                    doc.get("documentId") != null ? doc.get("documentId").toString() : "",
                    doc.get("productId") != null ? doc.get("productId").toString() : "",
                    doc.get("type") != null ? doc.get("type").toString() : "",
                    doc.get("transactionId") != null ? doc.get("transactionId").toString() : "",
                    doc.get("sessionId") != null ? doc.get("sessionId").toString() : "",
                    doc.get("userId") != null ? doc.get("userId").toString() : "",
                    doc.get("transactionAction") != null ? ToAction(doc.get("transactionAction").toString()) : TransactionAction.Empty,
                    doc.get("originalId") != null ? doc.get("originalId").toString() : "",
                    doc.get("isOriginal") == null || (boolean) doc.get("isOriginal")));

        }
        return result;
    }

    private TransactionAction ToAction(String action)
    {
        TransactionAction resAction;
        switch (action){
            case "Adding":
                resAction = TransactionAction.Adding;
                break;
            case "Modifying" :
                resAction = TransactionAction.Modifying;
                break;
            case "Removing":
                resAction = TransactionAction.Removing;
                break;
            default:
                resAction = TransactionAction.Empty;
                break;
        }
        return resAction;
    }
    public String getObjectId(String documentId, String productId, String RelType){
        Document query = new Document();

        query.append("documentId", documentId)
                .append("productId", productId)
                .append("type", RelType);

        Document result = table.find(query).first();
        return result.get("_id").toString();
    }



}
