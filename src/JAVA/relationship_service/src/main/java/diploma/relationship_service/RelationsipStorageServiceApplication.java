package diploma.relationship_service;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RelationsipStorageServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(RelationsipStorageServiceApplication.class, args);
    }

}
