package com.diploma.product_cards_storage.Models;

import com.diploma.product_cards_storage.TransactionsViewModels.TransactionDetailsViewModel;

import javax.validation.constraints.NotNull;

public class CardViewModelRemove extends TransactionDetailsViewModel {
    @NotNull
    private String productId;

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }
}
