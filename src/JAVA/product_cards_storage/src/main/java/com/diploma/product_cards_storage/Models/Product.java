package com.diploma.product_cards_storage.Models;

import com.diploma.product_cards_storage.Transactions.TransactableModel;
import com.diploma.product_cards_storage.Transactions.TransactionAction;

public class Product extends TransactableModel {
    private String id;
    private String designation;
    private String name;
    private Integer revision;
    private String state;
    private String modelId;
    private String viewModelId;
    private Double weight;

    public Product(String id,
                   String designation,
                   String name,
                   Integer revision,
                   String state,
                   String modelId,
                   String viewModelId,
                   Double weight){

        this(designation, name, revision, state, modelId, viewModelId, weight);
        this.id = id;
    }

    public Product(
                   String designation,
                   String name,
                   Integer revision,
                   String state,
                   String modelId,
                   String viewModelId,
                   Double weight,
                   String transactionId,
                   String sessionId,
                   String userId,
                   TransactionAction transactionAction){

        this(designation, name, revision, state, modelId, viewModelId, weight);
        this.setTransactionId(transactionId);
        this.setSessionId(sessionId);
        this.setUserId(userId);
        this.setTransactionAction(transactionAction);
    }

    public Product(String id,
                   String designation,
                   String name,
                   Integer revision,
                   String state,
                   String modelId,
                   String viewModelId,
                   Double weight,
                   String transactionId,
                   String sessionId,
                   String userId,
                   TransactionAction transactionAction,
                   String originalId,
                   boolean isOriginal){

        this(designation, name, revision, state, modelId, viewModelId, weight);
        this.id = id;
        this.setTransactionId(transactionId);
        this.setSessionId(sessionId);
        this.setUserId(userId);
        this.setTransactionAction(transactionAction);
        this.setOriginalId(originalId);
        this.setIsOriginal(isOriginal);
    }

    public Product(String designation,
                   String name,
                   Integer revision,
                   String state,
                   String modelId,
                   String viewModelId,
                   Double weight){
        this.designation = designation;
        this.name = name;
        this.revision = revision;
        this.state = state;
        this.modelId = modelId;
        this.viewModelId = viewModelId;
        this.weight = weight;
    }
    public Product(String designation,
                   String name,
                   Double weight,
                   String transactionId,
                   String sessionId,
                   String userId){
        this.designation = designation;
        this.name = name;
        this.weight = weight;
        this.setTransactionId(transactionId);
        this.setSessionId(sessionId);
        this.setUserId(userId);
    }
    public String getId() {
        return id;
    }

    public String getDesignation() {
        return designation;
    }

    public String getName() {
        return name;
    }

    public Integer getRevision() {
        return revision;
    }

    public String getState() {
        return state;
    }

    public String getModelId() {
        return modelId;
    }

    public String getViewModelId() {
        return viewModelId;
    }

    public Double getWeight() {
        return weight;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setRevision(Integer revision) {
        this.revision = revision;
    }

    public void setState(String state) {
        this.state = state;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public void setViewModelId(String viewModelId) {
        this.viewModelId = viewModelId;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }
}
