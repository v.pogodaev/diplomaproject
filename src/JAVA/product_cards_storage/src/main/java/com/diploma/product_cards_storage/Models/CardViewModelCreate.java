package com.diploma.product_cards_storage.Models;

import com.diploma.product_cards_storage.TransactionsViewModels.TransactionDetailsViewModel;

import javax.validation.constraints.NotNull;

public class CardViewModelCreate extends TransactionDetailsViewModel {
    @NotNull
    private String designation;
    private String name;
    private Double weight;

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Product takeModel(){return new Product(
            designation, name, weight, transactionId, sessionId, userId);
    }
}
