package com.diploma.product_cards_storage.Models.ReturnModels;

public class ProductReturnModel {
    private String id;
    private String designation;
    private String name;
    private Integer revision;
    private String state;
    private String modelId;
    private String viewModelId;
    private Double weight;

    public ProductReturnModel(String id,
                              String designation,
                              String name,
                              Integer revision,
                              String state,
                              String modelId,
                              String viewModelId,
                              Double weight){
        this.id = id;
        this.designation = designation;
        this.name = name;
        this.revision = revision;
        this.state = state;
        this.modelId = modelId;
        this.viewModelId = viewModelId;
        this.weight = weight;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDesignation() {
        return designation;
    }

    public void setDesignation(String designation) {
        this.designation = designation;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getRevision() {
        return revision;
    }

    public void setRevision(Integer revision) {
        this.revision = revision;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }

    public String getViewModelId() {
        return viewModelId;
    }

    public void setViewModelId(String viewModelId) {
        this.viewModelId = viewModelId;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }
}
