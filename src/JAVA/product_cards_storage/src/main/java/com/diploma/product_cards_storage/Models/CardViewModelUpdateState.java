package com.diploma.product_cards_storage.Models;

import com.diploma.product_cards_storage.TransactionsViewModels.TransactionDetailsViewModel;

public class CardViewModelUpdateState extends TransactionDetailsViewModel {
    private String objectId;

    private String state;

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }
}
