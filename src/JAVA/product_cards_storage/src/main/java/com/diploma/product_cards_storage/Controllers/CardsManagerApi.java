package com.diploma.product_cards_storage.Controllers;

import au.com.bytecode.opencsv.CSVReader;
import com.diploma.product_cards_storage.DB.WorkWithMongo;
import com.diploma.product_cards_storage.Models.*;
import com.diploma.product_cards_storage.Models.ReturnModels.ProductReturnModel;
import com.diploma.product_cards_storage.Transactions.TransactionAction;
import com.diploma.product_cards_storage.TransactionsViewModels.TransactionDetailsViewModel;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.Properties;

@RestController
@Controller
@RequestMapping("/cards_storage")
public class CardsManagerApi {

    WorkWithMongo mongo;

    public void setUp() throws Exception {
        Properties prop = new Properties();
        // получил переменные среды
        Map<String, String> envs = System.getenv();
        // тут используются переменные среды, прописанные в docker-compose
        // возможно (скорее всего надо) лучше перенести в другое место

        prop.setProperty("connectionString", envs.get("db_connectionString"));
        prop.setProperty("dbname", envs.get("db_dbname"));
        prop.setProperty("login", envs.get("db_login"));
        prop.setProperty("password", envs.get("db_password"));
        prop.setProperty("table", envs.get("db_table"));
        mongo = new WorkWithMongo(prop);
    }
    @RequestMapping(value = "/create", method = RequestMethod.POST)
    public String createNewProductCard(@Valid @RequestBody CardViewModelCreate object, HttpServletResponse response) throws Exception {
        setUp();

        Product item = object.takeModel();
        item.setTransactionAction(TransactionAction.Adding);
        if(mongo.findDoc(object.getDesignation())){
            response.setStatus(409);
            return "Документ с таким обозначением уже существует";}
        else {
            mongo.add(new Product(object.getDesignation(),
                                    object.getName(),
                                    1,
                                    "On Check",
                                    "",
                                    "",
                                    object.getWeight(),
                                    item.getTransactionId(),
                                    item.getSessionId(),
                                    item.getUserId(),
                                    item.getTransactionAction()));

            response.setStatus(200);
            return mongo.getObjectId(object.getDesignation());
        }
    }

    @RequestMapping(value = "/update/{productId}", method = RequestMethod.POST)
    public void updateProductCard(@PathVariable("productId") String productId, @Valid @RequestBody CardViewModelUpdate object, HttpServletResponse response) throws Exception {
        setUp();
        Product item = mongo.getDataById(productId);
        if (item == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return;
        }

        //Если данные о транзакции существуют и они не равны пришедшим данным, ловим бедреквест
        if(!item.getTransactionId().isEmpty() && !item.getSessionId().isEmpty() && !item.getUserId().isEmpty() &&
                !item.getTransactionId().toLowerCase().equals(object.getTransactionId().toLowerCase()) &&
                !item.getSessionId().toLowerCase().equals(object.getSessionId().toLowerCase()) &&
                !item.getUserId().toLowerCase().equals(object.getUserId().toLowerCase()))
        {
            response.setStatus(HttpStatus.BAD_REQUEST.value());

            return;
        }
        //Если данные о транзакции пусты, значит мы моем создать новую транзакцию и без проблем работать с документом
        if(item.getTransactionId().isEmpty() && item.getSessionId().isEmpty())
        {
            //Создание копии изменяемого документа с данными о транзакции
            mongo.add(
                    new Product(
                            item.getId(),
                            item.getDesignation(),
                            item.getName(),
                            item.getRevision(),
                            item.getState(),
                            item.getModelId(),
                            item.getViewModelId(),
                            item.getWeight(),
                            object.getTransactionId(),
                            object.getSessionId(),
                            object.getUserId(),
                            TransactionAction.Modifying ,
                            item.getId(),
                            false));
            //Обновление документа оригинального продукта
            mongo.updateAllProductAttributes(
                    new Product(
                            item.getId(),
                            object.getDesignation(),
                            object.getName(),
                            item.getRevision(),
                            item.getState(),
                            item.getModelId(),
                            item.getViewModelId(),
                            object.getWeight(),
                            object.getTransactionId(),
                            object.getSessionId(),
                            object.getUserId(),
                            TransactionAction.Modifying ,
                            item.getId(),
                            true));
            response.setStatus(HttpStatus.OK.value());
            return;
        }

        //Если данные о транзакции есть и они равны пришедшим данным, то мы тоже можем изменять документ
        if(!item.getTransactionId().isEmpty() &&
                item.getTransactionId().toLowerCase().equals(object.getTransactionId().toLowerCase()) &&
                item.getSessionId().toLowerCase().equals(object.getSessionId().toLowerCase())){
            mongo.updateAllProductAttributes(
                    new Product(
                            item.getId(),
                            object.getDesignation(),
                            object.getName(),
                            item.getRevision(),
                            item.getState(),
                            item.getModelId(),
                            item.getViewModelId(),
                            object.getWeight(),
                            object.getTransactionId(),
                            object.getSessionId(),
                            object.getUserId(),
                            TransactionAction.Modifying ,
                            item.getId(),
                            true));
            response.setStatus(HttpStatus.OK.value());
            return;
        }
        response.setStatus(HttpStatus.BAD_REQUEST.value());
    }

    @RequestMapping(value = "/updateState", method = RequestMethod.POST)
    public void updateProductState(@Valid @RequestBody CardViewModelUpdateState object, HttpServletResponse response) throws Exception{
        setUp();
        Product item = mongo.getDataById(object.getObjectId());
        if (item == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return;
        }

        //Если данные о транзакции существуют и они не равны пришедшим данным, ловим бедреквест
        if(!item.getTransactionId().isEmpty() && !item.getSessionId().isEmpty() && !item.getUserId().isEmpty() &&
                !item.getTransactionId().toLowerCase().equals(object.getTransactionId().toLowerCase()) &&
                !item.getSessionId().toLowerCase().equals(object.getSessionId().toLowerCase()) &&
                !item.getUserId().toLowerCase().equals(object.getUserId().toLowerCase()))
        {
            response.setStatus(HttpStatus.BAD_REQUEST.value());

            return;
        }

        //Если данные о транзакции пусты, значит мы моем создать новую транзакцию и без проблем работать с документом
        if(item.getTransactionId().isEmpty() && item.getSessionId().isEmpty())
        {
            //Создание копии изменяемого документа с данными о транзакции
            mongo.add(
                    new Product(
                            item.getId(),
                            item.getDesignation(),
                            item.getName(),
                            item.getRevision(),
                            item.getState(),
                            item.getModelId(),
                            item.getViewModelId(),
                            item.getWeight(),
                            object.getTransactionId(),
                            object.getSessionId(),
                            object.getUserId(),
                            TransactionAction.Modifying ,
                            item.getId(),
                            false));
            //Обновление документа оригинального продукта
            mongo.updateAllProductAttributes(
                    new Product(
                            item.getId(),
                            item.getDesignation(),
                            item.getName(),
                            item.getRevision(),
                            object.getState(),
                            item.getModelId(),
                            item.getViewModelId(),
                            item.getWeight(),
                            object.getTransactionId(),
                            object.getSessionId(),
                            object.getUserId(),
                            TransactionAction.Modifying ,
                            item.getId(),
                            true));
            response.setStatus(HttpStatus.OK.value());
            return;


        }

        //Если данные о транзакции есть и они равны пришедшим данным, то мы тоже можем изменять документ
        if(!item.getTransactionId().isEmpty() &&
                item.getTransactionId().toLowerCase().equals(object.getTransactionId().toLowerCase()) &&
                item.getSessionId().toLowerCase().equals(object.getSessionId().toLowerCase())){
            mongo.updateAllProductAttributes(
                    new Product(
                            item.getId(),
                            item.getDesignation(),
                            item.getName(),
                            item.getRevision(),
                            object.getState(),
                            item.getModelId(),
                            item.getViewModelId(),
                            item.getWeight(),
                            object.getTransactionId(),
                            object.getSessionId(),
                            object.getUserId(),
                            TransactionAction.Modifying ,
                            item.getId(),
                            true));
            response.setStatus(HttpStatus.OK.value());
            return;
        }
        response.setStatus(HttpStatus.BAD_REQUEST.value());

    }

    @RequestMapping(value = "/getProduct/{id}", method = RequestMethod.GET)
    @ResponseBody
    public ProductReturnModel getDataFromProduct(@PathVariable("id") String id) throws Exception{
        setUp();
        Product product = mongo.getDataById(id);
        return new ProductReturnModel(product.getId(),
                                    product.getDesignation(),
                                    product.getName(),
                                    product.getRevision(),
                                    product.getState(),
                                    product.getModelId(),
                                    product.getViewModelId(),
                                    product.getWeight());
    }

    @RequestMapping(value="/getAllProducts", method = RequestMethod.GET)
    public @ResponseBody
    ArrayList<ProductReturnModel> getAllDocuments() throws Exception{
        setUp();
        ArrayList<Product> allProducts = mongo.getAllProducts();
        ArrayList<ProductReturnModel> listToView = new ArrayList<>();
        for (Product doc: allProducts) {
            if((doc.getTransactionId().isEmpty() || doc.getTransactionId() == null)  && (!doc.getTransactionAction().equals(TransactionAction.Adding) ||
                    !doc.getTransactionAction().equals(TransactionAction.Modifying ))){
                listToView.add(new ProductReturnModel(doc.getId(),
                                                        doc.getDesignation(),
                                                        doc.getName(),
                                                        doc.getRevision(),
                                                        doc.getState(),
                                                        doc.getModelId(),
                                                        doc.getViewModelId(),
                                                        doc.getWeight()));
            }
        }
        return listToView;
    }

    @RequestMapping(value = "/postProductInWork/{id}", method = RequestMethod.POST)
    public @ResponseBody
    void changeStateInWork(@PathVariable("id") String id, @Valid @RequestBody TransactionInfo transactionInfo, HttpServletResponse response) throws Exception{
        setUp();
        Product item = mongo.getDataById(id);
        if (item == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return;
        }

        //Если данные о транзакции существуют и они не равны пришедшим данным, ловим бедреквест
        if(!item.getTransactionId().isEmpty() && !item.getSessionId().isEmpty() && !item.getUserId().isEmpty() &&
                !item.getTransactionId().toLowerCase().equals(transactionInfo.getTransactionId().toLowerCase()) &&
                !item.getSessionId().toLowerCase().equals(transactionInfo.getSessionId().toLowerCase()) &&
                !item.getUserId().toLowerCase().equals(transactionInfo.getUserId().toLowerCase()))
        {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return;
        }

        //Если данные о транзакции пусты, значит мы моем создать новую транзакцию и без проблем работать с документом
        if(item.getTransactionId().isEmpty() && item.getSessionId().isEmpty())
        {
            //Создание копии изменяемого документа с данными о транзакции
            mongo.add(
                    new Product(
                            item.getId(),
                            item.getDesignation(),
                            item.getName(),
                            item.getRevision(),
                            item.getState(),
                            item.getModelId(),
                            item.getViewModelId(),
                            item.getWeight(),
                            transactionInfo.getTransactionId(),
                            transactionInfo.getSessionId(),
                            transactionInfo.getUserId(),
                            TransactionAction.Modifying ,
                            item.getId(),
                            false));
            //Обновление документа оригинального продукта
            mongo.updateAllProductAttributes(
                    new Product(
                            item.getId(),
                            item.getDesignation(),
                            item.getName(),
                            item.getRevision(),
                            "InWork",
                            item.getModelId(),
                            item.getViewModelId(),
                            item.getWeight(),
                            transactionInfo.getTransactionId(),
                            transactionInfo.getSessionId(),
                            transactionInfo.getUserId(),
                            TransactionAction.Modifying ,
                            item.getId(),
                            true));
            response.setStatus(HttpStatus.OK.value());
            return;


        }

        //Если данные о транзакции есть и они равны пришедшим данным, то мы тоже можем изменять документ
        if(!item.getTransactionId().isEmpty() &&
                item.getTransactionId().toLowerCase().equals(transactionInfo.getTransactionId().toLowerCase()) &&
                item.getSessionId().toLowerCase().equals(transactionInfo.getSessionId().toLowerCase())){
            mongo.updateAllProductAttributes(
                    new Product(
                            item.getId(),
                            item.getDesignation(),
                            item.getName(),
                            item.getRevision(),
                            "InWork",
                            item.getModelId(),
                            item.getViewModelId(),
                            item.getWeight(),
                            transactionInfo.getTransactionId(),
                            transactionInfo.getSessionId(),
                            transactionInfo.getUserId(),
                            TransactionAction.Modifying ,
                            item.getId(),
                            true));
            response.setStatus(HttpStatus.OK.value());
            return;
        }
        response.setStatus(HttpStatus.BAD_REQUEST.value());


    }

    @RequestMapping(value = "/postProductInCheck/{id}", method = RequestMethod.POST)
    public @ResponseBody
    void changeStateInCheck(@PathVariable("id") String id, @Valid @RequestBody TransactionInfo transactionInfo, HttpServletResponse response) throws Exception{
        setUp();
        Product item = mongo.getDataById(id);
        if (item == null) {
            response.setStatus(HttpStatus.NOT_FOUND.value());
            return;
        }

        //Если данные о транзакции существуют и они не равны пришедшим данным, ловим бедреквест
        if(!item.getTransactionId().isEmpty() && !item.getSessionId().isEmpty() && !item.getUserId().isEmpty() &&
                !item.getTransactionId().toLowerCase().equals(transactionInfo.getTransactionId().toLowerCase()) &&
                !item.getSessionId().toLowerCase().equals(transactionInfo.getSessionId().toLowerCase()) &&
                !item.getUserId().toLowerCase().equals(transactionInfo.getUserId().toLowerCase()))
        {
            response.setStatus(HttpStatus.BAD_REQUEST.value());
            return;
        }

        //Если данные о транзакции пусты, значит мы моем создать новую транзакцию и без проблем работать с документом
        if(item.getTransactionId().isEmpty() && item.getSessionId().isEmpty())
        {
            //Создание копии изменяемого документа с данными о транзакции
            mongo.add(
                    new Product(
                            item.getId(),
                            item.getDesignation(),
                            item.getName(),
                            item.getRevision(),
                            item.getState(),
                            item.getModelId(),
                            item.getViewModelId(),
                            item.getWeight(),
                            transactionInfo.getTransactionId(),
                            transactionInfo.getSessionId(),
                            transactionInfo.getUserId(),
                            TransactionAction.Modifying ,
                            item.getId(),
                            false));
            //Обновление документа оригинального продукта
            mongo.updateAllProductAttributes(
                    new Product(
                            item.getId(),
                            item.getDesignation(),
                            item.getName(),
                            item.getRevision(),
                            "InCheck",
                            item.getModelId(),
                            item.getViewModelId(),
                            item.getWeight(),
                            transactionInfo.getTransactionId(),
                            transactionInfo.getSessionId(),
                            transactionInfo.getUserId(),
                            TransactionAction.Modifying ,
                            item.getId(),
                            true));
            response.setStatus(HttpStatus.OK.value());
            return;


        }

        //Если данные о транзакции есть и они равны пришедшим данным, то мы тоже можем изменять документ
        if(!item.getTransactionId().isEmpty() &&
                item.getTransactionId().toLowerCase().equals(transactionInfo.getTransactionId().toLowerCase()) &&
                item.getSessionId().toLowerCase().equals(transactionInfo.getSessionId().toLowerCase())){
            mongo.updateAllProductAttributes(
                    new Product(
                            item.getId(),
                            item.getDesignation(),
                            item.getName(),
                            item.getRevision(),
                            "InCheck",
                            item.getModelId(),
                            item.getViewModelId(),
                            item.getWeight(),
                            transactionInfo.getTransactionId(),
                            transactionInfo.getSessionId(),
                            transactionInfo.getUserId(),
                            TransactionAction.Modifying ,
                            item.getId(),
                            true));
            response.setStatus(HttpStatus.OK.value());
            return;
        }
        response.setStatus(HttpStatus.BAD_REQUEST.value());
    }

    @RequestMapping(value = "/removeProduct", method = RequestMethod.POST)
    public @ResponseBody
    void RemoveProduct(@Valid @RequestBody CardViewModelRemove product, HttpServletResponse response) throws Exception
    {
        setUp();
        Product remDocument = mongo.getDataById(product.getProductId());

        if(remDocument.getTransactionId().isEmpty() && remDocument.getSessionId().isEmpty() && remDocument.getUserId().isEmpty()){
            remDocument.setTransactionId(product.getTransactionId());
            remDocument.setSessionId(product.getSessionId());
            remDocument.setUserId(product.getUserId());
            remDocument.setTransactionAction(TransactionAction.Removing);
            mongo.updateAllProductAttributes(remDocument);
        }
    }

    @RequestMapping(value = "/getProductListByIds", method = RequestMethod.GET)
    public @ResponseBody
    ArrayList<ProductReturnModel> GetProductListByIds(@RequestParam("productIds") String[] productIds) throws Exception{
        setUp();
        ArrayList<ProductReturnModel> resultProducts = new ArrayList<>();
        for (String id : productIds){
            Product product = mongo.getDataById(id);
            resultProducts.add(new ProductReturnModel(product.getId(),
                    product.getDesignation(),
                    product.getName(),
                    product.getRevision(),
                    product.getState(),
                    product.getModelId(),
                    product.getViewModelId(),
                    product.getWeight()));
        }
        return resultProducts;
    }

    @RequestMapping(value = "/CommitTransaction", method = RequestMethod.PUT)
    public void CommitTransaction(@RequestBody @Valid TransactionDetailsViewModel tmodel, HttpServletResponse response) throws Exception{
        setUp();
        ArrayList<Product> tDocs =  mongo.getTObjects(tmodel);

        if (tDocs.size() == 0) {
            response.setStatus(404);
            return;
        }

        ArrayList<Product> docsToUpdate = new ArrayList<>();
        ArrayList<Product> docsToRemove = new ArrayList<>();
        for (Product d : tDocs) {
            if (d.getTransactionAction() == TransactionAction.Adding ) {
                d.ClearCommitData();
                docsToUpdate.add(d);
            }
            if(d.getTransactionAction() == TransactionAction.Modifying && !d.getIsOriginal()){
                docsToRemove.add(d);
            }
            if(d.getTransactionAction() == TransactionAction.Modifying && d.getIsOriginal()){
                d.ClearCommitData();
                docsToUpdate.add(d);
            }
            if(d.getTransactionAction() == TransactionAction.Removing)
            {
                docsToRemove.add(d);
            }
        }

        for (Product d : docsToUpdate) {
            mongo.updateAllProductAttributes(d);
        }

        for (Product d : docsToRemove){
            mongo.removeProduct(d.getId());
        }

        response.setStatus(200);
    }

    @RequestMapping(value = "/CancelTransaction", method = RequestMethod.PUT)
    public void CancelTransaction(@RequestBody @Valid TransactionDetailsViewModel tmodel, HttpServletResponse response) throws Exception {
        setUp();
        ArrayList<Product> tDocs =  mongo.getTObjects(tmodel),
                fakeDocs = new ArrayList<>(),
                docsToCancel = new ArrayList<>();

        if (tDocs.size() == 0) {
            response.setStatus(404);
            return;
        }

        for(Product d : tDocs){
            switch (d.getTransactionAction()){
                case Adding:
                    mongo.removeProduct(d.getId());
                    break;
                case Removing:
                    d.ClearCommitData();
                    docsToCancel.add(d);
                    break;
                case Modifying:
                    if (!d.getIsOriginal()) {
                        fakeDocs.add(d);
                    }
                    break;
            }
        }
        for(Product d : fakeDocs) {
            Product docToCancel = new Product(
                    d.getId(),
                    d.getDesignation(),
                    d.getName(),
                    d.getRevision(),
                    d.getState(),
                    d.getModelId(),
                    d.getViewModelId(),
                    d.getWeight(),
                    d.getTransactionId(),
                    d.getSessionId(),
                    d.getUserId(),
                    d.getTransactionAction(),
                    d.getOriginalId(),
                    true
            );
            docToCancel.ClearCommitData();
            docsToCancel.add(docToCancel);
        }
        for (Product d : docsToCancel) {
            mongo.updateAllProductAttributes(d);
        }

        for (Product d : fakeDocs){
            mongo.removeProduct(d.getId());
        }
    }

    @RequestMapping(value = "/UploadProducts", method = RequestMethod.POST)
    public void uploadProducts() throws Exception {
        setUp();
        CSVReader reader = new CSVReader(new FileReader("C:/Users/Nkravchenko/Desktop/items2.csv"), ';', '"', 0);
        String[] nextLine;
        while((nextLine = reader.readNext()) != null) {
            if (nextLine != null) {
                if(!mongo.findDoc(nextLine[1])){
                    mongo.add(new Product(nextLine[0],
                            nextLine[1],
                            1,
                            "On Check",
                            "",
                            "",
                            1.2,
                            "112233",
                            "112233",
                            "112233",
                            TransactionAction.Adding
                            ));
                }
            }
        }
    }
}
