package com.diploma.product_cards_storage.TransactionsViewModels;

import javax.validation.constraints.NotNull;

public class TransactionDetailsViewModel {
    @NotNull
    protected String transactionId;
    @NotNull
    protected String userId;
    @NotNull
    protected String sessionId;


    public String getTransactionId() {
        return transactionId;
    }

    public void setTransactionId(String transactionId) {
        this.transactionId = transactionId;
    }

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }


}
