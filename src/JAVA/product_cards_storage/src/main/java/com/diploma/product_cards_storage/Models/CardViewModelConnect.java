package com.diploma.product_cards_storage.Models;

import javax.validation.constraints.NotNull;

public class CardViewModelConnect {
    @NotNull
    private String objectId;
    @NotNull
    private String modelId;

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }

    public String getModelId() {
        return modelId;
    }

    public void setModelId(String modelId) {
        this.modelId = modelId;
    }
}
