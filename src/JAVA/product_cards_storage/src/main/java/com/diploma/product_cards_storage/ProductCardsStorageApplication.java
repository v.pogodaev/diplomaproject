package com.diploma.product_cards_storage;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductCardsStorageApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductCardsStorageApplication.class, args);
    }

}
