package com.diploma.product_cards_storage.Models;

import javax.validation.constraints.NotNull;

public class TransactionInfo {
    @NotNull
    private String TransactionId;
    @NotNull
    private String SessionId;
    @NotNull
    private String UserId;

    public String getTransactionId() {
        return TransactionId;
    }

    public void setTransactionId(String transactionId) {
        TransactionId = transactionId;
    }

    public String getSessionId() {
        return SessionId;
    }

    public void setSessionId(String sessionId) {
        SessionId = sessionId;
    }

    public String getUserId() {
        return UserId;
    }

    public void setUserId(String userId) {
        UserId = userId;
    }
}
