package com.diploma.product_cards_storage.DB;

import com.diploma.product_cards_storage.Models.Product;
import com.diploma.product_cards_storage.Transactions.TransactionAction;
import com.diploma.product_cards_storage.TransactionsViewModels.TransactionDetailsViewModel;
import com.mongodb.*;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import org.bson.Document;
import org.bson.types.ObjectId;

import java.util.ArrayList;
import java.util.Properties;

public class WorkWithMongo {
    // это клиент который обеспечит подключение к БД
    private MongoClient mongoClient;

    // В нашем случае, этот класс дает
// возможность аутентифицироваться в MongoDB
    private MongoDatabase db;

    // тут мы будем хранить состояние подключения к БД
    private boolean authenticate;

    // И класс который обеспечит возможность работать
// с коллекциями / таблицами MongoDB
    private MongoCollection<Document> table;

    public WorkWithMongo(Properties prop) {
        // Создаем подключение
        System.out.println("trying to connect to " + prop.getProperty("connectionString"));
        char[] pass = prop.getProperty("password").toCharArray();
        MongoCredential credential = MongoCredential.createCredential(prop.getProperty("login"),"admin", pass);
        int connectTimeout = 1000 * 60;
        MongoClientOptions options = new MongoClientOptions.Builder().connectTimeout(connectTimeout).build();
        mongoClient = new MongoClient(new ServerAddress(prop.getProperty("connectionString")), credential, options);

        // Выбираем БД для дальнейшей работы
        db = mongoClient.getDatabase(prop.getProperty("dbname"));

        // Выбираем коллекцию/таблицу для дальнейшей работы
        table = db.getCollection(prop.getProperty("table"));
    }

    public void add(Product product){
        Document document = new Document();
        // это поле будет записываться в коллекцию/таблицу
        document.append("designation", product.getDesignation())
                .append("name", product.getName())
                .append("revision", product.getRevision())
                .append("state", product.getState())
                .append("modelId", product.getModelId())
                .append("viewModelId", product.getViewModelId())
                .append("weight", product.getWeight())
                .append("transactionId", product.getTransactionId())
                .append("sessionId", product.getSessionId())
                .append("userId", product.getUserId())
                .append("transactionAction", product.getTransactionAction().toString())
                .append("isOriginal", product.getIsOriginal());
        // записываем данные в коллекцию/таблицу
        table.insertOne(document);
    }

    public boolean findDoc(String designation) {
        Document query = new Document();

        query.put("designation", designation);

        Document result = table.find(query).first();
        if (result != null) {
            return true;
        } else {
            return false;
        }
    }

    public Document findObjectById(String id){
        Document query = new Document();

        query.put("_id", new ObjectId(id));
        return table.find(query).first();

    }

    public String getObjectId(String designation){
        Document query = new Document();

        query.put("designation", designation);

        Document result = table.find(query).first();
        return result.get("_id").toString();
    }


    public void update(String id, String designation, String name, Integer weight){
        Document newDocument = new Document();
        Document searchDocument = findObjectById(id);
        if(designation == null){designation = searchDocument.get("designation").toString();}
        if(name == null){name = searchDocument.get("name").toString();}
        if(weight == null){weight = (Integer)searchDocument.get("weight");}


        newDocument.append("$set", new Document().append("designation",designation ).append("name", name).append("weight", weight));

        table.updateOne(searchDocument, newDocument);
    }

    public void update(String objectId, String modelId){
        Document newDocument = new Document();
        Document searchDocument = findObjectById(objectId);

        newDocument.append("$set", new Document().append("modelId", modelId));

        table.updateOne(searchDocument, newDocument);
    }

    public void disconnect(String objectId){
        Document newDocument = new Document();
        Document searchDocument = findObjectById(objectId);

        newDocument.append("$set", new Document().append("modelId", ""));

        table.updateOne(searchDocument, newDocument);
    }

    public boolean updateState(String objectId, String state){
        Document newDocument = new Document();
        Document searchDocument = findObjectById(objectId);

        if(checkState(objectId, state)){
            newDocument.append("$set", new Document().append("state", state));
            table.updateOne(searchDocument, newDocument);
            return true;
        }else
        {
            return false;
        }

    }

    public boolean checkState(String objectId, String state){
        Document checkDocument = findObjectById(objectId);
        boolean check = false;
            switch (checkDocument.get("state").toString()) {
                case "On Check":
                    if (state.equals("In Work") || state.equals("Approved")) {
                        check = true;
                    }
                    break;
                case "In Work":
                    if (state.equals("On Check")) {
                        check = true;
                    }
                    break;
                case "Approved":
                    if (state.equals("Outdated")) {
                        check = true;
                    }
                    break;
                default: check = false;
                break;
            }
        return check;

    }

    public Product getDataById(String objectId){
        Document object = findObjectById(objectId);
        ;
        Product resultObject = new Product(object.get("_id") != null ? object.get("_id").toString() : "",
                object.get("designation") != null ? object.get("designation").toString() : "",
                object.get("name") != null ? object.get("name").toString() : "",
                object.get("revision") != null ? Integer.parseInt(object.get("revision").toString()) : 0,
                object.get("state") != null ? object.get("state").toString() : "",
                object.get("modelId") != null ? object.get("modelId").toString() : "",
                object.get("viewModelId") != null ? object.get("viewModelId").toString() : "",
                object.get("weight") != null ? Double.parseDouble(object.get("weight").toString()) : 0,
                object.get("transactionId") != null ? object.get("transactionId").toString() : "",
                object.get("sessionId") != null ? object.get("sessionId").toString() : "",
                object.get("userId") != null ? object.get("userId").toString() : "",
                object.get("transactionAction") != null ? ToAction(object.get("transactionAction").toString()) : TransactionAction.Empty,
                object.get("originalId") != null ? object.get("originalId").toString() : "",
                object.get("isOriginal") == null || (boolean) object.get("isOriginal"));
        return resultObject;
    }


    public ArrayList<Product> getAllProducts(){
        ArrayList<Product> result = new ArrayList<>();
        ArrayList<Document> queryDB = new ArrayList<Document>();
        table.find().into(queryDB);
        for(Document doc : queryDB) {
            result.add(new Product(
                    doc.get("_id") != null ? doc.get("_id").toString() : "",
                    doc.get("designation") != null ? doc.get("designation").toString() : "",
                    doc.get("name") != null ? doc.get("name").toString() : "",
                    doc.get("revision") != null ? Integer.parseInt(doc.get("revision").toString()) : 0,
                    doc.get("state") != null ? doc.get("state").toString() : "",
                    doc.get("modelId") != null ? doc.get("modelId").toString() : "",
                    doc.get("viewModelId") != null ? doc.get("viewModelId").toString() : "",
                    doc.get("weight") != null ? Double.parseDouble(doc.get("weight").toString()) : 0,
                    doc.get("transactionId") != null ? doc.get("transactionId").toString() : "",
                    doc.get("sessionId") != null ? doc.get("sessionId").toString() : "",
                    doc.get("userId") != null ? doc.get("userId").toString() : "",
                    doc.get("transactionAction") != null ? ToAction(doc.get("transactionAction").toString()) : TransactionAction.Empty,
                    doc.get("originalId") != null ? doc.get("originalId").toString() : "",
                    doc.get("isOriginal") == null || (boolean) doc.get("isOriginal")));
        }
        return result;
    }

    public ArrayList<Product> getTObjects(TransactionDetailsViewModel tmodel) {
        ArrayList<Product> products = getAllProducts();
        ArrayList<Product> tDocs = new ArrayList<>();

        for (Product doc : products) {
            if (doc.getTransactionId() != null &&
                    doc.getTransactionId().toLowerCase().equals(tmodel.getTransactionId().toLowerCase()) &&
                    doc.getUserId().toLowerCase().equals(tmodel.getUserId().toLowerCase()) &&
                    doc.getSessionId().toLowerCase().equals(tmodel.getSessionId().toLowerCase())) {
                tDocs.add(doc);
            }
        }
        return tDocs;
    }

    public void removeProduct(String id){
        table.deleteOne(findObjectById(id));
    }

    private TransactionAction ToAction(String action)
    {
        TransactionAction resAction;
        switch (action){
            case "Adding":
                resAction = TransactionAction.Adding;
                break;
            case "Modifying" :
                resAction = TransactionAction.Modifying;
                break;
            case "Removing":
                resAction = TransactionAction.Removing;
                break;
                default:
                    resAction = TransactionAction.Empty;
                    break;
        }
        return resAction;
    }
    public void updateAllProductAttributes(Product doc)
    {
        Document newDocument = new Document();
        newDocument.append("$set",
                new Document()
                        .append("isOriginal", doc.getIsOriginal())
                        .append("name", doc.getName() == null ? "" : doc.getName())
                        .append("state", doc.getState() == null ? "" : doc.getState())
                        .append("userId", doc.getUserId() == null ? "" : doc.getUserId())
                        .append("revision", doc.getRevision() == null ? "" : doc.getRevision())
                        .append("sessionId", doc.getSessionId() == null ? "" : doc.getSessionId())
                        .append("designation", doc.getDesignation() == null ? "" : doc.getDesignation())
                        .append("transactionId", doc.getTransactionId() == null ? "" : doc.getTransactionId())
                        .append("transactionAction", doc.getTransactionAction() == null ? "" : doc.getTransactionAction().toString()));
        Document searchQuery = new Document().append("_id", new ObjectId(doc.getId()));
        table.updateOne(searchQuery, newDocument);
    }
}
