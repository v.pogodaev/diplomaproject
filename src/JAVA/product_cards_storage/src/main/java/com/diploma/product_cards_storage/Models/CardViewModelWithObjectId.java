package com.diploma.product_cards_storage.Models;

import javax.validation.constraints.NotNull;

public class CardViewModelWithObjectId {
    @NotNull
    private String objectId;

    public String getObjectId() {
        return objectId;
    }

    public void setObjectId(String objectId) {
        this.objectId = objectId;
    }
}
