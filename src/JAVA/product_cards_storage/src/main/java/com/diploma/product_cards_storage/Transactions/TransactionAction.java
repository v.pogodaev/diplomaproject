package com.diploma.product_cards_storage.Transactions;


public enum TransactionAction {
    Adding,

    Modifying,

    Removing,

    Empty

}
