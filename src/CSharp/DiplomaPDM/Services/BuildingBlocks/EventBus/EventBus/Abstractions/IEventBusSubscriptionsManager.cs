﻿using System;
using System.Collections.Generic;
using System.Text;

namespace EventBus.Abstractions
{
  public interface IEventBusSubscriptionsManager
  {
    bool IsEmpty { get; }

    event EventHandler<string> OnEventRemoved;


    
  }
}
