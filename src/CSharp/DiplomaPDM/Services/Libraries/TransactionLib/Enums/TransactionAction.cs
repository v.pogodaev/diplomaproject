﻿using System;
using System.Collections.Generic;
using System.Text;

namespace TransactionLib.Enums
{
  /// <summary>
  /// Действие над записью
  /// </summary>
  public enum TransactionAction
  {
    /// <summary>
    /// Добавление
    /// </summary>
    Adding = 0,
    /// <summary>
    /// Изменение
    /// </summary>
    Modifying,
    /// <summary>
    /// Удаление
    /// </summary>
    Removing
  }
}
