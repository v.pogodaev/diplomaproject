﻿using System;
using System.ComponentModel.DataAnnotations;
using TransactionLib.Enums;
using TransactionLib.Models.Abstract;

namespace TransactionLib.ViewModels.Abstract
{
  /// <summary>
  /// Данные для транзакции
  /// </summary>
  public abstract class TransactableCreatingViewModel
  {
    /// <summary>
    /// Id транзакции. Если транзакция завершена, то null
    /// </summary>
    [Required]
    public Guid TransactionId { get; set; }

    /// <summary>
    /// Id пользователя
    /// </summary>
    [Required]
    public string UserId { get; set; }

    /// <summary>
    /// Id сессии
    /// </summary>
    [Required]
    public string SessionId { get; set; }

    /// <summary>
    /// Передача транзакционных данных модели
    /// </summary>
    /// <param name="model">Модель, которой передаются данные</param>
    /// <param name="action">Действие транзакции</param>
    public void PassTransactionData(TransactableModel model, TransactionAction action)
    {
      model.TransactionAction = action;
      model.UserId = UserId;
      model.TransactionId = TransactionId;
      model.SessionId = SessionId;
    }
  }
}
