﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TransactionLib.Enums;
using TransactionLib.Models.Abstract;

namespace TransactionLib
{
  public static class TransactionsSort
  {
    /// <summary>
    /// Сортировка сущностей для коммита транзакции, по необходимым действиям над ними
    /// </summary>
    /// <param name="entities">Сущности этой транзакции</param>
    /// <returns>Если Item1 = false, то произошла ошибка. Item2 - сущности для обновления. Item3 - сущности для удаления</returns>
    public static Tuple<bool, List<TransactableModel>, List<TransactableModel>> SortCommitEntitiesByActionsAsync(
      IReadOnlyCollection<TransactableModel> entities)
    {
      var entitiesToUpdate = new List<TransactableModel>();
      var entitiesToRemove = new List<TransactableModel>();
      foreach (var entity in entities)
      {
        switch (entity.TransactionAction)
        {
          case TransactionAction.Adding:
            entity.ClearCommitData();
            entity.IsOriginal = true;
            entitiesToUpdate.Add(entity);
            break;

          case TransactionAction.Modifying when !entity.IsOriginal:
            continue;
          case TransactionAction.Modifying:
            {
              var tFolder = entities.Where(f => f.TransactionAction == TransactionAction.Modifying)
                .SingleOrDefault(f => f.OriginalId == entity.Id.ToString());
              if (tFolder == null)
              {
                // todo: logger
                return new Tuple<bool, List<TransactableModel>, List<TransactableModel>>(false, null, null);
              }

              entity.GetCommitData(tFolder);
              entity.ClearCommitData();
              entitiesToRemove.Add(tFolder);
              entitiesToUpdate.Add(entity);
              break;
            }

          case TransactionAction.Removing when !entity.IsOriginal:
            entitiesToRemove.Add(entity);
            break;
          case TransactionAction.Removing:
            {
              var tFolder = entities.Where(f => f.TransactionAction == TransactionAction.Removing)
                .SingleOrDefault(f => f.OriginalId == entity.Id.ToString());
              if (tFolder != null)
              {
                entitiesToRemove.Add(tFolder);
              }

              entitiesToRemove.Add(entity);
              break;
            }

          default:
            // todo: logger
            return new Tuple<bool, List<TransactableModel>, List<TransactableModel>>(false, null, null);
        }
      }

      return new Tuple<bool, List<TransactableModel>, List<TransactableModel>>(true, entitiesToUpdate, entitiesToRemove);
    }

    /// <summary>
    /// Сортировка сущностей для отмены транзакции, по необходимым действиям над ними
    /// </summary>
    /// <param name="entities">Сущности этой транзакции</param>
    /// <returns>Если Item1 = false, то произошла ошибка. Item2 - сущности для обновления. Item3 - сущности для удаления</returns>
    public static Tuple<bool, List<TransactableModel>, List<TransactableModel>> GetEntitiesForCancelTransactionAsync(
      IReadOnlyCollection<TransactableModel> entities)
    {
      var entitiesToUpdate = new List<TransactableModel>();
      var entitiesToRemove = new List<TransactableModel>();
      foreach (var entity in entities)
      {
        switch (entity.TransactionAction)
        {
          case TransactionAction.Adding:
            entitiesToRemove.Add(entity);
            break;

          case TransactionAction.Modifying when !entity.IsOriginal:
            continue;
          case TransactionAction.Modifying:
            {
              var tFolder = entities.Where(f => f.TransactionAction == TransactionAction.Modifying)
                .SingleOrDefault(f => f.OriginalId == entity.Id.ToString());
              if (tFolder == null)
              {
                // todo: logger
                return new Tuple<bool, List<TransactableModel>, List<TransactableModel>>(false, null, null);
              }

              entity.ClearCommitData();
              entitiesToRemove.Add(tFolder);
              entitiesToUpdate.Add(entity);
              break;
            }

          case TransactionAction.Removing when !entity.IsOriginal:
            continue;
          case TransactionAction.Removing:
            {
              entity.ClearCommitData();
              entitiesToUpdate.Add(entity);
              break;
            }

          default:
            // todo: logger
            return new Tuple<bool, List<TransactableModel>, List<TransactableModel>>(false, null, null);
        }
      }

      return new Tuple<bool, List<TransactableModel>, List<TransactableModel>>(true, entitiesToUpdate, entitiesToRemove);
    }
  }
}
