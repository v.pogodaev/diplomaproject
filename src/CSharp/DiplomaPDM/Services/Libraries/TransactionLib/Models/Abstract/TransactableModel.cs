﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using TransactionLib.Enums;

namespace TransactionLib.Models.Abstract
{
  /// <summary>
  /// Модель, пригодная для транзакций
  /// </summary>
  public abstract class TransactableModel
  {
    /// <summary>
    /// Id объекта
    /// </summary>
    [Key]
    public Guid Id { get; set; }

    /// <summary>
    /// Id транзакции. Если транзакция завершена, то null
    /// </summary>
    public Guid? TransactionId { get; set; }

    /// <summary>
    /// Текущее действие над транзакцией. Если транзакция завершена, то null
    /// </summary>
    public TransactionAction? TransactionAction { get; set; }

    /// <summary>
    /// Id пользователя
    /// </summary>
    public string UserId { get; set; }

    /// <summary>
    /// Id сессии
    /// </summary>
    public string SessionId { get; set; }

    /// <summary>
    /// Если это оригинальный объект, то значение true, если объект транзакции, то false
    /// </summary>
    [DefaultValue(true)]
    public bool IsOriginal { get; set; }

    /// <summary>
    /// Если это объект транзакции, то хранится id оригинального объекта, иначе поле пустое
    /// </summary>
    public string OriginalId { get; set; }

    /// <summary>
    /// Получение данных из транзакционного объекта
    /// </summary>
    /// <param name="model">Объект, хранящай новые данные во время транзакции</param>
    public abstract void GetCommitData(TransactableModel model);

    /// <summary>
    /// Очищение транзакционных полей
    /// </summary>
    public void ClearCommitData()
    {
      TransactionId = null;
      TransactionAction = null;
      SessionId = null;
      OriginalId = null;
      UserId = null;
    }
  }
}
