﻿
// ReSharper disable InconsistentNaming

namespace CATIAtoPDM
{
  public static class UrlsConfig
  {
    public static class PDMApi
    {
      #region CADIntegration

      private const string CONTROLLER_CAD_PATH = "api/v1/cad";

      private const string GET_PROJECTS = "getProjects";

      private const string GET_FOLDERS_BY_PROJECT = "getFoldersByProject";
      private const string GET_FOLDERS_BY_FOLDER = "getFoldersByFolder";
      private const string GET_FOLDER_ITEMS_BY_TYPE = "getFolderItemsByType";

      private const string GET_PRODUCT_BY_ID = "getProductById";
      private const string GET_PRODUCTS_BY_FOLDER = "getProductInfos";
      private const string POST_PRODUCT = "postProduct";

      private const string GET_MODEL = "getCADModel";
      private const string GET_MODEL_NAME = "getCADModelName";

      public static string GetFoldersByProject(string projectId) => $"{PDM_URL}/{CONTROLLER_CAD_PATH}/{GET_FOLDERS_BY_PROJECT}/{projectId}";
      public static string GetFoldersByFolder(string folderId) => $"{PDM_URL}/{CONTROLLER_CAD_PATH}/{GET_FOLDERS_BY_FOLDER}/{folderId}";
      public static string GetFolderItemsByType(string folderId, string type) => $"{PDM_URL}/{CONTROLLER_CAD_PATH}/{GET_FOLDER_ITEMS_BY_TYPE}?folderId={folderId}&type={type}";

      public static string GetProjects() => $"{PDM_URL}/{CONTROLLER_CAD_PATH}/{GET_PROJECTS}";

      public static string GetProductById(string productId) => $"{PDM_URL}/{CONTROLLER_CAD_PATH}/{GET_PRODUCT_BY_ID}/{productId}";
      public static string GetProductsByFolder(string folderId) => $"{PDM_URL}/{CONTROLLER_CAD_PATH}/{GET_PRODUCTS_BY_FOLDER}/{folderId}";
      public static string PostProduct() => $"{PDM_URL}/{CONTROLLER_CAD_PATH}/{POST_PRODUCT}";

      public static string GetModelById(string fileId) => $"{PDM_URL}/{CONTROLLER_CAD_PATH}/{GET_MODEL}/{fileId}";
      public static string GetModelNameById(string fileId) => $"{PDM_URL}/{CONTROLLER_CAD_PATH}/{GET_MODEL_NAME}/{fileId}";
      #endregion

      #region Authorize

      public static string PostLogin() => $"{PDM_URL}/api/Account/LoginFromService";
      #endregion

    }

    public static string PDM_URL { get; set; } = "http://localhost:55416";
  }
}
