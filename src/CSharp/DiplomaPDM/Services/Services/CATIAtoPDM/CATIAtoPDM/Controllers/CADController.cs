﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using CATIAtoPDM.Models;
using CATIAtoPDM.Models.Folder;
using CATIAtoPDM.Models.Product;
using CATIAtoPDM.Models.Project;
using CATIAtoPDM.Models.User;
using CATIAtoPDM.Services.Contract;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace CATIAtoPDM.Controllers
{
  [Route("api/v1/[controller]")]
  [ApiController]
  public class CADController : ControllerBase
  {
    private readonly IPDMService m_PDMService;
    private readonly ILogger m_Logger;

    public CADController(IPDMService pdmService, ILogger<CADController> logger)
    {
      m_PDMService = pdmService;
      m_Logger = logger;
    }


    [HttpGet]
    [Route("getProjects")]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType(typeof(ICollection<ProjectViewModel>), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<ICollection<ProjectViewModel>>> GetProjectsAsync()
    {
      try
      {
        var projects = await m_PDMService.GetProjectsAsync();
        if (projects != null)
        {
          return Ok(projects);
        }

        m_Logger.LogWarning(LoggingEvents.GET_ITEM_NOT_FOUND, $"{nameof(GetProjectsAsync)} NOT FOUND");
        return NotFound();
      }
      catch (Exception e)
      {
        m_Logger.LogError(LoggingEvents.ERROR, $"{nameof(GetProjectsAsync)} FAIL");
        return BadRequest(e.Message);
      }
    }

    [HttpGet]
    [Route("getFoldersByProject/{projectId}")]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType(typeof(ICollection<FolderViewModel>), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<ICollection<FolderViewModel>>> GetFoldersByProjectAsync(string projectId)
    {
      try
      {
        var folders = await m_PDMService.GetFoldersByProjectIdAsync(projectId);
        if (folders != null)
        {
          return Ok(folders);
        }

        m_Logger.LogWarning(LoggingEvents.GET_ITEM_NOT_FOUND, $"{nameof(GetFoldersByProjectAsync)} NOT FOUND");
        return NotFound();
      }
      catch (Exception e)
      {
        m_Logger.LogError(LoggingEvents.ERROR, $"{nameof(GetFoldersByProjectAsync)} FAIL");
        return BadRequest(e.Message);
      }
    }

    [HttpGet]
    [Route("getFoldersByFolder/{folderId}")]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType(typeof(ICollection<FolderViewModel>), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<ICollection<FolderViewModel>>> GetFoldersByFolderAsync(string folderId)
    {
      try
      {
        var folders = await m_PDMService.GetFoldersByFolderIdAsync(folderId);
        if (folders != null)
        {
          return Ok(folders);
        }

        m_Logger.LogWarning(LoggingEvents.GET_ITEM_NOT_FOUND, $"{nameof(GetFoldersByFolderAsync)} NOT FOUND");
        return NotFound();
      }
      catch (Exception e)
      {
        m_Logger.LogError(LoggingEvents.ERROR, $"{nameof(GetFoldersByFolderAsync)} FAIL");
        return BadRequest(e.Message);
      }
    }

    //[HttpGet]
    //[Route("getFolderItemsByType")]
    //[ProducesResponseType((int)HttpStatusCode.BadRequest)]
    //[ProducesResponseType(typeof(ICollection<FolderItem>), (int)HttpStatusCode.OK)]
    //public async Task<ActionResult<ICollection<FolderItem>>> GetFolderItemsByTypeAsync(string folderId, string type)
    //{
    //  try
    //  {
    //    var items = await m_PDMService.GetFolderItemsByTypeAsync(folderId, type);
    //    if (items != null)
    //    {
    //      return Ok(items);
    //    }

    //    m_Logger.LogWarning(LoggingEvents.GET_ITEM_NOT_FOUND, $"{nameof(GetFolderItemsByTypeAsync)} NOT FOUND");
    //    return NotFound();
    //  }
    //  catch (Exception e)
    //  {
    //    m_Logger.LogError(LoggingEvents.ERROR, $"{nameof(GetFolderItemsByTypeAsync)} FAIL");
    //    return BadRequest(e.Message);
    //  }
    //}

    [HttpGet]
    [Route("getProductById/{productId}")]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType(typeof(ICollection<ProductFromPdmViewModel>), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<ProductFromPdmViewModel>> GetProductByIdAsync(string productId)
    {
      return NotFound();

      //try
      //{
      //  var product = await m_PDMService.TakeProductInWorkByIdAsync(productId);
      //  if (product != null)
      //  {
      //    return Ok(product);
      //  }

      //  m_Logger.LogWarning(LoggingEvents.GET_ITEM_NOT_FOUND, $"{nameof(GetProductByIdAsync)} NOT FOUND");
      //  return NotFound();
      //}
      //catch (Exception e)
      //{
      //  m_Logger.LogError(LoggingEvents.ERROR, $"{nameof(GetProductByIdAsync)} FAIL");
      //  return BadRequest(e.Message);
      //}
    }

    [HttpGet]
    [Route("getProductInfos/{folderId}")]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType(typeof(ICollection<ProductFromPdmViewModel>), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<ICollection<ProductFromPdmViewModel>>> GetProductInfosByFolderAsync(string folderId)
    {
      try
      {
        var infos = await m_PDMService.GetProductInfosByFolderAsync(folderId);
        if (infos != null)
        {
          return Ok(infos);
        }

        m_Logger.LogWarning(LoggingEvents.GET_ITEM_NOT_FOUND, $"{nameof(GetProductInfosByFolderAsync)} NOT FOUND");
        return NotFound();
      }
      catch (Exception e)
      {
        m_Logger.LogError(LoggingEvents.ERROR, $"{nameof(GetProductInfosByFolderAsync)} FAIL");
        return BadRequest(e.Message);
      }
    }

    [HttpPost]
    [Route("postProduct")]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.OK)]
    public async Task<IActionResult> PostProductAsync([FromForm] ProductToPdmViewModel product)
    {
      try
      {
        await m_PDMService.PutInCheckProductAsync(product);
        return Ok();
      }
      catch (Exception e)
      {
        m_Logger.LogError(LoggingEvents.ERROR, $"{nameof(PostProductAsync)} FAIL");
        return BadRequest(e.Message);
      }
    }

    [HttpGet]
    [Route("getCadModel/{fileId}")]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(File), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> GetCADModelAsync(string fileId)
    {
      try
      {
        var file = await m_PDMService.GetModelById(fileId);
        if (file != null)
        {
          return File(file.FileStream, file.MimeType, file.FileName);
        }
        m_Logger.LogWarning(LoggingEvents.GET_ITEM_NOT_FOUND, $"{nameof(GetCADModelAsync)} NOT FOUND");
        return NotFound();
      }
      catch (Exception e)
      {
        m_Logger.LogError(LoggingEvents.ERROR, $"{nameof(GetCADModelAsync)} FAIL");
        return BadRequest(e.Message);
      }
    }

    [HttpGet]
    [Route("getCadModelName/{fileId}")]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.OK)]
    public async Task<IActionResult> GetCADModelNameAsync(string fileId)
    {
      var name = await m_PDMService.GetModelNameById(fileId);
      return Ok(name);
    }

    [HttpPost]
    [Route("login")]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(UserData), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<UserData>> LoginAsync([FromForm] LoginData loginData)
    {
      try
      {
        loginData.ServiceName = "CADService";
        var data = await m_PDMService.PostLogin(loginData);
        if (data != null)
        {
          return data;
        }
        m_Logger.LogWarning(LoggingEvents.GET_ITEM_NOT_FOUND, $"{nameof(LoginAsync)} NOT FOUND");
        return NotFound();
      }
      catch (Exception e)
      {
        m_Logger.LogError(LoggingEvents.ERROR, $"{nameof(LoginAsync)} FAIL");
        return BadRequest(e.Message);
      }
    }
  }
}