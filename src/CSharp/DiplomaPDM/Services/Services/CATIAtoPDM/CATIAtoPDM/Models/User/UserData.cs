﻿using System;
using System.ComponentModel.DataAnnotations;

namespace CATIAtoPDM.Models.User
{
  public class UserData
  {
    [Required]
    public Guid UserId { get; set; }

    [Required]
    public Guid SessionId { get; set; }
  }
}
