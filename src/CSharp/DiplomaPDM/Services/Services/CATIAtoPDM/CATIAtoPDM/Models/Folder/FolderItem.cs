﻿namespace CATIAtoPDM.Models.Folder
{
  public class FolderItem
  {
    public string ItemRealId { get; set; }
    public string ItemType { get; set; }
  }
}
