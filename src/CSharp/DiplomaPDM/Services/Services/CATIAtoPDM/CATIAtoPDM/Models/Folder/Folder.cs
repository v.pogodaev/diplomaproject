﻿using System;
using System.Collections.Generic;

namespace CATIAtoPDM.Models.Folder
{
  public class Folder
  {
    public Guid Id { get; set; }
    public string Sign { get; set; }
    public string Name { get; set; }
    public Guid? SuperiorFolderId { get; set; }
    public string ProjectId { get; set; }
    public ICollection<Guid> SubFolderIds { get; set; }
    public ICollection<FolderItem> Items { get; set; }
  }
}
