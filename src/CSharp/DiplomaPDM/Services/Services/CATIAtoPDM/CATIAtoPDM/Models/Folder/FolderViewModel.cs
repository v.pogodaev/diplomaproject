﻿using System;

namespace CATIAtoPDM.Models.Folder
{
  public class FolderViewModel
  {
    public Guid Id { get; set; }
    public string Sign { get; set; }
    public string Name { get; set; }

    //public ICollection<Product> Products { get; set; }

    public FolderViewModel()
    {
    }

    public FolderViewModel(Folder folder/*, ICollection<Product> products*/)
    {
      Id = folder.Id;
      Sign = folder.Sign;
      Name = folder.Name;
      //Products = products;
    }
  }
}
