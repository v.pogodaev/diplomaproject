﻿using System;
using System.Collections.Generic;

namespace CATIAtoPDM.Models.Project
{
  public class Project
  {
    public Guid Id { get; set; }
    public string Sign { get; set; }
    public string Name { get; set; }
    public List<Folder.Folder> Folders { get; set; }
  }
}
