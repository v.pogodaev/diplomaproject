﻿using System;

namespace CATIAtoPDM.Models.Project
{
  public class ProjectViewModel
  {
    public Guid Id { get; set; }
    public string Sign { get; set; }
    public string Name { get; set; }
    public ProjectViewModel() { }

    public ProjectViewModel(Project project)
    {
      Id = project.Id;
      Sign = project.Sign;
      Name = project.Name;
    }
  }
}
