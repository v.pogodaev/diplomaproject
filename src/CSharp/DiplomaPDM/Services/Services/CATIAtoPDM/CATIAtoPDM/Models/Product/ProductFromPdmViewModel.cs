﻿namespace CATIAtoPDM.Models.Product
{
  public class ProductFromPdmViewModel
  {
    public string Id { get; set; }
    public string PartNumber { get; set; }
    public string Description { get; set; }
    public double WeightKg { get; set; }
    public string State { get; set; }
    public string CADModelFullName { get; set; }
    public string CADModelId { get; set; }
  }
}
