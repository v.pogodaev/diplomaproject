﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;

namespace CATIAtoPDM.Models.Product
{
  public class ProductToPdmViewModel
  {
    [Required]
    public string ProductId { get; set; }

    [Required]
    public string PartNumber { get; set; }

    public string Description { get; set; }

    public double WeightKg { get; set; }
    
    [Required]
    public IFormFile CADModel { get; set; }

    [Required]
    public IFormFile ViewerModel { get; set; }
  }
}
