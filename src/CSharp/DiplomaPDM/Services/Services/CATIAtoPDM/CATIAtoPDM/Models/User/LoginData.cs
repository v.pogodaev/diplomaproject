﻿using System.ComponentModel.DataAnnotations;

namespace CATIAtoPDM.Models.User
{
  public class LoginData
  {
    [Required]
    public string Login { get; set; }

    [Required]
    public string Password { get; set; }

    public string ServiceName { get; set; }
  }
}
