﻿using System.IO;

namespace CATIAtoPDM.Models.File
{
  public class FileViewModel
  {
    public MemoryStream FileStream { get; set; }
    public string MimeType { get; set; }
    public string FileName { get; set; }
  }
}
