﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using CATIAtoPDM.Models;
using CATIAtoPDM.Models.File;
using CATIAtoPDM.Models.Folder;
using CATIAtoPDM.Models.Product;
using CATIAtoPDM.Models.Project;
using CATIAtoPDM.Models.User;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CATIAtoPDM.Services.Contract
{
  public interface IPDMService
  {
    #region ProjectPart

    Task<ICollection<ProjectViewModel>> GetProjectsAsync();
    #endregion

    #region FolderPart

    Task<ICollection<FolderViewModel>> GetFoldersByProjectIdAsync(string projectId);
    Task<ICollection<FolderViewModel>> GetFoldersByFolderIdAsync(string folderId);
    //Task<ICollection<FolderItem>> GetFolderItemsByTypeAsync(string folderId, string type);
    #endregion

    #region ProductPart

    Task<ICollection<ProductFromPdmViewModel>> GetProductInfosByFolderAsync(string folderId);
    Task<ProductFromPdmViewModel> TakeProductInWorkByIdAsync(string productId, string userId, string sessionId);
    Task PutInCheckProductAsync(ProductToPdmViewModel product);
    #endregion

    #region FilePart

    Task<FileViewModel> GetModelById(string fileId);
    Task<string> GetModelNameById(string fileId);
    #endregion

    #region Login

    Task<UserData> PostLogin(LoginData login);

    #endregion
  }
}
