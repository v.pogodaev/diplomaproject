﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using CATIAtoPDM.Models;
using CATIAtoPDM.Models.File;
using CATIAtoPDM.Models.Folder;
using CATIAtoPDM.Models.Product;
using CATIAtoPDM.Models.Project;
using CATIAtoPDM.Models.User;
using CATIAtoPDM.Services.Contract;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Http.Internal;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CATIAtoPDM.Services
{
  public class PDMService : IPDMService
  {
    private readonly HttpClient m_HttpClient;

    public PDMService(HttpClient httpClient)
    {
      m_HttpClient = httpClient;
    }


    public async Task<ICollection<ProjectViewModel>> GetProjectsAsync()
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.PDMApi.GetProjects());
      if (result.IsSuccessStatusCode)
      {
        return JsonConvert.DeserializeObject<ICollection<ProjectViewModel>>(await result.Content.ReadAsStringAsync());
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return null;
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<ICollection<FolderViewModel>> GetFoldersByProjectIdAsync(string projectId)
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.PDMApi.GetFoldersByProject(projectId));

      if (result.IsSuccessStatusCode)
      {
        return JsonConvert.DeserializeObject<ICollection<FolderViewModel>>(await result.Content.ReadAsStringAsync());
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return null;
        case HttpStatusCode.BadRequest:
        default:
          // todo: логгер, либо выше его вызывать, либо везде, хз
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<ICollection<FolderViewModel>> GetFoldersByFolderIdAsync(string folderId)
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.PDMApi.GetFoldersByFolder(folderId));

      if (result.IsSuccessStatusCode)
      {
        return JsonConvert.DeserializeObject<ICollection<FolderViewModel>>(await result.Content.ReadAsStringAsync());
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return null;
        case HttpStatusCode.BadRequest:
        default:
          // todo: логгер, либо выше его вызывать, либо везде, хз
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<ICollection<FolderItem>> GetFolderItemsByTypeAsync(string folderId, string type)
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.PDMApi.GetFolderItemsByType(folderId, type));

      if (result.IsSuccessStatusCode)
      {
        return JsonConvert.DeserializeObject<ICollection<FolderItem>>(await result.Content.ReadAsStringAsync());
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return null;
        case HttpStatusCode.BadRequest:
        default:
          // todo: логгер, либо выше его вызывать, либо везде, хз
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<ProductFromPdmViewModel> TakeProductInWorkByIdAsync(string productId)
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.PDMApi.GetProductById(productId));

      if (result.IsSuccessStatusCode)
      {
        return JsonConvert.DeserializeObject<ProductFromPdmViewModel>(await result.Content.ReadAsStringAsync());
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return null;
        case HttpStatusCode.BadRequest:
        default:
          // todo: логгер, либо выше его вызывать, либо везде, хз
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<ICollection<ProductFromPdmViewModel>> GetProductInfosByFolderAsync(string folderId)
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.PDMApi.GetProductsByFolder(folderId));

      if (result.IsSuccessStatusCode)
      {
        return JsonConvert.DeserializeObject<ICollection<ProductFromPdmViewModel>>(await result.Content.ReadAsStringAsync());
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return null;
        case HttpStatusCode.BadRequest:
        default:
          // todo: логгер, либо выше его вызывать, либо везде, хз
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<ProductFromPdmViewModel> TakeProductInWorkByIdAsync(string productId, string userId, string sessionId)
    {
      var stringContent = new StringContent(JsonConvert.SerializeObject(new {userId, sessionId}), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PostAsync(UrlsConfig.PDMApi.GetProductById(productId), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return JsonConvert.DeserializeObject<ProductFromPdmViewModel>(await result.Content.ReadAsStringAsync());
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return null;
        case HttpStatusCode.BadRequest:
        default:
          // todo: логгер, либо выше его вызывать, либо везде, хз
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task PutInCheckProductAsync(ProductToPdmViewModel product)
    {
      var multiContent = new MultipartFormDataContent
      {
        {
          new StringContent(product.ProductId, Encoding.UTF8),
          nameof(product.ProductId)
        },
        {
          new StringContent(product.Description, Encoding.UTF8),
          nameof(product.Description)
        },
        {
          new StringContent(product.PartNumber, Encoding.UTF8),
          nameof(product.PartNumber)
        },
        {
          new StringContent(product.WeightKg
            .ToString("G", CultureInfo.CreateSpecificCulture("ru-RU"))),
          nameof(product.WeightKg)
        },
        {
          new ByteArrayContent(GetBytesFromFile(product.CADModel)),
          nameof(product.CADModel),
          product.CADModel.FileName
        },
        {
          new ByteArrayContent(GetBytesFromFile(product.ViewerModel)),
          nameof(product.ViewerModel),
          product.ViewerModel.FileName
        }
      };

      var result = await m_HttpClient.PostAsync(UrlsConfig.PDMApi.PostProduct(), multiContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      switch (result.StatusCode)
      {
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<FileViewModel> GetModelById(string fileId)
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.PDMApi.GetModelById(fileId));
      
      if (result.IsSuccessStatusCode)
      {
        var memory = new MemoryStream();
        using (var stream = await result.Content.ReadAsStreamAsync())
        {
          await stream.CopyToAsync(memory);
        }
        memory.Position = 0;

        return new FileViewModel
        {
          FileStream = memory,
          MimeType = result.Content.Headers.ContentType.MediaType,
          FileName = result.Content.Headers.ContentDisposition.FileName
        };
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return null;
        case HttpStatusCode.BadRequest:
        default:
          // todo: логгер, либо выше его вызывать, либо везде, хз
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<string> GetModelNameById(string fileId)
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.PDMApi.GetModelNameById(fileId));

      if (result.IsSuccessStatusCode)
      {
        return await result.Content.ReadAsStringAsync();
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return null;
        case HttpStatusCode.BadRequest:
        default:
          // todo: логгер, либо выше его вызывать, либо везде, хз
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<UserData> PostLogin(LoginData login)
    {
      var stringContent = new StringContent(JsonConvert.SerializeObject(login), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PostAsync(UrlsConfig.PDMApi.PostLogin(), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return JsonConvert.DeserializeObject<UserData>(await result.Content.ReadAsStringAsync());
      }

      switch (result.StatusCode)
      {
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }


    private static byte[] GetBytesFromFile(IFormFile file)
    {
      if (file == null || file.Length == 0)
      {
        return null;
      }

      using (var br = new BinaryReader(file.OpenReadStream()))
      {
        return br.ReadBytes((int)file.Length);
      }
    }
  }
}
