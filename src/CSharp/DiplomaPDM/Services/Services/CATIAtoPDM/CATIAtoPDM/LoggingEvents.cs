﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CATIAtoPDM
{
  public class LoggingEvents
  {
    public const int GENERATE_ITEMS = 1000;
    public const int LIST_ITEMS = 1001;
    public const int GET_ITEM = 1002;
    public const int INSERT_ITEM = 1003;
    public const int UPDATE_ITEM = 1004;
    public const int DELETE_ITEM = 1005;

    public const int GET_ITEM_NOT_FOUND = 4000;
    public const int UPDATE_ITEM_NOT_FOUND = 4001;

    public const int ERROR = 5000;
  }
}
