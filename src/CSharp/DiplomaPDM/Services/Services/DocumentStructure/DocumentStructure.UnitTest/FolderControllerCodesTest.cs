﻿//using System;
//using DocumentStructure.API.Controllers;
//using DocumentStructure.API.Infrastructure;
//using DocumentStructure.API.Models;
//using EntityFrameworkCoreMock;
//using Microsoft.AspNetCore.Mvc;
//using Microsoft.EntityFrameworkCore;
//using System.Collections.Generic;
//using System.Threading.Tasks;
//using DocumentStructure.API.ViewModels;
//using Xunit;

//namespace DocumentStructure.UnitTest
//{
//  /// <summary>
//  /// В этих тестах не проверяем работу базы.
//  /// Только то, что контроллер выдает адекватные результаты.
//  /// </summary>
//  [Trait("Category", "FolderController")]
//  public class FolderControllerCodesTest
//  {
//    private readonly FolderController m_Controller;
//    private readonly DbSetMock<Folder> m_DbSetFolder;

//    public FolderControllerCodesTest()
//    {
//      //var options = new Mock<IOptions<AppSettings>>();
//      //var dummyOptions = new DbContextOptionsBuilder<DocumentStructureContext>().Options;
//      var folders = MakeFolders();
//      var dbContext = new DbContextMock<DocumentStructureContext>();//(dummyOptions);
//      m_DbSetFolder = dbContext.CreateDbSetMock(c => c.Folders, folders);

//      m_Controller = new FolderController(dbContext.Object/*, options.Object*/);
//    }

//    #region CreateFolderAsync
//    // NOTE:
//    //  этот экшн не проверить из-за [FromBody]

//    //[Fact]
//    //public async Task CreateFolderAsync_BadModel_Test()
//    //{
//    //  // Arrange
//    //  var viewModel = new FolderViewModel{Id = 1, Name = "Name", Sign = ""};

//    //  // Act
//    //  var result = await m_Controller.CreateFolderAsync(viewModel);
//    //  ;
//    //  // Assert
//    //}
//    #endregion

//    #region IncludeFolderToFolderAsync
//    [Theory]
//    [InlineData("7E735245-ABA5-414A-A386-404B6F9A6156", "4E0081F2-E856-4A3D-9D66-647F8874B224")]
//    [InlineData("DAF91021-9F4E-47E6-AD81-7EADC9E3AFBA", "AE5F00C4-9E7D-4DC3-8608-E572774624A6")]
//    [InlineData("4E0081F2-E856-4A3D-9D66-647F8874B224", "AE5F00C4-9E7D-4DC3-8608-E572774624A6")]
//    public async Task IncludeFolderToFolderAsync_Test(Guid superId, Guid subId)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.IncludeFolderToFolderAsync(superId, subId);

//      // Assert
//      Assert.IsType<OkResult>(result);
//    }

//    [Theory]
//    [InlineData("34ACFD8A-A8DC-4443-B0EE-8B8D4451FFB9", "DAF91021-9F4E-47E6-AD81-7EADC9E3AFBA")]
//    [InlineData("34ACFD8A-A8DC-4443-B0EE-8B8D4451FFB9", "9E66AEB9-8E5C-4699-ACB3-837E2E74B5F1")]
//    [InlineData("7E735245-ABA5-414A-A386-404B6F9A6156", "DAF91021-9F4E-47E6-AD81-7EADC9E3AFBA")]
//    public async Task IncludeFolderToFolderAsync_BadRequest_Test(Guid superId, Guid subId)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.IncludeFolderToFolderAsync(superId, subId);

//      // Assert
//      Assert.IsType<BadRequestObjectResult>(result);
//    }
    
//    [Theory]
//    [InlineData("", "7E735245-ABA5-414A-A386-404B6F9A6156")]
//    [InlineData("7E735245-ABA5-414A-A386-404B6F9A6156", "")]
//    [InlineData("", "")]
//    public async Task IncludeFolderToFolderAsync_BadRequest_BadId_Test(Guid superId, Guid subId)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.IncludeFolderToFolderAsync(superId, subId);

//      // Assert
//      Assert.IsType<BadRequestResult>(result);
//    }

//    [Theory]
//    [InlineData("3C99FB1B-20DE-4613-9224-021AFD89E885", "A874E7C2-E567-48EB-8A73-E71E78E281CF")] // обеих нет в бд
//    [InlineData("CA95EBFA-8B45-47E3-A55F-D6929747E972", "3C99FB1B-20DE-4613-9224-021AFD89E885")] // sub нет в бд
//    [InlineData("3C99FB1B-20DE-4613-9224-021AFD89E885", "CA95EBFA-8B45-47E3-A55F-D6929747E972")] // super нет в бд
//    public async Task IncludeFolderToFolderAsync_NotFound_Test(Guid superId, Guid subId)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.IncludeFolderToFolderAsync(superId, subId);

//      // Assert
//      Assert.IsType<NotFoundObjectResult>(result);
//    }
//    #endregion

//    #region ExcludeFolderFromFolderAsync
//    [Theory]
//    [InlineData("DAF91021-9F4E-47E6-AD81-7EADC9E3AFBA", "7E735245-ABA5-414A-A386-404B6F9A6156")]
//    [InlineData("DAF91021-9F4E-47E6-AD81-7EADC9E3AFBA", "3B88836B-6279-44FF-82D0-892E34A93761")]
//    [InlineData("67F26FAE-E12B-4BDD-82E1-85FE4657054C", "D70BA0AE-3CA3-45B1-B0D6-C6AF8A634BAB")]
//    [InlineData("9E66AEB9-8E5C-4699-ACB3-837E2E74B5F1", "34ACFD8A-A8DC-4443-B0EE-8B8D4451FFB9")]
//    public async Task ExcludeFolderFromFolderAsync_Test(Guid superId, Guid subId)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.ExcludeFolderFromFolderAsync(superId, subId);

//      // Assert
//      Assert.IsType<OkResult>(result);
//    }


//    [Theory]
//    [InlineData("34ACFD8A-A8DC-4443-B0EE-8B8D4451FFB9", "9E66AEB9-8E5C-4699-ACB3-837E2E74B5F1")]
//    [InlineData("0A1EDD44-083D-4986-ACDA-281D835D550F", "D70BA0AE-3CA3-45B1-B0D6-C6AF8A634BAB")]
//    [InlineData("DAF91021-9F4E-47E6-AD81-7EADC9E3AFBA", "34ACFD8A-A8DC-4443-B0EE-8B8D4451FFB9")]
//    public async Task ExcludeFolderFromFolderAsync_BadRequest_Test(Guid superId, Guid subId)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.ExcludeFolderFromFolderAsync(superId, subId);

//      // Assert
//      Assert.IsType<BadRequestObjectResult>(result);
//    }

//    [Theory]
//    [InlineData("7E735245-ABA5-414A-A386-404B6F9A6156", "")]
//    [InlineData("", "7E735245-ABA5-414A-A386-404B6F9A6156")]
//    [InlineData("", "")]
//    public async Task ExcludeFolderFromFolderAsync_BadRequest_BadId_Test(Guid superId, Guid subId)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.ExcludeFolderFromFolderAsync(superId, subId);

//      // Assert
//      Assert.IsType<BadRequestResult>(result);
//    }

//    [Theory]
//    [InlineData("3C99FB1B-20DE-4613-9224-021AFD89E885", "A874E7C2-E567-48EB-8A73-E71E78E281CF")] // обеих нет в бд
//    [InlineData("CA95EBFA-8B45-47E3-A55F-D6929747E972", "3C99FB1B-20DE-4613-9224-021AFD89E885")] // sub нет в бд
//    [InlineData("3C99FB1B-20DE-4613-9224-021AFD89E885", "CA95EBFA-8B45-47E3-A55F-D6929747E972")] // super нет в бд
//    public async Task ExcludeFolderFromFolderAsync_NotFound_Test(Guid superId, Guid subId)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.ExcludeFolderFromFolderAsync(superId, subId);

//      // Assert
//      Assert.IsType<NotFoundObjectResult>(result);
//    }
//    #endregion

//    #region IncludeFolderToProjectAsync
//    [Theory]
//    [InlineData("AE5F00C4-9E7D-4DC3-8608-E572774624A6", "80A28168-A85E-42D7-82ED-05BD1EA18452")]
//    [InlineData("4E0081F2-E856-4A3D-9D66-647F8874B224", "80A28168-A85E-42D7-82ED-05BD1EA18452")]
//    [InlineData("3A767563-320B-43C5-A5DC-4C2F785611DB", "80A28168-A85E-42D7-82ED-05BD1EA18452")]
//    public async Task IncludeFolderToProjectAsync_Test(Guid folderId, string projectId)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.IncludeFolderToProjectAsync(folderId, projectId);

//      // Assert
//      Assert.IsType<OkResult>(result);
//    }

//    [Theory]
//    [InlineData("7E735245-ABA5-414A-A386-404B6F9A6156", "")]
//    [InlineData("", "80A28168-A85E-42D7-82ED-05BD1EA18452")]
//    [InlineData("", "")]
//    public async Task IncludeFolderToProjectAsync_BadRequest_BadId_Test(Guid folderId, string projectId)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.IncludeFolderToProjectAsync(folderId, projectId);

//      // Assert
//      Assert.IsType<BadRequestResult>(result);
//    }

//    [Theory]
//    [InlineData("DAF91021-9F4E-47E6-AD81-7EADC9E3AFBA", "80A28168-A85E-42D7-82ED-05BD1EA18452")]
//    [InlineData("783F6F1C-D08A-434A-95F7-600F12F7F65F", "80A28168-A85E-42D7-82ED-05BD1EA18452")]
//    [InlineData("CA95EBFA-8B45-47E3-A55F-D6929747E972", "80A28168-A85E-42D7-82ED-05BD1EA18452")]
//    public async Task IncludeFolderToProjectAsync_BadRequest_InProject_Test(Guid folderId, string projectId)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.IncludeFolderToProjectAsync(folderId, projectId);

//      // Assert
//      Assert.IsType<BadRequestObjectResult>(result);
//    }

//    [Theory]
//    [InlineData("D70BA0AE-3CA3-45B1-B0D6-C6AF8A634BAB", "80A28168-A85E-42D7-82ED-05BD1EA18452")]
//    [InlineData("34ACFD8A-A8DC-4443-B0EE-8B8D4451FFB9", "80A28168-A85E-42D7-82ED-05BD1EA18452")]
//    [InlineData("3B88836B-6279-44FF-82D0-892E34A93761", "80A28168-A85E-42D7-82ED-05BD1EA18452")]
//    public async Task IncludeFolderToProjectAsync_BadRequest_HasRoot_Test(Guid folderId, string projectId)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.IncludeFolderToProjectAsync(folderId, projectId);

//      // Assert
//      Assert.IsType<BadRequestObjectResult>(result);
//    }

//    [Theory]
//    [InlineData("3C99FB1B-20DE-4613-9224-021AFD89E885", "80A28168-A85E-42D7-82ED-05BD1EA18452")]
//    public async Task IncludeFolderToProjectAsync_NotFound_Test(Guid folderId, string projectId)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.IncludeFolderToProjectAsync(folderId, projectId);

//      // Assert
//      Assert.IsType<NotFoundObjectResult>(result);
//    }
//    #endregion

//    #region ExcludeFolderFromProjectAsync
//    [Theory]
//    [InlineData("DAF91021-9F4E-47E6-AD81-7EADC9E3AFBA")]
//    [InlineData("CA95EBFA-8B45-47E3-A55F-D6929747E972")]
//    [InlineData("6F802E1E-B004-4B64-A6AD-A1BA6F1DCA73")]
//    public async Task ExcludeFolderFromProjectAsync_Test(Guid folderId)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.ExcludeFolderFromProjectAsync(folderId);

//      // Assert
//      Assert.IsType<OkResult>(result);
//    }

//    [Theory]
//    [InlineData("")]
//    public async Task ExcludeFolderFromProjectAsync_BadRequest_BadId_Test(Guid folderId)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.ExcludeFolderFromProjectAsync(folderId);

//      // Assert
//      Assert.IsType<BadRequestResult>(result);
//    }

//    [Theory]
//    [InlineData("9E66AEB9-8E5C-4699-ACB3-837E2E74B5F1")]
//    [InlineData("34ACFD8A-A8DC-4443-B0EE-8B8D4451FFB9")]
//    [InlineData("AE5F00C4-9E7D-4DC3-8608-E572774624A6")]
//    public async Task ExcludeFolderFromProjectAsync_BadRequest_Test(Guid folderId)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.ExcludeFolderFromProjectAsync(folderId);

//      // Assert
//      Assert.IsType<BadRequestObjectResult>(result);
//    }

//    [Theory]
//    [InlineData("3C99FB1B-20DE-4613-9224-021AFD89E885")]
//    public async Task ExcludeFolderFromProjectAsync_NotFound_Test(Guid folderId)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.ExcludeFolderFromProjectAsync(folderId);

//      // Assert
//      Assert.IsType<NotFoundObjectResult>(result);
//    }
//    #endregion

//    #region IncludeDocumentToFolderAsync
//    [Theory]
//    //[InlineData("7E735245-ABA5-414A-A386-404B6F9A6156", 1)]
//    //[InlineData("34ACFD8A-A8DC-4443-B0EE-8B8D4451FFB9", 1)]
//    [InlineData("34ACFD8A-A8DC-4443-B0EE-8B8D4451FFB9", "5cd409647238f4164b0ef2c8")]
//    [InlineData("9E66AEB9-8E5C-4699-ACB3-837E2E74B5F1", "5cd4096c7238f4164b0ef2ca")]
//    public async Task IncludeDocumentToFolderAsync_Test(Guid folderId, string documentId)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.IncludeDocumentToFolderAsync(folderId, documentId);

//      // Assert
//      Assert.IsType<OkResult>(result);
//    }

//    [Theory]
//    [InlineData("7E735245-ABA5-414A-A386-404B6F9A6156", "")]
//    [InlineData("", "5cd3f88a08c6807b00b700d0")]
//    [InlineData("", "")]
//    public async Task IncludeDocumentToFolderAsync_BadRequest_BadId_Test(Guid folderId, string documentId)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.IncludeDocumentToFolderAsync(folderId, documentId);

//      // Assert
//      Assert.IsType<BadRequestResult>(result);
//    }

//    [Theory]
//    [InlineData("54A216B9-F754-44B0-B902-1ECF527BADA2", "5cd3f88a08c6807b00b700d0")]
//    [InlineData("783F6F1C-D08A-434A-95F7-600F12F7F65F", "5cd3ffaff15d3e03aca58e75")]
//    public async Task IncludeDocumentToFolderAsync_BadRequest_Test(Guid folderId, string documentId)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.IncludeDocumentToFolderAsync(folderId, documentId);

//      // Assert
//      Assert.IsType<BadRequestObjectResult>(result);
//    }

//    [Theory]
//    [InlineData("3C99FB1B-20DE-4613-9224-021AFD89E885", "5cd3f88a08c6807b00b700d0")]
//    public async Task IncludeDocumentToFolderAsync_NotFound_Test(Guid folderId, string documentId)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.IncludeDocumentToFolderAsync(folderId, documentId);

//      // Assert
//      Assert.IsType<NotFoundObjectResult>(result);
//    }
//    #endregion

//    #region GetIncludedFoldersAsync
//    [Theory]
//    [InlineData("DAF91021-9F4E-47E6-AD81-7EADC9E3AFBA", 
//      new [] { "7E735245-ABA5-414A-A386-404B6F9A6156",
//        "3B88836B-6279-44FF-82D0-892E34A93761",
//        "54A216B9-F754-44B0-B902-1ECF527BADA2" })]
//    [InlineData("67F26FAE-E12B-4BDD-82E1-85FE4657054C", 
//      new[] { "D70BA0AE-3CA3-45B1-B0D6-C6AF8A634BAB",
//        "0A1EDD44-083D-4986-ACDA-281D835D550F",
//        "0AB85CE1-5807-4316-AA6C-5C60D1BFB727" })]
//    public async Task GetIncludedFoldersAsync_Test(Guid folderId, string[] expectedFoldersIds)
//    {
//      // Arrange
//      var expectedGuids = new List<Guid>();
//      foreach (string expectedFolderId in expectedFoldersIds)
//      {
//        expectedGuids.Add(new Guid(expectedFolderId));
//      }

//      // Act
//      var result = await m_Controller.GetIncludedFoldersAsync(folderId);

//      // Assert
//      Assert.IsType<OkObjectResult>(result);
//      var resObject = (OkObjectResult)result;
//      Assert.IsType<int[]>(resObject.Value);
//      Assert.Equal(expectedGuids, resObject.Value);
//    }

//    [Theory]
//    [InlineData("")]
//    public async Task GetIncludedFoldersAsync_BadRequest_BadId_Test(Guid folderId)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.GetIncludedFoldersAsync(folderId);

//      // Assert
//      Assert.IsType<BadRequestResult>(result);
//    }

//    [Theory]
//    [InlineData("3C99FB1B-20DE-4613-9224-021AFD89E885")]
//    public async Task GetIncludedFoldersAsync_NotFound_Test(Guid folderId)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.GetIncludedFoldersAsync(folderId);

//      // Assert
//      Assert.IsType<NotFoundObjectResult>(result);
//    }
//    #endregion

//    #region GetIncludedDocumentsAsync
//    [Theory]
//    [InlineData("54A216B9-F754-44B0-B902-1ECF527BADA2", 
//      new[] 
//      {
//        "5cd3f88a08c6807b00b700d0",
//        "5cd3ff8ff15d3e03aca58e6f",
//        "5cd3ff99f15d3e03aca58e71"
//      })]
//    [InlineData("783F6F1C-D08A-434A-95F7-600F12F7F65F", 
//      new[]
//      {
//        "5cd3ffa5f15d3e03aca58e73",
//        "5cd3ffaff15d3e03aca58e75",
//        "5cd3ffb7f15d3e03aca58e77"
//      })]
//    [InlineData("7E735245-ABA5-414A-A386-404B6F9A6156", new string[0])]
//    public async Task GetIncludedDocumentsAsync_Test(Guid folderId, string[] expectedDocumentsIds)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.GetIncludedDocumentsAsync(folderId);

//      // Assert
//      Assert.IsType<OkObjectResult>(result);
//      var resObject = (OkObjectResult)result;
//      Assert.IsType<int[]>(resObject.Value);
//      Assert.Equal(expectedDocumentsIds, resObject.Value);
//    }

//    [Theory]
//    [InlineData("")]
//    public async Task GetIncludedDocumentsAsync_BadRequest_BadId_Test(Guid folderId)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.GetIncludedDocumentsAsync(folderId);

//      // Assert
//      Assert.IsType<BadRequestResult>(result);
//    }

//    [Theory]
//    [InlineData("3C99FB1B-20DE-4613-9224-021AFD89E885")]
//    public async Task GetIncludedDocumentsAsync_NotFound_Test(Guid folderId)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.GetIncludedDocumentsAsync(folderId);

//      // Assert
//      Assert.IsType<NotFoundResult>(result);
//    }
//    #endregion

//    #region GetFolderByIdAsync
//    [Theory]
//    [InlineData("7E735245-ABA5-414A-A386-404B6F9A6156")]
//    [InlineData("DAF91021-9F4E-47E6-AD81-7EADC9E3AFBA")]
//    public async Task GetFolderByIdAsync_Test(Guid folderId)
//    {
//      // Arrange
//      var entity = await m_DbSetFolder.Object.SingleOrDefaultAsync(f => f.Id == folderId);
//      var entityVM = new FolderOutViewModel(entity);

//      // Act
//      var result = await m_Controller.GetFolderByIdAsync(folderId);

//      // Assert
//      Assert.IsType<OkObjectResult>(result.Result);
//      var resObject = (OkObjectResult)result.Result;

//      // workaround: не хочет напрямую сравнивать два экземпляра класса
//      var resFolder = (FolderOutViewModel) resObject.Value;
//      Assert.Equal(entityVM.Id, resFolder.Id);
//      Assert.Equal(entityVM.ItemRealIds, resFolder.ItemRealIds);
//      Assert.Equal(entityVM.Name, resFolder.Name);
//      Assert.Equal(entityVM.Sign, resFolder.Sign);
//      Assert.Equal(entityVM.SuperiorFolderId, resFolder.SuperiorFolderId);
//      Assert.Equal(entityVM.SubFolderIds, resFolder.SubFolderIds);
//    }

//    [Theory]
//    [InlineData("")]
//    public async Task GetFolderByIdAsync_BadRequest_BadId_Test(Guid folderId)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.GetFolderByIdAsync(folderId);

//      // Assert
//      Assert.IsType<BadRequestResult>(result.Result);
//    }

//    [Theory]
//    [InlineData("3C99FB1B-20DE-4613-9224-021AFD89E885")]
//    public async Task GetFolderByIdAsync_NotFound_Test(Guid folderId)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.GetFolderByIdAsync(folderId);

//      // Assert
//      Assert.IsType<NotFoundResult>(result.Result);
//    }
//    #endregion

//    #region LoopCheck
//    [Theory]
//    [InlineData("34ACFD8A-A8DC-4443-B0EE-8B8D4451FFB9", "DAF91021-9F4E-47E6-AD81-7EADC9E3AFBA")]
//    [InlineData("34ACFD8A-A8DC-4443-B0EE-8B8D4451FFB9", "9E66AEB9-8E5C-4699-ACB3-837E2E74B5F1")]
//    [InlineData("7E735245-ABA5-414A-A386-404B6F9A6156", "DAF91021-9F4E-47E6-AD81-7EADC9E3AFBA")]
//    [InlineData("D70BA0AE-3CA3-45B1-B0D6-C6AF8A634BAB", "67F26FAE-E12B-4BDD-82E1-85FE4657054C")]
//    public async Task LoopCheck_True_Test(Guid folderId, Guid superFolderId)
//    {
//      // Assign

//      // Act
//      var result = await m_Controller.LoopCheck(folderId, superFolderId);

//      // Assert
//      Assert.True(result);
//    }

//    [Theory]
//    [InlineData("9E66AEB9-8E5C-4699-ACB3-837E2E74B5F1", "34ACFD8A-A8DC-4443-B0EE-8B8D4451FFB9")]
//    [InlineData("783F6F1C-D08A-434A-95F7-600F12F7F65F", "DAF91021-9F4E-47E6-AD81-7EADC9E3AFBA")]
//    [InlineData("DAF91021-9F4E-47E6-AD81-7EADC9E3AFBA", "34ACFD8A-A8DC-4443-B0EE-8B8D4451FFB9")]
//    [InlineData("67F26FAE-E12B-4BDD-82E1-85FE4657054C", "0A1EDD44-083D-4986-ACDA-281D835D550F")]
//    public async Task LoopCheck_False_Test(Guid folderId, Guid superFolderId)
//    {
//      // Assign

//      // Act
//      var result = await m_Controller.LoopCheck(folderId, superFolderId);

//      // Assert
//      Assert.False(result);
//    }
//    #endregion

//    #region ReverseLoopCheck
//    [Theory]
//    [InlineData("DAF91021-9F4E-47E6-AD81-7EADC9E3AFBA", "34ACFD8A-A8DC-4443-B0EE-8B8D4451FFB9")]
//    [InlineData("9E66AEB9-8E5C-4699-ACB3-837E2E74B5F1", "34ACFD8A-A8DC-4443-B0EE-8B8D4451FFB9")]
//    [InlineData("DAF91021-9F4E-47E6-AD81-7EADC9E3AFBA", "7E735245-ABA5-414A-A386-404B6F9A6156")]
//    [InlineData("67F26FAE-E12B-4BDD-82E1-85FE4657054C", "D70BA0AE-3CA3-45B1-B0D6-C6AF8A634BAB")]
//    public async Task ReverseLoopCheck_True_Test(Guid folderId, Guid subFolderId)
//    {
//      // Assign

//      // Act
//      var result = await m_Controller.ReverseLoopCheckAsync(folderId, subFolderId);

//      // Assert
//      Assert.True(result);
//    }

//    [Theory]
//    [InlineData("34ACFD8A-A8DC-4443-B0EE-8B8D4451FFB9", "4E0081F2-E856-4A3D-9D66-647F8874B224")]
//    [InlineData("3B88836B-6279-44FF-82D0-892E34A93761", "7E735245-ABA5-414A-A386-404B6F9A6156")]
//    [InlineData("34ACFD8A-A8DC-4443-B0EE-8B8D4451FFB9", "DAF91021-9F4E-47E6-AD81-7EADC9E3AFBA")]
//    [InlineData("CA95EBFA-8B45-47E3-A55F-D6929747E972", "6F802E1E-B004-4B64-A6AD-A1BA6F1DCA73")]
//    public async Task ReverseLoopCheck_False_Test(Guid folderId, Guid subFolderId)
//    {
//      // Assign

//      // Act
//      var result = await m_Controller.ReverseLoopCheckAsync(folderId, subFolderId);

//      // Assert
//      Assert.False(result);
//    }
//    #endregion

//    /// <summary>
//    /// - P1:
//    ///   - F7:
//    ///     - F1
//    ///     - F2
//    ///       - F15:
//    ///         - F16
//    ///     - F3
//    ///       - D1
//    ///       - D2
//    ///       - D3
//    ///   - F8:
//    ///       - D5
//    ///       - D6
//    ///       - D7
//    /// - P2:
//    ///   - F9:
//    ///     - F4
//    ///     - F5:
//    ///     - F6
//    /// - P3:
//    ///   - F10:
//    ///   - F11:
//    ///   - F12:
//    /// </summary>
//    /// <returns></returns>
//    private List<Folder> MakeFolders()
//    {
//      var folders = new List<Folder>
//      {
//        new Folder {Id = new Guid("7E735245-ABA5-414A-A386-404B6F9A6156"), Sign = "F1", Name = "Folder 1"},
//        new Folder {Id = new Guid("3B88836B-6279-44FF-82D0-892E34A93761"), Sign = "F2", Name = "Folder 2"},
//        new Folder {Id = new Guid("54A216B9-F754-44B0-B902-1ECF527BADA2"), Sign = "F3", Name = "Folder 3"},
//        new Folder {Id = new Guid("D70BA0AE-3CA3-45B1-B0D6-C6AF8A634BAB"), Sign = "F4", Name = "Folder 4"},
//        new Folder {Id = new Guid("0A1EDD44-083D-4986-ACDA-281D835D550F"), Sign = "F5", Name = "Folder 5"},
//        new Folder {Id = new Guid("0AB85CE1-5807-4316-AA6C-5C60D1BFB727"), Sign = "F6", Name = "Folder 6"},

//        new Folder {Id = new Guid("DAF91021-9F4E-47E6-AD81-7EADC9E3AFBA"), Sign = "F7", Name = "Folder 7", ProjectId = "80A28168-A85E-42D7-82ED-05BD1EA18452"},
//        new Folder {Id = new Guid("783F6F1C-D08A-434A-95F7-600F12F7F65F"), Sign = "F8", Name = "Folder 8", ProjectId = "80A28168-A85E-42D7-82ED-05BD1EA18452"},

//        new Folder {Id = new Guid("67F26FAE-E12B-4BDD-82E1-85FE4657054C"), Sign = "F9", Name = "Folder 9", ProjectId = "1EE4DFD8-5A30-4173-BCA7-F43765345BAC"},

//        new Folder {Id = new Guid("CA95EBFA-8B45-47E3-A55F-D6929747E972"), Sign = "F10", Name = "Folder 10", ProjectId = "308386EB-D2A8-4E9B-8EB3-744FB1C36588"},
//        new Folder {Id = new Guid("6F802E1E-B004-4B64-A6AD-A1BA6F1DCA73"), Sign = "F11", Name = "Folder 11", ProjectId = "308386EB-D2A8-4E9B-8EB3-744FB1C36588"},
//        new Folder {Id = new Guid("392A5783-DFE9-4971-99B0-E38DC21277C7"), Sign = "F12", Name = "Folder 12", ProjectId = "308386EB-D2A8-4E9B-8EB3-744FB1C36588"},

//        new Folder {Id = new Guid("BA29C7D0-BA3F-4E5D-B15B-59C6CE6E3BB4"), Sign = "F13", Name = "Folder 13"},
//        new Folder {Id = new Guid("3A767563-320B-43C5-A5DC-4C2F785611DB"), Sign = "F14", Name = "Folder 14"},
//        new Folder {Id = new Guid("9E66AEB9-8E5C-4699-ACB3-837E2E74B5F1"), Sign = "F15", Name = "Folder 15"},
//        new Folder {Id = new Guid("34ACFD8A-A8DC-4443-B0EE-8B8D4451FFB9"), Sign = "F16", Name = "Folder 16"},
//        new Folder {Id = new Guid("4E0081F2-E856-4A3D-9D66-647F8874B224"), Sign = "F17", Name = "Folder 17"},
//        new Folder {Id = new Guid("AE5F00C4-9E7D-4DC3-8608-E572774624A6"), Sign = "F18", Name = "Folder 18"},
//      };

//      folders[6].SubFolders = new List<Folder>(new[] { folders[0], folders[1], folders[2] });
//      folders[8].SubFolders = new List<Folder>(new[] { folders[3], folders[4], folders[5] });

//      folders[0].SuperiorFolder = folders[1].SuperiorFolder = folders[2].SuperiorFolder = folders[6];
//      folders[3].SuperiorFolder = folders[4].SuperiorFolder = folders[5].SuperiorFolder = folders[8];

//      folders[2].Items = new List<FolderItem>(new[]
//      {
//        new FolderItem() {Id = new Guid("33A87245-152C-47B7-8AD6-F3966FB1EB3B"), ItemRealId = "5cd3f88a08c6807b00b700d0", Folder = folders[2]},
//        new FolderItem() {Id = new Guid("FCF1CB5D-359A-42F3-86D9-73FD7C6FFF86"), ItemRealId = "5cd3ff8ff15d3e03aca58e6f", Folder = folders[2] },
//        new FolderItem() {Id = new Guid("B351A349-B4B9-42E5-8102-83F43F3F7A25"), ItemRealId = "5cd3ff99f15d3e03aca58e71", Folder = folders[2] }
//      });
//      folders[7].Items = new List<FolderItem>(new[]
//      {
//        new FolderItem() {Id = new Guid("0E37C749-27D0-486D-A064-2F2D11E0C5FA"), ItemRealId = "5cd3ffa5f15d3e03aca58e73", Folder = folders[7] },
//        new FolderItem() {Id = new Guid("93C3BD7F-971D-4EB5-B55E-943C543BE973"), ItemRealId = "5cd3ffaff15d3e03aca58e75", Folder = folders[7] },
//        new FolderItem() {Id = new Guid("475D7EB2-4823-433F-9FA6-57594D4E8515"), ItemRealId = "5cd3ffb7f15d3e03aca58e77", Folder = folders[7] }
//      });

//      folders[1].SubFolders = new List<Folder>(new[] { folders[14] });
//      folders[14].SuperiorFolder = folders[1];

//      folders[14].SubFolders = new List<Folder>(new[] { folders[15] });
//      folders[15].SuperiorFolder = folders[14];

//      return folders;
//    }
//  }
//}
