﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using DocumentStructure.API.Infrastructure;
//using DocumentStructure.API.Models;
//using Microsoft.EntityFrameworkCore;
//using Xunit;
//using Xunit.Sdk;

//namespace DocumentStructure.UnitTest
//{
//  public class DbContextTest
//  {
//    private readonly DbContextOptions<DocumentStructureContext> m_CommonOptions;

//    public DbContextTest()
//    {
//      m_CommonOptions = new DbContextOptionsBuilder<DocumentStructureContext>()
//        .UseInMemoryDatabase("DocumentStructureDbInMemoryTest")
//        .Options;
//    }

//    #region CheckTestDbWork
//    [Fact]
//    public void CheckTestDbWork1()
//    {
//      using (var context = new DocumentStructureContext(m_CommonOptions))
//      {
//        context.Folders.Add(new Folder() { Sign = "Test" });
//        context.SaveChanges();
//      }

//      using (var context = new DocumentStructureContext(m_CommonOptions))
//      {
//        var folder = context.Folders.FirstOrDefault(f => f.Sign == "Test");
//        Assert.NotNull(folder);
//      }
//    }

//    [Fact]
//    public void CheckTestDbWork2()
//    {
//      using (var context = new DocumentStructureContext(m_CommonOptions))
//      {
//        var folder = context.Folders.FirstOrDefault(f => f.Sign == "Test");
//        Assert.NotNull(folder);
//      }
//    }
//    #endregion

//    #region CoreTests

//    [Fact]
//    public void Test1()
//    {
//      using (var db = new DocumentStructureContext(m_CommonOptions))
//      {
//        Folder f = new Folder()
//        {
//          Sign = "F1",
//          Name = "Folder1",
//        };

//        db.Folders.Add(f);
//        db.SaveChanges();

//        Document d1 = new Document()
//        {
//          DocumentRealId = 1,
//          Folder = f
//        };

//        db.Documents.Add(d1);
//        db.SaveChanges();

//        var folders = db.Folders.Include(d => d.Documents).FirstOrDefault();
//        ;
//      }
//    }

//    [Fact]
//    public void Test2()
//    {
//      using (var db = new DocumentStructureContext(m_CommonOptions))
//      {
//        Folder f2 = new Folder()
//        {
//          Sign = "F2",
//          Name = "Folder2",
//        };

//        Folder f3 = new Folder()
//        {
//          Sign = "F3",
//          Name = "Folder3",
//          SuperiorFolder = f2,
//        };

//        db.Folders.AddRange(new List<Folder> {f2, f3});
//        db.SaveChanges();

//        var folders = db.Folders.Include(f => f.SubFolders).ToList();
//        ;
//      }
//    }

//    #endregion
//  }
//}
