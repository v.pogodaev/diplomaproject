﻿//using System;
//using System.Collections.Generic;
//using DocumentStructure.API.Models;
//using Xunit;

//namespace DocumentStructure.UnitTest
//{
//  [Trait("Category", "FolderModel")]
//  public class FolderModelTest
//  {
//    #region ConnectFolders
//    [Fact]
//    public void ConnectFoldersTest1()
//    {
//      // Assign
//      var superFolder = new Folder
//      {
//        Id = new Guid("7E735245-ABA5-414A-A386-404B6F9A6156"), Sign = "S1", Name = "N1", SuperiorFolder = null, SubFolders = null, Documents = null, ProjectId = null
//      };
//      var subFolder = new Folder
//      {
//        Id = 2, Sign = "S2", Name = "N2", SuperiorFolder = null, SubFolders = null, Documents = null, ProjectId = null
//      };

//      // Act
//      var result = Folder.ConnectFolders(superFolder, subFolder);

//      // Assert
//      Assert.True(result);
//    }

//    [Fact]
//    public void ConnectFoldersTest2()
//    {
//      // Assign
//      var superFolder = new Folder
//      {
//        Id = new Guid("7E735245-ABA5-414A-A386-404B6F9A6156"),
//        Sign = "S1",
//        Name = "N1",
//        SuperiorFolder = new Folder()
//        {
//          Id = 3,
//          Sign = "S3",
//          Name = "N3",
//          SuperiorFolder = null,
//          Documents = null,
//          ProjectId = null
//        },
//        SubFolders = null,
//        Documents = null,
//        ProjectId = null
//      };
//      var subFolder = new Folder
//      {
//        Id = 2,
//        Sign = "S2",
//        Name = "N2",
//        SuperiorFolder = null,
//        SubFolders = null,
//        Documents = null,
//        ProjectId = null
//      };

//      // Act
//      var result = Folder.ConnectFolders(superFolder, subFolder);

//      // Assert
//      Assert.True(result);
//    }

//    [Fact]
//    public void ConnectFolders_FoldersAllReadyConnected_Test()
//    {
//      // Assign
//      var superFolder = new Folder
//      {
//        Id = 1,
//        Sign = "S1",
//        Name = "N1",
//        SuperiorFolder = null,
//        SubFolders = null,
//        Documents = null,
//        ProjectId = null
//      };
//      var subFolder = new Folder
//      {
//        Id = 2,
//        Sign = "S2",
//        Name = "N2",
//        SuperiorFolder = null,
//        SubFolders = null,
//        Documents = null,
//        ProjectId = null
//      };
//      superFolder.SubFolders = new List<Folder>(new[] {subFolder});
//      subFolder.SuperiorFolder = superFolder;

//      // Act
//      var result = Folder.ConnectFolders(superFolder, subFolder);

//      // Assert
//      Assert.True(result);
//    }

//    [Fact]
//    public void ConnectFolders_SubFolderHasSuperFolderAllReady_Test()
//    {
//      // Assign
//      var superFolder = new Folder
//      {
//        Id = 1,
//        Sign = "S1",
//        Name = "N1",
//        SuperiorFolder = null,
//        SubFolders = null,
//        Documents = null,
//        ProjectId = null
//      };
//      var superFolder2 = new Folder
//      {
//        Id = 3,
//        Sign = "S3",
//        Name = "N3",
//        SuperiorFolder = null,
//        SubFolders = null,
//        Documents = null,
//        ProjectId = null
//      };
//      var subFolder = new Folder
//      {
//        Id = 2,
//        Sign = "S2",
//        Name = "N2",
//        SuperiorFolder = null,
//        SubFolders = null,
//        Documents = null,
//        ProjectId = null
//      };
//      superFolder2.SubFolders = new List<Folder>(new[] {subFolder});
//      subFolder.SuperiorFolder = superFolder2;

//      // Act
//      var result = Folder.ConnectFolders(superFolder, subFolder);

//      // Assert
//      Assert.False(result);
//    }
//    #endregion

//    #region DisconnectFolders
//    [Fact]
//    public void DisconnectFoldersTest()
//    {
//      // Assing
//      var superFolder = new Folder
//      {
//        Id = 1,
//        Sign = "S1",
//        Name = "N1",
//        SuperiorFolder = null,
//        SubFolders = null,
//        Documents = null,
//        ProjectId = null
//      };
//      var subFolder = new Folder
//      {
//        Id = 2,
//        Sign = "S2",
//        Name = "N2",
//        SuperiorFolder = null,
//        SubFolders = null,
//        Documents = null,
//        ProjectId = null
//      };
//      superFolder.SubFolders = new List<Folder>(new[] { subFolder });
//      subFolder.SuperiorFolder = superFolder;

//      // Act
//      var result = Folder.DisconnectFolders(superFolder, subFolder);

//      // Assert
//      Assert.True(result);
//    }

//    [Fact]
//    public void DisconnectFolders_AreNotConnected_Test()
//    {
//      // Assing
//      var superFolder = new Folder
//      {
//        Id = 1,
//        Sign = "S1",
//        Name = "N1",
//        SuperiorFolder = null,
//        SubFolders = null,
//        Documents = null,
//        ProjectId = null
//      };
//      var subFolder = new Folder
//      {
//        Id = 2,
//        Sign = "S2",
//        Name = "N2",
//        SuperiorFolder = null,
//        SubFolders = null,
//        Documents = null,
//        ProjectId = null
//      };

//      // Act
//      var result = Folder.DisconnectFolders(superFolder, subFolder);

//      // Assert
//      Assert.True(result);
//    }

//    [Fact]
//    public void DisconnectFolders_AnotherSuper_Test()
//    {
//      // Assign
//      var superFolder = new Folder
//      {
//        Id = 1,
//        Sign = "S1",
//        Name = "N1",
//        SuperiorFolder = null,
//        SubFolders = null,
//        Documents = null,
//        ProjectId = null
//      };
//      var superFolder2 = new Folder
//      {
//        Id = 3,
//        Sign = "S3",
//        Name = "N3",
//        SuperiorFolder = null,
//        SubFolders = null,
//        Documents = null,
//        ProjectId = null
//      };
//      var subFolder = new Folder
//      {
//        Id = 2,
//        Sign = "S2",
//        Name = "N2",
//        SuperiorFolder = null,
//        SubFolders = null,
//        Documents = null,
//        ProjectId = null
//      };
//      superFolder2.SubFolders = new List<Folder>(new[] { subFolder });
//      subFolder.SuperiorFolder = superFolder2;

//      // Act
//      var result = Folder.ConnectFolders(superFolder, subFolder);

//      // Assert
//      Assert.False(result);
//    }
//    #endregion
//  }
//}
