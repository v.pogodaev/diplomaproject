﻿using Microsoft.AspNetCore.Mvc;

namespace DocumentStructure.API.Controllers
{
  public class HomeController : ControllerBase
  {
    public IActionResult Index()
    {
      return new RedirectResult("~/swagger");
    }
  }
}