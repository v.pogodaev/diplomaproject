﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using DocumentStructure.API.Infrastructure;
using DocumentStructure.API.Models;
using DocumentStructure.API.ViewModels;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using TransactionLib.Enums;
using TransactionLib.ViewModels;

namespace DocumentStructure.API.Controllers
{
  /// <summary>
  /// Контроллер структуры папок проекта
  /// </summary>
  [Route("api/v1/[controller]")]
  [ApiController]
  public class FolderController : ControllerBase
  {
    private readonly DocumentStructureContext m_DbContext;
    private readonly IOptions<AppSettings> m_Settings;

    public FolderController(DocumentStructureContext dbContext, IOptions<AppSettings> settings)
    {
      m_DbContext = dbContext;
      m_Settings = settings;
    }

    /// <summary>
    /// Создание новой папки
    /// </summary>
    /// <param name="model">Новая папка</param>
    /// <returns></returns>
    [HttpPost]
    [Route("create")]
    [ProducesResponseType(typeof(FolderOutViewModel), (int)HttpStatusCode.Created)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    public async Task<IActionResult> CreateFolderAsync([FromBody] FolderCreateViewModel model)
    {
      // Checks

      if (!TryValidateModel(model))
      {
        return BadRequest();
      }

      // End Checks

      var folder = model.GetModel();
      folder.TransactionAction = TransactionAction.Adding;

      var entity = await m_DbContext.Folders.AddAsync(folder);
      await m_DbContext.SaveChangesAsync();

      return CreatedAtAction(nameof(GetFolderByIdAsync), new { id = entity.Entity.Id }, entity.Entity);
    }

    /// <summary>
    /// Включение папки с id == <paramref name="subFolderId"/> в папку c id == <paramref name="superFolderId"/>
    /// </summary>
    /// <param name="superFolderId">Id вышестоящей папки</param>
    /// <param name="subFolderId">Id папки-потомка</param>
    /// <param name="tModel">Данные о транзакции</param>
    /// <returns></returns>
    [HttpPut]
    [Route("include")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.OK)]
    public async Task<IActionResult> IncludeFolderToFolderAsync(Guid superFolderId, Guid subFolderId, TransactionDetailsViewModel tModel)
    {
      // Checks

      // Проверяем модель
      if (!TryValidateModel(tModel))
      {
        return BadRequest("Некорректные данные транзакции");
      }

      // Ищем сущности
      var superEntity = await m_DbContext.Folders
        .Include(f => f.SubFolders)
        .SingleOrDefaultAsync(f => f.Id == superFolderId);
      if (superEntity == null)
      {
        return NotFound("Родительская папка не найдена");
      }

      var subEntity = await m_DbContext.Folders
        .SingleOrDefaultAsync(f => f.Id == subFolderId);
      if (subEntity == null)
      {
        return NotFound("Папка-потомок не найдена");
      }

      // Проверяем, что сущности пригодны для операции
      if (subEntity.SuperiorFolder != null || !string.IsNullOrEmpty(subEntity.ProjectId))
      {
        return BadRequest($"У папки {subFolderId} уже есть родительская папка");
      }

      // Проверяем, не начата ли другая транзакция
      if (superEntity.TransactionId != null && superEntity.TransactionId != tModel.TransactionId && superEntity.SessionId != tModel.SessionId ||
          subEntity.TransactionId != null && subEntity.TransactionId != tModel.TransactionId && subEntity.SessionId != tModel.SessionId)
      {
        return BadRequest("Другая транзакция уже начата");
      }

      // End Checks

      if (await DownLoopCheckAsync(subFolderId, superFolderId))
      {
        return BadRequest($"Папка {subFolderId} находится ниже {superFolderId}");
      }

      switch (subEntity.TransactionAction)
      {
        case TransactionAction.Adding:
          subEntity.SuperiorFolderId = superFolderId;
          m_DbContext.Update(subEntity);
          break;
        case TransactionAction.Modifying:
          {
            var tFl = await m_DbContext.Folders.SingleOrDefaultAsync(p =>
              p.OriginalId == subEntity.Id.ToString());
            if (tFl == null)
            {
              return BadRequest();
            }

            tFl.SuperiorFolderId = superFolderId;
            m_DbContext.Update(subEntity);
            break;
          }

        case null:
        case TransactionAction.Removing:
          {
            var tSubFolder = subEntity.CopyModelForTransaction();
            tModel.PassTransactionData(tSubFolder, TransactionAction.Modifying);
            tSubFolder.TransactionAction = TransactionAction.Modifying;
            tSubFolder.SuperiorFolderId = superFolderId;

            if (subEntity.TransactionAction != TransactionAction.Removing)
            {
              tModel.PassTransactionData(subEntity, TransactionAction.Modifying);
              await m_DbContext.AddAsync(tSubFolder);
            }

            break;
          }
      }

      try
      {
        await m_DbContext.SaveChangesAsync();
      }
      catch (Exception)
      {
        //todo: logger
        return BadRequest("Ошибка во время транзакции");
      }

      await m_DbContext.SaveChangesAsync();

      return Ok();
    }

    /// <summary>
    /// Отсоединение папки с id == <paramref name="subFolderId"/> от папки c id == <paramref name="superFolderId"/>
    /// </summary>
    /// <param name="superFolderId">Id вышестоящей папки</param>
    /// <param name="subFolderId">Id папки-потомка</param>
    /// <param name="tModel">Данные о транзакции</param>
    /// <returns></returns>
    [HttpPut]
    [Route("exclude")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.OK)]
    public async Task<IActionResult> ExcludeFolderFromFolderAsync(Guid superFolderId, Guid subFolderId, TransactionDetailsViewModel tModel)
    {
      // Checks

      if (!TryValidateModel(tModel))
      {
        return BadRequest("Некорректные данные транзакции");
      }

      var superEntity = await m_DbContext.Folders
        .Include(f => f.SubFolders)
        .SingleOrDefaultAsync(f => f.Id == superFolderId);
      if (superEntity == null)
      {
        return NotFound("Родительская папка не найдена");
      }

      var subEntity = await m_DbContext.Folders
        .SingleOrDefaultAsync(f => f.Id == subFolderId);
      if (subEntity == null)
      {
        return NotFound("Папка-потомок не найдена");
      }

      if (superEntity.SubFolders == null || subEntity.SuperiorFolder.Id != superEntity.Id)
      {
        return BadRequest($"Папка {superFolderId} не включает в себя папку {subFolderId}");
      }

      if (superEntity.TransactionId != null && superEntity.TransactionId != tModel.TransactionId && superEntity.SessionId != tModel.SessionId ||
          subEntity.TransactionId != null && subEntity.TransactionId != tModel.TransactionId && subEntity.SessionId != tModel.SessionId)
      {
        return BadRequest("Другая транзакция уже начата");
      }

      // End Checks

      switch (subEntity.TransactionAction)
      {
        case TransactionAction.Adding:
          subEntity.SuperiorFolderId = null;
          m_DbContext.Update(subEntity);
          break;
        case TransactionAction.Modifying:
          {
            var tFl = await m_DbContext.Folders.SingleOrDefaultAsync(p =>
              p.OriginalId == subEntity.Id.ToString());
            if (tFl == null)
            {
              return BadRequest();
            }

            tFl.SuperiorFolderId = null;
            m_DbContext.Update(subEntity);
            break;
          }

        case null:
        case TransactionAction.Removing:
          {
            var tSubFolder = subEntity.CopyModelForTransaction();
            tModel.PassTransactionData(tSubFolder, TransactionAction.Modifying);
            tSubFolder.TransactionAction = TransactionAction.Modifying;
            tSubFolder.SuperiorFolderId = null;

            if (subEntity.TransactionAction != TransactionAction.Removing)
            {
              tModel.PassTransactionData(subEntity, TransactionAction.Modifying);
              await m_DbContext.AddAsync(tSubFolder);
            }

            break;
          }
      }

      try
      {
        await m_DbContext.SaveChangesAsync();
      }
      catch (Exception)
      {
        //todo: logger
        return BadRequest("Ошибка во время транзакции");
      }

      //if (!Folder.DisconnectFolders(
      //  (superEntity.TransactionAction == TransactionAction.Adding) ? superEntity : tSuperFolder,
      //  (subEntity.TransactionAction == TransactionAction.Adding) ? subEntity : tSubFolder))
      //{
      //  // todo: это очень странное поведение, его (как и то что выше) необходимо логировать. пользователю надо выдавать не BadRequest, а что-то другое
      //  // todo: логгер
      //  // Такого быть не должно
      //  return BadRequest();
      //}

      await m_DbContext.SaveChangesAsync();

      return Ok();
    }

    /// <summary>
    /// Включение папки в проект
    /// </summary>
    /// <param name="folderId">Id папки</param>
    /// <param name="projectId">Настоящий (из его микросервиса) Id проекта</param>
    /// <param name="tModel">Данные о транзакции</param>
    /// <returns></returns>
    [HttpPut]
    [Route("includeToProject")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.OK)]
    public async Task<IActionResult> IncludeFolderToProjectAsync(Guid folderId, string projectId, TransactionDetailsViewModel tModel)
    {
      // Checks

      if (string.IsNullOrEmpty(projectId))
      {
        return BadRequest("Некорректные данные");
      }

      if (!TryValidateModel(tModel))
      {
        return BadRequest("Некорректные данные транзакции");
      }

      var folderEntity = await m_DbContext.Folders
        .SingleOrDefaultAsync(f => f.Id == folderId);
      if (folderEntity == null)
      {
        return NotFound("Папка не найдена");
      }

      if (folderEntity.SuperiorFolder != null)
      {
        return BadRequest($"Папка {folderId} не может быть корневой, т.к. есть вышестоящие папки");
      }

      if (folderEntity.ProjectId != null)
      {
        return BadRequest($"Папка {folderId} уже является корневой в проекте {folderEntity.ProjectId}");
      }

      if (folderEntity.TransactionId != null && folderEntity.TransactionId != tModel.TransactionId && folderEntity.SessionId != tModel.SessionId)
      {
        return BadRequest("Другая транзакция уже начата");
      }

      // End Checks

      switch (folderEntity.TransactionAction)
      {
        case TransactionAction.Adding:
          folderEntity.ProjectId = projectId;
          m_DbContext.Update(folderEntity);
          break;
        case TransactionAction.Modifying:
          {
            var tFl = await m_DbContext.Folders.SingleOrDefaultAsync(p =>
              p.OriginalId == folderEntity.Id.ToString());
            if (tFl == null)
            {
              return BadRequest();
            }

            tFl.ProjectId = projectId;
            m_DbContext.Update(tFl);
            break;
          }

        case null:
        case TransactionAction.Removing:
          {
            var tSubFolder = folderEntity.CopyModelForTransaction();
            tModel.PassTransactionData(tSubFolder, TransactionAction.Modifying);
            tSubFolder.TransactionAction = TransactionAction.Modifying;
            folderEntity.ProjectId = projectId;

            if (folderEntity.TransactionAction != TransactionAction.Removing)
            {
              tModel.PassTransactionData(folderEntity, TransactionAction.Modifying);
              await m_DbContext.AddAsync(tSubFolder);
            }

            break;
          }
      }

      try
      {
        await m_DbContext.SaveChangesAsync();
      }
      catch (Exception)
      {
        //todo: logger
        return BadRequest("Ошибка во время транзакции");
      }

      return Ok();
    }

    /// <summary>
    /// Исключение папки из проекта
    /// </summary>
    /// <param name="folderId">Id папки</param>
    /// <param name="tModel">Данные о транзакции</param>
    /// <returns></returns>
    [HttpPut]
    [Route("excludeFromProject/{folderId}")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.OK)]
    public async Task<IActionResult> ExcludeFolderFromProjectAsync(Guid folderId, TransactionDetailsViewModel tModel)
    {
      // Checks

      if (!TryValidateModel(tModel))
      {
        return BadRequest("Некорректные данные транзакции");
      }

      var folderEntity = await m_DbContext.Folders
        .SingleOrDefaultAsync(f => f.Id == folderId);
      if (folderEntity == null)
      {
        return NotFound("Папка не найдена");
      }

      if (folderEntity.ProjectId == null)
      {
        return BadRequest($"Папка {folderId} не является корневой");
      }

      if (folderEntity.TransactionId != null && folderEntity.TransactionId != tModel.TransactionId && folderEntity.SessionId != tModel.SessionId)
      {
        return BadRequest("Другая транзакция уже начата");
      }

      // End Checks

      switch (folderEntity.TransactionAction)
      {
        case TransactionAction.Adding:
          folderEntity.ProjectId = null;
          m_DbContext.Update(folderEntity);
          break;
        case TransactionAction.Modifying:
          {
            var tFl = await m_DbContext.Folders.SingleOrDefaultAsync(p =>
              p.OriginalId == folderEntity.Id.ToString());
            if (tFl == null)
            {
              return BadRequest();
            }

            tFl.ProjectId = null;
            m_DbContext.Update(tFl);
            break;
          }

        case null:
        case TransactionAction.Removing:
          {
            var tSubFolder = folderEntity.CopyModelForTransaction();
            tModel.PassTransactionData(tSubFolder, TransactionAction.Modifying);
            tSubFolder.TransactionAction = TransactionAction.Modifying;
            folderEntity.ProjectId = null;

            if (folderEntity.TransactionAction != TransactionAction.Removing)
            {
              tModel.PassTransactionData(folderEntity, TransactionAction.Modifying);
              await m_DbContext.AddAsync(tSubFolder);
            }

            break;
          }
      }

      try
      {
        await m_DbContext.SaveChangesAsync();
      }
      catch (Exception)
      {
        //todo: logger
        return BadRequest("Ошибка во время транзакции");
      }

      return Ok();
    }

    /// <summary>
    /// Включение элемента в папку
    /// </summary>
    /// <param name="folderId">Id папки</param>
    /// <param name="model">Данные элемента</param>
    /// <returns></returns>
    [HttpPut]
    [Route("includeItem/{folderId}")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.OK)]
    public async Task<IActionResult> IncludeItemToFolderAsync(Guid folderId, [FromBody] FolderItemCreateViewModel model)
    {
      // Checks

      if (!TryValidateModel(model))
      {
        return BadRequest("Некорректные данные");
      }

      if (!string.IsNullOrEmpty(model.ItemType) &&
          !m_Settings.Value.AllowedFolderTypes.AllowedTypes.Exists(t =>
            string.Equals(t, model.ItemType, StringComparison.CurrentCultureIgnoreCase)))
      {
        return BadRequest($"\"{model.ItemType}\" — неразрешенный тип для привязки к папке");
      }

      var folderEntity = await m_DbContext.Folders
        .Include(f => f.Items)
        .SingleOrDefaultAsync(f => f.Id == folderId);
      if (folderEntity == null)
      {
        return NotFound("Папка не найдена");
      }

      if (folderEntity.Items?.FirstOrDefault(i => i.ItemRealId == model.ItemRealId) != null)
      {
        return BadRequest($"В папке {folderEntity} уже содержится элемент {model.ItemRealId}");
      }

      if (folderEntity.TransactionId != null && folderEntity.TransactionId != model.TransactionId && folderEntity.SessionId != model.SessionId)
      {
        return BadRequest("Другая транзакция уже начата");
      }

      // End Checks

      var tFolderItem = model.GetModel(folderId);
      tFolderItem.TransactionAction = TransactionAction.Adding;
      await m_DbContext.FolderItems.AddAsync(tFolderItem);

      try
      {
        await m_DbContext.SaveChangesAsync();
      }
      catch (Exception)
      {
        //todo: logger
        return BadRequest("Ошибка во время транзакции");
      }

      return Ok();
    }

    /// <summary>
    /// Удаление элемента из папки
    /// </summary>
    /// <param name="folderId">Id папки</param>
    /// <param name="itemRealId">Настоящий Id элемента</param>
    /// <param name="tModel">Данные о транзакции</param>
    /// <returns></returns>
    [HttpPut]
    [Route("excludeItem/{folderId}")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.OK)]
    public async Task<IActionResult> ExcludeItemFromFolderAsync(Guid folderId, string itemRealId, TransactionDetailsViewModel tModel)
    {
      // Checks

      if (string.IsNullOrEmpty(itemRealId))
      {
        return BadRequest("Некорректные данные");
      }

      if (!TryValidateModel(tModel))
      {
        return BadRequest("Некорректные данные транзакции");
      }

      var folderEntity = await m_DbContext.Folders
        .Include(f => f.Items)
        .SingleOrDefaultAsync(f => f.Id == folderId);
      if (folderEntity == null)
      {
        return NotFound("Папка не найдена");
      }

      var itemEntity = await m_DbContext.FolderItems
        .SingleOrDefaultAsync(f => f.ItemRealId == itemRealId);
      if (itemEntity == null)
      {
        return NotFound("Элемент не найден");
      }

      if (folderEntity.Items?.Count == null || itemEntity.FolderId != folderEntity.Id)
      {
        return BadRequest($"Папка {folderId} не включает в себя элемент {itemRealId}");
      }

      if (folderEntity.TransactionId != null && folderEntity.TransactionId != tModel.TransactionId && folderEntity.SessionId != tModel.SessionId ||
          itemEntity.TransactionId != null && itemEntity.TransactionId != tModel.TransactionId && itemEntity.SessionId != tModel.SessionId)
      {
        return BadRequest("Другая транзакция уже начата");
      }

      // End Checks

      tModel.PassTransactionData(itemEntity, TransactionAction.Removing);


      try
      {
        // await m_DbContext.AddAsync(tItem);
        await m_DbContext.SaveChangesAsync();
      }
      catch (Exception)
      {
        //todo: logger
        return BadRequest("Ошибка во время транзакции");
      }

      return Ok();
    }

    /// <summary>
    /// Получение массива id папок, привязанных к папке с id == <paramref name="folderId"/>
    /// </summary>
    /// <param name="folderId">Id папки</param>
    /// <returns></returns>
    [HttpGet]
    [Route("getIncludedFolders/{folderId}")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(FolderOutViewModel[]), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<FolderOutViewModel[]>> GetIncludedFoldersAsync(Guid folderId)
    {
      // Checks

      var folderEntity = await m_DbContext.Folders
        .Include(f => f.SubFolders)
        .SingleOrDefaultAsync(f => f.Id == folderId);
      if (folderEntity == null)
      {
        return NotFound("Папка не найдена");
      }

      if (folderEntity.TransactionId != null &&
          (folderEntity.TransactionAction == TransactionAction.Adding ||
          folderEntity.TransactionAction == TransactionAction.Modifying))
      {
        return NotFound("Папка недоступна");
      }

      // End Checks

      return Ok(
        folderEntity.SubFolders != null
          ? folderEntity.SubFolders
            .Where(subFolder => subFolder.TransactionId == null ||
                                subFolder.TransactionAction != TransactionAction.Adding &&
                                subFolder.TransactionAction != TransactionAction.Modifying)
            .Select(subFolder => new FolderOutViewModel(subFolder)).ToArray()
          : new FolderOutViewModel[0]);
    }

    /// <summary>
    /// Получение массива id и типов элементов, привязанных к папке
    /// </summary>
    /// <param name="folderId">Id папки</param>
    /// <returns></returns>
    [HttpGet]
    [Route("getIncludedItems/{folderId}")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(FolderItemOutViewModel[]), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<FolderItemOutViewModel[]>> GetIncludedItemsAsync(Guid folderId)
    {
      // Checks

      var folderEntity = await m_DbContext.Folders
        .Include(f => f.Items)
        .SingleOrDefaultAsync(f => f.Id == folderId);
      if (folderEntity == null)
      {
        return NotFound();
      }

      if (folderEntity.TransactionId != null &&
          (folderEntity.TransactionAction == TransactionAction.Adding ||
           folderEntity.TransactionAction == TransactionAction.Modifying))
      {
        return NotFound("Папка недоступна");
      }

      // End Checks

      return Ok(
        folderEntity.Items != null
          ? folderEntity.Items
            .Where(item => item.TransactionId == null ||
                           item.TransactionAction != TransactionAction.Adding &&
                           item.TransactionAction != TransactionAction.Modifying)
            .Select(item =>
              new FolderItemOutViewModel(item))
            .ToArray()
          : new FolderItemOutViewModel[0]);
    }

    /// <summary>
    /// Получение списка элементов, привязанных к папке определенного типа
    /// </summary>
    /// <param name="folderId">Id папки</param>
    /// <param name="type">Тип элементов</param>
    /// <returns></returns>
    [HttpGet]
    [Route("getIncludedItemsByType")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType(typeof(FolderItemOutViewModel[]), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<FolderItemOutViewModel[]>> GetIncludedItemsByTypeAsync(Guid folderId, string type)
    {
      // Checks

      if (!string.IsNullOrEmpty(type) &&
          !m_Settings.Value.AllowedFolderTypes.AllowedTypes.Exists(t =>
            string.Equals(t, type, StringComparison.CurrentCultureIgnoreCase)))
      {
        return BadRequest($"\"{type}\" — неразрешенный тип для привязки к папке");
      }

      var folderEntity = await m_DbContext.Folders
        .Include(f => f.Items)
        .SingleOrDefaultAsync(f => f.Id == folderId);
      if (folderEntity == null)
      {
        return NotFound();
      }

      if (folderEntity.TransactionId != null &&
          (folderEntity.TransactionAction == TransactionAction.Adding ||
           folderEntity.TransactionAction == TransactionAction.Modifying))
      {
        return NotFound("Папка недоступна");
      }

      // End Checks

      return Ok(
        folderEntity.Items != null
          ? folderEntity.Items
            .Where(item => item.ItemType == type &&
                           (item.TransactionId == null ||
                            item.TransactionAction != TransactionAction.Adding &&
                            item.TransactionAction != TransactionAction.Modifying))
            .Select(item =>
              new FolderItemOutViewModel
              {
                ItemType = item.ItemType,
                ItemRealId = item.ItemRealId
              }).ToArray()
          : new FolderItemOutViewModel[0]);
    }

    /// <summary>
    /// Получение информации о папке
    /// </summary>
    /// <param name="id">Id папки</param>
    /// <returns></returns>
    [HttpGet]
    [Route("folder/{id:guid}")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(FolderOutViewModel), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<FolderOutViewModel>> GetFolderByIdAsync(Guid id)
    {
      // Checks
      var entity = await m_DbContext.Folders
        .Include(f => f.Items)
        .Include(f => f.SubFolders)
        .SingleOrDefaultAsync(f => f.Id == id);
      if (entity == null)
      {
        return NotFound();
      }

      if (entity.TransactionId != null &&
          (entity.TransactionAction == TransactionAction.Adding ||
           entity.TransactionAction == TransactionAction.Modifying))
      {
        return NotFound("Папка недоступна");
      }

      // End Checks

      return new FolderOutViewModel(entity);
    }

    /// <summary>
    /// Получение папок, входящих в проект
    /// </summary>
    /// <param name="projectId">Id проекта</param>
    /// <returns></returns>
    [HttpGet]
    [Route("foldersByProjectLazy/{id}")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType(typeof(List<FolderOutViewModel>), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<List<FolderOutViewModel>>> GetFoldersByProjectIdLazyAsync(string projectId)
    {
      if (string.IsNullOrEmpty(projectId))
      {
        return BadRequest();
      }

      var folders = await m_DbContext.Folders
        .Where(f => f.IsOriginal && f.TransactionId == null
                                 && f.ProjectId == projectId)
        .Select(f => new FolderOutViewModel(f))
        .ToListAsync();
      if (folders?.Count > 0)
      {
        return Ok(folders);
      }

      return NotFound();
    }

    /// <summary>
    /// Получение папок, подпапок и подэлементов, входящих в проект
    /// </summary>
    /// <param name="projectId">Id проекта</param>
    /// <returns></returns>
    [HttpGet]
    [Route("foldersByProjectGreedy/{id}")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType(typeof(List<FolderOutViewModel>), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<List<FolderOutViewModel>>> GetFoldersByProjectIdGreedyAsync(string projectId)
    {
      if (string.IsNullOrEmpty(projectId))
      {
        return BadRequest();
      }

      var folders = await m_DbContext.Folders
        .Where(f => f.IsOriginal && f.TransactionId == null
                                 && f.ProjectId == projectId)
        .Include(f => f.SubFolders)
        .Include(f => f.Items)
        .Select(f => new FolderOutViewModel(f))
        .ToListAsync();
      if (folders?.Count > 0)
      {
        return Ok(folders);
      }

      return NotFound();
    }

    /// <summary>
    /// Получение всех папок
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    [Route("folders")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType(typeof(List<FolderOutViewModel>), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<List<FolderOutViewModel>>> GetAllFoldersAsync()
    {
      var originalFolders = await m_DbContext.Folders
        .Where(f => f.IsOriginal && f.TransactionId == null)
        .ToListAsync();
      var originalFolderItems = await m_DbContext.FolderItems
        .Where(f => f.IsOriginal && f.TransactionId == null)
        .ToListAsync();

      var foldersAndItems = new List<FolderOutViewModel>();
      foreach (var folder in originalFolders)
      {
        folder.Items = originalFolderItems.Where(fi => fi.FolderId == folder.Id).ToList();
        folder.SubFolders = originalFolders.Where(f => f.SuperiorFolderId == folder.Id).ToList();
        foldersAndItems.Add(new FolderOutViewModel(folder));
      }

      return foldersAndItems;
    }

    /// <summary>
    /// Получение списка возможных типов для привязки к папке
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    [Route("allowedTypes")]
    [ProducesResponseType(typeof(List<string>), (int)HttpStatusCode.OK)]
    public ActionResult<List<string>> GetAllowedFolderItemTypes()
    {
      return m_Settings.Value.AllowedFolderTypes.AllowedTypes;
    }

    /// <summary>
    /// Удаление папки
    /// </summary>
    /// <param name="folderId">Id папки для удаления</param>
    /// <param name="tModel">Данные транзакции</param>
    /// <returns></returns>
    [HttpPut]
    [Route("delete/{folderId}")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.OK)]
    public async Task<IActionResult> RemoveFolderAsync(Guid folderId, TransactionDetailsViewModel tModel)
    {
      // Checks

      if (!TryValidateModel(tModel))
      {
        return BadRequest("Некорректные данные транзакции");
      }

      var folderEntity = await m_DbContext.Folders
        .Include(f => f.Items)
        .SingleOrDefaultAsync(f => f.Id == folderId);
      if (folderEntity == null)
      {
        return NotFound("Папка не найдена");
      }

      if (folderEntity.TransactionId != null && folderEntity.TransactionId != tModel.TransactionId && folderEntity.SessionId != tModel.SessionId)
      {
        return BadRequest("Другая транзакция уже начата");
      }

      if (folderEntity.Items?.Count > 0 || folderEntity.SubFolders?.Count > 0)
      {
        return BadRequest("Папка содержит элементы");
      }

      // End Checks

      tModel.PassTransactionData(folderEntity, TransactionAction.Removing);

      try
      {
        await m_DbContext.SaveChangesAsync();
      }
      catch (Exception)
      {
        //todo: logger
        return BadRequest("Ошибка во время транзакции");
      }

      return Ok();
    }

    /// <summary>
    /// Закрыть транзакцию и применить изменения
    /// </summary>
    /// <param name="tModel">Данные транзакциии</param>
    /// <returns></returns>
    [HttpPut]
    [Route("commitTransaction")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.OK)]
    public async Task<IActionResult> CommitTransactionAsync(TransactionDetailsViewModel tModel)
    {
      // Checks

      if (!TryValidateModel(tModel))
      {
        return BadRequest("Некорректные данные транзакции");
      }

      // End Checks

      var folders = await m_DbContext.Folders
        .Include(f => f.Items)
        .Where(f => f.TransactionId == tModel.TransactionId && f.SessionId == tModel.SessionId).ToListAsync();
      var folderItems = await m_DbContext.FolderItems
        .Where(fi => fi.TransactionId == tModel.TransactionId && fi.SessionId == tModel.SessionId).ToListAsync();

      if (folders?.Count == 0 && folderItems?.Count == 0)
      {
        return NotFound();
      }

      if (folders != null)
      {
        (bool result, var foldersToUpdate, var foldersToRemove) = TransactionLib.TransactionsSort.SortCommitEntitiesByActionsAsync(folders);
        if (!result)
        {
          return BadRequest();
        }

        if (foldersToUpdate?.Count > 0)
        {
          m_DbContext.UpdateRange(foldersToUpdate);
        }

        if (foldersToRemove?.Count > 0)
        {
          m_DbContext.RemoveRange(foldersToRemove);
        }
      }

      if (folderItems != null)
      {
        (bool result, var folderItemsToUpdate, var folderItemsToRemove) = TransactionLib.TransactionsSort.SortCommitEntitiesByActionsAsync(folderItems);
        if (!result)
        {
          return BadRequest();
        }

        if (folderItemsToUpdate?.Count > 0)
        {
          m_DbContext.UpdateRange(folderItemsToUpdate);
        }

        if (folderItemsToRemove?.Count > 0)
        {
          m_DbContext.RemoveRange(folderItemsToRemove);
        }
      }

      try
      {
        await m_DbContext.SaveChangesAsync();
      }
      catch (Exception)
      {
        //todo: logger
        return BadRequest();
      }

      return Ok();
    }

    /// <summary>
    /// Отменить транзакцию и откатить изменения
    /// </summary>
    /// <param name="tModel">Данные транзакциии</param>
    /// <returns></returns>
    [HttpPut]
    [Route("cancelTransaction")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.OK)]
    public async Task<IActionResult> CancelTransactionAsync(TransactionDetailsViewModel tModel)
    {
      // Checks

      if (!TryValidateModel(tModel))
      {
        return BadRequest("Некорректные данные транзакции");
      }

      // End Checks

      var folders = await m_DbContext.Folders
        .Include(f => f.Items)
        .Where(f => f.TransactionId == tModel.TransactionId && f.SessionId == tModel.SessionId).ToListAsync();
      var folderItems = await m_DbContext.FolderItems
        .Where(fi => fi.TransactionId == tModel.TransactionId && fi.SessionId == tModel.SessionId).ToListAsync();

      if (folders?.Count == 0 && folderItems?.Count == 0)
      {
        return NotFound();
      }

      if (folders != null)
      {
        (bool result, var foldersToUpdate, var foldersToRemove) = TransactionLib.TransactionsSort.GetEntitiesForCancelTransactionAsync(folders);
        if (!result)
        {
          return BadRequest();
        }

        if (foldersToUpdate?.Count > 0)
        {
          m_DbContext.UpdateRange(foldersToUpdate);
        }

        if (foldersToRemove?.Count > 0)
        {
          m_DbContext.RemoveRange(foldersToRemove);
        }
      }

      if (folderItems != null)
      {
        (bool result, var folderItemsToUpdate, var folderItemsToRemove) = TransactionLib.TransactionsSort.GetEntitiesForCancelTransactionAsync(folderItems);
        if (!result)
        {
          return BadRequest();
        }

        if (folderItemsToUpdate?.Count > 0)
        {
          m_DbContext.UpdateRange(folderItemsToUpdate);
        }

        if (folderItemsToRemove?.Count > 0)
        {
          m_DbContext.RemoveRange(folderItemsToRemove);
        }
      }

      try
      {
        await m_DbContext.SaveChangesAsync();
      }
      catch (Exception)
      {
        //todo: logger
        return BadRequest();
      }

      return Ok();
    }


    /// <summary>
    /// Проверка на петлю: является ли где-либо папка с id == <paramref name="subFolderId"/> нижестоящей у папки <paramref name="folderId"/>.
    /// </summary>
    /// <param name="folderId">Id проверяемой папка</param>
    /// <param name="subFolderId">Id нижестоящей папки</param>
    /// <returns>true если <paramref name="subFolderId"/> — id нижнстоящей папки</returns>
    [NonAction]
    public async Task<bool> DownLoopCheckAsync(
      Guid folderId, Guid subFolderId)
    {
      var folder = await m_DbContext.Folders
        .SingleOrDefaultAsync(f => f.Id == folderId);

      if (folder.SubFolders == null) { return false; }

      if (folder.SubFolders.FirstOrDefault(f => f.Id == subFolderId) != null)
      {
        return true;
      }

      bool result = false;
      foreach (var folderSubFolder in folder.SubFolders)
      {
        result = await DownLoopCheckAsync(
          folderSubFolder.Id, subFolderId);
        if (result)
        {
          break;
        }
      }

      return result;
    }
  }
}