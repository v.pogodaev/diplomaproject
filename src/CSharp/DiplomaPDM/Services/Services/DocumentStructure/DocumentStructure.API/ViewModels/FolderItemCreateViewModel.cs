﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using DocumentStructure.API.Models;
using TransactionLib.Enums;
using TransactionLib.ViewModels.Abstract;

namespace DocumentStructure.API.ViewModels
{
  /// <summary>
  /// Данные элемента папки
  /// </summary>
  public class FolderItemCreateViewModel : TransactableCreatingViewModel
  {
    /// <summary>
    /// Настоящий Id элемента из его сервиса
    /// </summary>
    [Required]
    [MaxLength(40)]
    public string ItemRealId { get; set; }

    /// <summary>
    /// Тип объекта
    /// </summary>
    [MaxLength(40)]
    [Required]
    public string ItemType { get; set; }

    public FolderItem GetModel(Guid folderId) => new FolderItem
    {
      ItemType = ItemType,
      FolderId = folderId,
      ItemRealId = ItemRealId,
      TransactionId = TransactionId,
      SessionId = SessionId,
      UserId = UserId,
    };
  }
}
