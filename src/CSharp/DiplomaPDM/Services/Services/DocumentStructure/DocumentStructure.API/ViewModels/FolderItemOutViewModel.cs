﻿using DocumentStructure.API.Models;

namespace DocumentStructure.API.ViewModels
{
  /// <summary>
  /// Элемент папки
  /// </summary>
  public class FolderItemOutViewModel
  {
    public FolderItemOutViewModel()
    {
    }

    public FolderItemOutViewModel(FolderItem item)
    {
      ItemRealId = item.ItemRealId;
      ItemType = item.ItemType;
    }

    /// <summary>
    /// Настоящий Id (из его микросервиса) элемента
    /// </summary>
    public string ItemRealId { get; set; }

    /// <summary>
    /// Тип элемента
    /// </summary>
    public string ItemType { get; set; }
  }
}
