﻿using System;
using System.ComponentModel.DataAnnotations;
using DocumentStructure.API.Models;
using TransactionLib.Enums;
using TransactionLib.Models.Abstract;
using TransactionLib.ViewModels.Abstract;

namespace DocumentStructure.API.ViewModels
{
  public class FolderCreateViewModel : TransactableCreatingViewModel
  {
    /// <summary>
    /// Обозначение папки
    /// </summary>
    [Required(ErrorMessage = "Folder Sign can't be null")]
    public string Sign { get; set; }

    /// <summary>
    /// Имя папки
    /// </summary>
    public string Name { get; set; }

    public Folder GetModel() => new Folder
    {
      Sign = Sign,
      Name = Name,
      TransactionId = TransactionId,
      SessionId = SessionId,
      UserId = UserId
    };
  }
}
