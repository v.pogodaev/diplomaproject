﻿using System;
using System.Collections.Generic;
using System.Linq;
using DocumentStructure.API.Models;

namespace DocumentStructure.API.ViewModels
{
  /// <summary>
  /// Папка
  /// </summary>
  public class FolderOutViewModel
  {
    public FolderOutViewModel() { }

    public FolderOutViewModel(Folder folder)
    {
      Id = folder.Id;
      Sign = folder.Sign;
      Name = folder.Name;
      ProjectId = folder.ProjectId;
      SuperiorFolderId = folder.SuperiorFolderId;
      SubFolderIds = folder.SubFolders?.Select(f => f.Id).ToList();
      Items = folder.Items?.Select(i => new FolderItemOutViewModel(i)).ToList();
    }

    /// <summary>
    /// Id папки
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// Обозначение папки
    /// </summary>
    public string Sign { get; set; }

    /// <summary>
    /// Имя папки
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Id проекта, в который входит папка (если есть)
    /// </summary>
    public string ProjectId { get; set; }

    /// <summary>
    /// Id вышестоящей папки (если есть)
    /// </summary>
    public Guid? SuperiorFolderId { get; set; }

    /// <summary>
    /// Список Id нижестоящих папок
    /// </summary>
    public ICollection<Guid> SubFolderIds { get; set; }

    /// <summary>
    /// Список настоящих Id (из их микросервисов) элементов, входящих в папку
    /// </summary>
    public ICollection<FolderItemOutViewModel> Items { get; set; }
  }
}
