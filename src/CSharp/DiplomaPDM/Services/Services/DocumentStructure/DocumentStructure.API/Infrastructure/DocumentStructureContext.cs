﻿using DocumentStructure.API.Models;
using Microsoft.EntityFrameworkCore;

namespace DocumentStructure.API.Infrastructure
{
  public class DocumentStructureContext : DbContext
  {
    public DocumentStructureContext()
    {
    }

    public DocumentStructureContext(DbContextOptions<DocumentStructureContext> options) : base(options)
    {
      //Database.EnsureDeleted();
      Database.EnsureCreated();
    }


    public virtual DbSet<Folder> Folders { get; set; }
    public virtual DbSet<FolderItem> FolderItems { get; set; }
  }
}
