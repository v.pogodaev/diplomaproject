﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TransactionLib.Models.Abstract;

namespace DocumentStructure.API.Models
{
  public class Folder : TransactableModel
  {
    /// <summary>
    /// Обозначение 
    /// </summary>
    [Required]
    [MaxLength(30)]
    public string Sign { get; set; }

    /// <summary>
    /// Наименование
    /// </summary>
    [Required]
    [MaxLength(255)]
    public string Name { get; set; }

    /// <summary>
    /// Id проекта, в котором находится папка (только если папка в корне)
    /// </summary>
    [MaxLength(40)]
    public string ProjectId { get; set; }

    /// <summary>
    /// Id вышестоящей папки
    /// </summary>
    public Guid? SuperiorFolderId { get; set; }

    /// <summary>
    /// Вышестоящая папка
    /// </summary>
    [ForeignKey(nameof(SuperiorFolderId))]
    public virtual Folder SuperiorFolder { get; set; }

    /// <summary>
    /// Список входящих папок
    /// </summary>
    public virtual ICollection<Folder> SubFolders { get; set; }

    /// <summary>
    /// Список входящих в папку документов
    /// </summary>
    public virtual ICollection<FolderItem> Items { get; set; }


    public Folder CopyModel() => new Folder
    {
      Items = Items,
      TransactionId = TransactionId,
      UserId = UserId,
      TransactionAction = TransactionAction,
      SessionId = SessionId,
      Name = Name,
      ProjectId = ProjectId,
      Sign = Sign,
      SubFolders = SubFolders,
      SuperiorFolder = SuperiorFolder,
      SuperiorFolderId = SuperiorFolderId
    };

    public Folder CopyModelForTransaction()
    {
      var tModel = CopyModel();
      tModel.IsOriginal = false;
      tModel.OriginalId = Id.ToString();
      return tModel;
    }


    public override void GetCommitData(TransactableModel commitData)
    {
      if (commitData.IsOriginal) { return; }

      if (!(commitData is Folder data)) { return; }

      Items = data.Items;
      TransactionId = data.TransactionId;
      UserId = data.UserId;
      TransactionAction = data.TransactionAction;
      SessionId = data.SessionId;
      Name = data.Name;
      ProjectId = data.ProjectId;
      Sign = data.Sign;
      SubFolders = data.SubFolders;
      SuperiorFolder = data.SuperiorFolder;
      SuperiorFolderId = data.SuperiorFolderId;
    }
  }
}
