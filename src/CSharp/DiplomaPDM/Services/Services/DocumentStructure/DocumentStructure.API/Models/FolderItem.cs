﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TransactionLib.Models.Abstract;

namespace DocumentStructure.API.Models
{
  /// <summary>
  /// Объект, входящий в папку
  /// </summary>
  public class FolderItem : TransactableModel
  {
    /// <summary>
    /// Тип объекта
    /// NOTE: возможно стоит сделать обязательным
    /// </summary>
    [MaxLength(40)]
    public string ItemType { get; set; }

    /// <summary>
    /// Настоящий id объекта, из его базы
    /// </summary>
    [Required]
    [MaxLength(40)]
    public string ItemRealId { get; set; }

    /// <summary>
    /// Внешний ключ к <see cref="Folder"/>
    /// </summary>
    public Guid FolderId { get; set; }

    /// <summary>
    /// Навигационное свойство к папке
    /// </summary>
    [ForeignKey(nameof(FolderId))]
    public virtual Folder Folder { get; set; }


    public FolderItem CopyModel() => new FolderItem
    {
      Folder = Folder,
      FolderId = FolderId,
      ItemRealId = ItemRealId,
      ItemType = ItemType,
      SessionId = SessionId,
      TransactionAction = TransactionAction,
      TransactionId = TransactionId,
      UserId = UserId
    };

    /// <inheritdoc cref="TransactableModel.GetCommitData" />
    public override void GetCommitData(TransactableModel commitData)
    {
      if (commitData.IsOriginal) { return; }

      if (!(commitData is FolderItem data)) { return; }

      TransactionId = data.TransactionId;
      UserId = data.UserId;
      TransactionAction = data.TransactionAction;
      SessionId = data.SessionId;
      Folder = data.Folder;
      FolderId = data.FolderId;
      ItemRealId = data.ItemRealId;
      ItemType = data.ItemType;
    }
  }
}
