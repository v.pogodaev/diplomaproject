﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DocumentStructure.API
{
  public class AppSettings
  {
    public AllowedFolderTypes AllowedFolderTypes { get; set; }
  }

  public class AllowedFolderTypes
  {
    public string DefaultValue { get; set; }
    public List<string> AllowedTypes { get; set; }
  }
}
