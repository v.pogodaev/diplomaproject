﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using TransactionHeaders.API.Models.Enums;

namespace TransactionHeaders.API.Models
{
  public class Transaction
  {
    /// <summary>
    /// Id транзакции
    /// </summary>
    [Key]
    public Guid Id { get; set; }

    //todo: задать определенную длину и мб индексировать
    /// <summary>
    /// Id пользователя
    /// </summary>
    public string UserId { get; set; }

    /// <summary>
    /// Id сессии
    /// </summary>
    public string SessionId { get; set; }

    /// <summary>
    /// Время открытия транзакции в UTC
    /// </summary>
    public DateTime OpenTransactionTimeUTC { get; set; }

    /// <summary>
    /// Время закрытия транзакции в UTC
    /// </summary>
    public DateTime? CloseTransactionTimeUTC { get; set; }

    /// <summary>
    /// Действие по транзакции
    /// </summary>
    public TransactionAction? TransactionAction { get; set; }
  }
}
