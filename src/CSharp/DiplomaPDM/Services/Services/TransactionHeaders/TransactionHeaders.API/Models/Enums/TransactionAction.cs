﻿namespace TransactionHeaders.API.Models.Enums
{
  public enum TransactionAction
  {
    Canceled = 0,
    Done
  }
}
