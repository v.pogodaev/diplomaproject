﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace TransactionHeaders.API.ViewModel
{
  /// <summary>
  /// Даннные, необходимые для открытия транзакции
  /// </summary>
  public class TransactionOpenViewModel
  {
    /// <summary>
    /// Id пользователя
    /// </summary>
    [Required]
    public string UserId { get; set; }

    /// <summary>
    /// Id сессии
    /// </summary>
    [Required]
    public string SessionId { get; set; }
  }
}
