﻿using Microsoft.EntityFrameworkCore;
using TransactionHeaders.API.Models;

namespace TransactionHeaders.API.Infrastructure
{
  public class TransactionContext : DbContext
  {
    public TransactionContext(DbContextOptions<TransactionContext> options) : base(options)
    {
      //Database.EnsureDeleted();
      Database.EnsureCreated();
    }

    public virtual DbSet<Transaction> Transactions { get; set; }
  }
}
