﻿using System;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using TransactionHeaders.API.Infrastructure;
using TransactionHeaders.API.Models;
using TransactionHeaders.API.Models.Enums;
using TransactionHeaders.API.ViewModel;

namespace TransactionHeaders.API.Controllers
{
  /// <summary>
  /// Контроллер заголовков транзакций
  /// </summary>
  [Route("api/v1/[controller]")]
  [ApiController]
  public class TransactionController : ControllerBase
  {
    private readonly TransactionContext m_TransactionContext;

    public TransactionController(TransactionContext transactionContext)
    {
      m_TransactionContext = transactionContext;
    }


    /// <summary>
    /// Открытие новой транзакции
    /// </summary>
    /// <param name="model">Данные, необходимые для открытия транзакции</param>
    /// <returns></returns>
    [HttpPost]
    [Route("open")]
    [ProducesResponseType((int) HttpStatusCode.BadRequest)]
    [ProducesResponseType(typeof(Transaction), (int) HttpStatusCode.Created)]
    public async Task<IActionResult> OpenTransactionAsync([FromBody] TransactionOpenViewModel model)
    {
      if (!TryValidateModel(model))
      {
        return BadRequest("Некорректные данные");
      }

      var oldTransaction = await m_TransactionContext.Transactions.SingleOrDefaultAsync(t =>
        t.UserId == model.UserId && t.CloseTransactionTimeUTC == null && t.TransactionAction == null);

      if (oldTransaction != null)
      {
        return BadRequest("У пользователя имеются открытые транзакции");
      }

      var entity = await m_TransactionContext.Transactions.AddAsync(new Transaction
      {
        UserId = model.UserId,
        SessionId = model.SessionId,
        OpenTransactionTimeUTC = DateTime.Now.ToUniversalTime()
      });
      await m_TransactionContext.SaveChangesAsync();

      return CreatedAtAction(nameof(GetTransactionByIdAsync), new {id = entity.Entity.Id}, entity.Entity);
    }

    /// <summary>
    /// Закрытие транзакции по id транзакции
    /// </summary>
    /// <param name="id">Id транзакции</param>
    /// <returns></returns>
    [HttpPut]
    [Route("closeByTransactionId/{id:guid}")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.OK)]
    public async Task<IActionResult> CloseTransactionByTransactionId(Guid id)
    {
      var transaction = await m_TransactionContext.Transactions.SingleOrDefaultAsync(t =>
        t.Id == id);
      if (transaction == null)
      {
        return NotFound();
      }

      if (transaction.CloseTransactionTimeUTC != null || transaction.TransactionAction != null)
      {
        return BadRequest(
          $"Транзакция уже была закрыта со статусом {((transaction.TransactionAction == TransactionAction.Canceled) ? "Откат" : "Закрыта")} ");
      }

      transaction.TransactionAction = TransactionAction.Done;
      transaction.CloseTransactionTimeUTC = DateTime.Now.ToUniversalTime();
      m_TransactionContext.Update(transaction);
      await m_TransactionContext.SaveChangesAsync();

      return Ok();
    }

    /// <summary>
    /// Закрытие транзакции по id пользователя
    /// </summary>
    /// <param name="id">Id пользователя</param>
    /// <returns></returns>
    [HttpPut]
    [Route("closeByUserId/{id}")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.OK)]
    public async Task<IActionResult> CloseTransactionByUserId(string id)
    {
      if (string.IsNullOrEmpty(id))
      {
        return BadRequest("Неверные данные");
      }

      var transaction = await m_TransactionContext.Transactions.SingleOrDefaultAsync(t =>
        t.UserId == id && t.CloseTransactionTimeUTC == null && t.TransactionAction == null);
      if (transaction == null)
      {
        return NotFound();
      }

      transaction.TransactionAction = TransactionAction.Done;
      transaction.CloseTransactionTimeUTC = DateTime.Now.ToUniversalTime();
      m_TransactionContext.Update(transaction);
      await m_TransactionContext.SaveChangesAsync();

      return Ok();
    }

    /// <summary>
    /// Откат транзакции по id транзакции
    /// </summary>
    /// <param name="id">Id транзакции</param>
    /// <returns></returns>
    [HttpPut]
    [Route("cancelByTransactionId/{id:guid}")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.OK)]
    public async Task<IActionResult> CancelTransactionByTransactionIdAsync(Guid id)
    {
      var transaction = await m_TransactionContext.Transactions.SingleOrDefaultAsync(t =>
        t.Id == id);
      if (transaction == null)
      {
        return NotFound();
      }

      if (transaction.CloseTransactionTimeUTC != null || transaction.TransactionAction != null)
      {
        return BadRequest(
          $"Транзакция уже была закрыта со статусом {((transaction.TransactionAction == TransactionAction.Canceled) ? "Откат" : "Закрыта")} ");
      }

      transaction.TransactionAction = TransactionAction.Canceled;
      transaction.CloseTransactionTimeUTC = DateTime.Now.ToUniversalTime();
      m_TransactionContext.Update(transaction);
      await m_TransactionContext.SaveChangesAsync();

      return Ok();
    }

    /// <summary>
    /// Откат транзакции по id пользователя
    /// </summary>
    /// <param name="id">Id пользователя</param>
    /// <returns></returns>
    [HttpPut]
    [Route("cancelByUserId/{id}")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.OK)]
    public async Task<IActionResult> CancelTransactionByUserIdAsync(string id)
    {
      var transaction = await m_TransactionContext.Transactions.SingleOrDefaultAsync(t =>
        t.UserId == id && t.CloseTransactionTimeUTC == null && t.TransactionAction == null);
      if (transaction == null)
      {
        return NotFound();
      }

      transaction.TransactionAction = TransactionAction.Canceled;
      transaction.CloseTransactionTimeUTC = DateTime.Now.ToUniversalTime();
      m_TransactionContext.Update(transaction);
      await m_TransactionContext.SaveChangesAsync();

      return Ok();
    }


    /// <summary>
    /// Получение информации открытой транзакции (если таковая имеется) по id пользователя
    /// </summary>
    /// <param name="id">Id пользователя</param>
    /// <returns></returns>
    [HttpGet]
    [Route("transactionByUser/{id}")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.OK)]
    public async Task<IActionResult> GetUserOpenedTransactionByUserIdAsync(string id)
    {
      if (string.IsNullOrEmpty(id))
      {
        return BadRequest("Неверные данные");
      }

      var transaction = await m_TransactionContext.Transactions.SingleOrDefaultAsync(t =>
        t.UserId == id && t.CloseTransactionTimeUTC == null && t.TransactionAction == null);
      if (transaction == null)
      {
        return NotFound();
      }

      return Ok(transaction);
    }

    /// <summary>
    /// Получение информации о транзакции
    /// </summary>
    /// <param name="id">Id транзакции</param>
    /// <returns></returns>
    [HttpGet]
    [Route("transaction/{id:guid}")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(Transaction), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> GetTransactionByIdAsync(Guid id)
    {
      var transaction = await m_TransactionContext.Transactions.SingleOrDefaultAsync(t =>
        t.Id == id);
      if (transaction == null)
      {
        return NotFound();
      }

      return Ok(transaction);
    }
  }
}