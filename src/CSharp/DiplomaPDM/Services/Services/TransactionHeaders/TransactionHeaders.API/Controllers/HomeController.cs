﻿using Microsoft.AspNetCore.Mvc;

namespace TransactionHeaders.API.Controllers
{
  public class HomeController : ControllerBase
  {
    public IActionResult Index()
    {
      return new RedirectResult("~/swagger");
    }
  }
}