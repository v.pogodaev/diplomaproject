﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using Project.API.Infrastructure;
using Project.API.ViewModels;
using TransactionLib.Enums;
using TransactionLib.ViewModels;

namespace Project.API.Controllers
{
  /// <summary>
  /// Контроллер проектов
  /// </summary>
  [Route("api/v1/[controller]")]              // это путь к контроллеру, т.е. будет что-то типа localhost:порт/api/v1/Project/...
  [ApiController]
  public class ProjectController : ControllerBase
  {
    private readonly ProjectContext m_ProjectContext;
    private readonly IOptions<AppSettings> m_Settings;

    public ProjectController(ProjectContext projectContext, IOptions<AppSettings> settings)
    {
      m_ProjectContext = projectContext;
      m_Settings = settings;
    }

    /// <summary>
    /// Создание проекта 
    /// </summary>
    /// <param name="newProject">Входные данные</param>
    /// <returns></returns>
    [Route("create")]
    [HttpPost]
    [ProducesResponseType(typeof(ProjectOutViewModel), (int)HttpStatusCode.Created)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    public async Task<IActionResult> CreateProjectAsync(
      [FromBody] ProjectCreateViewModel newProject)
    {
      // Checks

      if (!TryValidateModel(newProject))
      {
        return BadRequest("Некорректные данные");
      }

      // End Checks

      var item = newProject.GetModel();
      item.TransactionAction = TransactionAction.Adding;

      var entity = await m_ProjectContext.Projects.AddAsync(item);

      await m_ProjectContext.SaveChangesAsync();

      return CreatedAtAction(nameof(GetProjectByIdAsync), new { id = entity.Entity.Id }, entity.Entity);
    }

    /// <summary>
    /// Удаление проекта по id
    /// </summary>
    /// <param name="id">Id проекта</param>
    /// <param name="tModel">Данные транзакции</param>
    /// <returns></returns>
    [Route("delete/{id}")]
    [HttpPut] // при httpdelete нельзя передать транзакционные данные
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.OK)]
    public async Task<IActionResult> DeleteProjectByIdAsync(Guid id, TransactionDetailsViewModel tModel)
    {
      // Checks

      if (!TryValidateModel(tModel))
      {
        return BadRequest("Некорректные данные транзакции");
      }

      var project = await m_ProjectContext.Projects
        .Include(p => p.ProjectItems)
        .SingleOrDefaultAsync(p => p.Id == id);
      if (project == null)
      {
        return NotFound();
      }
      if (project.ProjectItems?.Count > 0)
      {
        return BadRequest("У проекта есть присоединенные элементы. Он не может быть удален");
      }

      if (project.TransactionId != null && project.TransactionId != tModel.TransactionId && project.SessionId != tModel.SessionId)
      {
        return BadRequest("Другая транзакция уже начата");
      }

      // End Checks

      tModel.PassTransactionData(project, TransactionAction.Removing);
      m_ProjectContext.Projects.Update(project);
      // зачем нам копия удаляемого объекта?
      //var tProject = project.CopyModelForTransaction(); 
      //tModel.PassTransactionData(tProject, TransactionAction.Removing);
      //tProject.TransactionAction = TransactionAction.Removing;

      try
      {
        //await m_ProjectContext.AddAsync(tProject);
        await m_ProjectContext.SaveChangesAsync();
      }
      catch (Exception)
      {
        //todo: logger
        return BadRequest("Ошибка во время транзакции");
      }

      return Ok();
    }

    /// <summary>
    /// Редактирование проекта
    /// </summary>
    /// <param name="model">Новые данные проекта</param>
    /// <returns></returns>
    [Route("update")]
    [HttpPut]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType(typeof(ProjectOutViewModel), (int)HttpStatusCode.Created)]
    public async Task<IActionResult> UpdateProjectAsync([FromBody]ProjectModifyViewModel model)
    {
      // Checks

      if (!TryValidateModel(model))
      {
        return BadRequest("Некорректные данные");
      }

      var projectEntity = await m_ProjectContext.Projects.SingleOrDefaultAsync(p => p.Id == model.Id);

      if (projectEntity == null)
      {
        return NotFound();
      }

      // если ничего не изменилось, обновлять не будем. можно ли как-то записать без сравнения всех свойств?
      if (projectEntity.Name == model.Name && projectEntity.Sign == model.Sign)
      {
        return CreatedAtAction(nameof(GetProjectByIdAsync), new { id = projectEntity.Id }, projectEntity);
      }

      if (projectEntity.TransactionId != null && projectEntity.TransactionId != model.TransactionId &&
          projectEntity.SessionId != model.SessionId)
      {
        return BadRequest("Другая транзакция уже начата");
      }

      // End Checks

      switch (projectEntity.TransactionAction)
      {
        case TransactionAction.Adding:
          projectEntity.Name = model.Name;
          projectEntity.Sign = model.Sign;
          m_ProjectContext.Projects.Update(projectEntity);
          break;
        case TransactionAction.Modifying:
          {
            var tPr = await m_ProjectContext.Projects.SingleOrDefaultAsync(p =>
              p.OriginalId == projectEntity.Id.ToString());
            if (tPr == null)
            {
              return BadRequest();
            }

            tPr.Name = model.Name;
            tPr.Sign = model.Sign;

            m_ProjectContext.Update(tPr);
            break;
          }

        case null:
        case TransactionAction.Removing:
          {
            var tProject = projectEntity.CopyModelForTransaction();

            model.PassTransactionData(tProject, TransactionAction.Modifying);

            tProject.Name = model.Name;
            tProject.Sign = model.Sign;

            await m_ProjectContext.AddAsync(tProject);

            if (projectEntity.TransactionAction != TransactionAction.Removing)
            {
              model.PassTransactionData(projectEntity, TransactionAction.Modifying);
              m_ProjectContext.Projects.Update(projectEntity);
            }

            break;
          }
      }

      try
      {
        await m_ProjectContext.SaveChangesAsync();
      }
      catch (Exception)
      {
        //todo: logger
        return BadRequest("Ошибка во время транзакции");
      }

      return CreatedAtAction(nameof(GetProjectByIdAsync), new { id = projectEntity.Id }, projectEntity);
    }

    /// <summary>
    /// Включение элемента в проект
    /// </summary>
    /// <param name="model">Данные элемента</param>
    /// <returns></returns>
    [HttpPut]
    [Route("includeItem")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.OK)]
    public async Task<IActionResult> IncludeItemToProjectAsync([FromBody] ProjectItemCreateViewModel model)
    {
      // Checks

      if (!TryValidateModel(model))
      {
        return BadRequest("Некорректные данные");
      }

      if (!string.IsNullOrEmpty(model.ItemType) &&
          !m_Settings.Value.AllowedProjectTypes.AllowedTypes.Exists(t =>
            string.Equals(t, model.ItemType, StringComparison.CurrentCultureIgnoreCase)))
      {
        return BadRequest($"\"{model.ItemType}\" — неразрешенный тип для привязки к проекту");
      }

      var projectEntity = await m_ProjectContext.Projects
        .Include(p => p.ProjectItems)
        .SingleOrDefaultAsync(f => f.Id == model.ProjectId);
      if (projectEntity == null)
      {
        return NotFound("Проект не найден");
      }

      if (projectEntity.ProjectItems?.FirstOrDefault(i => i.ItemRealId == model.ItemRealId) != null)
      {
        return BadRequest($"В проекте {model.ProjectId} уже содержится элемент {model.ItemRealId}");
      }

      if (projectEntity.TransactionId != null && projectEntity.TransactionId != model.TransactionId &&
          projectEntity.SessionId != model.SessionId)
      {
        return BadRequest("Другая транзакция уже начата");
      }
      // End Checks

      var tProjectItem = model.GetModel();
      model.PassTransactionData(tProjectItem, TransactionAction.Adding);

      await m_ProjectContext.ProjectItems.AddAsync(tProjectItem);

      try
      {
        await m_ProjectContext.SaveChangesAsync();
      }
      catch (Exception)
      {
        //todo: logger
        return BadRequest("Ошибка во время транзакции");
      }

      return Ok();
    }

    /// <summary>
    /// Получение массива id и типов элементов, привязанных к проекту
    /// </summary>
    /// <param name="projectId">Id проекта</param>
    /// <returns></returns>
    [HttpGet]
    [Route("getIncludedItems/{projectId}")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType(typeof(ProjectItemOutViewModel[]), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<ProjectItemOutViewModel[]>> GetIncludedItemsAsync(Guid projectId)
    {
      // Checks
      var projectEntity = await m_ProjectContext.Projects
        .Include(f => f.ProjectItems)
        .SingleOrDefaultAsync(f => f.Id == projectId);
      if (projectEntity == null)
      {
        return NotFound();
      }

      if (projectEntity.TransactionId != null &&
          (projectEntity.TransactionAction == TransactionAction.Adding ||
           projectEntity.TransactionAction == TransactionAction.Modifying))
      {
        return NotFound("Проект недоступен");
      }


      // End Checks

      return Ok(
        projectEntity.ProjectItems != null
          ? projectEntity.ProjectItems
            .Where(item => item.TransactionId == null ||
                           item.TransactionAction != TransactionAction.Adding &&
                           item.TransactionAction != TransactionAction.Modifying)
            .Select(item => new ProjectItemOutViewModel(item))
            .ToArray()
          : new ProjectItemOutViewModel[0]);
    }

    /// <summary>
    /// Удаление элемента из проекта
    /// </summary>
    /// <param name="projectId">Id проекта</param>
    /// <param name="itemRealId">Настоящий Id элемента</param>
    /// <param name="tModel">Данные о транзакции</param>
    /// <returns></returns>
    [HttpPut] // при httpdelete нельзя передать транзакционные данные
    [Route("excludeItem")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.OK)]
    public async Task<IActionResult> ExcludeItemFromProjectAsync(Guid projectId, string itemRealId, TransactionDetailsViewModel tModel)
    {
      // Checks

      if (string.IsNullOrEmpty(itemRealId))
      {
        return BadRequest("Некорректные данные");
      }

      if (!TryValidateModel(tModel))
      {
        return BadRequest("Некорректные данные транзакции");
      }

      var projectEntity = await m_ProjectContext.Projects
        .Include(f => f.ProjectItems)
        .SingleOrDefaultAsync(f => f.Id == projectId);
      if (projectEntity == null)
      {
        return NotFound("Проект не найден");
      }

      var itemEntity = await m_ProjectContext.ProjectItems
        .SingleOrDefaultAsync(f => f.ItemRealId == itemRealId);
      if (itemEntity == null)
      {
        return NotFound("Элемент не найден");
      }

      if (projectEntity.ProjectItems?.Count == null || itemEntity.ProjectId != projectEntity.Id)
      {
        return BadRequest($"Проект {projectId} не включает в себя элемент {itemRealId}");
      }

      if (projectEntity.TransactionId != null && projectEntity.TransactionId != tModel.TransactionId &&
            projectEntity.SessionId != tModel.SessionId ||
          itemEntity.TransactionId != null && itemEntity.TransactionId != tModel.TransactionId &&
            itemEntity.SessionId != tModel.SessionId)
      {
        return BadRequest("Другая транзакция уже начата");
      }

      tModel.PassTransactionData(itemEntity, TransactionAction.Removing);

      try
      {
        m_ProjectContext.Update(itemEntity);
        await m_ProjectContext.SaveChangesAsync();
      }
      catch (Exception)
      {
        //todo: logger
        return BadRequest("Ошибка во время транзакции");
      }

      return Ok();
    }

    /// <summary>
    /// Получение списка элементов, привязанных к проекту определенного типа
    /// </summary>
    /// <param name="projectId">Id папки</param>
    /// <param name="type">Тип элементов</param>
    /// <returns></returns>
    [HttpGet]
    [Route("getIncludedItemsByType")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType(typeof(ProjectItemOutViewModel[]), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<ProjectItemOutViewModel[]>> GetIncludedItemsByTypeAsync(
      Guid projectId, string type)
    {
      // Checks

      if (!string.IsNullOrEmpty(type) &&
          !m_Settings.Value.AllowedProjectTypes.AllowedTypes.Exists(t =>
            string.Equals(t, type, StringComparison.CurrentCultureIgnoreCase)))
      {
        return BadRequest($"\"{type}\" — неразрешенный тип для привязки к проекту");
      }

      var project = await m_ProjectContext.Projects
        .Include(f => f.ProjectItems)
        .SingleOrDefaultAsync(f => f.Id == projectId);
      if (project == null)
      {
        return NotFound();
      }

      if (project.TransactionId != null &&
          (project.TransactionAction == TransactionAction.Adding ||
           project.TransactionAction == TransactionAction.Modifying))
      {
        return NotFound("Проект недоступен");
      }

      // End Checks

      return Ok(
        project.ProjectItems != null
          ? project.ProjectItems
            .Where(i => i.ItemType == type && (i.TransactionId == null ||
                                               i.TransactionAction != TransactionAction.Adding &&
                                               i.TransactionAction != TransactionAction.Modifying))
            .Select(i => new ProjectItemOutViewModel(i))
            .ToArray()
          : new ProjectItemOutViewModel[0]);
    }

    /// <summary>
    /// Получение списка возможных типов для привязки к проекту
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    [Route("allowedTypes")]
    [ProducesResponseType(typeof(List<string>), (int)HttpStatusCode.OK)]
    public ActionResult<List<string>> GetAllowedProjectItemTypes()
    {
      return m_Settings.Value.AllowedProjectTypes.AllowedTypes;
    }

    /// <summary>
    /// Получение проекта по id
    /// </summary>
    /// <param name="id">Id проекта</param>
    /// <returns></returns>
    [HttpGet]
    [Route("project/{id:guid}")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(ProjectOutViewModel), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<ProjectOutViewModel>> GetProjectByIdAsync(Guid id)
    {
      // Checks

      var entity = await m_ProjectContext.Projects
        .Include(p => p.ProjectItems)
        .SingleOrDefaultAsync(p => p.Id == id);

      if (entity == null)
      {
        return NotFound();
      }

      if (entity.TransactionId != null &&
          (entity.TransactionAction == TransactionAction.Adding ||
           entity.TransactionAction == TransactionAction.Modifying))
      {
        return NotFound("Папка недоступна");
      }

      // End Checks

      return Ok(new ProjectOutViewModel(entity));
    }

    /// <summary>
    /// Получение всех проектов без вложений
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    [Route("lazyProjects")]
    [ProducesResponseType(typeof(List<ProjectOutViewModel>), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<List<ProjectOutViewModel>>> GetAllProjectsAsync()
    {
      return Ok(
        await m_ProjectContext.Projects
        .Where(f => f.IsOriginal && f.TransactionId == null)
        .ToListAsync());
    }

    /// <summary>
    /// Закрыть транзакцию и применить изменения
    /// </summary>
    /// <param name="tModel">Данные транзакциии</param>
    /// <returns></returns>
    [HttpPut]
    [Route("commitTransaction")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.OK)]
    public async Task<IActionResult> CommitTransactionAsync(TransactionDetailsViewModel tModel)
    {
      // Checks

      if (!TryValidateModel(tModel))
      {
        return BadRequest("Некорректные данные транзакции");
      }

      // End Checks

      var projects = await m_ProjectContext.Projects
        .Include(p => p.ProjectItems)
        .Where(p => p.TransactionId == tModel.TransactionId
                    && p.SessionId == tModel.SessionId)
        .ToListAsync();
      var projectItems = await m_ProjectContext.ProjectItems
        .Where(pi => pi.TransactionId == tModel.TransactionId
                     && pi.SessionId == tModel.SessionId)
        .ToListAsync();

      if (projects?.Count == 0 && projectItems?.Count == 0)
      {
        return NotFound();
      }

      if (projects != null)
      {
        (bool result, var projectsToUpdate, var projectsToRemove) = TransactionLib.TransactionsSort.SortCommitEntitiesByActionsAsync(projects);
        if (!result)
        {
          return BadRequest();
        }

        if (projectsToUpdate?.Count > 0)
        {
          m_ProjectContext.UpdateRange(projectsToUpdate);
        }

        if (projectsToRemove?.Count > 0)
        {
          m_ProjectContext.RemoveRange(projectsToRemove);
        }
      }

      if (projectItems != null)
      {
        (bool result, var projectItemsToUpdate, var projectItemsToRemove) = TransactionLib.TransactionsSort.SortCommitEntitiesByActionsAsync(projectItems);
        if (!result)
        {
          return BadRequest();
        }

        if (projectItemsToUpdate?.Count > 0)
        {
          m_ProjectContext.UpdateRange(projectItemsToUpdate);
        }

        if (projectItemsToRemove?.Count > 0)
        {
          m_ProjectContext.RemoveRange(projectItemsToRemove);
        }
      }

      try
      {
        await m_ProjectContext.SaveChangesAsync();
      }
      catch (Exception)
      {
        //todo: logger
        return BadRequest();
      }

      return Ok();
    }

    /// <summary>
    /// Отменить транзакцию и откатить изменения
    /// </summary>
    /// <param name="tModel">Данные транзакциии</param>
    /// <returns></returns>
    [HttpPut]
    [Route("cancelTransaction")]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.OK)]
    public async Task<IActionResult> CancelTransactionAsync(TransactionDetailsViewModel tModel)
    {
      // Checks

      if (!TryValidateModel(tModel))
      {
        return BadRequest("Некорректные данные транзакции");
      }

      // End Checks

      var projects = await m_ProjectContext.Projects
        .Include(p => p.ProjectItems)
        .Where(p => p.TransactionId == tModel.TransactionId
                    && p.SessionId == tModel.SessionId)
        .ToListAsync();
      var projectItems = await m_ProjectContext.ProjectItems
        .Where(pi => pi.TransactionId == tModel.TransactionId
                     && pi.SessionId == tModel.SessionId)
        .ToListAsync();

      if (projects?.Count == 0 && projectItems?.Count == 0)
      {
        return NotFound();
      }

      if (projects != null)
      {
        (bool result, var projectsToUpdate, var projectsToRemove) = TransactionLib.TransactionsSort.GetEntitiesForCancelTransactionAsync(projects);
        if (!result)
        {
          return BadRequest();
        }

        if (projectsToUpdate?.Count > 0)
        {
          m_ProjectContext.UpdateRange(projectsToUpdate);
        }

        if (projectsToRemove?.Count > 0)
        {
          m_ProjectContext.RemoveRange(projectsToRemove);
        }
      }

      if (projectItems != null)
      {
        (bool result, var projectItemsToUpdate, var projectItemsToRemove) = TransactionLib.TransactionsSort.GetEntitiesForCancelTransactionAsync(projectItems);
        if (!result)
        {
          return BadRequest();
        }

        if (projectItemsToUpdate?.Count > 0)
        {
          m_ProjectContext.UpdateRange(projectItemsToUpdate);
        }

        if (projectItemsToRemove?.Count > 0)
        {
          m_ProjectContext.RemoveRange(projectItemsToRemove);
        }
      }

      try
      {
        await m_ProjectContext.SaveChangesAsync();
      }
      catch (Exception)
      {
        //todo: logger
        return BadRequest();
      }

      return Ok();
    }
  }
}