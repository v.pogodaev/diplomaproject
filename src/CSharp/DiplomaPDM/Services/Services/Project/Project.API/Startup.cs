﻿using System;
using System.IO;
using System.Reflection;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpOverrides;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Project.API.Infrastructure;

namespace Project.API
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    public void ConfigureServices(IServiceCollection services)
    {
      services.AddCustomOptions(Configuration); // Добавляем наши настройки (пока не нужно)
      services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
      services.AddCustomDbContext(Configuration);
      services.AddSwagger();
    }

    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      app.UseForwardedHeaders(new ForwardedHeadersOptions
      {
        ForwardedHeaders = ForwardedHeaders.XForwardedFor | ForwardedHeaders.XForwardedProto
      });
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }
      else
      {
        app.UseHsts();
      }

      app.UseHttpsRedirection();
      // тут можно (нужно?) настроить маршруты
      app.UseMvc();

      // добавляем сваггер
      app.UseSwagger().UseSwaggerUI(c =>
      {
        c.SwaggerEndpoint("/swagger/v1/swagger.json", "Project API v1");
      });
    }
  }

  /// <summary>
  /// Класс с методами расширений для настройки сервисов
  /// </summary>
  public static class CustomServiceExtensionMethods
  {
    //public static IServiceCollection AddCustomMvc(this IServiceCollection services, IConfiguration configuration)
    //{
    //  // Поддержка mvc
    //  services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);

    //  // Добавляем сервисы для связи с нашими м-с

    //  return services;
    //}

    /// <summary>
    /// Добавление наших настроек
    /// </summary>
    /// <param name="services"></param>
    /// <param name="configuration"></param>
    /// <returns></returns>
    public static IServiceCollection AddCustomOptions(this IServiceCollection services, IConfiguration configuration)
    {
      services.AddOptions();
      services.Configure<AppSettings>(configuration);
      return services;
    }

    /// <summary>
    /// Настраиваем наш контекст БД
    /// </summary>
    /// <param name="services"></param>
    /// <param name="configuration"></param>
    /// <returns></returns>
    public static IServiceCollection AddCustomDbContext(this IServiceCollection services, IConfiguration configuration)
    {
      services.AddDbContext<ProjectContext>(options =>
      {
        options.UseSqlServer(configuration["ConnectionString"],
          sqlServerOptionsAction: sqlOptions =>
          {
            sqlOptions.MigrationsAssembly(typeof(Startup).GetTypeInfo().Assembly.GetName().Name);
            sqlOptions.EnableRetryOnFailure(maxRetryCount: 10, maxRetryDelay: TimeSpan.FromSeconds(30), errorNumbersToAdd: null);
          });
      });

      return services;
    }

    public static IServiceCollection AddSwagger(this IServiceCollection services)
    {
      services.AddSwaggerGen(options =>
      {
        options.DescribeAllEnumsAsStrings();
        options.SwaggerDoc("v1", new Swashbuckle.AspNetCore.Swagger.Info
        {
          Title = "DiplomaPDM - Project HTTP API",
          Version = "v1",
          Description = "Микросервис хранения проектов",
          TermsOfService = "..."
        });
        var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
        var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
        options.IncludeXmlComments(xmlPath);
      });

      return services;
    }
  }
}
