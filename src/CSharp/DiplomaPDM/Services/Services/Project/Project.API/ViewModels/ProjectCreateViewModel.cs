﻿using System.ComponentModel.DataAnnotations;
using TransactionLib.Enums;
using TransactionLib.ViewModels.Abstract;

namespace Project.API.ViewModels
{
  /// <summary>
  /// Данные, необходимые для создания
  /// </summary>
  public class ProjectCreateViewModel : TransactableCreatingViewModel
  {
    /// <summary>
    /// Обозначение проекта
    /// </summary>
    [Required(ErrorMessage = "Необходимо указать Обозначение проекта")]
    public string Sign { get; set; }

    /// <summary>
    /// Имя проекта
    /// </summary>
    public string Name { get; set; }

    public Models.Project GetModel() =>
      new Models.Project
      {
        Name = this.Name,
        Sign = this.Sign,
        TransactionId = TransactionId,
        SessionId = SessionId,
        UserId = UserId
      };

    public static ProjectCreateViewModel GetViewModel(Models.Project model) =>
      new ProjectCreateViewModel { Sign = model.Sign, Name = model.Name };
  }
}
