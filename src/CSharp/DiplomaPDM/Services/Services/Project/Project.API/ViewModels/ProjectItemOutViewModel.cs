﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Project.API.Models;

namespace Project.API.ViewModels
{
  /// <summary>
  /// Элемент проекта
  /// </summary>
  public class ProjectItemOutViewModel
  {
    public ProjectItemOutViewModel()
    {
    }

    public ProjectItemOutViewModel(ProjectItem item)
    {
      ItemRealId = item.ItemRealId;
      ItemType = item.ItemType;
    }

    /// <summary>
    /// Настоящий Id элемента из его микросервиса
    /// </summary>
    public string ItemRealId { get; set; }

    /// <summary>
    /// Тип объекта
    /// </summary>
    public string ItemType { get; set; }
  }
}
