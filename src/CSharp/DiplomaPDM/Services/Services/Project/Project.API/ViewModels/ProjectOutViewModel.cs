﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Project.API.ViewModels
{
  /// <summary>
  /// Проект
  /// </summary>
  public class ProjectOutViewModel
  {
    public ProjectOutViewModel() { }

    public ProjectOutViewModel(Models.Project project)
    {
      Id = project.Id;
      Sign = project.Sign;
      Name = project.Name;
      ProjectItems = new List<ProjectItemOutViewModel>();
      foreach (var projectProjectItem in project.ProjectItems)
      {
        ProjectItems.Add(new ProjectItemOutViewModel(projectProjectItem));
      }
    }

    /// <summary>
    /// Id проекта
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// Обозначение проекта
    /// </summary>
    public string Sign { get; set; }

    /// <summary>
    /// Имя проекта
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Список элементов, входящих в проект
    /// </summary>
    public ICollection<ProjectItemOutViewModel> ProjectItems { get; set; }
  }
}
