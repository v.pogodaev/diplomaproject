﻿using System;
using System.ComponentModel.DataAnnotations;
using TransactionLib.ViewModels.Abstract;

namespace Project.API.ViewModels
{
  /// <summary>
  /// Данные, необходимые для редактирования проекта
  /// </summary>
  public class ProjectModifyViewModel : TransactableCreatingViewModel
  {
    /// <summary>
    /// Id проекта
    /// </summary>
    [Required(ErrorMessage = "Необходимо указать Id проекта")]
    public Guid Id { get; set; }

    /// <summary>
    /// Новое обозначение проекта
    /// </summary>
    [Required(ErrorMessage = "У проекта должно быть обозначение")]
    public string Sign { get; set; }

    /// <summary>
    /// Новое имя проекта
    /// </summary>
    public string Name { get; set; }

    public Models.Project GetModel() =>
      new Models.Project { Name = this.Name, Sign = this.Sign };

    public static ProjectModifyViewModel GetViewModel(Models.Project model) =>
      new ProjectModifyViewModel { Id = model.Id, Sign = model.Sign, Name = model.Name };
  }
}
