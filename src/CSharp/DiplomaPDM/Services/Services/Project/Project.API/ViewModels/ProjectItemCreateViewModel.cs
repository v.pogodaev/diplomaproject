﻿using System;
using System.ComponentModel.DataAnnotations;
using Project.API.Models;
using TransactionLib.ViewModels.Abstract;

namespace Project.API.ViewModels
{
  /// <summary>
  /// Данные элемента проекта
  /// </summary>
  public class ProjectItemCreateViewModel : TransactableCreatingViewModel
  {
    /// <summary>
    /// Id проекта
    /// </summary>
    [Required]
    public Guid ProjectId { get; set; }

    /// <summary>
    /// Настоящий Id элемента из его сервиса
    /// </summary>
    [Required]
    [MaxLength(40)]
    public string ItemRealId { get; set; }

    /// <summary>
    /// Тип объекта
    /// </summary>
    [MaxLength(40)]
    [Required]
    public string ItemType { get; set; }

    public ProjectItem GetModel() => new ProjectItem { ItemType = ItemType, ProjectId = ProjectId, ItemRealId = ItemRealId };
  }
}
