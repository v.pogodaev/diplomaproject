﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.API
{
  public class LoggingEvents
  {
    public const int GENERATE_ITEMS = 1000;
    public const int LIST_ITEMS = 1001;
    public const int GET_ITEM = 1002;
    public const int INSERT_ITEM = 1003;
    public const int UPDATE_ITEM = 1004;
    public const int DELETE_ITEM = 1005;
    public const int COMMIT_TRANSACTION = 1006;
    public const int CANCEL_TRANSACTION = 1007;

    public const int BAD_PARAMETERS = 3000;
    public const int BAD_TRANSACTION_PARAMETERS = 3001;
    public const int TRANSACTION_ALREADY_STARTED = 3002;
    public const int ITEM_IN_TRANSACTION = 3003;
    public const int ITEM_NOT_INCLUDED = 3004;
    
    public const int GET_ITEM_NOT_FOUND = 4000;
    public const int UPDATE_ITEM_NOT_FOUND = 4001;
    public const int DELETE_ITEM_NOT_FOUND = 4002;
    public const int TRANSACTION_ITEM_NOT_FOUND = 4003;

    public const int ERROR = 5000;
    public const int ERROR_SAVE = 5001;
    public const int TRANSACTION_ERROR = 5002;
  }
}
