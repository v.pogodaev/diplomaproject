﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Project.API
{
  public class AppSettings
  {
    public AllowedProjectTypes AllowedProjectTypes { get; set; }
  }

  /// <summary>
  /// Разрешенные ти
  /// </summary>
  public class AllowedProjectTypes
  {
    public string DefaultValue { get; set; }
    public List<string> AllowedTypes { get; set; }
  }
}
