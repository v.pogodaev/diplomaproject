﻿using Microsoft.EntityFrameworkCore;
using Project.API.Models;

namespace Project.API.Infrastructure
{
  public class ProjectContext : DbContext
  {
    public ProjectContext()
    {

    }

    public ProjectContext(DbContextOptions<ProjectContext> options) : base(options)
    {
      //Database.EnsureDeleted();
      Database.EnsureCreated();
    }

    public virtual DbSet<Models.Project> Projects { get; set; }
    public virtual DbSet<ProjectItem> ProjectItems { get; set; }
  }
}
