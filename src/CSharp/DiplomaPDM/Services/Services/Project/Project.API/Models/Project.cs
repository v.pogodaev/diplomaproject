﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using TransactionLib.Models.Abstract;

namespace Project.API.Models
{
  /// <summary>
  /// Модель проекта
  /// </summary>
  public class Project : TransactableModel
  {
    /// <summary>
    /// Обозначение 
    /// </summary>
    [Required]
    [MaxLength(30)]
    public string Sign { get; set; }

    /// <summary>
    /// Наименование
    /// </summary>
    [MaxLength(255)]
    public string Name { get; set; }

    /// <summary>
    /// Объекты, входящие в проект
    /// </summary>
    public virtual ICollection<ProjectItem> ProjectItems { get; set; }


    /// <inheritdoc cref="TransactableModel.GetCommitData"/>
    public override void GetCommitData(TransactableModel model)
    {
      if (model.IsOriginal) { return; }

      if (!(model is Project data)) { return; }

      TransactionId = data.TransactionId;
      UserId = data.UserId;
      TransactionAction = data.TransactionAction;
      SessionId = data.SessionId;
      Name = data.Name;
      Sign = data.Sign;
      ProjectItems = data.ProjectItems;
    }

    public Project CopyModel() => new Project
    {
      Name = Name,
      ProjectItems = ProjectItems,
      Sign = Sign
    };

    public Project CopyModelForTransaction()
    {
      var tModel = CopyModel();
      tModel.IsOriginal = false;
      tModel.OriginalId = Id.ToString();
      return tModel;
    }
  }
}
