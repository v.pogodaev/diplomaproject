﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using TransactionLib.Models.Abstract;

namespace Project.API.Models
{
  /// <summary>
  /// Элемент, входящий в проект
  /// </summary>
  public class ProjectItem : TransactableModel
  {
    /// <summary>
    /// Настоящий Id элемента из его сервиса
    /// </summary>
    [Required]
    [MaxLength(40)]
    public string ItemRealId { get; set; }

    /// <summary>
    /// Тип объекта
    /// </summary>
    [MaxLength(40)]
    [Required]
    // NOTE: список типов задается в appsettings
    public string ItemType { get; set; }

    /// <summary>
    /// Внешний ключ к <see cref="ProjectItem.Project"/>
    /// </summary>
    public Guid ProjectId { get; set; }

    /// <summary>
    /// Навигационное свойство к папке
    /// </summary>
    [ForeignKey(nameof(ProjectId))]
    public virtual Project Project { get; set; }

    public override void GetCommitData(TransactableModel model)
    {
      if (model.IsOriginal) { return; }

      if (!(model is ProjectItem data)) { return; }

      TransactionId = data.TransactionId;
      UserId = data.UserId;
      TransactionAction = data.TransactionAction;
      SessionId = data.SessionId;
      ItemType = data.ItemType;
      ItemRealId = data.ItemRealId;
      ProjectId = data.ProjectId;
    }

    public ProjectItem CopyModel() => new ProjectItem
    {
      ItemType = ItemType,
      ItemRealId = ItemRealId,
      ProjectId = ProjectId,
    };

    public ProjectItem CopyModelForTransaction()
    {
      var tModel = CopyModel();
      tModel.IsOriginal = false;
      tModel.OriginalId = Id.ToString();
      return tModel;
    }
  }
}
