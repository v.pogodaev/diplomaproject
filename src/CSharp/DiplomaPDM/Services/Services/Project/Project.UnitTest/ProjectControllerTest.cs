﻿//using Microsoft.EntityFrameworkCore;
//using Moq;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using EntityFrameworkCoreMock;
//using Microsoft.AspNetCore.Mvc;
//using Project.API;
//using Project.API.Controllers;
//using Project.API.Infrastructure;
//using Xunit;
//using Microsoft.Extensions.Options;
//using Project.API.Models;
//using Project.API.ViewModels;

//namespace Project.UnitTest
//{
//  public class ProjectControllerTest
//  {
//    private readonly ProjectController m_Controller;
//    private readonly DbSetMock<API.Models.Project> m_DbSetProject;

//    public ProjectControllerTest()
//    {
//      var options = new Mock<IOptions<AppSettings>>();
//      //var dummyOptions = new DbContextOptionsBuilder<ProjectContext>().Options;
//      //var folders = MakeFolders();
//      var projects = MakeProjects();
//      var dbContext = new DbContextMock<ProjectContext>();
//      //m_DbSetFolder = dbContext.CreateDbSetMock(c => c.Folders, folders);
//      m_DbSetProject = dbContext.CreateDbSetMock(c => c.Projects, projects);

//      m_Controller = new ProjectController(dbContext.Object, options.Object);
//    }

//    #region GetProjectByIdAsync
//    [Theory]
//    [InlineData(2)]
//    public async Task GetProjectByIdAsyncTest(int folderId)
//    {
//      // Arrange
//      var entity = await m_DbSetProject.Object.SingleOrDefaultAsync(f => f.Id == folderId);
//      var entityVM = new ProjectOutViewModel(entity);

//      // Act
//      var result = await m_Controller.GetProjectByIdAsync(folderId);
//      var resObject = (OkObjectResult)result.Result;

//      // Assert
//      Assert.IsType<OkObjectResult>(resObject);

//      // workaround: не хочет напрямую сравнивать два экземпляра класса
//      var resProject = (ProjectOutViewModel)resObject.Value;
//      Assert.Equal(entityVM.Id, resProject.Id);
//      Assert.Equal(entityVM.Name, resProject.Name);
//      Assert.Equal(entityVM.Sign, resProject.Sign);
//      Assert.Equal(entityVM.ProjectItemIds, resProject.ProjectItemIds);
//    }

//    [Theory]
//    [InlineData(0)]
//    [InlineData(-1)]
//    public async Task GetProjectByIdAsync_BadRequestResult_BadId_Test(int id)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.GetProjectByIdAsync(id);

//      // Assert
//      Assert.IsType<BadRequestResult>(result.Result);
//    }

//    [Fact]
//    public async Task GetProjectByIdAsync_NotFound_Test()
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.GetProjectByIdAsync(15);

//      // Assert
//      Assert.IsType<NotFoundResult>(result.Result);
//    }
//    #endregion

//    #region CreateProjectAsync
//    [Fact]
//    public async Task CreateProjectAsync_BadRequest_BadSign_Test()
//    {
//      // Arrange

//      // Act
//      var project = new API.Models.Project { Id = 0, Name = "Name", Sign = "" };
//      var result = await m_Controller.CreateProjectAsync(ProjectCreateViewModel.GetViewModel(project));

//      // Assert
//      Assert.IsType<BadRequestObjectResult>(result);
//    }

//    [Fact]
//    public async Task CreateProjectAsync_BadRequest_NoBody_Test()
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.CreateProjectAsync(null);

//      // Assert
//      Assert.IsType<BadRequestResult>(result);
//    }

//    // NOTE:
//    //  не протестировать из-за [FromBody] скорее всего
//    //[Fact]
//    //public async Task CreateProjectAsyncTest()
//    //{
//    //  // Arrange

//    //  // Act
//    //  var project = new API.Models.Project { Id = 0, Name = "Name", Sign = "Test" };
//    //  var result = await m_Controller.CreateProjectAsync(ProjectViewModel.GetViewModel(project));

//    //  // Assert
//    //  Assert.IsType<CreatedAtActionResult>(result);
//    //}
//    #endregion

//    #region DeleteProject
//    [Theory]
//    [InlineData(0)]
//    [InlineData(-1)]
//    public async Task DeleteProjectByIdAsync_NotFound_Test(int id)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.DeleteProjectByIdAsync(id);

//      // Assert
//      Assert.IsType<NotFoundResult>(result);
//    }

//    [Theory]
//    [InlineData(2)]
//    [InlineData(3)]
//    public async Task DeleteProjectByIdAsync_ChildProjects_Test(int id)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.DeleteProjectByIdAsync(id);

//      // Assert
//      Assert.IsType<BadRequestObjectResult>(result);
//    }

//    [Theory]
//    [InlineData(1)]
//    [InlineData(4)]
//    public async Task DeleteProjectByIdAsync_Test(int id)
//    {
//      // Arrange

//      // Act
//      var result = await m_Controller.DeleteProjectByIdAsync(id);

//      // Assert
//      Assert.IsType<NoContentResult>(result);
//    }
//    #endregion

//    #region UpdateProjectAsync
//    [Fact]
//    public async Task UpdateProjectAsync_NotFound_Test()
//    {
//      // Arrange
//      var project = new API.Models.Project {Id = 100, Sign = "T1", Name = "Test"};
//      //project.Id = 105;

//      // Act
//      var result = await m_Controller.UpdateProjectAsync(ProjectCreateViewModel.GetViewModel(project));

//      // Assert
//      Assert.IsType<NotFoundResult>(result);
//    }

//    [Fact]
//    public async Task UpdateProjectAsync_Test()
//    {
//      // Arrange
//      var project = await m_DbSetProject.Object.FirstOrDefaultAsync();
//      string oldName = project.Name;
//      string newName = project.Name + "-1";
//      project.Name = newName;

//      // Act
//      var result = await m_Controller.UpdateProjectAsync(ProjectCreateViewModel.GetViewModel(project));

//      // Assert
//      Assert.IsType<CreatedAtActionResult>(result);
//      Assert.IsType<API.Models.Project>(((CreatedAtActionResult)result).Value);
//      Assert.Equal(((API.Models.Project) ((CreatedAtActionResult) result).Value).Name, newName);
//      Assert.NotEqual(((API.Models.Project)((CreatedAtActionResult)result).Value).Name, oldName);
//      //result.Result
//      //Assert.IsType<NotFoundResult>(result.Result);
//    }
//    #endregion

//    List<API.Models.Project> MakeProjects()
//    {
//      var projects = Enumerable.Range(1, 4).Select(i => new API.Models.Project
//      {
//        Id = i,
//        Name = $"Project {i}",
//        Sign = $"P{i}"
//      }).ToList();

//      projects[1].ProjectItems = new[]
//      {
//        new ProjectItem {Id = 1, ItemRealId = 0, ProjectId = projects[1].Id, Project = projects[1]},
//        new ProjectItem {Id = 2, ItemRealId = 1, ProjectId = projects[1].Id, Project = projects[1]},
//        new ProjectItem {Id = 3, ItemRealId = 4, ProjectId = projects[1].Id, Project = projects[1]},
//      };
//      projects[2].ProjectItems = new[]
//      {
//        new ProjectItem {Id = 4, ItemRealId = 5, ProjectId = projects[2].Id, Project = projects[2]},
//        new ProjectItem {Id = 5, ItemRealId = 3, ProjectId = projects[2].Id, Project = projects[2]},
//        new ProjectItem {Id = 6, ItemRealId = 7, ProjectId = projects[2].Id, Project = projects[2]},
//        new ProjectItem {Id = 7, ItemRealId = 8, ProjectId = projects[2].Id, Project = projects[2]},
//      };

//      return projects;
//    }
//  }
//}
