﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SPAWebApp.Server.Models.NonTransactable.FileService;
using SPAWebApp.Server.Services.Contracts;
using SPAWebApp.Shared.Models.Services.FileStorage;
using File = System.IO.File;

namespace ServiceRequestsTest.Controllers
{
  [Route("api/TEST/[controller]/[action]")]
  [ApiController]
  public class FileStorageServiceController : ControllerBase
  {
    private readonly IFileStorageService m_FileStorageService;

    public FileStorageServiceController(IFileStorageService fileStorageService)
    {
      m_FileStorageService = fileStorageService;
    }


    /// <summary>
    /// Скачать файл без информации
    /// </summary>
    /// <param name="fileId">Id файла</param>
    /// <returns></returns>
    [HttpGet]
    public async Task<IActionResult> DownloadFileAsync(string fileId)
    {
      try
      {
        var file = await m_FileStorageService.DownloadFileAsync(fileId);
        return File(file.FileData, file.MimeType, file.FileName);
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }
    
    /// <summary>
    /// Получить все файлы из системы
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ActionResult<List<FileInfo>>> GetAllFilesAsync()
    {
      try
      {
        return Ok(await m_FileStorageService.GetAllFilesAsync());
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Удалить файл
    /// </summary>
    /// <param name="id">id файла</param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> RemoveFileAsync(string id)
    {
      try
      {
        await m_FileStorageService.RemoveFileAsync(id);
        return Ok();
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Загрузить файл в систему
    /// </summary>
    /// <param name="file"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ActionResult<string>> UploadFileAsync([FromForm] UploadFileModel file)
    {
      try
      {
        return Ok(await m_FileStorageService.UploadFileAsync(file));
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Получить информацию о файле без самого файла
    /// </summary>
    /// <param name="fileId">id файла</param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ActionResult<FileInfo>> GetFileInfo(string fileId)
    {
      try
      {
        return Ok(await m_FileStorageService.GetFileInfo(fileId));
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }
  }
}
