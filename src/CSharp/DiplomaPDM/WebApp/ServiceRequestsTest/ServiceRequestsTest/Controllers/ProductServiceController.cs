﻿//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Threading.Tasks;
//using Microsoft.AspNetCore.Mvc;
//using SPAWebApp.Server.Models.Transactable;
//using SPAWebApp.Server.Models.Transactable.Product;
//using SPAWebApp.Server.Services.Contracts;
//using SPAWebApp.Shared.Models.Services.Product;

//namespace ServiceRequestsTest.Controllers
//{
//  public class ProductServiceController : ControllerBase
//  {
//    private IProductService m_ProductService;

//    public ProductServiceController(IProductService productService)
//    {
//      m_ProductService = productService;
//    }

//    /// <summary>
//    /// Отменить транзакцию и откатить изменения
//    /// </summary>
//    /// <param name="transaction">Данные транзакции</param>
//    /// <returns></returns>
//    [HttpPost]
//    public async Task<IActionResult> CancelTransactionAsync(TransactionInfo transaction)
//    {
//      try
//      {
//        await m_ProductService.CancelTransactionAsync(transaction);
//        return Ok();
//      }
//      catch (Exception e)
//      {
//        return BadRequest(e.Message);
//      }
//    }

//    /// <summary>
//    /// Закрыть транзакцию и применить изменения
//    /// </summary>
//    /// <param name="transaction">Данные транзакции</param>
//    /// <returns></returns>
//    [HttpPost]
//    public async Task<IActionResult> CommitTransactionAsync(TransactionInfo transaction)
//    {
//      try
//      {
//        await m_ProductService.CommitTransactionAsync(transaction);
//        return Ok();
//      }
//      catch (Exception e)
//      {
//        return BadRequest(e.Message);
//      }
//    }

//    /// <summary>
//    /// Создание нового изделия
//    /// </summary>
//    /// <param name="productCreateModel">Модель изделия</param>
//    /// <returns>Id изделия</returns>
//    [HttpPost]
//    public async Task<ActionResult<string>> CreateProductAsync(TProductCreateModel productCreateModel)
//    {
//      try
//      {
//        return Ok(await m_ProductService.CreateProductAsync(productCreateModel));
//      }
//      catch (Exception e)
//      {
//        return BadRequest(e.Message);
//      }
//    }


//    /// <summary>
//    /// Получение списка всех изделий
//    /// </summary>
//    /// <returns></returns>
//    Task<List<Product>> GetAllProductsAsync();

//    /// <summary>
//    /// Получение изделия по Id
//    /// </summary>
//    /// <param name="id">Id изделия</param>
//    /// <returns></returns>
//    Task<Product> GetProductByIdAsync(string id);

//    /// <summary>
//    /// Получение списка изделий по их ids
//    /// </summary>
//    /// <param name="ids">Id изделий</param>
//    /// <returns></returns>
//    Task<List<Product>> GetProductListByIdsAsync(string[] ids);

//    /// <summary>
//    /// Передача изделия на проверку
//    /// </summary>
//    /// <param name="productId">Id изделия</param>
//    /// <param name="transactionInfo">Данные транзакции</param>
//    /// <returns></returns>
//    Task PutInCheckAsync(string productId, TransactionInfo transactionInfo);

//    /// <summary>
//    /// Взятие изделия в работу
//    /// </summary>
//    /// <param name="productId">Id изделия</param>
//    /// <param name="transactionInfo">Данные транзакции</param>
//    /// <returns></returns>
//    Task GetInWorkAsync(string productId, TransactionInfo transactionInfo);

//    /// <summary>
//    /// Удалить изделие
//    /// </summary>
//    /// <param name="productRemoveModel"></param>
//    /// <returns></returns>
//    Task RemoveProductAsync(TProductRemoveModel productRemoveModel);

//    /// <summary>
//    /// Обновить информацию об изделии
//    /// </summary>
//    /// <param name="productUpdateModel">Модель изделия</param>
//    /// <returns></returns>
//    Task UpdateAsync(string productId, TProductUpdateModel productUpdateModel);

//    /// <summary>
//    /// Обновить состояние изделия
//    /// </summary>
//    /// <param name="productUpdateStateModel">Модель состояния</param>
//    /// <returns></returns>
//    Task UpdateStateAsync(TProductUpdateStateModel productUpdateStateModel);
//  }
//}
