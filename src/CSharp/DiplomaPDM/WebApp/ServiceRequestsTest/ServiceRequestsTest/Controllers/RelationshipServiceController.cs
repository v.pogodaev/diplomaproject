﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SPAWebApp.Server.Models.Transactable;
using SPAWebApp.Server.Models.Transactable.Relationship;
using SPAWebApp.Server.Services.Contracts;
using SPAWebApp.Shared.Models.Services.Relationship;

namespace ServiceRequestsTest.Controllers
{
  [Route("api/TEST/[controller]/[action]")]
  [ApiController]
  public class RelationshipServiceController : ControllerBase
  {
    private IRelationshipService m_RelationshipService;

    public RelationshipServiceController(IRelationshipService relationshipService)
    {
      m_RelationshipService = relationshipService;
    }

    ///// <summary>
    ///// Отменить транзакцию и откатить изменения
    ///// </summary>
    ///// <param name="transaction">Данные транзакции</param>
    ///// <returns></returns>
    //Task CancelTransactionAsync(TransactionInfo transaction);

    ///// <summary>
    ///// Закрыть транзакцию и применить изменения
    ///// </summary>
    ///// <param name="transaction">Данные транзакции</param>
    ///// <returns></returns>
    //Task CommitTransactionAsync(TransactionInfo transaction);

    /// <summary>
    /// Создать связь между изделием и документом
    /// </summary>
    /// <param name="relationship"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ActionResult<string>> CreateRelationshipAsync(TRelationshipCreateModel relationship)
    {
      try
      {
        return Ok(await m_RelationshipService.CreateRelationshipAsync(relationship));
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    ///// <summary>
    ///// Получить id связанных с изделием документов
    ///// </summary>
    ///// <param name="productId"></param>
    ///// <returns></returns>
    //Task<string[]> GetRelatedDocumentsByProductIdAsync(string productId);

    ///// <summary>
    ///// Получить id связанных с документов изделий
    ///// </summary>
    ///// <param name="documentId"></param>
    ///// <returns></returns>
    //Task<string[]> GetRelatedProductsByDocumentIdAsync(string documentId);

    ///// <summary>
    ///// Получить информацию об отношении по его id
    ///// </summary>
    ///// <param name="relationshipId"></param>
    ///// <returns></returns>
    //Task<Relationship> GetRelationshipByIdAsync(string relationshipId);

    ///// <summary>
    ///// Удалить связь
    ///// </summary>
    ///// <param name="relationshipId"></param>
    ///// <param name="transaction"></param>
    ///// <returns></returns>
    //Task RemoveRelationshipAsync(string relationshipId, TransactionInfo transaction);

    ///// <summary>
    ///// Изменить тип связи
    ///// </summary>
    ///// <param name="relationshipId"></param>
    ///// <param name="relationship"></param>
    ///// <returns></returns>
    //Task UpdateRelationshipTypeAsync(string relationshipId, TRelationshipUpdateModel relationship);
  }
}
