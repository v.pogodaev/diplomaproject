﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SPAWebApp.Server.Models.Transactable;
using SPAWebApp.Server.Models.Transactable.DocumentStructure;
using SPAWebApp.Server.Services.Contracts;
using SPAWebApp.Shared.Models.Services.DocumentStructure;

namespace ServiceRequestsTest.Controllers
{
  [Route("api/TEST/[controller]/[action]")]
  [ApiController]
  public class DocumentStructureService : ControllerBase
  {
    private readonly IDocumentStructureService m_DocumentStructureService;

    public DocumentStructureService(IDocumentStructureService documentStructureService)
    {
      m_DocumentStructureService = documentStructureService;
    }

    /// <summary>
    /// Создание новой папки
    /// </summary>
    /// <param name="folder">Новая папка</param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ActionResult<Folder>> CreateFolderAsync(TFolderCreateModel folder)
    {
      try
      {
        return Ok(await m_DocumentStructureService.CreateFolderAsync(folder));
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Включение папки с id == <paramref name="subFolderId"/> в папку c id == <paramref name="superFolderId"/>
    /// </summary>
    /// <param name="superFolderId">Id вышестоящей папки</param>
    /// <param name="subFolderId">Id папки-потомка</param>
    /// <param name="transaction">Данные о транзакции</param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> IncludeFolderToFolderAsync(string superFolderId, string subFolderId, TransactionInfo transaction)
    {
      try
      {
        await m_DocumentStructureService.IncludeFolderToFolderAsync(superFolderId, subFolderId, transaction);
        return Ok();
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Отсоединение папки с id == <paramref name="subFolderId"/> от папки c id == <paramref name="superFolderId"/>
    /// </summary>
    /// <param name="superFolderId">Id вышестоящей папки</param>
    /// <param name="subFolderId">Id папки-потомка</param>
    /// <param name="transaction">Данные о транзакции</param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> ExcludeFolderFromFolderAsync(Guid superFolderId, Guid subFolderId, TransactionInfo transaction)
    {
      try
      {
        await m_DocumentStructureService.ExcludeFolderFromFolderAsync(superFolderId, subFolderId, transaction);
        return Ok();
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Включение папки в проект
    /// </summary>
    /// <param name="folderId">Id папки</param>
    /// <param name="projectRealId">Настоящий (из его микросервиса) Id проекта</param>
    /// <param name="transaction">Данные о транзакции</param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> IncludeFolderToProjectAsync(Guid folderId, string projectRealId, TransactionInfo transaction)
    {
      try
      {
        await m_DocumentStructureService.IncludeFolderToProjectAsync(folderId, projectRealId, transaction);
        return Ok();
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Исключение папки из проекта
    /// </summary>
    /// <param name="folderId">Id папки</param>
    /// <param name="transaction">Данные о транзакции</param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> ExcludeFolderFromProjectAsync(Guid folderId, TransactionInfo transaction)
    {
      try
      {
        await m_DocumentStructureService.ExcludeFolderFromProjectAsync(folderId, transaction);
        return Ok();
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Включение элемента в папку
    /// </summary>
    /// <param name="folderId">Id папки</param>
    /// <param name="item">Данные элемента</param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> IncludeItemToFolderAsync(Guid folderId, TFolderItemCreateModel item)
    {
      try
      {
        await m_DocumentStructureService.IncludeItemToFolderAsync(folderId, item);
        return Ok();
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Удаление элемента из папки
    /// </summary>
    /// <param name="folderId">Id папки</param>
    /// <param name="itemRealId">Настоящий Id элемента</param>
    /// <param name="transaction">Данные о транзакции</param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> ExcludeItemFromFolderAsync(Guid folderId, string itemRealId, TransactionInfo transaction)
    {
      try
      {
        await m_DocumentStructureService.ExcludeItemFromFolderAsync(folderId, itemRealId, transaction);
        return Ok();
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Получение массива id папок, привязанных к папке с id == <paramref name="folderId"/>
    /// </summary>
    /// <param name="folderId">Id папки</param>
    /// <returns></returns>
    [HttpGet]
    public async Task<ActionResult<ICollection<Folder>>> GetIncludedFoldersAsync(Guid folderId)
    {
      try
      {
        return Ok(await m_DocumentStructureService.GetIncludedFoldersAsync(folderId));
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Получение массива id и типов элементов, привязанных к папке
    /// </summary>
    /// <param name="folderId">Id папки</param>
    /// <returns></returns>
    [HttpGet]
    public async Task<ActionResult<ICollection<FolderItem>>> GetIncludedItemsAsync(Guid folderId)
    {
      try
      {
        return Ok(await m_DocumentStructureService.GetIncludedItemsAsync(folderId));
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Получение списка элементов, привязанных к папке определенного типа
    /// </summary>
    /// <param name="folderId">Id папки</param>
    /// <param name="type">Тип элементов</param>
    /// <returns></returns>
    [HttpGet]
    public async Task<ActionResult<ICollection<FolderItem>>> GetIncludedItemsByTypeAsync(string folderId, string type)
    {
      try
      {
        return Ok(await m_DocumentStructureService.GetIncludedItemsByTypeAsync(folderId, type));
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Получение информации о папке
    /// </summary>
    /// <param name="id">Id папки</param>
    /// <returns></returns>
    [HttpGet]
    public async Task<ActionResult<Folder>> GetFolderByIdAsync(Guid id)
    {
      try
      {
        return Ok(await m_DocumentStructureService.GetFolderByIdAsync(id));
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Получение папок, входящих в проект
    /// </summary>
    /// <param name="projectId">Id проекта</param>
    /// <returns></returns>
    [HttpGet]
    public async Task<ActionResult<ICollection<Folder>>> GetFoldersByProjectIdLazyAsync(string projectId)
    {
      try
      {
        return Ok(await m_DocumentStructureService.GetFoldersByProjectIdLazyAsync(projectId));
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Получение папок, подпапок и подэлементов, входящих в проект
    /// </summary>
    /// <param name="projectId">Id проекта</param>
    /// <returns></returns>
    [HttpGet]
    public async Task<ActionResult<ICollection<Folder>>> GetFoldersByProjectIdGreedyAsync(string projectId)
    {
      try
      {
        return Ok(await m_DocumentStructureService.GetFoldersByProjectIdGreedyAsync(projectId));
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Получение всех папок
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ActionResult<ICollection<Folder>>> GetAllFoldersAsync()
    {
      try
      {
        return Ok(await m_DocumentStructureService.GetAllFoldersAsync());
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Получение списка возможных типов для привязки к папке
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ActionResult<ICollection<string>>> GetAllowedTypesAsync()
    {
      try
      {
        return Ok(await m_DocumentStructureService.GetAllowedTypesAsync());
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Удаление папки
    /// </summary>
    /// <param name="folderId">Id папки для удаления</param>
    /// <param name="transaction">Данные транзакции</param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> RemoveFolderByIdAsync(Guid folderId, TransactionInfo transaction)
    {
      try
      {
        await m_DocumentStructureService.RemoveFolderByIdAsync(folderId, transaction);
        return Ok();
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Закрыть транзакцию и применить изменения
    /// </summary>
    /// <param name="transaction">Данные транзакции</param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> CommitTransactionAsync(TransactionInfo transaction)
    {
      try
      {
        await m_DocumentStructureService.CommitTransactionAsync(transaction);
        return Ok();
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Отменить транзакцию и откатить изменения
    /// </summary>
    /// <param name="transaction">Данные транзакции</param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> CancelTransactionAsync(TransactionInfo transaction)
    {
      try
      {
        await m_DocumentStructureService.CancelTransactionAsync(transaction);
        return Ok();
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }
  }
}
