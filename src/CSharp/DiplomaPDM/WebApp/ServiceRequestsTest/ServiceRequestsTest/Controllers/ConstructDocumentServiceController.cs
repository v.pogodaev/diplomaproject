﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SPAWebApp.Server.Models.Transactable;
using SPAWebApp.Server.Models.Transactable.ConstructDocument;
using SPAWebApp.Server.Services.Contracts;
using SPAWebApp.Shared.Models.Services.ConstructDocument;

namespace ServiceRequestsTest.Controllers
{
  [Route("api/TEST/[controller]/[action]")]
  [ApiController]
  public class ConstructDocumentServiceController : ControllerBase
  {
    private readonly IConstructDocumentService m_ConstructDocumentService;

    public ConstructDocumentServiceController(IConstructDocumentService constructDocumentService)
    {
      m_ConstructDocumentService = constructDocumentService;
    }

    /// <summary>
    /// Отменить транзакцию и откатить изменения
    /// </summary>
    /// <param name="transaction">Данные транзакции</param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> CancelTransactionAsync(TransactionInfo transaction)
    {
      try
      {
        await m_ConstructDocumentService.CancelTransactionAsync(transaction);
        return Ok();
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Закрыть транзакцию и применить изменения
    /// </summary>
    /// <param name="transaction">Данные транзакции</param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> CommitTransactionAsync(TransactionInfo transaction)
    {
      try
      {
        await m_ConstructDocumentService.CommitTransactionAsync(transaction);
        return Ok();
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Получить данные документа по его id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet]
    public async Task<ActionResult<Document>> GetDocumentByIdAsync(string id)
    {
      try
      {
        return Ok(await m_ConstructDocumentService.GetDocumentByIdAsync(id));
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Изменить информации о документе
    /// </summary>
    /// <param name="documentModel"></param>
    /// <param name="documentId"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> UpdateDocumentAsync(TDocumentUpdateModel documentModel, string documentId)
    {
      try
      {
        await m_ConstructDocumentService.UpdateDocumentAsync(documentModel, documentId);
        return Ok();
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Изменить состояние документа
    /// </summary>
    /// <param name="stateModel"></param>
    /// <param name="documentId"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> ChangeStateAsync(TDocumentStateUpdateModel stateModel, string documentId)
    {
      try
      {
        await m_ConstructDocumentService.ChangeStateAsync(stateModel, documentId);
        return Ok();
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Проверить на возможность создания новой версии документа
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ActionResult<string>> CheckDocumentForCreateNewRevisionAsync(string id)
    {
      try
      {
        return Ok(await m_ConstructDocumentService.CheckDocumentForCreateNewRevisionAsync(id));
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Создать новый документ
    /// </summary>
    /// <param name="document"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ActionResult<string>> CreateDocumentAsync(TDocumentCreateModel document)
    {
      try
      {
        return Ok(await m_ConstructDocumentService.CreateDocumentAsync(document));
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Создать новую версию документа
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ActionResult<string>> CreateNewRevAsync(string id)
    {
      try
      {
        return Ok(await m_ConstructDocumentService.CreateNewRevAsync(id));
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Получить список документов
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ActionResult<List<Document>>> GetDocumentsAsync()
    {
      try
      {
        return Ok(await m_ConstructDocumentService.GetDocumentsAsync());
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Получить список документов, включая документы с открытыми транзакциями
    /// </summary>
    /// <returns></returns>
    [HttpGet]
    public async Task<ActionResult<List<Document>>> GetDocumentsWithTransactAsync()
    {
      try
      {
        return Ok(await m_ConstructDocumentService.GetDocumentsWithTransactAsync());
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Получить список документов по их id
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<ActionResult<List<Document>>> GetDocumentListByIdsAsync(string[] ids)
    {
      try
      {
        return Ok(await m_ConstructDocumentService.GetDocumentListByIdsAsync(ids));
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    /// <summary>
    /// Удалить документ
    /// </summary>
    /// <param name="transaction"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpPost]
    public async Task<IActionResult> RemoveDocumentAsync(TransactionInfo transaction, string id)
    {
      try
      {
        await m_ConstructDocumentService.RemoveDocumentAsync(transaction, id);
        return Ok();
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }
  }
}