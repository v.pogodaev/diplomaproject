﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using SPAWebApp.Server.Services.Concrete;
using SPAWebApp.Server.Services.Contracts;

namespace ServiceRequestsTest
{
  public class Startup
  {
    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    public IConfiguration Configuration { get; }

    // This method gets called by the runtime. Use this method to add services to the container.
    public void ConfigureServices(IServiceCollection services)
    {
      services.AddMvc().AddMvcOptions(options => options.EnableEndpointRouting = false);
      //services.AddHttpContextAccessor();
      //services.AddResponseCompression(opts =>
      //{
      //  opts.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(
      //    new[] { "application/octet-stream" });
      //});
      services.AddSwagger();
      services.AddHttpServices();
      services.AddLogging();
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IHostingEnvironment env)
    {
      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
      }
      else
      {
        app.UseHsts();
      }

      app.UseHttpsRedirection();
      app.UseSwagger().UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Test API v1"));
      app.UseMvc();
    }
  }

  public static class CustomServiceExtensionMethods
  {
    public static IServiceCollection AddHttpServices(this IServiceCollection services)
    {
      //todo: всякие политики для повтора
      services.AddHttpClient<IConstructDocumentService, ConstructDocumentService>();
      services.AddHttpClient<IFileStorageService, FileStorageService>();
      services.AddHttpClient<IProductService, ProductService>();
      services.AddHttpClient<IRelationshipService, RelationshipService>();

      services.AddHttpClient<ITransactionHeadersService, TransactionHeadersService>();
      services.AddHttpClient<IProjectService, ProjectService>();
      services.AddHttpClient<IDocumentStructureService, DocumentStructureService>();

      //Тут тоже пришлось ставить библиотечку для работы с файлами
      //services.AddScoped<IFileReaderService, FileReaderService>();

      // fakes
      //services.AddTransient<IConstructDocumentService, FakeConstructDocumentService>();
      //services.AddTransient<IFileStorageService, FileStorageService>();
      //services.AddTransient<IProductService, FakeProductService>();
      //services.AddTransient<IRelationshipService, FakeRelationshipService>();

      //services.AddTransient<ITransactionHeadersService, FakeTransactionHeadersService>();
      //services.AddTransient<IProjectService, FakeProjectService>();
      //services.AddTransient<IDocumentStructureService, FakeDocumentStructureService>();

      return services;
    }

    public static IServiceCollection AddSwagger(this IServiceCollection services)
    {
      services.AddSwaggerGen(options =>
      {
        options.DescribeAllEnumsAsStrings();
        options.SwaggerDoc("v1", new OpenApiInfo
        {
          Title = "DiplomaPDM - Document Structure HTTP API",
          Version = "v1",
          Description = "Тест API",
        });
        var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
        var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
        options.IncludeXmlComments(xmlPath);
      });

      return services;
    }
  }
}
