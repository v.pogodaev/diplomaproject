﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using SPAWebApp.Shared.Models;
using SPAWebApp.Shared.Models.Services.FileStorage;

namespace SPAWebApp.Shared.Services
{
  public interface IFileStorageService
  {
    //Task<string> UploadNewFile(MultipartFormDataContent File, string comment);

    Task<File> GetDataAsync(string Id);

    Task<List<File>> GetAllFilesAsync();

    Task RemoveFileAsync(string Id);


  }
}
