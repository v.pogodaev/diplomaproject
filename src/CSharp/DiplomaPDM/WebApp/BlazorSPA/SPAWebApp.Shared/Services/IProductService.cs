﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SPAWebApp.Shared.Models;
using SPAWebApp.Shared.Models.Services.Product;

namespace SPAWebApp.Shared.Services
{
  public interface IProductService
  {
    Task<List<Product>> GetProductListByIdsAsync(string[] ids);

    Task<List<Product>> GetAllProductsAsync();
  }
}
