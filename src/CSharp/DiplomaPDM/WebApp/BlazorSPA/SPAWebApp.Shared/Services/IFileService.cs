﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using SPAWebApp.Shared.Models;
using SPAWebApp.Shared.Models.Services.FileStorage;

namespace SPAWebApp.Shared.Services
{
  public interface IFileService
  {
    Task<List<File>> GetFileListByIdsAsync(string[] ids);

    Task<List<File>> GetAllFilesAsync();
  }
}
