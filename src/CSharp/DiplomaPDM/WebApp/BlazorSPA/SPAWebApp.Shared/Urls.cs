﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace SPAWebApp.Shared
{
  public static class Urls
  {
    public static class Project
    {
      public const string CONTROLLER = "api/projects";
      public const string CREATE_PROJECT = "create";
      public const string DELETE_PROJECT = "delete/{projectId}";
      public const string MODIFY_PROJECT = "update";
      public const string INCLUDE_ITEM = "includeItem";
      public const string GET_INCLUDED_ITEMS_BY_ID = "includedItems/{projectId}";
      public const string GET_INCLUDED_ITEMS_BY_TYPE = "includedItemsByType";
      public const string GET_ALLOWED_TYPES = "allowedTypes";
      public const string GET_PROJECT = "project/{projectId}";
      public const string GET_PROJECTS_LAZY = "lazyProjects";
      public const string COMMIT = "commitTransaction";
      public const string CANCEL_TRANSACTION = "cancelTransaction";

      public static string CreateProject() => $"{CONTROLLER}/{CREATE_PROJECT}";
      public static string DeleteProject(Guid projectId) => $"{CONTROLLER}/delete/{projectId}";
      public static string ModifyProject() => $"{CONTROLLER}/{MODIFY_PROJECT}";
      public static string GetIncludeItem() => $"{CONTROLLER}/{INCLUDE_ITEM}";
      public static string GetIncludedItems(Guid projectId) => $"{CONTROLLER}/includedItems/{projectId}";
      public static string GetIncludedItemsByType() => $"{CONTROLLER}/{GET_INCLUDED_ITEMS_BY_TYPE}";
      public static string GetAllowedTypes() => $"{CONTROLLER}/{GET_ALLOWED_TYPES}";
      public static string GetProject(Guid projectId) => $"{CONTROLLER}/project/{projectId}";
      public static string GetLazyProjects() => $"{CONTROLLER}/{GET_PROJECTS_LAZY}";
      public static string CommitTransaction() => $"{CONTROLLER}/{COMMIT}";
      public static string CancelTransaction() => $"{CONTROLLER}/{CANCEL_TRANSACTION}";
    }

    public static class DocumentStructure
    {
      public const string CONTROLLER = "api/folders";

      public const string GET_ALL_FOLDERS = "folders";
      public const string GET_FOLDER = "folder/{folderId}";
      public const string GET_INCLUDED_FOLDERS = "includedFolders/{folderId}";
      public const string GET_INCLUDED_DOCUMENTS = "includedDocuments/{folderId}";
      public const string DELETE_FOLDER = "delete/{folderId}";
      public const string CREATE_FOLDER = "create";
      public const string GET_DELETE_ABILITY = "isDeletable/{folderId}";
      public const string INCLUDE_ITEM_TO_FOLDER = "includeItem/{folderId}";


      public static string GetFolder(Guid folderId) => $"{CONTROLLER}/folder/{folderId}";
      public static string GetAllFolders() => $"{CONTROLLER}/{GET_ALL_FOLDERS}";
      public static string GetIncludedFolders(Guid folderId) => $"{CONTROLLER}/includedFolders/{folderId}";
      public static string GetIncludedDocuments(Guid folderId) => $"{CONTROLLER}/includedDocuments/{folderId}";
      public static string DeleteFolder(Guid folderId) => $"{CONTROLLER}/delete/{folderId}";
      public static string CreateFolder() => $"{CONTROLLER}/{CREATE_FOLDER}";
      public static string GetFolderDeleteAbility(Guid folderId) => $"{CONTROLLER}/isDeletable/{folderId}";
      public static string IncludeItemToFolder(string folderId) => $"{CONTROLLER}/includeItem/{folderId}";
    }

    public static class CAD
    {
      public const string CONTROLLER = "api/v1/cad";

      public const string GET_PROJECTS = "getProjects";

      public const string GET_FOLDERS_BY_PROJECT = "getFoldersByProject/{projectId}";
      public const string GET_FOLDERS_BY_FOLDER = "getFoldersByFolder/{folderId}";
      //public const string GET_FOLDER_ITEMS_BY_TYPE = "getFolderItemsByType";

      //public const string GET_PRODUCT_BY_ID = "getProductById/{productId}";
      public const string GET_PRODUCTS_BY_FOLDER = "getProductInfos/{folderId}";
      public const string TAKE_PRODUCT_IN_WORK = "takeProductInWork";
      public const string POST_PRODUCT_IN_CHECK = "postProductInCheck";
      //public const string POST_PRODUCT = "postProduct";

      public const string GET_MODEL = "getCADModel/{fileId}";
      public const string GET_MODEL_NAME = "getCADModelName/{fileId}";
    }

    public static class Files
    {
      public const string UPLOAD_FILE = "upload";
      public const string DOWNLOAD_FILE = "download";
    }
  }
}
