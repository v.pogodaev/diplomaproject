﻿namespace SPAWebApp.Shared.Models.Services.ConstructDocument
{
  /// <summary>
  /// Документ из микросервиса Конструкторских документов
  /// </summary>
  public class Document
  {
    public string Id { get; set; }
    public string Designation { get; set; }
    public string Name { get; set; }
    public string State { get; set; }
    public int Revision { get; set; }
  }
}
