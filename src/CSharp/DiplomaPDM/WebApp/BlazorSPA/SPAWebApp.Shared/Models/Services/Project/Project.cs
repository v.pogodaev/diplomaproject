﻿using System;
using System.Collections.Generic;

namespace SPAWebApp.Shared.Models.Services.Project
{
  /// <summary>
  /// Проект из микросервиса проектов
  /// </summary>
  public class Project
  {
    public Guid Id { get; set; }

    public string Sign { get; set; }

    public string Name { get; set; }

    public List<ProjectItem> ProjectItems { get; set; }
  }
}
