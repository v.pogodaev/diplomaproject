﻿using System;
using System.Collections.Generic;

namespace SPAWebApp.Shared.Models.Services.DocumentStructure
{
  /// <summary>
  /// Папка из микросервиса Структур документов
  /// </summary>
  public class Folder
  {
    /// <summary>
    /// Id папки
    /// </summary>
    public Guid Id { get; set; }

    /// <summary>
    /// Обозначение папки
    /// </summary>
    public string Sign { get; set; }

    /// <summary>
    /// Имя папки
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Id проекта, в который входит папка (если есть)
    /// </summary>
    public string ProjectId { get; set; }

    /// <summary>
    /// Id вышестоящей папки (если есть)
    /// </summary>
    public Guid? SuperiorFolderId { get; set; }

    /// <summary>
    /// Список Id нижестоящих папок
    /// </summary>
    public ICollection<Guid> SubFolderIds { get; set; }

    /// <summary>
    /// Список настоящих Id (из их микросервисов) элементов, входящих в папку
    /// </summary>
    public ICollection<FolderItem> Items { get; set; }
  }
}
