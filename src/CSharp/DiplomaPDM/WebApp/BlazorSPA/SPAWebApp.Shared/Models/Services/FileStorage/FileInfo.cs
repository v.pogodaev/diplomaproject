﻿namespace SPAWebApp.Server.Models.NonTransactable.FileService
{
  /// <summary>
  /// Информация о файле без файла
  /// </summary>
  public class FileInfo
  {
    ///// <summary>
    ///// Имя + расширение
    ///// </summary>
    //public string FullName { get; set; }
    /// <summary>
    /// Имя без расширения
    /// </summary>
    public string Name { get; set; }
    /// <summary>
    /// Комментарий
    /// </summary>
    public string Comment { get; set; }
    /// <summary>
    /// Размер файла
    /// </summary>
    public double Size { get; set; }
    /// <summary>
    /// Тип файла
    /// </summary>
    public string Type { get; set; }
  }
}
