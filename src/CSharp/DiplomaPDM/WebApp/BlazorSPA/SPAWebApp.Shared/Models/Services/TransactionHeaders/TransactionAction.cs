﻿namespace SPAWebApp.Shared.Models.Services.TransactionHeaders
{
  public enum TransactionAction
  {
    Canceled = 0,
    Done = 1
  }
}
