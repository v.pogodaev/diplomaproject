﻿using System;

namespace SPAWebApp.Shared.Models.Services.DocumentStructure
{
  /// <summary>
  /// Элемент папки из микросервиса Структур документов
  /// </summary>
  public class FolderItem
  {
    public Guid FolderId { get; set; }

    /// <summary>
    /// Настоящий Id (из его микросервиса) элемента
    /// </summary>
    public string ItemRealId { get; set; }

    /// <summary>
    /// Тип элемента
    /// </summary>
    public string ItemType { get; set; }
  }
}
