﻿namespace SPAWebApp.Shared.Models.Services.Product
{
  /// <summary>
  /// Изделие из микросервиса Изделий
  /// </summary>
  public class Product
  {
    public string Id { get; set; }
    public string Designation { get; set; }
    public string Name { get; set; }
    public int Version { get; set; }
    public string State { get; set; }
    public string ModelId { get; set; }
    public string ModelIdView { get; set; }
    public double Weight { get; set; } 
  }
}