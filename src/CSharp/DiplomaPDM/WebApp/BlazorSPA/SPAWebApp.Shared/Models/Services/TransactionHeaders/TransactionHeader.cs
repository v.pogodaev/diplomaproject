﻿using System;

namespace SPAWebApp.Shared.Models.Services.TransactionHeaders
{
  public class TransactionHeader
  {
    public Guid Id { get; set; }
    public string UserId { get; set; }
    public string SessionId { get; set; }
    public DateTime OpenTransactionTimeUTC { get; set; }
    public DateTime? CloseTransactionTimeUTC { get; set; }
    public TransactionAction? TransactionAction { get; set; }
  }
}
