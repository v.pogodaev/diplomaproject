﻿using System.IO;

namespace SPAWebApp.Shared.Models.Services.FileStorage
{
  /// <summary>
  /// Файл из микросервиса Хранения файлов
  /// </summary>
  public class File
  {
    public string Id { get; set; }
    public string Name { get; set; }
    public int Size { get; set; }
    public string Type { get; set; }
    public byte FileData { get; set; }
    public string Comment { get; set; }
  }
}
