﻿using System;
using System.Collections.Generic;
using System.Text;

namespace SPAWebApp.Shared.Models.Services.Relationship
{
  public class Relationship
  {
    public string DocumentId { get; set; }
    public string ProductId { get; set; }
    public string Type { get; set; }
  }
}
