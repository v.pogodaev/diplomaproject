﻿namespace SPAWebApp.Shared.Models.Services.Project
{
  /// <summary>
  /// Элемент проекта из микросервиса Проектов
  /// </summary>
  public class ProjectItem
  {
    public string ItemRealId { get; set; }

    public string ItemType { get; set; }
  }
}
