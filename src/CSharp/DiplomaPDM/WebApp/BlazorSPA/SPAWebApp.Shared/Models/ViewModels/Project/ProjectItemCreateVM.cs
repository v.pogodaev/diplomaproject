﻿namespace SPAWebApp.Shared.Models.ViewModels.Project
{
  /// <summary>
  /// Данные для создания элемента проекта
  /// </summary>
  public class ProjectItemCreateVM
  {
    public string ProjectId { get; set; }
    public string ItemRealId { get; set; }
    public string ItemType { get; set; }
  }
}
