﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SPAWebApp.Shared.Models.ViewModels.DocumentStructure
{
  public class FolderModifyVM : FolderCreateVM
  {
    [Required]
    public Guid Id { get; set; }
  }
}
