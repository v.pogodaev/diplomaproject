﻿using System.ComponentModel.DataAnnotations;

namespace SPAWebApp.Shared.Models.ViewModels.ConstructDocument
{
  public class CreateNewRelModel
  {
    [Required(ErrorMessage = "Обязательно для заполнения")]
    public string DocumentId { get; set; }

    [Required(ErrorMessage = "Обязательно для заполнения")]
    public string ProductId { get; set; }

    [Required(ErrorMessage = "Обязательно для заполнения")]
    public string Type { get; set; }
  }
}