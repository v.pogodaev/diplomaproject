﻿using System.ComponentModel.DataAnnotations;

namespace SPAWebApp.Shared.Models.ViewModels.ConstructDocument
{
  public class ConstructDocumentCreateNewRevVM
  {
    public ConstructDocumentCreateNewRevVM() { }

    public ConstructDocumentCreateNewRevVM(string id)
    {
      Id = id;
    }

    [Required(ErrorMessage = "Обязательно для заполнения")]
    public string Id { get; set; }
  }
}
