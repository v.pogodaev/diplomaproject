﻿using System.ComponentModel.DataAnnotations;

namespace SPAWebApp.Shared.Models.ViewModels.Account
{
  /// <summary>
  /// Данные для регистрации пользователя
  /// </summary>
  public class RegisterVM
  {
    [Required]
    public string UserName { get; set; }

    [Required]
    public string Password { get; set; }

    [Required]
    [Compare(nameof(Password), ErrorMessage = "Пароли не совпадают")]
    public string PasswordConfirm { get; set; }
  }
}
