﻿using System.ComponentModel.DataAnnotations;

namespace SPAWebApp.Shared.Models.ViewModels.ConstructDocument
{
  public class ChangeStateByDocumentVM
  {
    public ChangeStateByDocumentVM() { }
    public ChangeStateByDocumentVM(string id, string state)
    {
      State = state;
      Id = id;
    }

    [Required(ErrorMessage = "Обязательно для заполнения")]
    public string Id { get; set; }

    public string State { get; set; }
  }
}
