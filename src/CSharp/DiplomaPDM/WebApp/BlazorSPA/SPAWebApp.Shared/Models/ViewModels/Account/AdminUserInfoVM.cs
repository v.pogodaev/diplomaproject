﻿using System;

namespace SPAWebApp.Shared.Models.ViewModels.Account
{
  public class AdminUserInfoVM
  {
    public Guid Id { get; set; }
    public string UserName { get; set; }
  }
}
