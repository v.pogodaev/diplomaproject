﻿namespace SPAWebApp.Shared.Models.ViewModels.DocumentStructure
{
  /// <summary>
  /// Данные для создания элемента папки
  /// </summary>
  public class FolderItemVM
  {
    /// <summary>
    /// Настоящий Id элемента из его сервиса
    /// </summary>
    public string ItemRealId { get; set; }

    /// <summary>
    /// Тип объекта
    /// </summary>
    public string ItemType { get; set; }
  }
}
