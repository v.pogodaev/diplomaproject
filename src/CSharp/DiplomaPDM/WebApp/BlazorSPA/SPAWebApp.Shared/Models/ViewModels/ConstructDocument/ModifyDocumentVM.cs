﻿using System.ComponentModel.DataAnnotations;
using SPAWebApp.Shared.Models.Services.ConstructDocument;

namespace SPAWebApp.Shared.Models.ViewModels.ConstructDocument
{
  public class ModifyDocumentVM
  {
    public ModifyDocumentVM() { }
    public ModifyDocumentVM(Document document)
    {
      Id = document.Id;
      NewDesignation = document.Designation;
      NewName = document.Name;
    }

    [Required(ErrorMessage = "Требуется id")]
    public string Id { get; set; }

    [Required(ErrorMessage = "Не должно быть пустым")]
    public string NewDesignation { get; set; }

    public string NewName { get; set; }
  }
}
