﻿using System.ComponentModel.DataAnnotations;

namespace SPAWebApp.Shared.Models.ViewModels.Project
{
  /// <summary>
  /// Данные для создания проекта
  /// </summary>
  public class ProjectCreateVM
  {
    [Required(ErrorMessage = "У проекта должно быть обозначение")]
    [MaxLength(30, ErrorMessage = "Обозначение не должно превышать 30 символов в длину")]
    public string Sign { get; set; }

    public string Name { get; set; }
  }
}
