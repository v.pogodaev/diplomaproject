﻿using System.ComponentModel.DataAnnotations;

namespace SPAWebApp.Shared.Models.ViewModels.Account
{
  /// <summary>
  /// Данные для входа в систему
  /// </summary>
  public class LoginVM
  {
    [Required]
    public string UserName { get; set; }

    [Required]
    public string Password { get; set; }

    public bool RememberMe { get; set; }
  }
}
