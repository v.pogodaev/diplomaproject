﻿using System;

namespace SPAWebApp.Shared.Models.ViewModels.FileService
{
  public class UploadFileVM
  {
    public UploadFileVM() { }

    public UploadFileVM(byte[] fileData, string name, string type, string comment)
    {
      FileData = fileData;
      Name = name;
      Type = type;
      Comment = comment;
    }
    public Byte[] FileData { get; set; }
    public string Name { get; set; }
    public string Type { get; set; }
    public string Comment { get; set; }
  }
}
