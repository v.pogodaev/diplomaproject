﻿using System.ComponentModel.DataAnnotations;

namespace SPAWebApp.Shared.Models.ViewModels.DocumentStructure
{
  /// <summary>
  /// Данные для создания папки
  /// </summary>
  public class FolderCreateVM
  {
    /// <summary>
    /// Обозначение папки
    /// </summary>
    [Required]
    public string Sign { get; set; }

    /// <summary>
    /// Имя папки
    /// </summary>
    [Required]
    public string Name { get; set; }

    public string ProjectId { get; set; }

    public string SuperiorFolderId { get; set; }
  }
}
