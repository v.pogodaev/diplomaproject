﻿namespace SPAWebApp.Shared.Models.ViewModels.Product
{
  public class UpdateProductVM
  {
    public UpdateProductVM() { }

    public UpdateProductVM(string productId, string designation, string name, int version, string modelId, string modelIdView, int weight)
    {
      ProductId = productId;
      Designation = designation;
      Name = name;
      Version = version;
      ModelId = modelId;
      ModelIdView = modelIdView;
      Weight = weight;
    }
    public string ProductId { get; set; }
    public string Designation { get; set; }
    public string Name { get; set; }
    public int Version { get; set; }
    public string ModelId { get; set; }
    public string ModelIdView { get; set; }
    public double Weight { get; set; }
  }
}
