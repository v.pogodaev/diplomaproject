﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SPAWebApp.Shared.Authentication.Models
{
  public class UserRoleVM
  {
    public string RoleName { get; set; }
    public bool IsAssigned { get; set; }
  }
}
