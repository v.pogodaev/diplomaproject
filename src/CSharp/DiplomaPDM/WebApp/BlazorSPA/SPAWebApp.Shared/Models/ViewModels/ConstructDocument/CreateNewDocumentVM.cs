﻿using System.ComponentModel.DataAnnotations;

namespace SPAWebApp.Shared.Models.ViewModels.ConstructDocument
{
  public class CreateNewDocumentVM
  {
    [Required(ErrorMessage = "Обязательно для заполнения")]
    public string Designation { get; set; }

    public string Name { get; set; }

  }
}
