﻿using System.ComponentModel.DataAnnotations;

namespace SPAWebApp.Shared.Models.ViewModels.Product
{
 public class CreateProductVM
  {
    public CreateProductVM() { }

    public CreateProductVM(string designation, string name, int weight)
    {
      Designation = designation;
      Name = name;
      Weight = weight;
    }

    [Required]
    public string Designation { get; set; }
    public string Name { get; set; }
    public int Weight { get; set; }
  }
}
