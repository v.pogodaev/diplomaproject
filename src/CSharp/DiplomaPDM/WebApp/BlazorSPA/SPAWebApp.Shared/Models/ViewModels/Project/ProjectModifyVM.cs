﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SPAWebApp.Shared.Models.ViewModels.Project
{
  /// <summary>
  /// Данные для изменения информации о проекте
  /// </summary>
  public class ProjectModifyVM : ProjectCreateVM
  {
    [Required]
    public Guid Id { get; set; }
  }
}
