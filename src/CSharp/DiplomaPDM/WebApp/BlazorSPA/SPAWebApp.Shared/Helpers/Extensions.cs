﻿using System.Collections;

// ReSharper disable InconsistentNaming

// ReSharper disable once CheckNamespace
namespace SPAWebApp.Shared.Helpers
{
  public static class IEnumerableExtensions
  {
    public static bool Any(this IEnumerable enumerable) => enumerable.GetEnumerator().MoveNext() == true;
  }
}
