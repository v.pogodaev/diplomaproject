﻿//using System;
//using System.Collections.Generic;
//using System.Threading.Tasks;
//using SPAWebApp.Server.Models.NonTransactable.FileService;
//using SPAWebApp.Server.Services.Contracts;
//using SPAWebApp.Shared.Models.Services.FileStorage;
//using SPAWebApp.Shared.Models.ViewModels.FileService;

//// ReSharper disable StringLiteralTypo

//namespace SPAWebApp.Server.Services.Fakes
//{
//#pragma warning disable CS1998
//  public class FakeFileStorageService : IFileStorageService
//  {
//    //private List<File> m_Files = new List<File>
//    //{
//    //  new File {Id = "5ce856efffac3a056ae55500", Name = "13var_CATPart", Type = "CATPart", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55501", Name = "15 Var", Type = "CATPart", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55502", Name = "Crankshaft_parts", Type = "CATPart", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55503", Name = "Daria1_CATPart", Type = "CATPart", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55504", Name = "Daria2_2_CATPart", Type = "CATPart", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55505", Name = "G1_CATPart (1)", Type = "CATPart", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55506", Name = "HuynhNV", Type = "CATPart", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55507", Name = "Korpus_klapana", Type = "CATPart", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55508", Name = "Part1", Type = "CATPart", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55509", Name = "Part1a", Type = "CATPart", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55510", Name = "Part1b", Type = "CATPart", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55511", Name = "Part3", Type = "CATPart", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55512", Name = "Part11", Type = "CATPart", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55513", Name = "Part111", Type = "CATPart", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55514", Name = "Pereima", Type = "CATPart", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55515", Name = "SHESTERNYA_CATPart (2)", Type = "CATPart", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55516", Name = "vint.anl.1", Type = "CATPart", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55530", Name = "13var_CATPart", Type = "wrl", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55531", Name = "15 Var", Type = "wrl", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55532", Name = "Crankshaft_parts", Type = "wrl", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55533", Name = "Daria1_CATPart", Type = "wrl", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55534", Name = "Daria2_2_CATPart", Type = "wrl", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55535", Name = "G1_CATPart (1)", Type = "wrl", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55536", Name = "HuynhNV", Type = "wrl", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55537", Name = "Korpus_klapana", Type = "wrl", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55538", Name = "Part1", Type = "wrl", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55539", Name = "Part1a", Type = "wrl", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55540", Name = "Part1b", Type = "wrl", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55541", Name = "Part3", Type = "wrl", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55542", Name = "Part11", Type = "wrl", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55543", Name = "Part111", Type = "wrl", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55544", Name = "Pereima", Type = "wrl", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55545", Name = "SHESTERNYA_CATPart (2)", Type = "wrl", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55546", Name = "vint.anl.1", Type = "wrl", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55522", Name = "Анализ", Type = "docx", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55523", Name = "Документация", Type = "docx", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55524", Name = "Инструкция", Type = "docx", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55525", Name = "Отчет", Type = "docx", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55526", Name = "Сведения", Type = "docx", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55527", Name = "Расчеты", Type = "xlsx", Comment = "Это комментарий", Size = 1},
//    //  new File {Id = "5ce856efffac3a056ae55528", Name = "Статистика", Type = "xlsx", Comment = "Это комментарий", Size = 1},
//    //};

//    //public Task<File> GetDataAsync(string Id)
//    //{
//    //  throw new NotImplementedException();
//    //}

//    //public Task<List<File>> GetAllFilesAsync()
//    //{
//    //  throw new NotImplementedException();
//    //}

//    //public Task RemoveFileAsync(string Id)
//    //{
//    //  throw new NotImplementedException();
//    //}
//    public Task<File> DownloadFileAsync(string fileId)
//    {
//      throw new NotImplementedException();
//    }

//    public Task<List<File>> GetAllFilesAsync()
//    {
//      throw new NotImplementedException();
//    }

//    public Task RemoveFileAsync(string id)
//    {
//      throw new NotImplementedException();
//    }

//    public Task<string> UploadFileAsync(UploadFileVM file)
//    {
//      throw new NotImplementedException();
//    }

//    public Task DeleteFileAsync(string fileId)
//    {
//      throw new NotImplementedException();
//    }

//    public Task<FileInfo> GetFileInfo(string fileId)
//    {
//      throw new NotImplementedException();
//    }

//    public Task<DownloadFileModel> GetFileAndInfo(string fileId)
//    {
//      throw new NotImplementedException();
//    }
//  }
//}
