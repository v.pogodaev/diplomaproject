﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SPAWebApp.Server.Models.Transactable;
using SPAWebApp.Server.Services.Contracts;
using SPAWebApp.Shared.Models.Services.Project;

namespace SPAWebApp.Server.Services.Fakes
{
#pragma warning disable CS1998
  /// <summary>
  /// Фэйковый сервис проектов для тестов отдельно от сервисов
  /// </summary>
  public class FakeProjectService : IProjectService
  {
    //private static readonly List<Project> m_Projects = new List<Project>
    //{
    //  new Project
    //  {
    //    Id = Guid.Parse("58a5843c-5851-4562-853c-8268de2fc549"), Name = "Project Name 1", Sign = "P1",
    //    ProjectItems = new List<ProjectItem>
    //    {
    //      new ProjectItem {ItemType = "Folder", ItemRealId = "5997bfec-ce94-47eb-8a3d-b18a94db9e69"},
    //      new ProjectItem {ItemType = "Folder", ItemRealId = "209a7635-5a8d-42ac-8f8c-1b031e0bebf3"},
    //      new ProjectItem {ItemType = "Folder", ItemRealId = "183d6ff4-7bb2-4270-a3cd-99df2bae06fc"}
    //    }
    //  },
    //  new Project
    //  {
    //    Id = Guid.Parse("039d2c46-3342-4f45-92d0-30be2c94fc1c"), Name = "Project Name 2", Sign = "P2",
    //    ProjectItems = new List<ProjectItem>()
    //  },
    //  new Project
    //  {
    //    Id = Guid.Parse("77c2f851-28de-4ced-bc10-cc388f9ad133"), Name = "Project Name 3", Sign = "P3",
    //    ProjectItems = new List<ProjectItem>
    //    {
    //      new ProjectItem {ItemType = "Folder", ItemRealId = "7219e4ad-b3a7-4005-a88f-937430b0f474"},
    //      new ProjectItem {ItemType = "Folder", ItemRealId = "2f1e9a16-60da-4abc-b9d7-9efacfd0cbda"}
    //    }
    //  },
    //  new Project
    //  {
    //    Id = Guid.Parse("043eef97-bd3d-45f8-99b2-be823a8dbeb6"), Name = "Project Name 4", Sign = "P4",
    //    ProjectItems = new List<ProjectItem>()
    //  },
    //  new Project
    //  {
    //    Id = Guid.Parse("e5104689-63eb-468f-ac76-a950321d7ac0"), Name = "Project Name 5", Sign = "P5",
    //    ProjectItems = new List<ProjectItem>
    //      {new ProjectItem {ItemType = "Folder", ItemRealId = "a909c38c-0e89-4221-91ab-8fb3936f1fe6"}}
    //  },
    //  new Project
    //  {
    //    Id = Guid.Parse("551c27c5-01cd-4956-b9b3-7a126abf08c2"), Name = "Project Name 6", Sign = "P6",
    //    ProjectItems = new List<ProjectItem>
    //      {new ProjectItem {ItemType = "Folder", ItemRealId = "25421148-bc88-47b5-912d-f4f0812d5d87"}}
    //  },
    //  new Project
    //  {
    //    Id = Guid.Parse("2cf6b3e6-e686-4434-a41d-5f80141845a7"), Name = "Project Name 7", Sign = "P7",
    //    ProjectItems = new List<ProjectItem>()
    //  },
    //  new Project
    //  {
    //    Id = Guid.Parse("8bd4f0bc-800a-4d0a-a787-fe9ec488b25e"), Name = "Project Name 8", Sign = "P8",
    //    ProjectItems = new List<ProjectItem>
    //    {
    //      new ProjectItem {ItemType = "Folder", ItemRealId = "3052cd5a-039c-4914-865a-38d5ba624226"},
    //      new ProjectItem {ItemType = "Folder", ItemRealId = "9e3c9093-b276-4dec-a03e-e8477568a170"},
    //      new ProjectItem {ItemType = "Folder", ItemRealId = "ebf6910e-63b4-4ca1-94ee-0effce03fc1e"},
    //      new ProjectItem {ItemType = "Folder", ItemRealId = "a5d5bf6a-99b1-4c99-a6e2-bdc699c146ec"}
    //    }
    //  },
    //  new Project
    //  {
    //    Id = Guid.Parse("7b305d95-8bab-4b55-90e1-e82a9f25ea38"), Name = "Project Name 9", Sign = "P9",
    //    ProjectItems = new List<ProjectItem>()
    //  },
    //  new Project
    //  {
    //    Id = Guid.Parse("ffebbf3a-b254-457d-a41c-7b4c72fc9700"), Name = "Project Name 10", Sign = "P10",
    //    ProjectItems = new List<ProjectItem>
    //      {new ProjectItem {ItemType = "Folder", ItemRealId = "5cb2d9eb-5b92-49de-907c-5720680e9978"}}
    //  },
    //  new Project
    //  {
    //    Id = Guid.Parse("539af7a8-273f-43d8-b5a6-501a9881ed5c"), Name = "Project Name 11", Sign = "P11",
    //    ProjectItems = new List<ProjectItem>()
    //  },
    //  new Project
    //  {
    //    Id = Guid.Parse("d1e86f19-3739-4d83-9c7f-136edd799534"), Name = "Project Name 12", Sign = "P12",
    //    ProjectItems = new List<ProjectItem>
    //    {
    //      new ProjectItem {ItemType = "Folder", ItemRealId = "92834871-ae58-444f-baa3-4365e0c2b8d9"},
    //      new ProjectItem {ItemType = "Folder", ItemRealId = "7176a1c2-490c-47c4-acce-70b8aff3063b"},
    //      new ProjectItem {ItemType = "Folder", ItemRealId = "d534087d-0c41-450a-a5e0-5500eb469776"},
    //      new ProjectItem {ItemType = "Folder", ItemRealId = "ae822fbb-bbcc-4bc9-a8a9-2e98a9544ea0"},
    //      new ProjectItem {ItemType = "Folder", ItemRealId = "00c742a8-724e-4b47-81b1-5a2236bc31a2"}
    //    }
    //  },
    //  new Project
    //  {
    //    Id = Guid.Parse("dee8dff3-0a91-40ed-9e82-c6892a41e55b"), Name = "Project Name 13", Sign = "P13",
    //    ProjectItems = new List<ProjectItem>
    //      {new ProjectItem {ItemType = "Folder", ItemRealId = "dc348b14-b2d7-4c36-90a3-5762b1de3513"}}
    //  },
    //  new Project
    //  {
    //    Id = Guid.Parse("491bfdd1-39ae-4fdd-b3c8-d7b8e71b9a3f"), Name = "Project Name 14", Sign = "P14",
    //    ProjectItems = new List<ProjectItem>()
    //  },
    //  new Project
    //  {
    //    Id = Guid.Parse("781b0837-c14c-47ad-a8b5-552637191889"), Name = "Project Name 15", Sign = "P15",
    //    ProjectItems = new List<ProjectItem>
    //      {new ProjectItem {ItemType = "Folder", ItemRealId = "aefd5bc4-7759-4c20-8406-fc8677a8f3af"}}
    //  },
    //  new Project
    //  {
    //    Id = Guid.Parse("e8731548-94cd-47db-9339-d07ea321ef26"), Name = "Project Name 16", Sign = "P16",
    //    ProjectItems = new List<ProjectItem>
    //      {new ProjectItem {ItemType = "Folder", ItemRealId = "d6eb0bb6-4b05-4182-aa8b-be2a38947ea2"}}
    //  },
    //  new Project
    //  {
    //    Id = Guid.Parse("cec2616a-8e1b-4eed-adf7-9c338e1ecdbb"), Name = "Project Name 17", Sign = "P17",
    //    ProjectItems = new List<ProjectItem>()
    //  },
    //  new Project
    //  {
    //    Id = Guid.Parse("573cb3be-9676-446e-92b2-d3f9b13d5aef"), Name = "Project Name 18", Sign = "P18",
    //    ProjectItems = new List<ProjectItem>()
    //  },
    //  new Project
    //  {
    //    Id = Guid.Parse("f04bfc3f-c9a8-4f13-adde-a68a474039d9"), Name = "Project Name 19", Sign = "P19",
    //    ProjectItems = new List<ProjectItem>()
    //  },
    //  new Project
    //  {
    //    Id = Guid.Parse("5448c662-ce1f-4441-937d-47151047fa27"), Name = "Project Name 20", Sign = "P20",
    //    ProjectItems = new List<ProjectItem>()
    //  },
    //  new Project
    //  {
    //    Id = Guid.Parse("969a202d-6d9a-4d7a-a136-039f51019e09"), Name = "Project Name 21", Sign = "P21",
    //    ProjectItems = new List<ProjectItem>()
    //  },
    //  new Project
    //  {
    //    Id = Guid.Parse("e00e565b-eaa4-4346-8658-3f8bf50a31db"), Name = "Project Name 22", Sign = "P22",
    //    ProjectItems = new List<ProjectItem>()
    //  },
    //};

    //public async Task<Project> CreateProjectAsync(TProjectCreateModel projectModel)
    //{
    //  var project = new Project { Id = Guid.NewGuid(), Name = projectModel.Name, Sign = projectModel.Sign, ProjectItems = new List<ProjectItem>() };
    //  m_Projects.Add(project);
    //  return project;
    //}

    //public async Task DeleteProjectByIdAsync(Guid projectId, TransactionViewModel transaction = null)
    //{
    //  var project = m_Projects.FirstOrDefault(p => p.Id == projectId);
    //  if (project != null)
    //  {
    //    m_Projects.Remove(project);
    //  }
    //}

    //public async Task<Project> ModifyProjectAsync(TProjectModifyModel updatedProject)
    //{
    //  var project = m_Projects.FirstOrDefault(p => p.Id == updatedProject.Id);
    //  if (project != null)
    //  {
    //    project.Name = updatedProject.Name;
    //    project.Sign = updatedProject.Sign;
    //    return project;
    //  }

    //  return null;
    //}

    //public async Task IncludeItemToProjectAsync(TProjectItemModel item)
    //{
    //  var project = m_Projects.FirstOrDefault(p => item.ProjectId.Equals(p.Id.ToString()));

    //  project?.ProjectItems.Add(new ProjectItem() { ItemRealId = item.ItemRealId, ItemType = item.ItemType });
    //}

    //public async Task<ICollection<ProjectItem>> GetIncludedItemsAsync(Guid projectId)
    //{
    //  return m_Projects.FirstOrDefault(p => p.Id == projectId)?.ProjectItems;
    //}

    //public Task ExcludeItemAsync(Guid projectId, string itemRealId, TransactionViewModel transaction = null)
    //{
    //  throw new NotImplementedException();
    //}

    //public Task<ICollection<ProjectItem>> GetIncludedItemsByTypeAsync(Guid projectId, string type)
    //{
    //  throw new NotImplementedException();
    //}

    //public Task<ICollection<string>> GetAllowedTypesAsync()
    //{
    //  throw new NotImplementedException();
    //}

    //public async Task<Project> GetProjectByIdAsync(Guid projectId)
    //{
    //  return m_Projects.FirstOrDefault(p => p.Id == projectId);
    //}

    //public async Task<ICollection<Project>> GetAllProjectsLazyAsync()
    //{
    //  return m_Projects;
    //}

    //public async Task CommitTransaction(TransactionViewModel transaction)
    //{

    //}

    //public async Task CancelTransaction(TransactionViewModel transaction)
    //{

    //}
    public Task<Project> CreateProjectAsync(Models.Transactable.Project.TProjectCreateModel projectModel)
    {
      throw new NotImplementedException();
    }

    public Task DeleteProjectByIdAsync(Guid projectId, TransactionInfo transaction)
    {
      throw new NotImplementedException();
    }

    public Task<Project> ModifyProjectAsync(Models.Transactable.Project.TProjectModifyModel updatedProject)
    {
      throw new NotImplementedException();
    }

    public Task IncludeItemToProjectAsync(Models.Transactable.Project.TProjectItemModel item)
    {
      throw new NotImplementedException();
    }

    public Task<ICollection<ProjectItem>> GetIncludedItemsAsync(Guid projectId)
    {
      throw new NotImplementedException();
    }

    public Task ExcludeItemAsync(Guid projectId, string itemRealId, TransactionInfo transaction)
    {
      throw new NotImplementedException();
    }

    public Task<ICollection<ProjectItem>> GetIncludedItemsByTypeAsync(Guid projectId, string type)
    {
      throw new NotImplementedException();
    }

    public Task<ICollection<string>> GetAllowedTypesAsync()
    {
      throw new NotImplementedException();
    }

    public Task<Project> GetProjectByIdAsync(Guid projectId)
    {
      throw new NotImplementedException();
    }

    public Task<ICollection<Project>> GetAllProjectsLazyAsync()
    {
      throw new NotImplementedException();
    }

    public Task CommitTransactionAsync(TransactionInfo transaction)
    {
      throw new NotImplementedException();
    }

    public Task CancelTransactionAsync(TransactionInfo transaction)
    {
      throw new NotImplementedException();
    }
  }
}
