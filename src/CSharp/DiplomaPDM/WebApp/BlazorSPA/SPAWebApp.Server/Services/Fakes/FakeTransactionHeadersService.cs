﻿using System;
using System.Threading.Tasks;
using SPAWebApp.Server.Models.NonTransactable.TransactionHeaders;
using SPAWebApp.Server.Services.Contracts;
using SPAWebApp.Shared.Models.Services.TransactionHeaders;

namespace SPAWebApp.Server.Services.Fakes
{
#pragma warning disable CS1998
  /// <summary>
  /// Фэйковый сервис заголовков транзакций для тестов отдельно от сервисов
  /// </summary>
  public class FakeTransactionHeadersService : ITransactionHeadersService
  {
    //private static readonly List<Transaction> m_Transactions = new List<Transaction>();

    ///// <inheritdoc />
    //public async Task<Transaction> OpenTransactionAsync(TransactionOpenViewModel model)
    //{
    //  var transaction = new Transaction
    //  {
    //    Id = Guid.NewGuid(),
    //    UserId = model.UserId,
    //    SessionId = model.SessionId,
    //    OpenTransactionTimeUTC = DateTime.Now.ToUniversalTime()
    //  };
    //  m_Transactions.Add(transaction);
    //  return transaction;
    //}

    ///// <inheritdoc />
    //public async Task CloseTransactionByTransactionIdAsync(Guid id)
    //{
    //  var transaction = m_Transactions.FirstOrDefault(t => t.Id == id);
    //  if (transaction == null) return;
    //  transaction.TransactionAction = TransactionAction.Done;
    //  transaction.CloseTransactionTimeUTC = DateTime.Now.ToUniversalTime();
    //}

    ///// <inheritdoc />
    //public async Task CloseTransactionByUserIdAsync(string id)
    //{
    //  var transaction = m_Transactions.FirstOrDefault(t => t.UserId == id);
    //  if (transaction == null) return;
    //  transaction.TransactionAction = TransactionAction.Done;
    //  transaction.CloseTransactionTimeUTC = DateTime.Now.ToUniversalTime();
    //}

    ///// <inheritdoc />
    //public async Task CancelTransactionByTransactionIdAsync(Guid id)
    //{
    //  var transaction = m_Transactions.FirstOrDefault(t => t.Id == id);
    //  if (transaction == null) return;
    //  transaction.TransactionAction = TransactionAction.Canceled;
    //  transaction.CloseTransactionTimeUTC = DateTime.Now.ToUniversalTime();
    //}

    ///// <inheritdoc />
    //public async Task CancelTransactionByUserIdAsync(string id)
    //{
    //  var transaction = m_Transactions.FirstOrDefault(t => t.UserId == id);
    //  if (transaction == null) return;
    //  transaction.TransactionAction = TransactionAction.Canceled;
    //  transaction.CloseTransactionTimeUTC = DateTime.Now.ToUniversalTime();
    //}

    ///// <inheritdoc />
    //public async Task<Transaction> GetTransactionByTransactionIdAsync(Guid id)
    //{
    //  return m_Transactions.FirstOrDefault(t => t.Id == id);
    //}

    ///// <inheritdoc />
    //public async Task<Transaction> GetTransactionByUserIdAsync(string id)
    //{
    //  return m_Transactions.FirstOrDefault(t => t.UserId == id);
    //}
    public Task<TransactionHeader> OpenTransactionAsync(TransactionOpen model)
    {
      throw new NotImplementedException();
    }

    public Task CloseTransactionByTransactionIdAsync(string id)
    {
      throw new NotImplementedException();
    }

    public Task CloseTransactionByUserIdAsync(string id)
    {
      throw new NotImplementedException();
    }

    public Task CancelTransactionByTransactionIdAsync(string id)
    {
      throw new NotImplementedException();
    }

    public Task CancelTransactionByUserIdAsync(string id)
    {
      throw new NotImplementedException();
    }

    public Task<TransactionHeader> GetTransactionByTransactionIdAsync(Guid id)
    {
      throw new NotImplementedException();
    }

    public Task<TransactionHeader> GetTransactionByUserIdAsync(string id)
    {
      throw new NotImplementedException();
    }
  }
}
