﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SPAWebApp.Server.Models.Transactable;
using SPAWebApp.Server.Services.Contracts;
using SPAWebApp.Shared.Models.Services.DocumentStructure;

namespace SPAWebApp.Server.Services.Fakes
{
#pragma warning disable CS1998
  public class FakeDocumentStructureService : IDocumentStructureService
  {
    //  private readonly List<Folder> m_Folders = new List<Folder>
    //  {
    //    new Folder
    //    {
    //      Id = new Guid("5997bfec-ce94-47eb-8a3d-b18a94db9e69"), Sign = "F1", Name = "Folder 1",
    //      ProjectId = "58a5843c-5851-4562-853c-8268de2fc549",
    //      SubFolderIds = new List<Guid>
    //      {
    //        new Guid("18257e6c-4628-44c8-9077-39d456bfb68d"),
    //        new Guid("12399e7b-bb13-4bf6-a928-a4738f06eb40"),
    //        new Guid("324b2a77-91a6-4fc8-bb1a-b441c0a9ffbb")
    //      },
    //      Items = new List<FolderItem>
    //      {
    //        new FolderItem
    //        {
    //          ItemType = "Document",
    //          ItemRealId = "5ce856efffac3a056ae77738"
    //        },
    //        new FolderItem
    //        {
    //          ItemType = "Product",
    //          ItemRealId = "5ce856efffac3a056ae88838"
    //        }
    //      }
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("7219e4ad-b3a7-4005-a88f-937430b0f474"), Sign = "F2", Name = "Folder 2",
    //      ProjectId = "77c2f851-28de-4ced-bc10-cc388f9ad133",
    //      SubFolderIds = new List<Guid>
    //      {
    //        new Guid("52657aa3-5c52-40ba-bbc0-975e95c4dfc4"), new Guid("ae037979-3391-4204-a856-d61dd54eacde"),
    //        new Guid("a635b6f1-39e7-43a1-9e51-242cc8c371c0"), new Guid("8ca6b235-7bb8-499e-87e8-f74146cedb38"),
    //        new Guid("9ed24309-8674-4ae9-8759-5b9af50999a5"), new Guid("ce900dfd-ac13-4b08-bced-fa263bd4f3f0")
    //      },
    //      Items = new List<FolderItem>
    //      {
    //        new FolderItem
    //        {
    //          ItemType = "Document",
    //          ItemRealId = "5ce856f6ffac3a056ae7773a"
    //        }
    //      }
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("a909c38c-0e89-4221-91ab-8fb3936f1fe6"), Sign = "F3", Name = "Folder 3",
    //      ProjectId = "e5104689-63eb-468f-ac76-a950321d7ac0"
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("25421148-bc88-47b5-912d-f4f0812d5d87"), Sign = "F4", Name = "Folder 4",
    //      ProjectId = "551c27c5-01cd-4956-b9b3-7a126abf08c2"
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("3052cd5a-039c-4914-865a-38d5ba624226"), Sign = "F5", Name = "Folder 5",
    //      ProjectId = "8bd4f0bc-800a-4d0a-a787-fe9ec488b25e"
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("5cb2d9eb-5b92-49de-907c-5720680e9978"), Sign = "F6", Name = "Folder 6",
    //      ProjectId = "ffebbf3a-b254-457d-a41c-7b4c72fc9700"
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("92834871-ae58-444f-baa3-4365e0c2b8d9"), Sign = "F7", Name = "Folder 7",
    //      ProjectId = "d1e86f19-3739-4d83-9c7f-136edd799534"
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("dc348b14-b2d7-4c36-90a3-5762b1de3513"), Sign = "F8", Name = "Folder 8",
    //      ProjectId = "dee8dff3-0a91-40ed-9e82-c6892a41e55b"
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("aefd5bc4-7759-4c20-8406-fc8677a8f3af"), Sign = "F9", Name = "Folder 9",
    //      ProjectId = "781b0837-c14c-47ad-a8b5-552637191889"
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("d6eb0bb6-4b05-4182-aa8b-be2a38947ea2"), Sign = "F10", Name = "Folder 10",
    //      ProjectId = "e8731548-94cd-47db-9339-d07ea321ef26"
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("18257e6c-4628-44c8-9077-39d456bfb68d"), Sign = "F11", Name = "Folder 11",
    //      SuperiorFolderId = new Guid("5997bfec-ce94-47eb-8a3d-b18a94db9e69"),
    //      SubFolderIds = new List<Guid>
    //        {new Guid("5252ddca-2712-4982-97f4-fabe8883c1b8"), new Guid("83da9ccb-5e1d-49b9-84a2-97630857b502"),},
    //      Items = new List<FolderItem>
    //      {
    //        new FolderItem
    //        {
    //          ItemType = "Document",
    //          ItemRealId = "5ce8571affac3a056ae7773d"
    //        },
    //        new FolderItem
    //        {
    //          ItemType = "Product",
    //          ItemRealId = "5ce856f6ffac3a056ae8883a"
    //        }
    //      }
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("12399e7b-bb13-4bf6-a928-a4738f06eb40"), Sign = "F12", Name = "Folder 12",
    //      SuperiorFolderId = new Guid("5997bfec-ce94-47eb-8a3d-b18a94db9e69")
    //    ,
    //    Items = new List<FolderItem>
    //    {
    //      new FolderItem
    //      {
    //        ItemType = "Document",
    //        ItemRealId = "5ce8571effac3a056ae7773f"
    //      }
    //    }
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("324b2a77-91a6-4fc8-bb1a-b441c0a9ffbb"), Sign = "F13", Name = "Folder 13",
    //      SuperiorFolderId = new Guid("5997bfec-ce94-47eb-8a3d-b18a94db9e69"),
    //      SubFolderIds = new List<Guid>
    //        {new Guid("d75e119c-9147-4879-8d7a-20703c112faf"), new Guid("f3c90e21-0390-44bf-88f5-ecdfdbc96b22"),},
    //      Items = new List<FolderItem>
    //      {
    //        new FolderItem
    //        {
    //          ItemType = "Document",
    //          ItemRealId = "5ce8571affac3a056ae8883d"
    //        }
    //      }
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("52657aa3-5c52-40ba-bbc0-975e95c4dfc4"), Sign = "F14", Name = "Folder 14",
    //      SuperiorFolderId = new Guid("a909c38c-0e89-4221-91ab-8fb3936f1fe6")
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("ae037979-3391-4204-a856-d61dd54eacde"), Sign = "F15", Name = "Folder 15",
    //      SuperiorFolderId = new Guid("a909c38c-0e89-4221-91ab-8fb3936f1fe6")
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("a635b6f1-39e7-43a1-9e51-242cc8c371c0"), Sign = "F16", Name = "Folder 16",
    //      SuperiorFolderId = new Guid("a909c38c-0e89-4221-91ab-8fb3936f1fe6")
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("8ca6b235-7bb8-499e-87e8-f74146cedb38"), Sign = "F17", Name = "Folder 17",
    //      SuperiorFolderId = new Guid("a909c38c-0e89-4221-91ab-8fb3936f1fe6"),
    //      Items = new List<FolderItem>
    //      {
    //        new FolderItem
    //        {
    //          ItemType = "Document",
    //          ItemRealId = "5ce85724ffac3a056ae88843"
    //        }
    //      }
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("9ed24309-8674-4ae9-8759-5b9af50999a5"), Sign = "F18", Name = "Folder 18",
    //      SuperiorFolderId = new Guid("a909c38c-0e89-4221-91ab-8fb3936f1fe6"),
    //      SubFolderIds = new List<Guid> {new Guid("209a7635-5a8d-42ac-8f8c-1b031e0bebf3")},
    //      Items = new List<FolderItem>
    //      {
    //        new FolderItem
    //        {
    //          ItemType = "Document",
    //          ItemRealId = "5ce85721ffac3a056ae77741"
    //        }
    //      }
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("ce900dfd-ac13-4b08-bced-fa263bd4f3f0"), Sign = "F19", Name = "Folder 19",
    //      SuperiorFolderId = new Guid("a909c38c-0e89-4221-91ab-8fb3936f1fe6"),
    //      Items = new List<FolderItem>
    //      {
    //        new FolderItem
    //        {
    //          ItemType = "Document",
    //          ItemRealId = "5ce8571effac3a056ae8883f"
    //        }
    //      }
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("5252ddca-2712-4982-97f4-fabe8883c1b8"), Sign = "F20", Name = "Folder 20",
    //      SuperiorFolderId = new Guid("18257e6c-4628-44c8-9077-39d456bfb68d")
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("83da9ccb-5e1d-49b9-84a2-97630857b502"), Sign = "F31", Name = "Folder 31",
    //      SuperiorFolderId = new Guid("18257e6c-4628-44c8-9077-39d456bfb68d")
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("d75e119c-9147-4879-8d7a-20703c112faf"), Sign = "F32", Name = "Folder 32",
    //      SuperiorFolderId = new Guid("324b2a77-91a6-4fc8-bb1a-b441c0a9ffbb"),
    //      Items = new List<FolderItem>
    //      {
    //        new FolderItem
    //        {
    //          ItemType = "Document",
    //          ItemRealId = "5ce85721ffac3a056ae88841"
    //        }
    //      }
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("f3c90e21-0390-44bf-88f5-ecdfdbc96b22"), Sign = "F33", Name = "Folder 33",
    //      SuperiorFolderId = new Guid("324b2a77-91a6-4fc8-bb1a-b441c0a9ffbb"),
    //      SubFolderIds = new List<Guid>
    //      {
    //        new Guid("73b57bed-4773-4df6-8ea6-0fd8800f2ed8"), new Guid("ecb479c1-c479-44d6-9524-3b0a459b356a"),
    //        new Guid("f550f302-f4f2-4876-948c-39b9136a9dd7"),
    //      },
    //      Items = new List<FolderItem>
    //      {
    //        new FolderItem
    //        {
    //          ItemType = "Document",
    //          ItemRealId = "5ce8572dffac3a056ae77747"
    //        }
    //      }
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("73b57bed-4773-4df6-8ea6-0fd8800f2ed8"), Sign = "F34", Name = "Folder 34",
    //      SuperiorFolderId = new Guid("f3c90e21-0390-44bf-88f5-ecdfdbc96b22"),
    //      SubFolderIds = new List<Guid> {new Guid("79762b0f-2497-4346-8aad-8bb337747d29"),}
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("ecb479c1-c479-44d6-9524-3b0a459b356a"), Sign = "F35", Name = "Folder 35",
    //      SuperiorFolderId = new Guid("f3c90e21-0390-44bf-88f5-ecdfdbc96b22")
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("f550f302-f4f2-4876-948c-39b9136a9dd7"), Sign = "F36", Name = "Folder 36",
    //      SuperiorFolderId = new Guid("f3c90e21-0390-44bf-88f5-ecdfdbc96b22")
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("79762b0f-2497-4346-8aad-8bb337747d29"), Sign = "F37", Name = "Folder 37",
    //      SuperiorFolderId = new Guid("73b57bed-4773-4df6-8ea6-0fd8800f2ed8"),
    //      SubFolderIds = new List<Guid>
    //      {
    //        new Guid("ccbcd8b0-3438-40be-8581-6644be445932"), new Guid("0ae1304f-1d70-4edb-ab45-b9a62454396d"),
    //        new Guid("29ee2118-60a3-43ea-9fe7-fe4ad4a076a5"),
    //      },
    //      Items = new List<FolderItem>
    //      {
    //        new FolderItem
    //        {
    //          ItemType = "Document",
    //          ItemRealId = "5ce8572affac3a056ae77745"
    //        }
    //      }
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("ccbcd8b0-3438-40be-8581-6644be445932"), Sign = "F38", Name = "Folder 38",
    //      SuperiorFolderId = new Guid("79762b0f-2497-4346-8aad-8bb337747d29")
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("0ae1304f-1d70-4edb-ab45-b9a62454396d"), Sign = "F39", Name = "Folder 39",
    //      SuperiorFolderId = new Guid("79762b0f-2497-4346-8aad-8bb337747d29"),
    //      Items = new List<FolderItem>
    //      {
    //        new FolderItem
    //        {
    //          ItemType = "Document",
    //          ItemRealId = "5ce85724ffac3a056ae77743"
    //        }
    //      }
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("29ee2118-60a3-43ea-9fe7-fe4ad4a076a5"), Sign = "F40", Name = "Folder 40",
    //      SuperiorFolderId = new Guid("79762b0f-2497-4346-8aad-8bb337747d29")
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("209a7635-5a8d-42ac-8f8c-1b031e0bebf3"), Sign = "F41", Name = "Folder 41",
    //      SuperiorFolderId = new Guid("9ed24309-8674-4ae9-8759-5b9af50999a5"),
    //      Items = new List<FolderItem>
    //      {
    //        new FolderItem
    //        {
    //          ItemType = "Document",
    //          ItemRealId = "5ce8572affac3a056ae88845"
    //        }
    //      }
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("183d6ff4-7bb2-4270-a3cd-99df2bae06fc"), Sign = "F42", Name = "Folder 42",
    //      ProjectId = "58a5843c-5851-4562-853c-8268de2fc549",
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("9e3c9093-b276-4dec-a03e-e8477568a170"), Sign = "F43", Name = "Folder 43",
    //      ProjectId = "8bd4f0bc-800a-4d0a-a787-fe9ec488b25e"
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("ebf6910e-63b4-4ca1-94ee-0effce03fc1e"), Sign = "F44", Name = "Folder 44",
    //      ProjectId = "8bd4f0bc-800a-4d0a-a787-fe9ec488b25e"
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("a5d5bf6a-99b1-4c99-a6e2-bdc699c146ec"), Sign = "F45", Name = "Folder 45",
    //      ProjectId = "8bd4f0bc-800a-4d0a-a787-fe9ec488b25e"
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("2f1e9a16-60da-4abc-b9d7-9efacfd0cbda"), Sign = "F46", Name = "Folder 46",
    //      ProjectId = "77c2f851-28de-4ced-bc10-cc388f9ad133"
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("7176a1c2-490c-47c4-acce-70b8aff3063b"), Sign = "F47", Name = "Folder 47",
    //      ProjectId = "d1e86f19-3739-4d83-9c7f-136edd799534"
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("d534087d-0c41-450a-a5e0-5500eb469776"), Sign = "F48", Name = "Folder 48",
    //      ProjectId = "d1e86f19-3739-4d83-9c7f-136edd799534"
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("ae822fbb-bbcc-4bc9-a8a9-2e98a9544ea0"), Sign = "F49", Name = "Folder 49",
    //      ProjectId = "d1e86f19-3739-4d83-9c7f-136edd799534"
    //    },
    //    new Folder
    //    {
    //      Id = new Guid("00c742a8-724e-4b47-81b1-5a2236bc31a2"), Sign = "F50", Name = "Folder 50",
    //      ProjectId = "d1e86f19-3739-4d83-9c7f-136edd799534"
    //    },
    //  };

    //  public async Task<Folder> CreateFolderAsync(TFolderCreateModel newFolder)
    //  {
    //    var folder = new Folder() { Id = Guid.NewGuid(), Name = newFolder.Name, Sign = newFolder.Sign };
    //    m_Folders.Add(folder);
    //    return folder;
    //  }
    //  public async Task ExcludeFolderFromProjectAsync(Guid folderId, TransactionViewModel transaction)
    //  {
    //    var folder = m_Folders.FirstOrDefault(f => f.Id == folderId);
    //    if (folder == null) { return; }

    //    folder.ProjectId = string.Empty;
    //  }

    //  public async Task ExcludeFolderFromFolderAsync(Guid superFolderId, Guid subFolderId, TransactionViewModel transaction)
    //  {
    //    var supFolder = m_Folders.FirstOrDefault(f => f.Id == superFolderId);
    //    if (supFolder == null) { return; }

    //    var subItem = supFolder.Items.FirstOrDefault(i => i.ItemRealId == subFolderId.ToString());
    //    supFolder.Items.Remove(subItem);

    //    var subFolder = m_Folders.FirstOrDefault(f => f.Id == subFolderId);
    //    m_Folders.Remove(subFolder);
    //  }

    //  public Task ExcludeItemFromFolderAsync(Guid folderId, string itemRealId, TransactionViewModel transaction) => throw new NotImplementedException();
    //  public async Task<ICollection<Folder>> GetAllFoldersAsync() => m_Folders;
    //  public Task<ICollection<string>> GetAllowedTypesAsync() => throw new NotImplementedException();
    //  public async Task<Folder> GetFolderByIdAsync(Guid id) => m_Folders.FirstOrDefault(f => f.Id == id);
    //  public Task<ICollection<Folder>> GetFoldersByProjectIdGreedyAsync(string projectId) => throw new NotImplementedException();
    //  public Task<ICollection<Folder>> GetFoldersByProjectIdLazyAsync(string projectId) => throw new NotImplementedException();

    //  public async Task<ICollection<Folder>> GetIncludedFoldersAsync(Guid folderId)
    //  {
    //    var folderIds = m_Folders.FirstOrDefault(f => f.Id == folderId)?.SubFolderIds;
    //    return folderIds == null
    //      ? new List<Folder>()
    //      : folderIds.Select(id => m_Folders.FirstOrDefault(f => f.Id == id)).Where(folder => folder != null).ToList();
    //  }

    //  public async Task<ICollection<FolderItem>> GetIncludedItemsAsync(Guid folderId)
    //  {
    //    var folder = m_Folders.FirstOrDefault(f => f.Id == folderId);
    //    if (folder == null) return new List<FolderItem>();

    //    var items = new List<FolderItem>();
    //    if (folder.Items != null)
    //    {
    //      items.AddRange(folder.Items);
    //    }

    //    if (folder.SubFolderIds != null)
    //    {
    //      items.AddRange(folder.SubFolderIds.Select(folderSubFolderId => new FolderItem { ItemType = "Folder", ItemRealId = folderSubFolderId.ToString() }));
    //    }
    //    return items;
    //  }
    //  public Task<ICollection<FolderItem>> GetIncludedItemsByTypeAsync(string folderId, string type) => throw new NotImplementedException();
    //  public Task IncludeFolderToFolderAsync(string superFolderId, string subFolderId, TransactionViewModel transaction) => throw new NotImplementedException();
    //  public async Task IncludeFolderToProjectAsync(Guid folderId, string projectRealId, TransactionViewModel transaction)
    //  {
    //    var folderToInclude = m_Folders.FirstOrDefault(f => f.Id == folderId);
    //    if (folderToInclude == null || !string.IsNullOrEmpty(folderToInclude.ProjectId)) { return; }

    //    folderToInclude.ProjectId = projectRealId;
    //  }

    //  public Task IncludeItemToFolderAsync(Guid folderId, TFolderItemCreateModel item) => throw new NotImplementedException();

    //  public async Task RemoveFolderByIdAsync(Guid folderId, TransactionViewModel transaction)
    //  {
    //    var folderToRemove = m_Folders.FirstOrDefault(f => f.Id == folderId);
    //    if (folderToRemove == null) { return; }

    //    m_Folders.Remove(folderToRemove);
    //  }
    //  public async Task CommitTransaction(TransactionViewModel tVm) { }
    //  public async Task CancelTransaction(TransactionViewModel tVm) { }
    //}
    public Task<Folder> CreateFolderAsync(Models.Transactable.DocumentStructure.TFolderCreateModel folder)
    {
      throw new NotImplementedException();
    }

    public Task IncludeFolderToFolderAsync(string superFolderId, string subFolderId, TransactionInfo transaction)
    {
      throw new NotImplementedException();
    }

    public Task ExcludeFolderFromFolderAsync(Guid superFolderId, Guid subFolderId, TransactionInfo transaction)
    {
      throw new NotImplementedException();
    }

    public Task IncludeFolderToProjectAsync(Guid folderId, string projectRealId, TransactionInfo transaction)
    {
      throw new NotImplementedException();
    }

    public Task ExcludeFolderFromProjectAsync(Guid folderId, TransactionInfo transaction)
    {
      throw new NotImplementedException();
    }

    public Task IncludeItemToFolderAsync(Guid folderId,
      Models.Transactable.DocumentStructure.TFolderItemCreateModel item)
    {
      throw new NotImplementedException();
    }

    public Task ExcludeItemFromFolderAsync(Guid folderId, string itemRealId, TransactionInfo transaction)
    {
      throw new NotImplementedException();
    }

    public Task<ICollection<Folder>> GetIncludedFoldersAsync(Guid folderId)
    {
      throw new NotImplementedException();
    }

    public Task<ICollection<FolderItem>> GetIncludedItemsAsync(Guid folderId)
    {
      throw new NotImplementedException();
    }

    public Task<ICollection<FolderItem>> GetIncludedItemsByTypeAsync(string folderId, string type)
    {
      throw new NotImplementedException();
    }

    public Task<Folder> GetFolderByIdAsync(Guid id)
    {
      throw new NotImplementedException();
    }

    public Task<ICollection<Folder>> GetFoldersByProjectIdLazyAsync(string projectId)
    {
      throw new NotImplementedException();
    }

    public Task<ICollection<Folder>> GetFoldersByProjectIdGreedyAsync(string projectId)
    {
      throw new NotImplementedException();
    }

    public Task<ICollection<Folder>> GetAllFoldersAsync()
    {
      throw new NotImplementedException();
    }

    public Task<ICollection<string>> GetAllowedTypesAsync()
    {
      throw new NotImplementedException();
    }

    public Task RemoveFolderByIdAsync(Guid folderId, TransactionInfo transaction)
    {
      throw new NotImplementedException();
    }

    public Task CommitTransactionAsync(TransactionInfo tInfo)
    {
      throw new NotImplementedException();
    }

    public Task CancelTransactionAsync(TransactionInfo tInfo)
    {
      throw new NotImplementedException();
    }
  }
}
