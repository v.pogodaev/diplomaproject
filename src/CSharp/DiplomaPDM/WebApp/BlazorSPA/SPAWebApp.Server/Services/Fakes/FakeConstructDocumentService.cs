﻿//using System;
//using System.Collections.Generic;
//using System.Threading.Tasks;
//using SPAWebApp.Server.Services.Contracts;
//using SPAWebApp.Shared.Models.Services.ConstructDocument;
//using SPAWebApp.Shared.Models.ViewModels.ConstructDocument;

//namespace SPAWebApp.Server.Services.Fakes
//{
//#pragma warning disable CS1998
//  public class FakeConstructDocumentService : IConstructDocumentService
//  {
//    //  private readonly List<Document> m_Documents = new List<Document>
//    //  {
//    //    new Document
//    //      {Id = "5ce856efffac3a056ae77738", Designation = "ИЮКЛ.494311.012.И1", Revision = 1, Name = "Клапан невозвратный с датчиком. Инструкция по эксплуатации", State = "На проверке",},
//    //    new Document
//    //      {Id = "5ce856f6ffac3a056ae7773a", Designation = "ИЮКЛ.491415.001-01.И1", Revision = 2, Name = "Захлопка путевая фланцевая вертикальная. Инструкция по эксплуатации", State = "Утвержден",},
//    //    new Document
//    //      {Id = "5ce8571affac3a056ae7773d", Designation = "Н-167893", Revision = 3, Name = "Клапан запорный (невозвратно--запорный) прямоточный с ручным управлением", State = "В работе",},
//    //    new Document
//    //      {Id = "5ce8571effac3a056ae7773f", Designation = "ГОСТ 1536", Revision = 1, Name = "Клапан пусковой проходной фланцевый", State = "Устарел",},
//    //    new Document
//    //      {Id = "5ce85721ffac3a056ae77741", Designation = "ИЮКЛ.493671.012", Revision = 2, Name = "Клапан пусковой штуцерный ", State = "Устарел",},
//    //    new Document
//    //      {Id = "5ce85724ffac3a056ae77743", Designation = "ИЮКЛ.491415.001-01.И1", Revision = 1, Name = "Захлопка путевая фланцевая угловая. Инструкция по эксплуатации", State = "В работе",},
//    //    new Document
//    //      {Id = "5ce8572affac3a056ae77745", Designation = "ИЮКЛ.493211.001", Revision = 5, Name = "Переключатель АС-002", State = "В работе",},
//    //    new Document
//    //      {Id = "5ce8572dffac3a056ae77747", Designation = "Н-153267", Revision = 1, Name = "Клапан редукционный проходной штуцерный с автоматическим регулированием ", State = "Устарел",},
//    //    new Document
//    //      {Id = "5ce85732ffac3a056ae77749", Designation = "ИЮКЛ.493211.001.ГОСТ 2822", Revision = 2, Name = "Клапан регулирующий угловой штуцерный", State = "На проверке",},
//    //    new Document
//    //      {Id = "5ce85738ffac3a056ae7774b", Designation = "ИЮКЛ.491425.008.И1", Revision = 1, Name = "Захлопка под приварку. Инструкция по эксплуатации", State = "На проверке",},
//    //    new Document
//    //      {Id = "5ce85741ffac3a056ae7774d", Designation = "Н-129742", Revision = 3, Name = "Клапан регулирующий проходной штуцерный с гидроприводом", State = "В работе",},
//    //    new Document
//    //      {Id = "5ce85745ffac3a056ae7774f", Designation = "ИЮКЛ.493211.002.ГОСТ 1536", Revision = 3, Name = "Клапан регулирующий", State = "Утвержден",},
//    //    new Document
//    //      {Id = "5ce8574cffac3a056ae77751", Designation = "ИЮКЛ.493685.005.ГОСТ 2822", Revision = 1, Name = "Клапан дроссельный проходной фланцевый", State = "Устарел",},
//    //    new Document
//    //      {Id = "5ce85750ffac3a056ae77753", Designation = "ИЮКЛ.491425.008-01.И1", Revision = 7, Name = "Клапан запорный проходной фланцевый. Инструкция по эксплуатации", State = "В работе",},
//    //    new Document
//    //      {Id = "5ce85754ffac3a056ae77755", Designation = "ИЮКЛ.493625.001.ГОСТ 2822", Revision = 2, Name = "Клапан дроссельный проходной фланцевый", State = "Утвержден",},
//    //    new Document
//    //      {Id = "5ce85757ffac3a056ae77757", Designation = "ИЮКЛ.491425.002.И1", Revision = 1, Name = "Клапан запорный проходной фланцевый. Инструкция по эксплуатации", State = "Утвержден",},
//    //    new Document
//    //      {Id = "5ce8575affac3a056ae77759", Designation = "Н-152367", Revision = 1, Name = "Клапан дроссельный проходной фланцевый с ручным управлением", State = "В работе",},
//    //    new Document
//    //      {Id = "5ce85760ffac3a056ae7775b", Designation = "Н-786423", Revision = 2, Name = "Клапан дроссельный проходной штуцерный", State = "На проверке",},
//    //    new Document
//    //      {Id = "5ce85763ffac3a056ae7775d", Designation = "ИЮКЛ.491425.002-01.И1", Revision = 1, Name = "Клапан запорный прямоточный с дистанционным и ручным управлением. Инструкция по эксплуатации", State = "В работе",},
//    //    new Document
//    //      {Id = "5ce8576affac3a056ae7775f", Designation = "ИЮКЛ.491425.004.И1", Revision = 1, Name = "Клапан прямоточный запорный двухседельчатый с дистанционным и ручным управлением. Инструкция по эксплуатации", State = "В работе",}
//    //  };

//    //  public async Task<List<Document>> GetDocumentListByIdsAsync(string[] ids)
//    //  {
//    //    return m_Documents.Where(d => ids.Contains(d.Id)).ToList();
//    //  }

//    //  public async Task<List<Document>> GetAllDocumentsAsync()
//    //  {
//    //    return m_Documents;
//    //  }

//    //  public Task<string> AddFilesAsync(string[] ids)
//    //  {
//    //    throw new NotImplementedException();
//    //  }

//    //  public async Task<ControllerResult> ChangeDocumentAsync(ModifyDocumentViewModel modifyDocumentViewModel)
//    //  {
//    //    // мб не будет работать
//    //    var doc = m_Documents.FirstOrDefault(d => d.Id == modifyDocumentViewModel.Id);
//    //    if (doc == null) return new ControllerResult { Succeed = true };
//    //    m_Documents.Remove(doc);
//    //    doc.Designation = modifyDocumentViewModel.NewDesignation;
//    //    doc.Name = modifyDocumentViewModel.NewName;
//    //    m_Documents.Add(doc);
//    //    return new ControllerResult() { Succeed = true };
//    //  }

//    //  public async Task<ControllerResult> ChangeStateAsync(ChangeStateByDocumentViewModel changeStateByDocumentViewModel)
//    //  {
//    //    // ТЗ не учитывается
//    //    var doc = m_Documents.FirstOrDefault(d => d.Id == changeStateByDocumentViewModel.Id);
//    //    if (doc == null) return new ControllerResult { Succeed = true };
//    //    m_Documents.Remove(doc);
//    //    doc.State = changeStateByDocumentViewModel.State;
//    //    m_Documents.Add(doc);
//    //    return new ControllerResult() { Succeed = true };
//    //  }

//    //  public async Task<string> CreateNewRevAsync(string Id)
//    //  {
//    //    var doc = m_Documents.FirstOrDefault(d => d.Id == Id);
//    //    if (doc == null || doc.State != "Approved") return "";
//    //    var doc2 = new Document()
//    //    {
//    //      Designation = doc.Designation,
//    //      Name = doc.Name,
//    //      Revision = doc.Revision + 1,
//    //      State = "In Work",
//    //      Id = Guid.NewGuid().ToString().Replace("-", "")
//    //    };
//    //    m_Documents.Add(doc2);
//    //    return doc2.Id;
//    //  }

//    //  public async Task<Document> GetDataAsync(string id)
//    //  {
//    //    var doc = m_Documents.FirstOrDefault(d => d.Id == id);
//    //    return doc;
//    //  }

//    //  public async Task<List<Document>> GetAllDataAsync()
//    //  {
//    //    return m_Documents;
//    //  }

//    //  public Task RemoveDocumentAsync(string id)
//    //  {
//    //    throw new NotImplementedException();
//    //  }

//    //  public Task RemoveFiles(List<string> ids)
//    //  {
//    //    throw new NotImplementedException();
//    //  }

//    //  public async Task<string> CreateAsync(CreateNewDocumentViewModel createNewDocumentViewModel)
//    //  {
//    //    var doc = new Document()
//    //    {
//    //      Designation = createNewDocumentViewModel.Designation,
//    //      Name = createNewDocumentViewModel.Name,
//    //      Revision = 1,
//    //      State = "In Check",
//    //      Id = Guid.NewGuid().ToString().Replace("-", "")
//    //    };
//    //    m_Documents.Add(doc);
//    //    return doc.Id;
//    //  }

//    //  public async Task<string> CheckDocumentForCreateNewRevision(string id)
//    //  {
//    //    // как он должен работать?
//    //    return "";
//    //  }

//    //  public Task test()
//    //  {
//    //    throw new NotImplementedException();
//    //  }
//    //}
//    public Task test()
//    {
//      throw new NotImplementedException();
//    }

//    public Task<string> AddFilesAsync(string[] ids)
//    {
//      throw new NotImplementedException();
//    }

//    public Task UpdateDocumentAsync(ModifyDocumentVM modifyDocumentViewModel)
//    {
//      throw new NotImplementedException();
//    }

//    public Task ChangeStateAsync(ChangeStateByDocumentVM changeStateByDocumentViewModel)
//    {
//      throw new NotImplementedException();
//    }

//    public Task<string> CheckDocumentForCreateNewRevisionAsync(string id)
//    {
//      throw new NotImplementedException();
//    }

//    public Task<string> CreateAsync(CreateNewDocumentVM createNewDocumentViewModel)
//    {
//      throw new NotImplementedException();
//    }

//    public Task<string> CreateNewRevAsync(string id)
//    {
//      throw new NotImplementedException();
//    }

//    public Task<List<Document>> GetDocumentListByIdsAsync(string[] ids)
//    {
//      throw new NotImplementedException();
//    }

//    public Task<Document> GetDocumentByIdAsync(string id)
//    {
//      throw new NotImplementedException();
//    }

//    public Task<List<Document>> GetAllDocumentsAsync()
//    {
//      throw new NotImplementedException();
//    }

//    public Task RemoveDocumentAsync(string id)
//    {
//      throw new NotImplementedException();
//    }

//    public Task RemoveFilesAsync(List<string> ids)
//    {
//      throw new NotImplementedException();
//    }
//  }
//}