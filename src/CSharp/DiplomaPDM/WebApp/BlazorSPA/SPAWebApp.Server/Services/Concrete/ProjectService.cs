﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.JSInterop;
using SPAWebApp.Server.Models.Transactable;
using SPAWebApp.Shared.Models.Services.Project;
using IProjectService = SPAWebApp.Server.Services.Contracts.IProjectService;
using TProjectCreateModel = SPAWebApp.Server.Models.Transactable.Project.TProjectCreateModel;
using TProjectItemModel = SPAWebApp.Server.Models.Transactable.Project.TProjectItemModel;
using TProjectModifyModel = SPAWebApp.Server.Models.Transactable.Project.TProjectModifyModel;

namespace SPAWebApp.Server.Services.Concrete
{
  public class ProjectService : IProjectService
  {
    private readonly HttpClient m_HttpClient;

    public ProjectService(HttpClient httpClient)
    {
      m_HttpClient = httpClient;
    }

    public async Task<Project> CreateProjectAsync(TProjectCreateModel projectModel)
    {
      var stringContent = new StringContent(Json.Serialize(projectModel), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PostAsync(UrlsConfig.ProjectApi.CreateProject(), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return Json.Deserialize<Project>(await result.Content.ReadAsStringAsync());
      }

      // указываю все возможные коды, мб потом с ними что-нибудь сделаем
      // неизвестный код надо обязательно логировать отдельно (пока что всё идёт в default)
      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          // todo: логгер, либо выше его вызывать, либо везде, хз
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task DeleteProjectByIdAsync(Guid projectId, TransactionInfo transaction)
    {
      var stringContent = new StringContent(Json.Serialize(transaction), Encoding.UTF8, "application/json");

      var result = await m_HttpClient.PutAsync(UrlsConfig.ProjectApi.DeleteProjectById(projectId), stringContent);

      if (result.IsSuccessStatusCode) { return; }

      // указываю все возможные коды, мб потом с ними что-нибудь сделаем
      // неизвестный код надо обязательно логировать отдельно (пока что всё идёт в default)
      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return;
        case HttpStatusCode.BadRequest:
        default:
          // todo: логгер, либо выше его вызывать, либо везде, хз
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<Project> ModifyProjectAsync(TProjectModifyModel updatedProject)
    {
      var stringContent = new StringContent(Json.Serialize(updatedProject), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PutAsync(UrlsConfig.ProjectApi.UpdateProject(), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return Json.Deserialize<Project>(await result.Content.ReadAsStringAsync());
      }

      // указываю все возможные коды, мб потом с ними что-нибудь сделаем
      // неизвестный код надо обязательно логировать отдельно (пока что всё идёт в default)
      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return null;
        case HttpStatusCode.BadRequest:
        default:
          // todo: логгер, либо выше его вызывать, либо везде, хз
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task IncludeItemToProjectAsync(TProjectItemModel item)
    {
      var stringContent = new StringContent(Json.Serialize(item), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PutAsync(UrlsConfig.ProjectApi.IncludeItemToProject(), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return;
        case HttpStatusCode.BadRequest:
        default:
          // todo: логгер, либо выше его вызывать, либо везде, хз
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<ICollection<ProjectItem>> GetIncludedItemsAsync(Guid projectId)
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.ProjectApi.GetIncludedItemsByProjectId(projectId));

      if (result.IsSuccessStatusCode)
      {
        var r = Json.Deserialize<ICollection<ProjectItem>>(await result.Content.ReadAsStringAsync());
        return r;
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return new List<ProjectItem>();
        case HttpStatusCode.BadRequest:
        default:
          // todo: логгер, либо выше его вызывать, либо везде, хз
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task ExcludeItemAsync(Guid projectId, string itemRealId, TransactionInfo transaction)
    {
      var stringContent = new StringContent(Json.Serialize(transaction), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PutAsync(UrlsConfig.ProjectApi.ExcludeItemFromProject(projectId, itemRealId), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return;
        case HttpStatusCode.BadRequest:
        default:
          // todo: логгер, либо выше его вызывать, либо везде, хз
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    /// <summary>
    /// NOT IMPLEMENTED
    /// </summary>
    /// <returns></returns>
    public Task<ICollection<ProjectItem>> GetIncludedItemsByTypeAsync(Guid projectId, string type)
    {
      throw new NotImplementedException();
    }

    /// <summary>
    /// NOT IMPLEMENTED
    /// </summary>
    /// <returns></returns>
    public Task<ICollection<string>> GetAllowedTypesAsync()
    {
      throw new NotImplementedException();
    }

    public async Task<ICollection<Project>> GetAllProjectsLazyAsync()
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.ProjectApi.GetAllProjects());

      if (result.IsSuccessStatusCode)
      {
        return Json.Deserialize<ICollection<Project>>(await result.Content.ReadAsStringAsync());
      }

      // todo: логгер, либо выше его вызывать, либо везде, хз
      throw new HttpRequestException(await result.Content.ReadAsStringAsync());
    }

    public async Task<Project> GetProjectByIdAsync(Guid projectId)
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.ProjectApi.GetProjectById(projectId));

      if (result.IsSuccessStatusCode)
      {
        return Json.Deserialize<Project>(await result.Content.ReadAsStringAsync());
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return null;
        case HttpStatusCode.BadRequest:
        default:
          // todo: логгер, либо выше его вызывать, либо везде, хз
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task CommitTransactionAsync(TransactionInfo transaction)
    {
      var stringContent = new StringContent(Json.Serialize(transaction), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PutAsync(UrlsConfig.ProjectApi.CommitTransaction(), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      // указываю все возможные коды, мб потом с ними что-нибудь сделаем
      // неизвестный код надо обязательно логировать отдельно (пока что всё идёт в default)
      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return;
        case HttpStatusCode.BadRequest:
        default:
          // todo: логгер, либо выше его вызывать, либо везде, хз
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task CancelTransactionAsync(TransactionInfo transaction)
    {
      var stringContent = new StringContent(Json.Serialize(transaction), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PutAsync(UrlsConfig.ProjectApi.CancelTransaction(), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      // указываю все возможные коды, мб потом с ними что-нибудь сделаем
      // неизвестный код надо обязательно логировать отдельно (пока что всё идёт в default)
      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return;
        case HttpStatusCode.BadRequest:
        default:
          // todo: логгер, либо выше его вызывать, либо везде, хз
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }
  }
}
