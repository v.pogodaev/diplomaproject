﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.JSInterop;
using Newtonsoft.Json;
using SPAWebApp.Server.Models.NonTransactable.FileService;
using SPAWebApp.Server.Services.Contracts;
using File = SPAWebApp.Shared.Models.Services.FileStorage.File;
using FileInfo = SPAWebApp.Server.Models.NonTransactable.FileService.FileInfo;

namespace SPAWebApp.Server.Services.Concrete
{
  public class FileStorageService : IFileStorageService
  {
    private readonly HttpClient m_HttpClient;
    
    public FileStorageService(HttpClient httpClient)
    {
      m_HttpClient = httpClient;
    }
    
    public async Task<DownloadFileModel> DownloadFileAsync(string fileId)
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.FileManager +
                                                 UrlsConfig.FilesManagerOperations.DownloadFileById(fileId));

      if (result.IsSuccessStatusCode)
      {
        var fileData = await result.Content.ReadAsByteArrayAsync();

        return new DownloadFileModel
        {
          FileData = fileData,
          MimeType = result.Content.Headers.ContentType.MediaType,
          FileName = result.Content.Headers.ContentDisposition.FileName
        };
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return null;
        case HttpStatusCode.BadRequest:
        default:
          // todo: логгер, либо выше его вызывать, либо везде, хз
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<List<FileInfo>> GetAllFilesAsync()
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.FileManager + UrlsConfig.FilesManagerOperations.GetAllFile());

      if (result.IsSuccessStatusCode)
      {
        return Json.Deserialize<List<FileInfo>>(await result.Content.ReadAsStringAsync());
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task RemoveFileAsync(string id)
    {
      var result = await m_HttpClient.DeleteAsync(UrlsConfig.FileManager + UrlsConfig.FilesManagerOperations.RemoveFile(id));

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<string> UploadFileAsync(UploadFileModel file)
    {

      file.MultipartFormDataFile.Add(new StringContent(file.Comment, Encoding.UTF8), "comment");
      var result = await m_HttpClient.PostAsync(UrlsConfig.DocumentManager + UrlsConfig.DocumentManagerOperations.PutCommitTransaction(), file.MultipartFormDataFile);

      if (result.IsSuccessStatusCode)
      {
        return await result.Content.ReadAsStringAsync();
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<FileInfo> GetFileInfo(string fileId)
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.FileManager + UrlsConfig.FilesManagerOperations.GetFileById(fileId));

      if (result.IsSuccessStatusCode)
      {
        return Json.Deserialize<FileInfo>(await result.Content.ReadAsStringAsync());
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    private static byte[] GetBytesFromFile(IFormFile file)
    {
      if (file == null || file.Length == 0)
      {
        return null;
      }

      using (var br = new BinaryReader(file.OpenReadStream()))
      {
        return br.ReadBytes((int)file.Length);
      }
    }
  }

  //public async Task<File> DownloadFileAsync(string fileId)
  //{
  //  var result = await m_HttpClient.GetAsync(UrlsConfig.FileManager + 
  //                                           UrlsConfig.FilesManagerOperations.DownloadFileById(fileId));

  //  if (result.IsSuccessStatusCode)
  //  {
  //    var memory = new MemoryStream();
  //    using (var stream = await result.Content.ReadAsStreamAsync())
  //    {
  //      await stream.CopyToAsync(memory);
  //    }
  //    memory.Position = 0;

  //    new File
  //    {
  //      FileData = memory
  //    }

  //    return new FileViewModel
  //    {
  //      FileStream = memory,
  //      MimeType = result.Content.Headers.ContentType.MediaType,
  //      FileName = result.Content.Headers.ContentDisposition.FileName
  //    };
  //  }

  //  switch (result.StatusCode)
  //  {
  //    case HttpStatusCode.NotFound:
  //      return null;
  //    case HttpStatusCode.BadRequest:
  //    default:
  //      // todo: логгер, либо выше его вызывать, либо везде, хз
  //      throw new HttpRequestException(await result.Content.ReadAsStringAsync());
  //  }
  //}




  //public async Task<string> UploadFileAsync(UploadFileModel file)
  //{
  //  var result = await m_HttpClient.PostAsync(UrlsConfig.FilesManagerOperations.UploadFile(file.Comment), file.MultipartFormDataFile);

  //  if (result.IsSuccessStatusCode)
  //  {
  //    return await result.Content.ReadAsStringAsync();
  //  }

  //  switch (result.StatusCode)
  //  {
  //    case HttpStatusCode.BadRequest:
  //    default:
  //      throw new HttpRequestException(await result.Content.ReadAsStringAsync());
  //  }
  //}

  //public async Task DeleteFileAsync(string id)
  //{

  //  var response = await m_HttpClient.GetAsync(UrlsConfig.FilesManagerOperations.DeleteFile(id));
  //  //return new ControllerResult { Succeed = (response.IsSuccessStatusCode), Message = response.StatusCode.ToString() };
  //}
  //public async Task<List<File>> GetAllFilesAsync()
  //{
  //  var data = await m_HttpClient.GetStringAsync(UrlsConfig.DbManager +
  //                                              UrlsConfig.FilesManagerOperations.GetAllFile());

  //  var files = !string.IsNullOrEmpty(data) ? JsonConvert.DeserializeObject<List<File>>(data) : null;

  //  return files;
  //}

  //public async Task<File> GetDataAsync(string id)
  //{
  //  var data = await m_HttpClient.GetStringAsync(UrlsConfig.DbManager +
  //                                               UrlsConfig.FilesManagerOperations.GetFileById(id));

  //  var file = !string.IsNullOrEmpty(data) ? JsonConvert.DeserializeObject<File>(data) : null;

  //  return file;
  //}

  //public async Task RemoveFileAsync(string id)
  //{
  //  string tmp = UrlsConfig.DocumentManager + UrlsConfig.FilesManagerOperations.RemoveFile(id);
  //  await m_HttpClient.DeleteAsync(UrlsConfig.DbManager + UrlsConfig.FilesManagerOperations.RemoveFile(id));
  //}



  //public Task<FileInfo> GetFileInfo(string fileId)
  //{
  //  throw new NotImplementedException();
  //}

  //public Task<DownloadFileModel> GetFileAndInfo(string fileId)
  //{
  //  throw new NotImplementedException();
  //}
}
