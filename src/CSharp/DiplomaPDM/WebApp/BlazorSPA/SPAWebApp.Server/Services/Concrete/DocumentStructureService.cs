﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.JSInterop;
using SPAWebApp.Server.Models.Transactable;
using SPAWebApp.Server.Services.Contracts;
using SPAWebApp.Shared.Models.Services.DocumentStructure;
using TFolderCreateModel = SPAWebApp.Server.Models.Transactable.DocumentStructure.TFolderCreateModel;
using TFolderItemCreateModel = SPAWebApp.Server.Models.Transactable.DocumentStructure.TFolderItemCreateModel;

namespace SPAWebApp.Server.Services.Concrete
{
  public class DocumentStructureService : IDocumentStructureService
  {
    private readonly HttpClient m_HttpClient;

    public DocumentStructureService(HttpClient httpClient)
    {
      m_HttpClient = httpClient;
    }


    public async Task<Folder> CreateFolderAsync(TFolderCreateModel folder)
    {
      var stringContent = new StringContent(Json.Serialize(folder), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PostAsync(UrlsConfig.DocumentStructureApi.CreateFolder(), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return Json.Deserialize<Folder>(await result.Content.ReadAsStringAsync());
      }

      // указываю все возможные коды, мб потом с ними что-нибудь сделаем
      // неизвестный код надо обязательно логировать отдельно (пока что всё идёт в default)
      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task IncludeFolderToFolderAsync(string superFolderId, string subFolderId,
      TransactionInfo transaction)
    {
      var stringContent = new StringContent(Json.Serialize(transaction), 
        Encoding.UTF8, 
        "application/json");
      var result = await m_HttpClient.PutAsync(
        UrlsConfig.DocumentStructureApi.PutIncludeFolderToFolder(superFolderId, subFolderId), 
        stringContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return;
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task ExcludeFolderFromFolderAsync(Guid superFolderId, Guid subFolderId, TransactionInfo transaction)
    {
      var stringContent = new StringContent(Json.Serialize(transaction), 
        Encoding.UTF8, 
        "application/json");
      var result = await m_HttpClient.PutAsync(
        UrlsConfig.DocumentStructureApi.PutExcludeFolderFromFolder(superFolderId.ToString(), subFolderId.ToString()),
        stringContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return;
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task IncludeFolderToProjectAsync(Guid folderId, string projectRealId, TransactionInfo transaction)
    {
      var stringContent = new StringContent(Json.Serialize(transaction), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PutAsync(UrlsConfig.DocumentStructureApi.PutIncludeFolderToProject(folderId.ToString(), projectRealId), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return;
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task ExcludeFolderFromProjectAsync(Guid folderId, TransactionInfo transaction)
    {
      var stringContent = new StringContent(Json.Serialize(transaction), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PutAsync(UrlsConfig.DocumentStructureApi.PutExcludeFolderFromProject(folderId.ToString()), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return;
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task IncludeItemToFolderAsync(Guid folderId, TFolderItemCreateModel item)
    {
      var stringContent = new StringContent(Json.Serialize(item), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PutAsync(UrlsConfig.DocumentStructureApi.PutIncludeItemToFolder(folderId.ToString()), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return;
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task ExcludeItemFromFolderAsync(Guid folderId, string itemRealId, TransactionInfo transaction)
    {
      var stringContent = new StringContent(Json.Serialize(transaction), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PutAsync(UrlsConfig.DocumentStructureApi.PutExcludeItemFromFolder(folderId.ToString(), itemRealId), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return;
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<ICollection<Folder>> GetIncludedFoldersAsync(Guid folderId)
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.DocumentStructureApi.GetIncludedFolders(folderId.ToString()));

      if (result.IsSuccessStatusCode)
      {
        return Json.Deserialize<ICollection<Folder>>(await result.Content.ReadAsStringAsync());
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return new List<Folder>();
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<ICollection<FolderItem>> GetIncludedItemsAsync(Guid folderId)
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.DocumentStructureApi.GetIncludedItems(folderId.ToString()));

      if (result.IsSuccessStatusCode)
      {
        return Json.Deserialize<ICollection<FolderItem>>(await result.Content.ReadAsStringAsync());
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return new List<FolderItem>();
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<ICollection<FolderItem>> GetIncludedItemsByTypeAsync(string folderId, string type)
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.DocumentStructureApi.GetIncludedItemsByType(folderId.ToString(), type));

      if (result.IsSuccessStatusCode)
      {
        return Json.Deserialize<ICollection<FolderItem>>(await result.Content.ReadAsStringAsync());
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return new List<FolderItem>();
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<Folder> GetFolderByIdAsync(Guid id)
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.DocumentStructureApi.GetFolderById(id.ToString()));

      if (result.IsSuccessStatusCode)
      {
        return Json.Deserialize<Folder>(await result.Content.ReadAsStringAsync());
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return null;
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<ICollection<Folder>> GetFoldersByProjectIdLazyAsync(string projectId)
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.DocumentStructureApi.GetFoldersByProjectIdLazy(projectId));

      if (result.IsSuccessStatusCode)
      {
        return Json.Deserialize<ICollection<Folder>>(await result.Content.ReadAsStringAsync());
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return new List<Folder>();
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<ICollection<Folder>> GetFoldersByProjectIdGreedyAsync(string projectId)
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.DocumentStructureApi.GetFoldersByProjectIdGreedy(projectId));

      if (result.IsSuccessStatusCode)
      {
        return Json.Deserialize<ICollection<Folder>>(await result.Content.ReadAsStringAsync());
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return new List<Folder>();
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<ICollection<Folder>> GetAllFoldersAsync()
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.DocumentStructureApi.GetAllFolders());

      if (result.IsSuccessStatusCode)
      {
        return Json.Deserialize<ICollection<Folder>>(await result.Content.ReadAsStringAsync());
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return new List<Folder>();
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<ICollection<string>> GetAllowedTypesAsync()
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.DocumentStructureApi.GetAllowedTypes());

      if (result.IsSuccessStatusCode)
      {
        return Json.Deserialize<ICollection<string>>(await result.Content.ReadAsStringAsync());
      }

      switch (result.StatusCode)
      {
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task RemoveFolderByIdAsync(Guid folderId, TransactionInfo transaction)
    {
      var stringContent = new StringContent(Json.Serialize(transaction), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PutAsync(UrlsConfig.DocumentStructureApi.PutRemoveFolder(folderId.ToString()), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return;
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task CommitTransactionAsync(TransactionInfo tInfo)
    {
      var stringContent = new StringContent(Json.Serialize(tInfo), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PutAsync(UrlsConfig.DocumentStructureApi.PutCommitTransaction(), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return;
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task CancelTransactionAsync(TransactionInfo tInfo)
    {
      var stringContent = new StringContent(Json.Serialize(tInfo), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PutAsync(UrlsConfig.DocumentStructureApi.PutCommitTransaction(), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
          return;
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }
  }
}
