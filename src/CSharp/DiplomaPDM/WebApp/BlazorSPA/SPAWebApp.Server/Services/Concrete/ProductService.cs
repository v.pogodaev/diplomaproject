﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using SPAWebApp.Server.Models.Transactable;
using SPAWebApp.Server.Models.Transactable.Product;
using SPAWebApp.Server.Services.Contracts;
using SPAWebApp.Shared.Models.Services.Product;
using TProductCreateModel = SPAWebApp.Server.Models.Transactable.Product.TProductCreateModel;
using TProductUpdateModel = SPAWebApp.Server.Models.Transactable.Product.TProductUpdateModel;

namespace SPAWebApp.Server.Services.Concrete
{

  public class ProductService : IProductService
  {
    private readonly HttpClient m_HttpClient;

    public ProductService(HttpClient httpClient)
    {
      m_HttpClient = httpClient;
    }


    public async Task CancelTransactionAsync(TransactionInfo transaction)
    {
      var stringContent = new StringContent(Json.Serialize(transaction), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PutAsync(UrlsConfig.ProductManager + UrlsConfig.ProductManagerOperations.CancelTransaction(), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task CommitTransactionAsync(TransactionInfo transaction)
    {
      var stringContent = new StringContent(Json.Serialize(transaction), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PutAsync(UrlsConfig.ProductManager + UrlsConfig.ProductManagerOperations.CommitTransaction(), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<string> CreateProductAsync(TProductCreateModel productCreateModel)
    {
      var stringContent = new StringContent(Json.Serialize(productCreateModel), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PostAsync(UrlsConfig.ProductManager + UrlsConfig.ProductManagerOperations.CreateProduct(), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return await result.Content.ReadAsStringAsync();
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }
    
    public async Task<List<Product>> GetAllProductsAsync()
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.ProductManager +
                                               UrlsConfig.ProductManagerOperations.GetAllProducts());

      if (result.IsSuccessStatusCode)
      {
        return Json.Deserialize<List<Product>>(await result.Content.ReadAsStringAsync());
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<Product> GetProductByIdAsync(string id)
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.ProductManager +
                                               UrlsConfig.ProductManagerOperations.GetProductById(id));

      if (result.IsSuccessStatusCode)
      {
        return Json.Deserialize<Product>(await result.Content.ReadAsStringAsync());
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<List<Product>> GetProductListByIdsAsync(string[] ids)
    {
      var s = UrlsConfig.ProductManager + UrlsConfig.ProductManagerOperations.GetProductListByIds();
      if (ids != null)
      {
        s += "?";
        foreach (string id in ids)
        {
          s += $"productIds={id}&";
        }

        s = s.Remove(s.Length - 1);
      }
      
      var result = await m_HttpClient.GetAsync(s);
      if (result.IsSuccessStatusCode)
      {
        return Json.Deserialize<List<Product>>(await result.Content.ReadAsStringAsync());
      }
      
      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }
    public async Task PutInCheckAsync(string productId, TransactionInfo transactionInfo)
    {
      var stringContent = new StringContent(Json.Serialize(transactionInfo), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PostAsync(UrlsConfig.ProductManager + UrlsConfig.ProductManagerOperations.PutInCheck(productId), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task GetInWorkAsync(string productId, TransactionInfo transactionInfo)
    {
      var stringContent = new StringContent(Json.Serialize(transactionInfo), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PostAsync(UrlsConfig.ProductManager + UrlsConfig.ProductManagerOperations.GetInWork(productId), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task RemoveProductAsync(TProductRemoveModel productRemoveModel)
    {
      var stringContent = new StringContent(Json.Serialize(productRemoveModel), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PostAsync(UrlsConfig.ProductManager + UrlsConfig.ProductManagerOperations.RemoveProduct(), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task UpdateAsync(string productId, TProductUpdateModel productUpdateModel)
    {
      var stringContent = new StringContent(Json.Serialize(productUpdateModel), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PostAsync(UrlsConfig.ProductManager + UrlsConfig.ProductManagerOperations.Update(productId), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task UpdateStateAsync(TProductUpdateStateModel productUpdateStateModel)
    {
      var stringContent = new StringContent(Json.Serialize(productUpdateStateModel), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PostAsync(UrlsConfig.ProductManager + UrlsConfig.ProductManagerOperations.UpdateState(), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }
  }
}
