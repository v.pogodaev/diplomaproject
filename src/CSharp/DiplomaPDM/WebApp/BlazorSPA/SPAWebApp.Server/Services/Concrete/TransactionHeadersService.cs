﻿using System;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.JSInterop;
using SPAWebApp.Server.Models.NonTransactable.TransactionHeaders;
using SPAWebApp.Shared.Models.Services.TransactionHeaders;

namespace SPAWebApp.Server.Services.Concrete
{
  public class TransactionHeadersService : Contracts.ITransactionHeadersService
  {
    private readonly HttpClient m_HttpClient;

    public TransactionHeadersService(HttpClient httpClient)
    {
      m_HttpClient = httpClient;
    }

    public async Task<TransactionHeader> OpenTransactionAsync(TransactionOpen model)
    {
      var stringContent = new StringContent(Json.Serialize(model), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PostAsync(UrlsConfig.TransactionHeadersApi.OpenTransaction(), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return Json.Deserialize<TransactionHeader>(await result.Content.ReadAsStringAsync());
      }

      // указываю все возможные коды, мб потом с ними что-нибудь сделаем
      // неизвестный код надо обязательно логировать отдельно (пока что всё идёт в default)
      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          // todo: логгер, либо выше его вызывать, либо везде, хз
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task CloseTransactionByTransactionIdAsync(string id)
    {
      var result = await m_HttpClient.PutAsync(UrlsConfig.TransactionHeadersApi.CloseByTransactionId(id), null);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      // указываю все возможные коды, мб потом с ними что-нибудь сделаем
      // неизвестный код надо обязательно логировать отдельно (пока что всё идёт в default)
      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        case HttpStatusCode.NotFound:
        default:
          // todo: логгер, либо выше его вызывать, либо везде, хз
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task CloseTransactionByUserIdAsync(string id)
    {
      var result = await m_HttpClient.PutAsync(UrlsConfig.TransactionHeadersApi.CloseByUserId(id), null);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      // указываю все возможные коды, мб потом с ними что-нибудь сделаем
      // неизвестный код надо обязательно логировать отдельно (пока что всё идёт в default)
      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        case HttpStatusCode.NotFound:
        default:
          // todo: логгер, либо выше его вызывать, либо везде, хз
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task CancelTransactionByTransactionIdAsync(string id)
    {
      var result = await m_HttpClient.PutAsync(UrlsConfig.TransactionHeadersApi.CancelByTransactionId(id), null);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      // указываю все возможные коды, мб потом с ними что-нибудь сделаем
      // неизвестный код надо обязательно логировать отдельно (пока что всё идёт в default)
      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        case HttpStatusCode.NotFound:
        default:
          // todo: логгер, либо выше его вызывать, либо везде, хз
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task CancelTransactionByUserIdAsync(string id)
    {
      var result = await m_HttpClient.PutAsync(UrlsConfig.TransactionHeadersApi.CancelByUserId(id), null);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      // указываю все возможные коды, мб потом с ними что-нибудь сделаем
      // неизвестный код надо обязательно логировать отдельно (пока что всё идёт в default)
      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        case HttpStatusCode.NotFound:
        default:
          // todo: логгер, либо выше его вызывать, либо везде, хз
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<TransactionHeader> GetTransactionByTransactionIdAsync(Guid id)
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.TransactionHeadersApi.GetByTransactionId(id.ToString()));

      if (result.IsSuccessStatusCode)
      {
        return Json.Deserialize<TransactionHeader>(await result.Content.ReadAsStringAsync());
      }

      // указываю все возможные коды, мб потом с ними что-нибудь сделаем
      // неизвестный код надо обязательно логировать отдельно (пока что всё идёт в default)
      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        case HttpStatusCode.NotFound:
        default:
          // todo: логгер, либо выше его вызывать, либо везде, хз
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<TransactionHeader> GetTransactionByUserIdAsync(string id)
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.TransactionHeadersApi.GetByUserId(id));

      if (result.IsSuccessStatusCode)
      {
        return Json.Deserialize<TransactionHeader>(await result.Content.ReadAsStringAsync());
      }

      // указываю все возможные коды, мб потом с ними что-нибудь сделаем
      // неизвестный код надо обязательно логировать отдельно (пока что всё идёт в default)
      switch (result.StatusCode)
      {
        case HttpStatusCode.NotFound:
        default:
          // todo: логгер, либо выше его вызывать, либо везде, хз
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }
  }
}
