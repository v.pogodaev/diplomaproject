﻿using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.JSInterop;
using SPAWebApp.Server.Models.Transactable;
using SPAWebApp.Server.Models.Transactable.Relationship;
using SPAWebApp.Server.Services.Contracts;
using SPAWebApp.Shared.Models.Services.Relationship;

namespace SPAWebApp.Server.Services.Concrete
{
  public class RelationshipService : IRelationshipService
  {
    private readonly HttpClient m_HttpClient;

    public RelationshipService(HttpClient httpClient)
    {
      m_HttpClient = httpClient;
    }


    public async Task CancelTransactionAsync(TransactionInfo transaction)
    {
      var stringContent = new StringContent(Json.Serialize(transaction), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PutAsync(UrlsConfig.RelationshipManager + UrlsConfig.RelationshipManagerOperations.CancelTransaction(), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task CommitTransactionAsync(TransactionInfo transaction)
    {
      var stringContent = new StringContent(Json.Serialize(transaction), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PutAsync(UrlsConfig.RelationshipManager + UrlsConfig.RelationshipManagerOperations.CommitTransaction(), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<string> CreateRelationshipAsync(TRelationshipCreateModel relationship)
    {
      var stringContent = new StringContent(Json.Serialize(relationship), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PostAsync(UrlsConfig.RelationshipManager + 
                                                UrlsConfig.RelationshipManagerOperations.CreateRelationship(), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return await result.Content.ReadAsStringAsync();
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<string[]> GetRelatedDocumentsByProductIdAsync(string productId)
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.RelationshipManager + 
                                               UrlsConfig.RelationshipManagerOperations.GetRelatedDocuments(productId));

      if (result.IsSuccessStatusCode)
      {
        return Json.Deserialize<string[]>(await result.Content.ReadAsStringAsync());
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<string[]> GetRelatedProductsByDocumentIdAsync(string documentId)
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.RelationshipManager +
                                               UrlsConfig.RelationshipManagerOperations.GetRelatedProducts(documentId));

      if (result.IsSuccessStatusCode)
      {
        return Json.Deserialize<string[]>(await result.Content.ReadAsStringAsync());
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<Relationship> GetRelationshipByIdAsync(string relationshipId)
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.RelationshipManager +
                                               UrlsConfig.RelationshipManagerOperations.GetRelationshipById(relationshipId));

      if (result.IsSuccessStatusCode)
      {
        return Json.Deserialize<Relationship>(await result.Content.ReadAsStringAsync());
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task RemoveRelationshipAsync(string relationshipId, TransactionInfo transaction)
    {
      var stringContent = new StringContent(Json.Serialize(transaction), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PostAsync(UrlsConfig.RelationshipManager +
                                                UrlsConfig.RelationshipManagerOperations.RemoveRelationship(relationshipId), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task UpdateRelationshipTypeAsync(string relationshipId, TRelationshipUpdateModel relationship)
    {
      var stringContent = new StringContent(Json.Serialize(relationship), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PutAsync(UrlsConfig.RelationshipManager +
                                                UrlsConfig.RelationshipManagerOperations.UpdateRelationship(relationshipId), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }
  }
}
