﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.JSInterop;
using SPAWebApp.Server.Models.Transactable;
using SPAWebApp.Server.Models.Transactable.ConstructDocument;
using SPAWebApp.Server.Services.Contracts;
using SPAWebApp.Shared.Models.Services.ConstructDocument;

namespace SPAWebApp.Server.Services.Concrete
{
  public class ConstructDocumentService : IConstructDocumentService
  {
    private readonly HttpClient m_HttpClient;

    public ConstructDocumentService(HttpClient httpClient)
    {
      m_HttpClient = httpClient;
    }


    public async Task CancelTransactionAsync(TransactionInfo transaction)
    {
      var stringContent = new StringContent(Json.Serialize(transaction), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PutAsync(UrlsConfig.DocumentManager + UrlsConfig.DocumentManagerOperations.PutCancelTransaction(), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task CommitTransactionAsync(TransactionInfo transaction)
    {
      var stringContent = new StringContent(Json.Serialize(transaction), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PutAsync(UrlsConfig.DocumentManager + UrlsConfig.DocumentManagerOperations.PutCommitTransaction(), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<Document> GetDocumentByIdAsync(string id)
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.DocumentManager +
                                               UrlsConfig.DocumentManagerOperations.GetDocumentById(id));

      if (result.IsSuccessStatusCode)
      {
        return Json.Deserialize<Document>(await result.Content.ReadAsStringAsync());
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task UpdateDocumentAsync(TDocumentUpdateModel documentModel, string documentId)
    {
      var stringContent = new StringContent(Json.Serialize(documentModel), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PostAsync(UrlsConfig.DocumentManager + UrlsConfig.DocumentManagerOperations.ChangeDocument(documentId), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task ChangeStateAsync(TDocumentStateUpdateModel stateModel, string documentId)
    {
      var stringContent = new StringContent(Json.Serialize(stateModel), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PostAsync(UrlsConfig.DocumentManager + UrlsConfig.DocumentManagerOperations.ChangeDocumentState(documentId), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<string> CheckDocumentForCreateNewRevisionAsync(string id)
    {
      var result = await m_HttpClient.GetStringAsync(UrlsConfig.DocumentManager + UrlsConfig.DocumentManagerOperations.CheckDocumentForCreateNewRevision(id));
      return result;
    }

    public async Task<string> CreateDocumentAsync(TDocumentCreateModel document)
    {
      var stringContent = new StringContent(Json.Serialize(document), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PostAsync(UrlsConfig.DocumentManager + UrlsConfig.DocumentManagerOperations.CreateDocument(), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return await result.Content.ReadAsStringAsync();
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task<string> CreateNewRevAsync(string id)
    {
      var response = await m_HttpClient.GetStringAsync(UrlsConfig.DocumentManager + UrlsConfig.DocumentManagerOperations.CreateNewRevDocument(id));
      return response;
    }

    public async Task<List<Document>> GetDocumentsAsync()
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.DocumentManager + UrlsConfig.DocumentManagerOperations.GetAllDocuments());

      if (result.IsSuccessStatusCode)
      {
        return Json.Deserialize<List<Document>>(await result.Content.ReadAsStringAsync());
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }
    
    public async Task<List<Document>> GetDocumentsWithTransactAsync()
    {
      var result = await m_HttpClient.GetAsync(UrlsConfig.DocumentManager + UrlsConfig.DocumentManagerOperations.GetAllDocumentsWithTransact());

      if (result.IsSuccessStatusCode)
      {
        return Json.Deserialize<List<Document>>(await result.Content.ReadAsStringAsync());
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }
    
    public async Task<List<Document>> GetDocumentListByIdsAsync(string[] ids)
    {
      //var stringContent = new StringContent(Json.Serialize(ids), Encoding.UTF8, "application/json");
      var s = UrlsConfig.DocumentManager + UrlsConfig.DocumentManagerOperations.GetDocumentsByIds();
      var result = await m_HttpClient.GetAsync(s);

      if (result.IsSuccessStatusCode)
      {
        return Json.Deserialize<List<Document>>(await result.Content.ReadAsStringAsync());
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task RemoveDocumentAsync(TransactionInfo transaction, string id)
    {
      var stringContent = new StringContent(Json.Serialize(transaction), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PostAsync(UrlsConfig.DocumentManager + UrlsConfig.DocumentManagerOperations.RemoveDocument(id), stringContent);

      if (result.IsSuccessStatusCode)
      {
        return;
      }

      switch (result.StatusCode)
      {
        case HttpStatusCode.BadRequest:
        default:
          throw new HttpRequestException(await result.Content.ReadAsStringAsync());
      }
    }

    public async Task test()
    {
      var g = Guid.NewGuid();
      var data = await m_HttpClient.PostAsync(UrlsConfig.DocumentManager + UrlsConfig.DocumentManagerOperations.Test(g),
                                                                        new StringContent(g.ToString(), Encoding.UTF8, "application/json"));
      var s = await data.Content.ReadAsStringAsync();
      //var tmp = Json.Deserialize<string>();
      ;
    }
  }
}
