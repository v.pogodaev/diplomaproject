﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SPAWebApp.Server.Models.Transactable;
using SPAWebApp.Server.Models.Transactable.Relationship;
using SPAWebApp.Shared.Models.Services.Relationship;

namespace SPAWebApp.Server.Services.Contracts
{
  /// <summary>
  /// Микросервис связей изделий и документов
  /// </summary>
  public interface IRelationshipService
  {
    /// <summary>
    /// Отменить транзакцию и откатить изменения
    /// </summary>
    /// <param name="transaction">Данные транзакции</param>
    /// <returns></returns>
    Task CancelTransactionAsync(TransactionInfo transaction);

    /// <summary>
    /// Закрыть транзакцию и применить изменения
    /// </summary>
    /// <param name="transaction">Данные транзакции</param>
    /// <returns></returns>
    Task CommitTransactionAsync(TransactionInfo transaction);

    /// <summary>
    /// Создать связь между изделием и документом
    /// </summary>
    /// <param name="relationship"></param>
    /// <returns></returns>
    Task<string> CreateRelationshipAsync(TRelationshipCreateModel relationship);

    /// <summary>
    /// Получить id связанных с изделием документов
    /// </summary>
    /// <param name="productId"></param>
    /// <returns></returns>
    Task<string[]> GetRelatedDocumentsByProductIdAsync(string productId);

    /// <summary>
    /// Получить id связанных с документов изделий
    /// </summary>
    /// <param name="documentId"></param>
    /// <returns></returns>
    Task<string[]> GetRelatedProductsByDocumentIdAsync(string documentId);

    /// <summary>
    /// Получить информацию об отношении по его id
    /// </summary>
    /// <param name="relationshipId"></param>
    /// <returns></returns>
    Task<Relationship> GetRelationshipByIdAsync(string relationshipId);

    /// <summary>
    /// Удалить связь
    /// </summary>
    /// <param name="relationshipId"></param>
    /// <param name="transaction"></param>
    /// <returns></returns>
    Task RemoveRelationshipAsync(string relationshipId, TransactionInfo transaction);

    /// <summary>
    /// Изменить тип связи
    /// </summary>
    /// <param name="relationshipId"></param>
    /// <param name="relationship"></param>
    /// <returns></returns>
    Task UpdateRelationshipTypeAsync(string relationshipId, TRelationshipUpdateModel relationship);
  }
}
