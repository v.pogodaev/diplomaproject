﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SPAWebApp.Server.Models.Transactable;
using SPAWebApp.Shared.Models.Services.DocumentStructure;
using TFolderCreateModel = SPAWebApp.Server.Models.Transactable.DocumentStructure.TFolderCreateModel;
using TFolderItemCreateModel = SPAWebApp.Server.Models.Transactable.DocumentStructure.TFolderItemCreateModel;

namespace SPAWebApp.Server.Services.Contracts
{
  /// <summary>
  /// Микросервис хранения структур документов
  /// </summary>
  public interface IDocumentStructureService
  {
    /// <summary>
    /// Создание новой папки
    /// </summary>
    /// <param name="folder">Новая папка</param>
    /// <returns></returns>
    Task<Folder> CreateFolderAsync(TFolderCreateModel folder);

    /// <summary>
    /// Включение папки с id == <paramref name="subFolderId"/> в папку c id == <paramref name="superFolderId"/>
    /// </summary>
    /// <param name="superFolderId">Id вышестоящей папки</param>
    /// <param name="subFolderId">Id папки-потомка</param>
    /// <param name="transaction">Данные о транзакции</param>
    /// <returns></returns>
    Task IncludeFolderToFolderAsync(string superFolderId, string subFolderId, TransactionInfo transaction);

    /// <summary>
    /// Отсоединение папки с id == <paramref name="subFolderId"/> от папки c id == <paramref name="superFolderId"/>
    /// </summary>
    /// <param name="superFolderId">Id вышестоящей папки</param>
    /// <param name="subFolderId">Id папки-потомка</param>
    /// <param name="transaction">Данные о транзакции</param>
    /// <returns></returns>
    Task ExcludeFolderFromFolderAsync(Guid superFolderId, Guid subFolderId, TransactionInfo transaction);

    /// <summary>
    /// Включение папки в проект
    /// </summary>
    /// <param name="folderId">Id папки</param>
    /// <param name="projectRealId">Настоящий (из его микросервиса) Id проекта</param>
    /// <param name="transaction">Данные о транзакции</param>
    /// <returns></returns>
    Task IncludeFolderToProjectAsync(Guid folderId, string projectRealId, TransactionInfo transaction);

    /// <summary>
    /// Исключение папки из проекта
    /// </summary>
    /// <param name="folderId">Id папки</param>
    /// <param name="transaction">Данные о транзакции</param>
    /// <returns></returns>
    Task ExcludeFolderFromProjectAsync(Guid folderId, TransactionInfo transaction);

    /// <summary>
    /// Включение элемента в папку
    /// </summary>
    /// <param name="folderId">Id папки</param>
    /// <param name="item">Данные элемента</param>
    /// <returns></returns>
    Task IncludeItemToFolderAsync(Guid folderId, TFolderItemCreateModel item);

    /// <summary>
    /// Удаление элемента из папки
    /// </summary>
    /// <param name="folderId">Id папки</param>
    /// <param name="itemRealId">Настоящий Id элемента</param>
    /// <param name="transaction">Данные о транзакции</param>
    /// <returns></returns>
    Task ExcludeItemFromFolderAsync(Guid folderId, string itemRealId, TransactionInfo transaction);

    /// <summary>
    /// Получение массива id папок, привязанных к папке с id == <paramref name="folderId"/>
    /// </summary>
    /// <param name="folderId">Id папки</param>
    /// <returns></returns>
    Task<ICollection<Folder>> GetIncludedFoldersAsync(Guid folderId);

    /// <summary>
    /// Получение массива id и типов элементов, привязанных к папке
    /// </summary>
    /// <param name="folderId">Id папки</param>
    /// <returns></returns>
    Task<ICollection<FolderItem>> GetIncludedItemsAsync(Guid folderId);

    /// <summary>
    /// Получение списка элементов, привязанных к папке определенного типа
    /// </summary>
    /// <param name="folderId">Id папки</param>
    /// <param name="type">Тип элементов</param>
    /// <returns></returns>
    Task<ICollection<FolderItem>> GetIncludedItemsByTypeAsync(string folderId, string type);

    /// <summary>
    /// Получение информации о папке
    /// </summary>
    /// <param name="id">Id папки</param>
    /// <returns></returns>
    Task<Folder> GetFolderByIdAsync(Guid id);

    /// <summary>
    /// Получение папок, входящих в проект
    /// </summary>
    /// <param name="projectId">Id проекта</param>
    /// <returns></returns>
    Task<ICollection<Folder>> GetFoldersByProjectIdLazyAsync(string projectId);

    /// <summary>
    /// Получение папок, подпапок и подэлементов, входящих в проект
    /// </summary>
    /// <param name="projectId">Id проекта</param>
    /// <returns></returns>
    Task<ICollection<Folder>> GetFoldersByProjectIdGreedyAsync(string projectId);

    /// <summary>
    /// Получение всех папок
    /// </summary>
    /// <returns></returns>
    Task<ICollection<Folder>> GetAllFoldersAsync();

    /// <summary>
    /// Получение списка возможных типов для привязки к папке
    /// </summary>
    /// <returns></returns>
    Task<ICollection<string>> GetAllowedTypesAsync();

    /// <summary>
    /// Удаление папки
    /// </summary>
    /// <param name="folderId">Id папки для удаления</param>
    /// <param name="transaction">Данные транзакции</param>
    /// <returns></returns>
    Task RemoveFolderByIdAsync(Guid folderId, TransactionInfo transaction);

    /// <summary>
    /// Закрыть транзакцию и применить изменения
    /// </summary>
    /// <param name="tInfo">Данные транзакции</param>
    /// <returns></returns>
    Task CommitTransactionAsync(TransactionInfo tInfo);

    /// <summary>
    /// Отменить транзакцию и откатить изменения
    /// </summary>
    /// <param name="tInfo">Данные транзакции</param>
    /// <returns></returns>
    Task CancelTransactionAsync(TransactionInfo tInfo);
  }
}
