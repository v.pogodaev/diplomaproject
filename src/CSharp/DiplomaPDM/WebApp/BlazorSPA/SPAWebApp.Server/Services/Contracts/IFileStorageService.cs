﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SPAWebApp.Server.Models.NonTransactable.FileService;
using SPAWebApp.Shared.Models.Services.FileStorage;

namespace SPAWebApp.Server.Services.Contracts
{
  /// <summary>
  /// Микросервис хранения файлов
  /// </summary>
  public interface IFileStorageService
  {
    /// <summary>
    /// Скачать файл без информации
    /// </summary>
    /// <param name="fileId">Id файла</param>
    /// <returns></returns>
    Task<DownloadFileModel> DownloadFileAsync(string fileId);

    /// <summary>
    /// Получить все файлы из системы
    /// </summary>
    /// <returns></returns>
    Task<List<FileInfo>> GetAllFilesAsync();

    /// <summary>
    /// Удалить файл
    /// </summary>
    /// <param name="id">id файла</param>
    /// <returns></returns>
    Task RemoveFileAsync(string id);

    /// <summary>
    /// Загрузить файл в систему
    /// </summary>
    /// <param name="file"></param>
    /// <returns></returns>
    Task<string> UploadFileAsync(UploadFileModel file);

    /// <summary>
    /// Получить информацию о файле без самого файла
    /// </summary>
    /// <param name="fileId">id файла</param>
    /// <returns></returns>
    Task<FileInfo> GetFileInfo(string fileId);
  }
}
