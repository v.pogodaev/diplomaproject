﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SPAWebApp.Server.Models.Transactable;
using SPAWebApp.Shared.Models.Services.Project;
using TProjectCreateModel = SPAWebApp.Server.Models.Transactable.Project.TProjectCreateModel;
using TProjectItemModel = SPAWebApp.Server.Models.Transactable.Project.TProjectItemModel;
using TProjectModifyModel = SPAWebApp.Server.Models.Transactable.Project.TProjectModifyModel;

namespace SPAWebApp.Server.Services.Contracts
{
  /// <summary>
  /// Микросервис хранения проектов
  /// </summary>
  public interface IProjectService
  {
    /// <summary>
    /// Создание проекта 
    /// </summary>
    /// <param name="projectModel">Модель проекта</param>
    Task<Project> CreateProjectAsync(TProjectCreateModel projectModel);

    /// <summary>
    /// Удаление проекта по id
    /// </summary>
    /// <param name="projectId">Id проекта</param>
    /// <param name="transaction">Данные транзакции</param>
    /// <returns></returns>
    Task DeleteProjectByIdAsync(Guid projectId, TransactionInfo transaction);

    /// <summary>
    /// Редактирование проекта
    /// </summary>
    /// <param name="updatedProject">Новые данные проекта</param>
    /// <returns></returns>
    Task<Project> ModifyProjectAsync(TProjectModifyModel updatedProject);

    /// <summary>
    /// Включение элемента в проект
    /// </summary>
    /// <param name="item">Данные элемента</param>
    /// <returns></returns>
    Task IncludeItemToProjectAsync(TProjectItemModel item);

    /// <summary>
    /// Получение списка id и типов элементов, привязанных к проекту
    /// </summary>
    /// <param name="projectId">Id проекта</param>
    /// <returns></returns>
    Task<ICollection<ProjectItem>> GetIncludedItemsAsync(Guid projectId);

    /// <summary>
    /// Удаление элемента из проекта
    /// </summary>
    /// <param name="projectId">Id проекта</param>
    /// <param name="itemRealId">Настоящий Id элемента</param>
    /// <param name="transaction">Данные о транзакции</param>
    /// <returns></returns>
    Task ExcludeItemAsync(Guid projectId, string itemRealId, TransactionInfo transaction);

    /// <summary>
    /// Получение списка элементов, привязанных к проекту определенного типа
    /// </summary>
    /// <param name="projectId">Id папки</param>
    /// <param name="type">Тип элементов</param>
    /// <returns></returns>
    Task<ICollection<ProjectItem>> GetIncludedItemsByTypeAsync(Guid projectId, string type);

    /// <summary>
    /// Получение списка возможных типов для привязки к проекту
    /// </summary>
    /// <returns></returns>
    Task<ICollection<string>> GetAllowedTypesAsync();

    /// <summary>
    /// Получение проекта по id
    /// </summary>
    /// <param name="projectId">Id проекта</param>
    /// <returns></returns>
    Task<Project> GetProjectByIdAsync(Guid projectId);

    /// <summary>
    /// Получение всех проектов без вложений
    /// </summary>
    /// <returns></returns>
    Task<ICollection<Project>> GetAllProjectsLazyAsync();

    /// <summary>
    /// Закрыть транзакцию и применить изменения
    /// </summary>
    /// <param name="transaction">Данные транзакциии</param>
    /// <returns></returns>
    Task CommitTransactionAsync(TransactionInfo transaction);

    /// <summary>
    /// Отменить транзакцию и откатить изменения
    /// </summary>
    /// <param name="transaction">Данные транзакциии</param>
    /// <returns></returns>
    Task CancelTransactionAsync(TransactionInfo transaction);
  }
}
