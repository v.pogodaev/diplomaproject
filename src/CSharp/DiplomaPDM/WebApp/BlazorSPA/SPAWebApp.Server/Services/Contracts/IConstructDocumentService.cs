﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SPAWebApp.Server.Models.Transactable;
using SPAWebApp.Server.Models.Transactable.ConstructDocument;
using SPAWebApp.Shared.Models.Services.ConstructDocument;

namespace SPAWebApp.Server.Services.Contracts
{
  /// <summary>
  /// Микросервис хранения документов
  /// </summary>
  public interface IConstructDocumentService
  {
    /// <summary>
    /// Отменить транзакцию и откатить изменения
    /// </summary>
    /// <param name="transaction">Данные транзакции</param>
    /// <returns></returns>
    Task CancelTransactionAsync(TransactionInfo transaction);

    /// <summary>
    /// Закрыть транзакцию и применить изменения
    /// </summary>
    /// <param name="transaction">Данные транзакции</param>
    /// <returns></returns>
    Task CommitTransactionAsync(TransactionInfo transaction);

    /// <summary>
    /// Получить данные документа по его id
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    Task<Document> GetDocumentByIdAsync(string id);
    
    /// <summary>
    /// Изменить информации о документе
    /// </summary>
    /// <param name="documentModel"></param>
    /// <param name="documentId"></param>
    /// <returns></returns>
    Task UpdateDocumentAsync(TDocumentUpdateModel documentModel, string documentId);
    
    /// <summary>
    /// Изменить состояние документа
    /// </summary>
    /// <param name="stateModel"></param>
    /// <param name="documentId"></param>
    /// <returns></returns>
    Task ChangeStateAsync(TDocumentStateUpdateModel stateModel, string documentId);
    
    /// <summary>
    /// Проверить на возможность создания новой версии документа
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    Task<string> CheckDocumentForCreateNewRevisionAsync(string id);

    /// <summary>
    /// Создать новый документ
    /// </summary>
    /// <param name="document"></param>
    /// <returns></returns>
    Task<string> CreateDocumentAsync(TDocumentCreateModel document);

    /// <summary>
    /// Создать новую версию документа
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    Task<string> CreateNewRevAsync(string id);
    
    /// <summary>
    /// Получить список документов
    /// </summary>
    /// <returns></returns>
    Task<List<Document>> GetDocumentsAsync();

    /// <summary>
    /// Получить список документов, включая документы с открытыми транзакциями
    /// </summary>
    /// <returns></returns>
    Task<List<Document>> GetDocumentsWithTransactAsync();

    /// <summary>
    /// Получить список документов по их id
    /// </summary>
    /// <param name="ids"></param>
    /// <returns></returns>
    Task<List<Document>> GetDocumentListByIdsAsync(string[] ids);

    /// <summary>
    /// Удалить документ
    /// </summary>
    /// <param name="transaction"></param>
    /// <param name="id"></param>
    /// <returns></returns>
    Task RemoveDocumentAsync(TransactionInfo transaction, string id);


    Task test();
  }
}
