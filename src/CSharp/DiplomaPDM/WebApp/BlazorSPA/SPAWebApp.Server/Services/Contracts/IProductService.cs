﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SPAWebApp.Server.Models.Transactable;
using SPAWebApp.Server.Models.Transactable.Product;
using SPAWebApp.Shared.Models.Services.Product;
using TProductCreateModel = SPAWebApp.Server.Models.Transactable.Product.TProductCreateModel;
using TProductUpdateModel = SPAWebApp.Server.Models.Transactable.Product.TProductUpdateModel;

namespace SPAWebApp.Server.Services.Contracts
{
  /// <summary>
  /// Микросервис хранения карточек изделия
  /// </summary>
  public interface IProductService
  {
    /// <summary>
    /// Отменить транзакцию и откатить изменения
    /// </summary>
    /// <param name="transaction">Данные транзакциии</param>
    /// <returns></returns>
    Task CancelTransactionAsync(TransactionInfo transaction);

    /// <summary>
    /// Закрыть транзакцию и применить изменения
    /// </summary>
    /// <param name="transaction">Данные транзакциии</param>
    /// <returns></returns>
    Task CommitTransactionAsync(TransactionInfo transaction);

    /// <summary>
    /// Создание нового изделия
    /// </summary>
    /// <param name="productCreateModel">Модель изделия</param>
    /// <returns>Id изделия</returns>
    Task<string> CreateProductAsync(TProductCreateModel productCreateModel);

    /// <summary>
    /// Получение списка всех изделий
    /// </summary>
    /// <returns></returns>
    Task<List<Product>> GetAllProductsAsync();

    /// <summary>
    /// Получение изделия по Id
    /// </summary>
    /// <param name="id">Id изделия</param>
    /// <returns></returns>
    Task<Product> GetProductByIdAsync(string id);
    
    /// <summary>
    /// Получение списка изделий по их ids
    /// </summary>
    /// <param name="ids">Id изделий</param>
    /// <returns></returns>
    Task<List<Product>> GetProductListByIdsAsync(string[] ids);

    /// <summary>
    /// Передача изделия на проверку
    /// </summary>
    /// <param name="productId">Id изделия</param>
    /// <param name="transactionInfo">Данные транзакции</param>
    /// <returns></returns>
    Task PutInCheckAsync(string productId, TransactionInfo transactionInfo);
    
    /// <summary>
    /// Взятие изделия в работу
    /// </summary>
    /// <param name="productId">Id изделия</param>
    /// <param name="transactionInfo">Данные транзакции</param>
    /// <returns></returns>
    Task GetInWorkAsync(string productId, TransactionInfo transactionInfo);

    /// <summary>
    /// Удалить изделие
    /// </summary>
    /// <param name="productRemoveModel"></param>
    /// <returns></returns>
    Task RemoveProductAsync(TProductRemoveModel productRemoveModel);
    
    /// <summary>
    /// Обновить информацию об изделии
    /// </summary>
    /// <param name="productUpdateModel">Модель изделия</param>
    /// <returns></returns>
    Task UpdateAsync(string productId, TProductUpdateModel productUpdateModel);
    
    /// <summary>
    /// Обновить состояние изделия
    /// </summary>
    /// <param name="productUpdateStateModel">Модель состояния</param>
    /// <returns></returns>
    Task UpdateStateAsync(TProductUpdateStateModel productUpdateStateModel);
  }
}
