﻿using System;
using System.Threading.Tasks;
using SPAWebApp.Server.Models.NonTransactable.TransactionHeaders;
using SPAWebApp.Shared.Models.Services.TransactionHeaders;

namespace SPAWebApp.Server.Services.Contracts
{
  /// <summary>
  /// Интерфейс для взаимодействия с сервисом заголовков транзакций
  /// </summary>
  public interface ITransactionHeadersService
  {
    /// <summary>
    /// Открыть транзакцию
    /// </summary>
    /// <param name="model">Данные для создания транзакции</param>
    /// <returns></returns>
    Task<TransactionHeader> OpenTransactionAsync(TransactionOpen model);

    /// <summary>
    /// Завершить транзакцию (транзакция завершена успешно) по Id транзакции
    /// </summary>
    /// <param name="id">Id транзакции</param>
    /// <returns></returns>
    Task CloseTransactionByTransactionIdAsync(string id);

    /// <summary>
    /// Завершить транзакцию (транзакция завершена успешно) по Id пользователя
    /// </summary>
    /// <param name="id">Id пользователя</param>
    /// <returns></returns>
    Task CloseTransactionByUserIdAsync(string id);

    /// <summary>
    /// Отменить транзакцию (транзакция завершена неуспешно) по Id транзакции
    /// </summary>
    /// <param name="id">Id транзакции</param>
    /// <returns></returns>
    Task CancelTransactionByTransactionIdAsync(string id);

    /// <summary>
    /// Завершить транзакцию (транзакция завершена неуспешно) по Id пользователя
    /// </summary>
    /// <param name="id">Id пользователя</param>
    /// <returns></returns>
    Task CancelTransactionByUserIdAsync(string id);

    /// <summary>
    /// Получить данные транзакции по Id транзакции
    /// </summary>
    /// <param name="id">Id транзакции</param>
    /// <returns></returns>
    Task<TransactionHeader> GetTransactionByTransactionIdAsync(Guid id);

    /// <summary>
    /// Получить данные открытой транзакции пользователя по его Id
    /// </summary>
    /// <param name="id">Id пользователя</param>
    /// <returns></returns>
    Task<TransactionHeader> GetTransactionByUserIdAsync(string id);
  }
}
