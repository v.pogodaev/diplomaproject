﻿using System.Collections.Generic;

namespace SPAWebApp.Server.Services
{
  public class ExternalServiceLogin
  {
    // NOTE:
    //  пока один сервис с внешней авторизацией,
    //  можно сделать просто через if
    //  и без констант/.json-файлов и т.д.
    public static List<string> NeededRolesForService(string serviceName)
    {
      if (serviceName == "CADService")
      {
        return new List<string>{"engineer"};
      }

      return null;
    }
  }
}
