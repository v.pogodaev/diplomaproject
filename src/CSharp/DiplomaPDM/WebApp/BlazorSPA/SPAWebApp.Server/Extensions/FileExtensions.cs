﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using SPAWebApp.Server.Models.NonTransactable.FileService;
using SPAWebApp.Shared.Models.ViewModels.FileService;

namespace SPAWebApp.Server.Extensions
{
  public static class FileExtensions
  {
    public static MultipartFormDataContent GetMultipartFormData(this IFormFile file)
    {
      byte[] fileBytes;
      using (var br = new BinaryReader(file.OpenReadStream()))
      {
        fileBytes = br.ReadBytes((int) file.Length);
      }

      return new MultipartFormDataContent()
      {
        {
          new ByteArrayContent(fileBytes),
          file.Name,
          file.FileName
        }
      };
    }
  }
}
