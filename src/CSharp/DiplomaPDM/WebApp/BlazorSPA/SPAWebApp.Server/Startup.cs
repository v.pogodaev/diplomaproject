using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.ResponseCompression;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using SPAWebApp.Server.Infrastructure;
using SPAWebApp.Server.Models.NonTransactable.Account;
using SPAWebApp.Server.Services.Concrete;
using SPAWebApp.Server.Services.Contracts;
using SPAWebApp.Server.Services.Fakes;

namespace SPAWebApp.Server
{
  public class Startup
  {
    public IConfiguration Configuration { get; }

    public Startup(IConfiguration configuration)
    {
      Configuration = configuration;
    }

    // This method gets called by the runtime. Use this method to add services to the container.
    // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
    public void ConfigureServices(IServiceCollection services)
    {
      services.AddCustomOptions(Configuration);

      services.AddCustomIdentity(Configuration);

      services.AddMvc().AddNewtonsoftJson();
      services.AddHttpContextAccessor();
      services.AddCustomSession();
      services.AddResponseCompression(opts =>
      {
        opts.MimeTypes = ResponseCompressionDefaults.MimeTypes.Concat(
                  new[] { "application/octet-stream" });
      });
      services.AddHttpServices();
    }

    // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
      using (var serviceScope = app.ApplicationServices.GetRequiredService<IServiceScopeFactory>().CreateScope())
      {
        serviceScope.ServiceProvider.GetService<UserDbContext>().Database.Migrate();
      }

      app.UseResponseCompression();

      if (env.IsDevelopment())
      {
        app.UseDeveloperExceptionPage();
        app.UseBlazorDebugging();
      }

      app.UseRouting();
      app.UseAuthentication();
      app.UseAuthorization();

      app.UseSession();

      app.UseEndpoints(routes =>
      {
        routes.MapDefaultControllerRoute();
      });

      app.UseBlazor<Client.Startup>();
    }
  }

  public static class CustomServiceExtensionMethods
  {
    public static IServiceCollection AddCustomOptions(this IServiceCollection services, IConfiguration configuration)
    {
      services.AddOptions();
      services.Configure<AppSettings>(configuration);
      return services;
    }

    public static IServiceCollection AddCustomIdentity(this IServiceCollection services, IConfiguration configuration)
    {
      services.AddDbContext<UserDbContext>(options =>
      {
        options.UseSqlServer(configuration["UserDBConnectionString"],
          sqlServerOptionsAction: sqlOptions =>
          {
            sqlOptions.MigrationsAssembly(typeof(Startup).GetTypeInfo().Assembly.GetName().Name);
            sqlOptions.EnableRetryOnFailure(maxRetryCount: 10, maxRetryDelay: TimeSpan.FromSeconds(30),
              errorNumbersToAdd: null);
          });
      });

      services.AddIdentity<ApplicationUser, IdentityRole<Guid>>()
        .AddRoleManager<RoleManager<IdentityRole<Guid>>>()
        .AddEntityFrameworkStores<UserDbContext>()
        .AddDefaultTokenProviders();

      services.Configure<IdentityOptions>(options =>
      {
        // Password settings
        options.Password.RequireDigit = false;
        options.Password.RequiredLength = 6;
        options.Password.RequireNonAlphanumeric = false;
        options.Password.RequireUppercase = false;
        options.Password.RequireLowercase = false;
        //options.Password.RequiredUniqueChars = 6;

        // Lockout settings
        options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(30);
        options.Lockout.MaxFailedAccessAttempts = 10;
        options.Lockout.AllowedForNewUsers = true;

        // User settings
        options.User.RequireUniqueEmail = false;
      });

      services.Configure<ApiBehaviorOptions>(options =>
      {
        options.SuppressModelStateInvalidFilter = true;
      });

      services.ConfigureApplicationCookie(options =>
      {
        options.Cookie.HttpOnly = false;
        options.Events.OnRedirectToLogin = context =>
        {
          context.Response.StatusCode = 401;
          return Task.CompletedTask;
        };
      });

      return services;
    }

    public static IServiceCollection AddCustomSession(this IServiceCollection services)
    {
      services.AddDistributedMemoryCache();
      services.AddSession(options =>
      {
        options.Cookie.Name = "DiplomaWeb.Session";
        options.IdleTimeout = TimeSpan.FromMinutes(30);
      });
      return services;
    }
    public static IServiceCollection AddHttpServices(this IServiceCollection services)
    {
    //todo: всякие политики для повтора
      services.AddHttpClient<IConstructDocumentService, ConstructDocumentService>();
      services.AddHttpClient<IFileStorageService, FileStorageService>();
      services.AddHttpClient<IProductService, ProductService>();
      services.AddHttpClient<IRelationshipService, RelationshipService>();

      services.AddHttpClient<ITransactionHeadersService, TransactionHeadersService>();
      services.AddHttpClient<IProjectService, ProjectService>();
      services.AddHttpClient<IDocumentStructureService, DocumentStructureService>();

      //Тут тоже пришлось ставить библиотечку для работы с файлами
      //services.AddScoped<IFileReaderService, FileReaderService>();

      // fakes
      //services.AddTransient<IConstructDocumentService, FakeConstructDocumentService>();
      //services.AddTransient<IFileStorageService, FileStorageService>();
      //services.AddTransient<IProductService, FakeProductService>();
      //services.AddTransient<IRelationshipService, FakeRelationshipService>();

      //services.AddTransient<ITransactionHeadersService, FakeTransactionHeadersService>();
      //services.AddTransient<IProjectService, FakeProjectService>();
      //services.AddTransient<IDocumentStructureService, FakeDocumentStructureService>();

      return services;
    }
  }
}
