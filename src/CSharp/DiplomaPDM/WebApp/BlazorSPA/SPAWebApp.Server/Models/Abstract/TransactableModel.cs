﻿using System;
using System.ComponentModel.DataAnnotations;
using SPAWebApp.Server.Models.Transactable;

namespace SPAWebApp.Server.Models.Abstract
{
  /// <summary>
  /// Транзакционная модель
  /// </summary>
  public abstract class TransactableModel
  {
    protected TransactableModel() { }

    protected TransactableModel(TransactionInfo info)
    {
      TransactionId = info.TransactionId;
      SessionId = info.SessionId;
      UserId = info.UserId;
    }

    /// <summary>
    /// Id транзакции. Если транзакция завершена, то null
    /// </summary>
    [Required]
    public string TransactionId { get; set; }

    /// <summary>
    /// Id пользователя
    /// </summary>
    [Required]
    public string UserId { get; set; }

    /// <summary>
    /// Id сессии
    /// </summary>
    [Required]
    public string SessionId { get; set; }
  }
}
