﻿using SPAWebApp.Server.Models.Abstract;
using SPAWebApp.Shared.Models.ViewModels;
using SPAWebApp.Shared.Models.ViewModels.DocumentStructure;

namespace SPAWebApp.Server.Models.Transactable.DocumentStructure
{
  public class TFolderCreateModel : TransactableModel
  {
    public TFolderCreateModel() { }

    public TFolderCreateModel(FolderCreateVM folder, TransactionInfo transaction)
    {
      Sign = folder.Sign;
      Name = folder.Name;
      TransactionId = transaction.TransactionId;
      UserId = transaction.UserId;
      SessionId = transaction.SessionId;
    }

    /// <summary>
    /// Обозначение папки
    /// </summary>
    public string Sign { get; set; }

    /// <summary>
    /// Имя папки
    /// </summary>
    public string Name { get; set; }
  }
}
