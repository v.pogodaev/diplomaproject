﻿using SPAWebApp.Server.Models.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SPAWebApp.Server.Models.Transactable.Product
{
  public class TProductUpdateStateModel : TransactableModel
  {
    public TProductUpdateStateModel() { }

    public TProductUpdateStateModel(string productId,string state,  TransactionInfo transaction)
    {
      ObjectId = productId;
      State = state;
      TransactionId = transaction.TransactionId;
      UserId = transaction.UserId;
      SessionId = transaction.SessionId;
    }

    public string ObjectId { get; set; }

    public string State { get; set; }
  }
}
