﻿using System;
using SPAWebApp.Server.Models.Abstract;
using SPAWebApp.Shared.Models.Services.DocumentStructure;

namespace SPAWebApp.Server.Models.Transactable.DocumentStructure
{
  public class TFolderItemCreateModel : TransactableModel
  {
    public TFolderItemCreateModel() { }

    public TFolderItemCreateModel(FolderItem item, TransactionInfo transaction)
    {
      FolderId = item.FolderId;
      ItemRealId = item.ItemRealId;
      ItemType = item.ItemType;
      TransactionId = transaction.TransactionId;
      UserId = transaction.UserId;
      SessionId = transaction.SessionId;
    }

    public Guid FolderId { get; set; }
    public string ItemRealId { get; set; }
    public string ItemType { get; set; }
  }
}
