﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SPAWebApp.Server.Models.Abstract;
using SPAWebApp.Shared.Models.ViewModels.ConstructDocument;

namespace SPAWebApp.Server.Models.Transactable.ConstructDocument
{
  public class TDocumentStateUpdateModel : TransactableModel
  {
    public TDocumentStateUpdateModel() { }

    public TDocumentStateUpdateModel(ChangeStateByDocumentVM state, TransactionInfo transaction) : base(transaction)
    {
      State = state.State;
    }

    public string State { get; set; }
  }
}
