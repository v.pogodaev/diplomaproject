﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SPAWebApp.Server.Models.Abstract;

namespace SPAWebApp.Server.Models.Transactable.Relationship
{
  public class TRelationshipCreateModel : TransactableModel
  {
    public TRelationshipCreateModel() { }

    public TRelationshipCreateModel(Shared.Models.Services.Relationship.Relationship relationship,
      TransactionInfo transaction) : base(transaction)
    {
      DocumentId = relationship.DocumentId;
      ProductId = relationship.ProductId;
      Type = relationship.Type;
    }
   
    public TRelationshipCreateModel(TransactionInfo transaction) : base(transaction) { }
    
    public string Type { get; set; }
    public string DocumentId { get; set; }
    public string ProductId { get; set; }
  }
}
