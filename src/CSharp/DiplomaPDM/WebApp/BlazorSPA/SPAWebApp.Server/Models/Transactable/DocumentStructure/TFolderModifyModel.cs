﻿using System;
using SPAWebApp.Server.Models.Abstract;
using SPAWebApp.Shared.Models.ViewModels.DocumentStructure;

namespace SPAWebApp.Server.Models.Transactable.DocumentStructure
{
  public class TFolderModifyModel : TransactableModel
  {
    public TFolderModifyModel() { }
    public TFolderModifyModel(FolderModifyVM folder, TransactionInfo transaction)
    {
      Id = folder.Id;
      Sign = folder.Sign;
      Name = folder.Name;
      TransactionId = transaction.TransactionId;
      UserId = transaction.UserId;
      SessionId = transaction.SessionId;
    }

    public Guid Id { get; set; }
    public string Sign { get; set; }
    public string Name { get; set; }
  }
}
