﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SPAWebApp.Server.Models.Abstract;
using SPAWebApp.Shared.Models.ViewModels.ConstructDocument;

namespace SPAWebApp.Server.Models.Transactable.ConstructDocument
{
  public class TDocumentUpdateModel : TransactableModel
  {
    public TDocumentUpdateModel() { }

    public TDocumentUpdateModel(ModifyDocumentVM document, TransactionInfo transaction) : base(transaction)
    {
      NewDesignation = document.NewDesignation;
      NewName = document.NewName;
    }

    public string NewDesignation { get; set; }
    public string NewName { get; set; }
  }
}
