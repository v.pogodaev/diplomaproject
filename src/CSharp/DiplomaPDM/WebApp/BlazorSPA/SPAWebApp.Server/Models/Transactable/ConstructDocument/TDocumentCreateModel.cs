﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SPAWebApp.Server.Models.Abstract;
using SPAWebApp.Shared.Models.ViewModels.ConstructDocument;

namespace SPAWebApp.Server.Models.Transactable.ConstructDocument
{
  public class TDocumentCreateModel : TransactableModel
  {
    public TDocumentCreateModel() { }

    public TDocumentCreateModel(CreateNewDocumentVM createModel, TransactionInfo transaction) : base(transaction)
    {
      Name = createModel.Name;
      Designation = createModel.Designation;
    }

    public string Name { get; set; }
    public string Designation { get; set; }
  }
}
