﻿using SPAWebApp.Server.Models.Abstract;
using SPAWebApp.Shared.Models.ViewModels.Product;

namespace SPAWebApp.Server.Models.Transactable.Product
{
  public class TProductUpdateModel : TransactableModel
  {
    public TProductUpdateModel() { }

    public TProductUpdateModel(UpdateProductVM updateProductViewModel, TransactionInfo transaction)
    {
      Designation = updateProductViewModel.Designation;
      Name = updateProductViewModel.Name;
      Weight = updateProductViewModel.Weight;
      TransactionId = transaction.TransactionId;
      UserId = transaction.UserId;
      SessionId = transaction.SessionId;
    }
    public string Designation { get; set; }
    public string Name { get; set; }
    public double Weight { get; set; }
  }
}
