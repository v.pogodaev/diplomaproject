﻿using System;
using SPAWebApp.Server.Models.Abstract;
using SPAWebApp.Shared.Models.ViewModels;
using SPAWebApp.Shared.Models.ViewModels.Project;

namespace SPAWebApp.Server.Models.Transactable.Project
{
  public class TProjectModifyModel : TransactableModel
  {
    public TProjectModifyModel() { }
    public TProjectModifyModel(ProjectModifyVM project, TransactionInfo transaction)
    {
      Id = project.Id;
      Sign = project.Sign;
      Name = project.Name;
      TransactionId = transaction.TransactionId;
      UserId = transaction.UserId;
      SessionId = transaction.SessionId;
    }

    public Guid Id { get; set; }
    public string Sign { get; set; }
    public string Name { get; set; }
  }
}
