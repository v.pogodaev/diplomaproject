﻿using SPAWebApp.Server.Models.Abstract;
using SPAWebApp.Shared.Models.ViewModels.Project;

namespace SPAWebApp.Server.Models.Transactable.Project
{
  public class TProjectCreateModel : TransactableModel
  {
    public TProjectCreateModel() { }
    public TProjectCreateModel(ProjectCreateVM project, TransactionInfo transaction)
    {
      Sign = project.Sign;
      Name = project.Name;
      TransactionId = transaction.TransactionId;
      UserId = transaction.UserId;
      SessionId = transaction.SessionId;
    }

    public string Sign { get; set; }
    public string Name { get; set; }
  }
}
