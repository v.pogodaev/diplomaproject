﻿using SPAWebApp.Server.Models.Abstract;
using SPAWebApp.Shared.Models.ViewModels.Project;

namespace SPAWebApp.Server.Models.Transactable.Project
{
  public class TProjectItemModel : TransactableModel
  {
    public TProjectItemModel() { }
    public TProjectItemModel(ProjectItemCreateVM item, TransactionInfo transaction)
    {
      ProjectId = item.ProjectId;
      ItemRealId = item.ItemRealId;
      ItemType = item.ItemType;
      TransactionId = transaction.TransactionId;
      UserId = transaction.UserId;
      SessionId = transaction.SessionId;
    }

    public string ProjectId { get; set; }
    public string ItemRealId { get; set; }
    public string ItemType { get; set; }
  }
}
