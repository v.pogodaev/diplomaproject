﻿using SPAWebApp.Server.Models.Abstract;
using SPAWebApp.Shared.Models.ViewModels.Product;

namespace SPAWebApp.Server.Models.Transactable.Product
{
  public class TProductCreateModel : TransactableModel
  {
    public TProductCreateModel() { }

    public TProductCreateModel(CreateProductVM createProductViewModel, TransactionInfo transaction)
    {
      Designation = createProductViewModel.Designation;
      Name = createProductViewModel.Name;
      Weight = createProductViewModel.Weight;
      TransactionId = transaction.TransactionId;
      UserId = transaction.UserId;
      SessionId = transaction.SessionId;
    }

    public string Designation { get; set; }
    public string Name { get; set; }
    public double Weight { get; set; }
  }
}
