﻿using SPAWebApp.Server.Models.Abstract;

namespace SPAWebApp.Server.Models.Transactable.Product
{
  public class TProductRemoveModel : TransactableModel
  {
    public TProductRemoveModel() { }

    public TProductRemoveModel(string productId, TransactionInfo transaction)
    {
      ProductId = productId;
      TransactionId = transaction.TransactionId;
      UserId = transaction.UserId;
      SessionId = transaction.SessionId;
    }

    public string ProductId { get; set; }
  }
}
