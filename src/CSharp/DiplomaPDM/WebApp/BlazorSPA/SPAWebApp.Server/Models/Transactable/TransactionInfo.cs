﻿using System.ComponentModel.DataAnnotations;
using SPAWebApp.Shared.Models.Services.TransactionHeaders;

namespace SPAWebApp.Server.Models.Transactable
{
  /// <summary>
  /// Необходимая информация по транзакции
  /// </summary>
  public class TransactionInfo
  {
    public TransactionInfo() { }
    public TransactionInfo(TransactionHeader transaction)
    {
      TransactionId = transaction.Id.ToString();
      UserId = transaction.UserId;
      SessionId = transaction.SessionId;
    }

    /// <summary>
    /// Id транзакции
    /// </summary>
    [Required]
    public string TransactionId { get; set; }

    /// <summary>
    /// Id пользователя
    /// </summary>
    [Required]
    public string UserId { get; set; }

    /// <summary>
    /// Id сессии
    /// </summary>
    [Required]
    public string SessionId { get; set; }
  }
}
