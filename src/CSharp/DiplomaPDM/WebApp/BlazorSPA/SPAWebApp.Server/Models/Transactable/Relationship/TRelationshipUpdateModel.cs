﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SPAWebApp.Server.Models.Abstract;

namespace SPAWebApp.Server.Models.Transactable.Relationship
{
  public class TRelationshipUpdateModel : TransactableModel
  {
    public TRelationshipUpdateModel() { }

    public TRelationshipUpdateModel(TransactionInfo transaction) : base(transaction) { }

    public string NewType { get; set; }
  }
}
