﻿using System.ComponentModel.DataAnnotations;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace SPAWebApp.Server.Models.NonTransactable.CAD
{
  public class ProductFromCadViewModel
  {
    [Required]
    [FromForm(Name = "ProductId")]
    public string ProductId { get; set; }

    [Required]
    [FromForm(Name = "PartNumber")]
    public string PartNumber { get; set; }

    [FromForm(Name = "Description")]
    public string Description { get; set; }

    [FromForm(Name = "WeightKg")]
    public double WeightKg { get; set; }

    [FromForm(Name = "State")]
    public string State { get; set; }

    [Required]
    [FromForm(Name = "CADModel")]
    public IFormFile CADModel { get; set; }

    [Required]
    [FromForm(Name = "ViewerModel")]
    public IFormFile ViewerModel { get; set; }
  }
}
