﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SPAWebApp.Server.Models.NonTransactable.FileService
{
  public class DownloadFileModel
  {
    public byte[] FileData { get; set; }
    public string MimeType { get; set; }
    public string FileName { get; set; }
  }
}
