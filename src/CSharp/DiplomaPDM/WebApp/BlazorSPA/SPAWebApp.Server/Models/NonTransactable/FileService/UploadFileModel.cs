﻿using System.Net.Http;
using Microsoft.AspNetCore.Http;

namespace SPAWebApp.Server.Models.NonTransactable.FileService
{
  /// <summary>
  /// Модель для загрузки файла и его информации в Файловый сервис
  /// </summary>
  public class UploadFileModel
  {
    /// <summary>
    /// Комментарий к файлу
    /// </summary>
    public string Comment { get; set; }

    /// <summary>
    /// Данные для передачи
    /// </summary>
    //public IFormFile File { get; set; }
    public MultipartFormDataContent MultipartFormDataFile { get; set; }
  }
}
