﻿using System;

namespace SPAWebApp.Server.Models.NonTransactable.CAD
{
  public class ProjectViewModel
  {
    public Guid Id { get; set; }
    public string Sign { get; set; }
    public string Name { get; set; }
  }
}
