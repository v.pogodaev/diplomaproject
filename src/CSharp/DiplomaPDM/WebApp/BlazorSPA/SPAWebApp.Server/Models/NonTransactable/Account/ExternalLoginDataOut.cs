﻿using System;

namespace SPAWebApp.Server.Models.NonTransactable.Account
{
  /// <summary>
  /// Данные выдаваемые при входе в систему для внешних сервисов, а не через веб-клиент
  /// </summary>
  public class ExternalLoginDataOut
  {
    public Guid UserId { get; set; }
    public Guid SessionId { get; set; }
  }
}
