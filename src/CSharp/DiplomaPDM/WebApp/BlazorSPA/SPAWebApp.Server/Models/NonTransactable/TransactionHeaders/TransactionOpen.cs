﻿namespace SPAWebApp.Server.Models.NonTransactable.TransactionHeaders
{
  /// <summary>
  /// Данные для открытия транзакции
  /// </summary>
  public class TransactionOpen
  {
    public string UserId { get; set; }
    public string SessionId { get; set; }
  }
}
