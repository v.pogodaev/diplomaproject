﻿using System;
using Microsoft.AspNetCore.Identity;
using SPAWebApp.Shared.Authentication.Models;
using SPAWebApp.Shared.Models.ViewModels.Account;

namespace SPAWebApp.Server.Models.NonTransactable.Account
{
  /// <summary>
  /// Данные пользователя
  /// </summary>
  public class ApplicationUser : IdentityUser<Guid>
  {
    public AdminUserInfoVM GetAdminUserInfo() => new AdminUserInfoVM
    {
      Id = Id,
      UserName = UserName
    };
  }
}
