﻿using System.ComponentModel.DataAnnotations;

namespace SPAWebApp.Server.Models.NonTransactable.Account
{
  /// <summary>
  /// Данные для входа в систему для внешних сервисов, а не через веб-клиент
  /// </summary>
  public class ExternalLoginDataIn
  {
    [Required]
    public string Login { get; set; }

    [Required]
    public string Password { get; set; }

    [Required]
    public string ServiceName { get; set; }
  }
}
