﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SPAWebApp.Server.Extensions;
using SPAWebApp.Server.Models.NonTransactable.Account;
using SPAWebApp.Server.Services;
using SPAWebApp.Shared;
using SPAWebApp.Shared.Authentication.Models;
using SPAWebApp.Shared.Models.ViewModels.Account;

namespace SPAWebApp.Server.Controllers
{
  [Route("api/[controller]/[action]")]
  [ApiController]
  [ApiExplorerSettings(IgnoreApi = true)]
  public class AccountController : ControllerBase
  {
    private readonly UserManager<ApplicationUser> m_UserManager;
    private readonly SignInManager<ApplicationUser> m_SignInManager;
    private readonly RoleManager<IdentityRole<Guid>> m_RoleManager;

    public AccountController(
      UserManager<ApplicationUser> userManager, 
      SignInManager<ApplicationUser> signInManager, 
      RoleManager<IdentityRole<Guid>> roleManager)
    {
      m_UserManager = userManager;
      m_SignInManager = signInManager;
      m_RoleManager = roleManager;
    }

    [AllowAnonymous]
    [HttpPost]
    public async Task<IActionResult> Login(LoginVM model)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState.Values
          .SelectMany(state => state.Errors)
          .Select(error => error.ErrorMessage)
          .FirstOrDefault());
      }

      var user = await m_UserManager.FindByNameAsync(model.UserName);
      if (user == null)
      {
        return BadRequest("Пользователь не существует");
      }

      var singInResult = await m_SignInManager
        .CheckPasswordSignInAsync(user, model.Password, false);
      if (!singInResult.Succeeded)
      {
        return BadRequest("Неверный пароль");
      }

      await m_SignInManager.SignInAsync(user, model.RememberMe);

      return Ok(BuildUserInfo(user));
    }

    [AllowAnonymous]
    [HttpPost]
    public async Task<IActionResult> Register(RegisterVM parameters)
    {
      if (!ModelState.IsValid)
      {
        return BadRequest(ModelState.Values
          .SelectMany(state => state.Errors)
          .Select(error => error.ErrorMessage)
          .FirstOrDefault());
      }

      var user = new ApplicationUser { UserName = parameters.UserName };
      var result = await m_UserManager.CreateAsync(user, parameters.Password);
      if (!result.Succeeded)
      {
        return BadRequest(result.Errors.FirstOrDefault()?.Description);
      }

      await m_UserManager.AddToRoleAsync(user, "user");

      return await Login(new LoginVM
      {
        UserName = parameters.UserName,
        Password = parameters.Password
      });
    }
    
    [Authorize]
    [HttpGet]
    public async Task<List<string>> GetAssignedUserRoles()
    {
      var user = await m_UserManager.GetUserAsync(HttpContext.User);

      if (user == null) return null;

      var userRoles = await m_UserManager.GetRolesAsync(user);

      return userRoles.ToList();
    }

    [Authorize]
    [HttpPost]
    public async Task<IActionResult> Logout()
    {
      await m_SignInManager.SignOutAsync();
      HttpContext.Session.Set("SessionId", Guid.Empty);
      return Ok();
    }

    [Authorize]
    [HttpGet]
    public async Task<UserInfo> UserInfo()
    {
      var user = await m_UserManager.GetUserAsync(HttpContext.User);
      return BuildUserInfo(user);
    }

    [HttpPost]
    public async Task<ActionResult<ExternalLoginDataOut>> LoginFromService(
      ExternalLoginDataIn loginData)
    {
      if (!TryValidateModel(loginData))
      {
        return BadRequest();
      }
      var user = await m_UserManager.FindByNameAsync(loginData.Login);
      if (user == null)
      {
        return BadRequest("User does not exist");
      }
      var singInResult = await m_SignInManager.CheckPasswordSignInAsync(user, loginData.Password, false);
      if (!singInResult.Succeeded)
      {
        return BadRequest("Invalid password");
      }

      var neededRoles = ExternalServiceLogin.NeededRolesForService(loginData.ServiceName);
      var userRoles = await m_UserManager.GetRolesAsync(user);
      bool hasRoles = true;
      foreach (string role in neededRoles)
      {
        if (!userRoles.Contains(role))
        {
          hasRoles = false;
        }
      }

      if (!hasRoles)
      {
        return BadRequest("User has not all needed roles for this service");
      }

      return new ExternalLoginDataOut { UserId = user.Id, SessionId = Guid.NewGuid() };
    }

    #region Admin

    [Authorize]
    [HttpGet]
    public async Task<bool> IsAdmin()
    {
      return await m_UserManager.IsInRoleAsync(await m_UserManager.GetUserAsync(HttpContext.User), "admin");
    }

    [Authorize(Roles = "admin")]
    [HttpGet]
    public List<IdentityRole<Guid>> GetRoles()
    {
      return m_RoleManager.Roles.ToList();
    }

    [Authorize(Roles = "admin")]
    [HttpGet]
    public List<AdminUserInfoVM> GetUsers()
    {
      return m_UserManager.Users.Select(u => new AdminUserInfoVM() { UserName = u.UserName, Id = u.Id }).ToList();
    }

    [Authorize(Roles = "admin")]
    [HttpPost]
    public async Task<IActionResult> ChangeUserRoles(List<string> roles, [FromQuery]string userId)
    {
      // получаем пользователя
      var user = await m_UserManager.FindByIdAsync(userId);
      if (user != null)
      {
        // получем список ролей пользователя
        var userRoles = await m_UserManager.GetRolesAsync(user);


        // получаем список ролей, которые были добавлены
        var addedRoles = roles.Except(userRoles);
        // получаем роли, которые были удалены
        var removedRoles = userRoles.Except(roles);

        var enumerable = removedRoles as string[] ?? removedRoles.ToArray();
        if (user.UserName == "admin" && enumerable.Contains("admin"))
          return BadRequest();

        await m_UserManager.AddToRolesAsync(user, addedRoles);

        await m_UserManager.RemoveFromRolesAsync(user, enumerable);

        return Ok();
      }

      return NotFound();
    }

    [Authorize(Roles = "admin")]
    [HttpGet]
    public async Task<ActionResult<IEnumerable<UserRoleVM>>> GetAllUserRoles(string userId)
    {
      var user = await m_UserManager.FindByIdAsync(userId);

      var userRoles = await m_UserManager.GetRolesAsync(user);
      var result = m_RoleManager.Roles.ToList().Select(r => new UserRoleVM { IsAssigned = userRoles.Contains(r.Name), RoleName = r.Name });

      return Ok(result);
    }
    #endregion

    [NonAction]
    private UserInfo BuildUserInfo(ApplicationUser user)
    {
      if (HttpContext.Session.Get<Guid>("SessionId") == default)
      {
        HttpContext.Session.Set("SessionId", Guid.NewGuid());
      }

      return new UserInfo
      {
        Username = user.UserName
      };
    }
  }
}
