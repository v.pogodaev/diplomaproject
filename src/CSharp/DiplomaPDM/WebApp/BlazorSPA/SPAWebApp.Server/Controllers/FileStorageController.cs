﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SPAWebApp.Shared;
using System;
using SPAWebApp.Server.Models.NonTransactable.FileService;
using SPAWebApp.Server.Services.Contracts;
using SPAWebApp.Shared.Models.Services.FileStorage;
using SPAWebApp.Shared.Models.ViewModels.FileService;

namespace SPAWebApp.Server.Controllers
{
  [Route("storage")]
  [ApiExplorerSettings(IgnoreApi = true)]
  [ApiController]
  public class FileStorageController : ControllerBase
  {
    private readonly IFileStorageService m_FileStorageService;
    public FileStorageController(IFileStorageService fileStorageService)
    {
      m_FileStorageService = fileStorageService;
    }
    [Route("Test")]
    [HttpGet]
    public async Task<ActionResult> test()
    {
      ;
      return Ok();
    }

    [Route(Urls.Files.UPLOAD_FILE)]
    [HttpPost]
    public async Task<ActionResult<File>> UploadFileAsync([FromBody] UploadFileVM uploadFileViewModel)
    {
      ;
      
      if (uploadFileViewModel == null)
      {
        return BadRequest("Модель не должна быть пустой");
      }

     // var data = await m_FileStorageService.UploadFileAsync(uploadFileViewModel);

      //if (data == null)
      //{
      //  return BadRequest("Запрос не прошел");
      //}

      return Ok(uploadFileViewModel);
    }

    [Route("getFile/{id}")]
    [HttpGet]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(File), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<File>> GetFileByIdAsync(string id)
    {
      if (string.IsNullOrEmpty(id))
      {
        return BadRequest("Id не должен быть пустым");
      }
      //KRAVCHENKO: Здесь был вызов метода GetData и по твоим словам, это теперь DownloadFileAsync, так что я его заменил что запустилось
      var file = await m_FileStorageService.DownloadFileAsync(id);

      if (file == null)
      {
        return NotFound("Документа с таким id нет в системе");
      }

      return Ok(file);
    }

    [Route("getAllFilesData")]
    [HttpGet]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(File), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<FileInfo>> GetAllDocumentsAsync()
    {
      Console.WriteLine("BBBBBBB");
      List<FileInfo> files = await m_FileStorageService.GetAllFilesAsync();

      if (files.Count == 0)
      {
        return NotFound("Ничего не найдено");
      }

      return Ok(files);
    }

    [Route("removeFile/{id}")]
    [HttpDelete]
    public async Task RemoveFileAsync(string id)
    {
      Console.WriteLine("Controller");
      await m_FileStorageService.RemoveFileAsync(id);
    }
  }
}
