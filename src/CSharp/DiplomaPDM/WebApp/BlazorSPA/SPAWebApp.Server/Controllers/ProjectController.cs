﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SPAWebApp.Server.Extensions;
using SPAWebApp.Server.Models.NonTransactable.Account;
using SPAWebApp.Server.Models.NonTransactable.TransactionHeaders;
using SPAWebApp.Server.Models.Transactable;
using SPAWebApp.Shared;
using SPAWebApp.Shared.Models.Services.Project;
using SPAWebApp.Shared.Models.ViewModels.Project;
using TProjectCreateModel = SPAWebApp.Server.Models.Transactable.Project.TProjectCreateModel;
using TProjectModifyModel = SPAWebApp.Server.Models.Transactable.Project.TProjectModifyModel;

namespace SPAWebApp.Server.Controllers
{
  [Route(Urls.Project.CONTROLLER)]
  [ApiController]
  [ApiExplorerSettings(IgnoreApi = true)]
  [Authorize]
  public class ProjectController : ControllerBase
  {
    private readonly Services.Contracts.IProjectService m_ProjectService;
    private readonly Services.Contracts.ITransactionHeadersService m_TransactionService;
    private readonly UserManager<ApplicationUser> m_UserManager;


    public ProjectController(
      Services.Contracts.IProjectService projectService, 
      Services.Contracts.ITransactionHeadersService transactionService, 
      UserManager<ApplicationUser> userManager)
    {
      m_ProjectService = projectService;
      m_TransactionService = transactionService;
      m_UserManager = userManager;
    }

    [HttpGet]
    [Route(Urls.Project.GET_PROJECTS_LAZY)]
    public async Task<IActionResult> GetAllProjectsLazyAsync()
    {
      return Ok(await m_ProjectService.GetAllProjectsLazyAsync());
    }

    [HttpGet]
    [Route(Urls.Project.GET_PROJECT)]
    public async Task<IActionResult> GetProjectByIdAsync(Guid projectId)
    {
      try
      {
        return Ok(await m_ProjectService.GetProjectByIdAsync(projectId));
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    [HttpDelete]
    [Route(Urls.Project.DELETE_PROJECT)]
    public async Task<IActionResult> DeleteProjectByIdAsync(Guid projectId)
    {
      TransactionInfo tVM = null;

      try
      {
        var transaction = await m_TransactionService.OpenTransactionAsync(new TransactionOpen
        {
          UserId = m_UserManager.GetUserId(HttpContext.User),
          SessionId = HttpContext.Session.Get<Guid>("SessionId").ToString()
        });
        tVM = new TransactionInfo(transaction);
      }
      catch
      {
        return BadRequest();
      }

      try
      {
        // todo: можно ещё направить запрос в DocumentStructureService, дабы убедиться, что нет папок, привязанных к этому проекту
        await m_ProjectService.DeleteProjectByIdAsync(projectId, tVM);
        await m_ProjectService.CommitTransactionAsync(tVM);
        await m_TransactionService.CloseTransactionByTransactionIdAsync(tVM.TransactionId);
        return Ok();
      }
      catch
      {
        // todo: logger
        try
        {
          await m_ProjectService.CancelTransactionAsync(tVM);
        }
        catch (Exception e)
        {
          // todo: logger
        }
        try
        {
          await m_TransactionService.CancelTransactionByTransactionIdAsync(tVM.TransactionId);
        }
        catch (Exception e)
        {
          // todo: logger
        }

        return BadRequest("Во время транзакции произошла ошибка");
      }
    }

    [HttpPost]
    [Route(Urls.Project.CREATE_PROJECT)]
    public async Task<IActionResult> CreateProjectAsync(ProjectCreateVM model)
    {
      if (!TryValidateModel(model))
      {
        return BadRequest("Некорректные данные");
      }

      TransactionInfo tVM = null;

      try
      {
        var transaction = await m_TransactionService.OpenTransactionAsync(new TransactionOpen
        {
          UserId = m_UserManager.GetUserId(HttpContext.User),
          SessionId = HttpContext.Session.Get<Guid>("SessionId").ToString()
        });
        tVM = new TransactionInfo(transaction);
      }
      catch
      {
        return BadRequest();
      }

      try
      {
        var result = await m_ProjectService.CreateProjectAsync(new TProjectCreateModel(model, tVM));
        await m_ProjectService.CommitTransactionAsync(tVM);
        await m_TransactionService.CloseTransactionByTransactionIdAsync(tVM.TransactionId);
        return Ok(result);
      }
      catch
      {
        // todo: logger
        try
        {
          await m_ProjectService.CancelTransactionAsync(tVM);
        }
        catch (Exception e)
        {
          // todo: logger
        }
        try
        {
          await m_TransactionService.CancelTransactionByTransactionIdAsync(tVM.TransactionId);
        }
        catch (Exception e)
        {
          // todo: logger
        }

        return BadRequest("Во время транзакции произошла ошибка");
      }
    }

    [HttpPut]
    [Route(Urls.Project.MODIFY_PROJECT)]
    public async Task<IActionResult> ModifyProjectByIdAsync(ProjectModifyVM model)
    {
      if (!TryValidateModel(model))
      {
        return BadRequest("Некорректные данные");
      }

      TransactionInfo tVM = null;

      try
      {
        var transaction = await m_TransactionService.OpenTransactionAsync(new TransactionOpen
        {
          UserId = m_UserManager.GetUserId(HttpContext.User),
          SessionId = HttpContext.Session.Get<Guid>("SessionId").ToString()
        });
        tVM = new TransactionInfo(transaction);
      }
      catch
      {
        return BadRequest();
      }

      try
      {
        var updatedProject = await m_ProjectService.ModifyProjectAsync(new TProjectModifyModel(model, tVM));
        await m_ProjectService.CommitTransactionAsync(tVM);
        await m_TransactionService.CloseTransactionByTransactionIdAsync(tVM.TransactionId);
        return Ok(updatedProject);
      }
      catch
      {
        // todo: logger
        try
        {
          await m_ProjectService.CancelTransactionAsync(tVM);
        }
        catch (Exception e)
        {
          // todo: logger
        }
        try
        {
          await m_TransactionService.CancelTransactionByTransactionIdAsync(tVM.TransactionId);
        }
        catch (Exception e)
        {
          // todo: logger
        }

        return BadRequest("Во время транзакции произошла ошибка");
      }
    }

    [HttpGet]
    [Route(Urls.Project.GET_INCLUDED_ITEMS_BY_ID)]
    public async Task<ActionResult<ICollection<ProjectItem>>> GetIncludedItemsByProjectIdAsync(Guid projectId)
    {
      return Ok(await m_ProjectService.GetIncludedItemsAsync(projectId));
    }
  }
}