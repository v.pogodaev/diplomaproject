﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using SPAWebApp.Server.Services.Contracts;
using SPAWebApp.Shared.Models.Services.ConstructDocument;
using SPAWebApp.Shared.Models.Services.Product;


namespace SPAWebApp.Server.Controllers
{
  [Route("cards_storage")]
  [ApiExplorerSettings(IgnoreApi = true)]
  [ApiController]
  public class ProductCardController : ControllerBase
  {
    private readonly IRelationshipService m_RelationshipService;
    private readonly IConstructDocumentService m_ConstructDocumentService;
    private readonly IProductService m_ProductService;

    public ProductCardController(IProductService productService, IRelationshipService relationshipService, IConstructDocumentService constructDocumentService)
    {
      m_ProductService = productService;
      m_RelationshipService = relationshipService;
      m_ConstructDocumentService = constructDocumentService;
    }

    [Route("getAllProducts")]
    [HttpGet]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(Product), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<Product>> GetAllDocumentsAsync()
    {
      List<Product> documents = await m_ProductService.GetAllProductsAsync();

      if (documents.Count == 0)
      {
        return NotFound("Ничего не найдено");
      }

      return Ok(documents);
    }

    private const int PRODUCTS_PER_PAGE = 20;


    /// <summary>
    /// Возвращает количество страниц и изделия с первой страницы
    /// </summary>
    /// <returns></returns>
    [Route("GetProductsOfFirstPageAsync")]
    [HttpGet]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(ProductAndPage), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<ProductAndPage>> GetProductsOfFirstPageAsync()
    {
      List<Product> products = await m_ProductService.GetAllProductsAsync();

      if (products.Count == 0)
      {
        return NotFound("Ничего не найдено");
      }

      int pages = products.Count / PRODUCTS_PER_PAGE;

      var result = new ProductAndPage {Pages = pages, Products = products.GetRange(0, PRODUCTS_PER_PAGE)};

      return Ok(result);
    }

    public class ProductAndPage
    {
      public int Pages { get; set; }
      public List<Product> Products { get; set; }
    }

    [Route("GetProductsOfPageAsync/{page}")]
    [HttpGet]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(Tuple<int, List<Product>>), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<List<Product>>> GetProductsOfPageAsync(int page)
    {
      List<Product> products = await m_ProductService.GetAllProductsAsync();

      if (products.Count == 0)
      {
        return NotFound("Ничего не найдено");
      }

      int pages = products.Count / PRODUCTS_PER_PAGE;

      var result = products.GetRange((pages > 0) ? pages * page : 0, PRODUCTS_PER_PAGE);

      return Ok(result);
    }


    [Route("removeproduct")]
    [HttpPost]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(Product), (int)HttpStatusCode.OK)]
    public async Task RemoveDocumentAsync([FromBody]string id)
    {
      //Тут теперь нужно еще и транзакции отправлять, так что пока не работает
     // await m_productService.RemoveProductAsync(id);
     
    }

    [Route("getRelatedDocument/{productId}")]
    [HttpGet]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(Tuple<int, List<Product>>), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<List<Document>>> GetRelatedProducts(string productId)
    {
      string[] ids = await m_RelationshipService.GetRelatedProductsByDocumentIdAsync(productId);
      var tmp = await m_ConstructDocumentService.GetDocumentListByIdsAsync(ids);
      return tmp;
    }
  }
}
