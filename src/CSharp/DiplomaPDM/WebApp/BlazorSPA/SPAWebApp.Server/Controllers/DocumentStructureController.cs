﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using SPAWebApp.Server.Extensions;
using SPAWebApp.Server.Models.NonTransactable.Account;
using SPAWebApp.Server.Models.NonTransactable.TransactionHeaders;
using SPAWebApp.Server.Models.Transactable;
using SPAWebApp.Server.Services.Contracts;
using SPAWebApp.Shared;
using SPAWebApp.Shared.Models.Services.ConstructDocument;
using SPAWebApp.Shared.Models.Services.DocumentStructure;
using SPAWebApp.Shared.Models.ViewModels.DocumentStructure;
using SPAWebApp.Shared.Models.ViewModels.Project;
using TFolderCreateModel = SPAWebApp.Server.Models.Transactable.DocumentStructure.TFolderCreateModel;
using TFolderItemCreateModel = SPAWebApp.Server.Models.Transactable.DocumentStructure.TFolderItemCreateModel;
using TProjectItemModel = SPAWebApp.Server.Models.Transactable.Project.TProjectItemModel;

namespace SPAWebApp.Server.Controllers
{
  [Route(Urls.DocumentStructure.CONTROLLER)]
  [ApiController]
  [ApiExplorerSettings(IgnoreApi = true)]
  [Authorize]
  public class DocumentStructureController : ControllerBase
  {
    private readonly IDocumentStructureService m_DocumentStructureService;
    private readonly IConstructDocumentService m_ConstructDocumentService;
    private readonly ITransactionHeadersService m_TransactionService;
    private readonly IProjectService m_ProjectService;
    private readonly UserManager<ApplicationUser> m_UserManager;

    public DocumentStructureController(IDocumentStructureService documentStructureService, IConstructDocumentService constructDocumentService, ITransactionHeadersService transactionService, UserManager<ApplicationUser> userManager, IProjectService projectService)
    {
      m_DocumentStructureService = documentStructureService;
      m_ConstructDocumentService = constructDocumentService;
      m_TransactionService = transactionService;
      m_UserManager = userManager;
      m_ProjectService = projectService;
    }

    [HttpGet]
    [Route(Urls.DocumentStructure.GET_ALL_FOLDERS)]
    public async Task<ActionResult<ICollection<Folder>>> GetAllFoldersAsync()
    {
      try
      {
        return Ok(await m_DocumentStructureService.GetAllFoldersAsync());
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    [HttpGet]
    [Route(Urls.DocumentStructure.GET_FOLDER)]
    public async Task<ActionResult<Folder>> GetFolderAsync(Guid folderId)
    {
      try
      {
        var folder = await m_DocumentStructureService.GetFolderByIdAsync(folderId);
        folder.Items = await m_DocumentStructureService.GetIncludedItemsAsync(folder.Id);
        return Ok(folder);
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    [HttpGet]
    [Route(Urls.DocumentStructure.GET_INCLUDED_FOLDERS)]
    public async Task<ActionResult<ICollection<Folder>>> GetIncludedFoldersAsync(Guid folderId)
    {
      try
      {
        return Ok(await m_DocumentStructureService.GetIncludedFoldersAsync(folderId));
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    [HttpGet]
    [Route(Urls.DocumentStructure.GET_INCLUDED_DOCUMENTS)]
    public async Task<ActionResult<ICollection<Document>>> GetIncludedDocumentsAsync(Guid folderId)
    {
      try
      {
        var items = await m_DocumentStructureService.GetIncludedItemsAsync(folderId);
        if (items == null || items.Count == 0)
        {
          return Ok(new List<Document>());
        }

        var itemsIds = items.Select(item => item.ItemRealId).ToArray();
        var docs = await m_ConstructDocumentService.GetDocumentListByIdsAsync(itemsIds);
        // хз, что за бредовые проверки
        if (docs == null || docs.Count == 0)
        {
          return Ok(new List<Document>());
        }

        return Ok(docs);
      }
      catch (Exception e)
      {
        return BadRequest(e.Message);
      }
    }

    [HttpDelete]
    [Route(Urls.DocumentStructure.DELETE_FOLDER)]
    public async Task<IActionResult> DeleteFolderAsync(Guid folderId)
    {
      TransactionInfo tVM = null;

      try
      {
        var transaction = await m_TransactionService.OpenTransactionAsync(new TransactionOpen
        {
          UserId = m_UserManager.GetUserId(HttpContext.User),
          SessionId = HttpContext.Session.Get<Guid>("SessionId").ToString()
        });
        tVM = new TransactionInfo(transaction);
      }
      catch
      {
        return BadRequest();
      }

      try
      {
        var folder = await m_DocumentStructureService.GetFolderByIdAsync(folderId);
        await m_DocumentStructureService.RemoveFolderByIdAsync(folderId, tVM);
        if (!string.IsNullOrEmpty(folder.ProjectId))
        {
          await m_ProjectService.ExcludeItemAsync(new Guid(folder.ProjectId), folder.ProjectId.ToString(), tVM);
        }
        else if (folder.SuperiorFolderId.HasValue)
        {
          // todo: а это надо?

          await m_DocumentStructureService.ExcludeFolderFromFolderAsync(folder.SuperiorFolderId.Value, folder.Id, tVM);
        }
        await m_DocumentStructureService.CommitTransactionAsync(tVM);
        await m_ProjectService.CommitTransactionAsync(tVM);
        await m_TransactionService.CloseTransactionByTransactionIdAsync(tVM.TransactionId);
        return Ok();
      }
      catch
      {
        // todo: logger
        try
        {
          await m_DocumentStructureService.CancelTransactionAsync(tVM);
        }
        catch (Exception e)
        {
          // todo: logger
        }
        try
        {
          await m_ProjectService.CancelTransactionAsync(tVM);
        }
        catch (Exception e)
        {
          // todo: logger
        }
        try
        {
          await m_TransactionService.CancelTransactionByTransactionIdAsync(tVM.TransactionId);
        }
        catch (Exception e)
        {
          // todo: logger
        }

        return BadRequest("Во время транзакции произошла ошибка");
      }
    }

    [HttpPost]
    [Route(Urls.DocumentStructure.CREATE_FOLDER)]
    public async Task<ActionResult<Folder>> CreateFolderAsync(FolderCreateVM model)
    {
      // todo: лучше эту проверку через атрибуты реализовать
      if (model.ProjectId == null && model.SuperiorFolderId == null ||
          model.ProjectId != null && model.SuperiorFolderId != null)
      {
        return BadRequest("Некорректные данные");
      }

      TransactionInfo tVM = null;

      try
      {
        var transaction = await m_TransactionService.OpenTransactionAsync(new TransactionOpen
        {
          UserId = m_UserManager.GetUserId(HttpContext.User),
          SessionId = HttpContext.Session.Get<Guid>("SessionId").ToString()
        });
        tVM = new TransactionInfo(transaction);
      }
      catch
      {
        return BadRequest();
      }

      try
      {
        var newFolder = await m_DocumentStructureService.CreateFolderAsync(
          new TFolderCreateModel(new FolderCreateVM {Sign = model.Sign, Name = model.Name}, tVM));
        if (model.ProjectId != null)
        {
          await m_DocumentStructureService.IncludeFolderToProjectAsync(newFolder.Id, model.ProjectId, tVM);
          await m_ProjectService.IncludeItemToProjectAsync(new TProjectItemModel(
            new ProjectItemCreateVM {ProjectId = model.ProjectId, ItemType = "Folder", ItemRealId = newFolder.Id.ToString()}, 
            tVM));
        }
        else if (model.SuperiorFolderId != null)
        {
          await m_DocumentStructureService.IncludeFolderToFolderAsync(model.SuperiorFolderId, newFolder.Id.ToString(), tVM);
        }
        
        await m_DocumentStructureService.CommitTransactionAsync(tVM);
        await m_ProjectService.CommitTransactionAsync(tVM);

        await m_TransactionService.CloseTransactionByTransactionIdAsync(tVM.TransactionId);

        return Ok(await m_DocumentStructureService.GetFolderByIdAsync(newFolder.Id));
      }
      catch
      {
        // todo: logger
        try
        {
          await m_DocumentStructureService.CancelTransactionAsync(tVM);
        }
        catch (Exception e)
        {
          // todo: logger
        }
        // todo: logger
        try
        {
          await m_ProjectService.CancelTransactionAsync(tVM);
        }
        catch (Exception e)
        {
          // todo: logger
        }
        try
        {
          await m_TransactionService.CancelTransactionByTransactionIdAsync(tVM.TransactionId);
        }
        catch (Exception e)
        {
          // todo: logger
        }

        return BadRequest("Во время транзакции произошла ошибка");
      }
    }

    [HttpGet]
    [Route(Urls.DocumentStructure.GET_DELETE_ABILITY)]
    public async Task<ActionResult<bool>> CanFolderBeDeletedAsync(Guid folderId)
    {
      var items = await m_DocumentStructureService.GetIncludedItemsAsync(folderId);
      if (items != null && items.Count > 0)
      {
        return Ok(false);
      }
      var subFolders = await m_DocumentStructureService.GetIncludedFoldersAsync(folderId);
      if (subFolders != null && subFolders.Count > 0)
      {
        return Ok(false);
      }

      return Ok(true);
    }

    [HttpPost]
    [Route(Urls.DocumentStructure.INCLUDE_ITEM_TO_FOLDER)]
    public async Task<ActionResult> IncludeItemToFolderAsync(Guid folderId, FolderItem item)
    {
      // todo: лучше эту проверку через атрибуты реализовать
      TransactionInfo tVM = null;

      try
      {
        var transaction = await m_TransactionService.OpenTransactionAsync(new TransactionOpen
        {
          UserId = m_UserManager.GetUserId(HttpContext.User),
          SessionId = HttpContext.Session.Get<Guid>("SessionId").ToString()
        });
        tVM = new TransactionInfo(transaction);
      }
      catch
      {
        return BadRequest();
      }

      try
      {
        await m_DocumentStructureService.IncludeItemToFolderAsync(folderId,
          new TFolderItemCreateModel(
            new FolderItem { ItemType = item.ItemType, ItemRealId = item.ItemRealId, FolderId = folderId }, 
            tVM));

        await m_DocumentStructureService.CommitTransactionAsync(tVM);

        await m_TransactionService.CloseTransactionByTransactionIdAsync(tVM.TransactionId);

        return Ok();
      }
      catch
      {
        // todo: logger
        try
        {
          await m_DocumentStructureService.CancelTransactionAsync(tVM);
        }
        catch (Exception e)
        {
          // todo: logger
        }
        // todo: logger
        try
        {
          await m_TransactionService.CancelTransactionByTransactionIdAsync(tVM.TransactionId);
        }
        catch (Exception e)
        {
          // todo: logger
        }

        return BadRequest("Во время транзакции произошла ошибка");
      }
    }
  }
}