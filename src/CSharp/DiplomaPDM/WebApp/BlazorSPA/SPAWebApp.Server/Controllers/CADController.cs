﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SPAWebApp.Server.Extensions;
using SPAWebApp.Server.Models.NonTransactable.CAD;
using SPAWebApp.Server.Models.NonTransactable.FileService;
using SPAWebApp.Server.Models.NonTransactable.TransactionHeaders;
using SPAWebApp.Server.Models.Transactable;
using SPAWebApp.Server.Models.Transactable.Product;
using SPAWebApp.Server.Services.Contracts;
using SPAWebApp.Shared;
using SPAWebApp.Shared.Models.Services.DocumentStructure;
using SPAWebApp.Shared.Models.ViewModels.Product;

namespace SPAWebApp.Server.Controllers
{
  [Route(Urls.CAD.CONTROLLER)]
  [ApiController]
  [ApiExplorerSettings(IgnoreApi = true)]
  public class CADController : ControllerBase
  {
    private readonly IProjectService m_ProjectService;
    private readonly IDocumentStructureService m_DocumentStructureService;
    private readonly IFileStorageService m_FileStorageService;
    private readonly IProductService m_ProductService;
    private readonly ITransactionHeadersService m_TransactionService;
    private readonly ILogger m_Logger;

    public CADController(IProjectService projectService, IDocumentStructureService documentStructureService, IProductService productService, IFileStorageService fileStorageService, ILogger<CADController> logger, ITransactionHeadersService transactionService)
    {
      m_ProjectService = projectService;
      m_DocumentStructureService = documentStructureService;
      m_ProductService = productService;
      m_FileStorageService = fileStorageService;
      m_Logger = logger;
      m_TransactionService = transactionService;
    }


    //#region ProjectPart

    //[HttpGet]
    //[Route(Urls.CAD.GET_PROJECTS)]
    //[ProducesResponseType((int)HttpStatusCode.BadRequest)]
    //[ProducesResponseType(typeof(ICollection<ProjectViewModel>), (int)HttpStatusCode.OK)]
    //public async Task<ActionResult<ICollection<ProjectViewModel>>> GetProjectsAsync()
    //{
    //  try
    //  {
    //    var projects = await m_ProjectService.GetAllProjectsLazyAsync();
    //    return Ok(projects
    //      .Select(p => new ProjectViewModel { Id = p.Id, Name = p.Name, Sign = p.Name })
    //      .ToList());
    //  }
    //  catch (Exception e)
    //  {
    //    m_Logger.LogError(LoggingEvents.ERROR, $"{nameof(GetProjectsAsync)}: {e.Message}");
    //    return BadRequest();
    //  }
    //}
    //#endregion

    //#region FolderPart
    
    //[HttpGet]
    //[Route(Urls.CAD.GET_FOLDERS_BY_PROJECT)]
    //[ProducesResponseType((int)HttpStatusCode.BadRequest)]
    //[ProducesResponseType(typeof(ICollection<FolderViewModel>), (int)HttpStatusCode.OK)]
    //public async Task<ActionResult<ICollection<FolderViewModel>>> GetFoldersByProjectAsync(string projectId)
    //{
    //  try
    //  {
    //    var folders = await m_DocumentStructureService.GetFoldersByProjectIdLazyAsync(projectId);
    //    return Ok(folders
    //      .Select(f => new FolderViewModel { Id = f.Id, Sign = f.Sign, Name = f.Name })
    //      .ToList());
    //  }
    //  catch (Exception e)
    //  {
    //    m_Logger.LogError(LoggingEvents.ERROR, $"{nameof(GetFoldersByProjectAsync)}: {e.Message}");
    //    return BadRequest();
    //  }
    //}

    //[HttpGet]
    //[Route(Urls.CAD.GET_FOLDERS_BY_FOLDER)]
    //[ProducesResponseType((int)HttpStatusCode.BadRequest)]
    //[ProducesResponseType(typeof(ICollection<FolderViewModel>), (int)HttpStatusCode.OK)]
    //public async Task<ActionResult<ICollection<FolderViewModel>>> GetFoldersByFolderAsync(string folderId)
    //{
    //  try
    //  {
    //    var folders = await m_DocumentStructureService.GetIncludedFoldersAsync(new Guid(folderId));
    //    return Ok(folders
    //      .Select(f => new FolderViewModel { Id = f.Id, Sign = f.Sign, Name = f.Name })
    //      .ToList());
    //  }
    //  catch (Exception e)
    //  {
    //    m_Logger.LogError(LoggingEvents.ERROR, $"{nameof(GetFoldersByFolderAsync)}: {e.Message}");
    //    return BadRequest();
    //  }
    //}
    //#endregion

    //#region ProductPart
    
    //[HttpGet]
    //[Route(Urls.CAD.GET_PRODUCTS_BY_FOLDER)]
    //[ProducesResponseType((int)HttpStatusCode.BadRequest)]
    //[ProducesResponseType(typeof(ICollection<ProductToCadViewModel>), (int)HttpStatusCode.OK)]
    //public async Task<ActionResult<ICollection<ProductToCadViewModel>>> GetProductInfosByFolderAsync(string folderId)
    //{
    //  try
    //  {
    //    var folderItems = await m_DocumentStructureService.GetIncludedItemsByTypeAsync(folderId, "Product");
    //    var products =
    //      await m_ProductService.GetProductListByIdsAsync(folderItems.Select(fi => fi.ItemRealId).ToArray());
    //    var productViewModels = new List<ProductToCadViewModel>();
    //    foreach (var product in products)
    //    {
    //      var file = await m_FileStorageService.GetFileInfo(product.ModelId);

    //      var vm = new ProductToCadViewModel
    //      {
    //        Id = product.Id,
    //        Description = product.Designation,
    //        State = product.State,
    //        CADModelId = product.ModelId,
    //        PartNumber = product.Name,
    //        WeightKg = product.Weight,
    //        CADModelFullName = file.FullName
    //      };
    //      productViewModels.Add(vm);
    //    }

    //    return productViewModels;
    //  }
    //  catch (Exception e)
    //  {
    //    m_Logger.LogError(LoggingEvents.ERROR, $"{nameof(GetFoldersByFolderAsync)}: {e.Message}");
    //    return BadRequest();
    //  }
    //}

    //[HttpGet]
    //[Route(Urls.CAD.TAKE_PRODUCT_IN_WORK)]
    //[ProducesResponseType((int)HttpStatusCode.BadRequest)]
    //[ProducesResponseType(typeof(ICollection<ProductToCadViewModel>), (int)HttpStatusCode.OK)]
    //public async Task<ActionResult<ProductToCadViewModel>> TakeProductInWork(string productId, string userId, string sessionId)
    //{
    //  TransactionInfo tVM = null;

    //  // создаем транзакцию
    //  try
    //  {
    //    var transaction = await m_TransactionService.OpenTransactionAsync(new TransactionOpen
    //    {
    //      UserId = userId,
    //      SessionId = sessionId
    //    });
    //    tVM = new TransactionInfo(transaction);
    //  }
    //  catch (Exception e)
    //  {
    //    // если что -- логируем (надо добавить ошибок)
    //    m_Logger.LogError(LoggingEvents.ERROR, $"{nameof(TakeProductInWork)}: {e.Message}");
    //    return BadRequest();
    //  }

    //  try
    //  {
    //    // берем изделие в работу (открываем транзакцию)
    //    await m_ProductService.GetInWorkAsync(productId, tVM);
    //    // если не сорвалось -- закрываем транзакцию
    //    await m_ProductService.CommitTransactionAsync(tVM);
    //    await m_TransactionService.CloseTransactionByTransactionIdAsync(tVM.TransactionId);
    //  }
    //  catch (Exception e)
    //  {
    //    // сорвалось -- всё отменяем
    //    try
    //    {
    //      await m_ProductService.CancelTransactionAsync(tVM);
    //    }
    //    catch (Exception e1)
    //    {
    //      m_Logger.LogError(LoggingEvents.ERROR, $"{nameof(TakeProductInWork)}: {e1.Message}");
    //    }
    //    try
    //    {
    //      await m_TransactionService.CancelTransactionByTransactionIdAsync(tVM.TransactionId);
    //    }
    //    catch (Exception e1)
    //    {
    //      m_Logger.LogError(LoggingEvents.ERROR, $"{nameof(TakeProductInWork)}: {e1.Message}");
    //    }

    //    m_Logger.LogError(LoggingEvents.ERROR, $"{nameof(TakeProductInWork)}: {e.Message}");
    //    return BadRequest("Во время транзакции произошла ошибка");
    //  }

    //  // пробуем достать инфу об изделии
    //  try
    //  {
    //    var product = await m_ProductService.GetProductByIdAsync(productId);
    //    var file = await m_FileStorageService.GetFileInfo(product.ModelId);

    //    var vm = new ProductToCadViewModel
    //    {
    //      Id = product.Id,
    //      Description = product.Designation,
    //      State = product.State,
    //      CADModelId = product.ModelId,
    //      PartNumber = product.Name,
    //      WeightKg = product.Weight,
    //      CADModelFullName = file.FullName
    //    };
    //    return Ok(vm);
    //  }
    //  catch (Exception e)
    //  {
    //    m_Logger.LogError(LoggingEvents.ERROR, $"{nameof(TakeProductInWork)}: {e.Message}");
    //    return BadRequest();
    //  }
    //}

    //[HttpPut]
    //[Route(Urls.CAD.POST_PRODUCT_IN_CHECK)]
    //[ProducesResponseType((int)HttpStatusCode.BadRequest)]
    //[ProducesResponseType((int)HttpStatusCode.OK)]
    //public async Task<IActionResult> PutProductInCheckAsync([FromForm]ProductFromCadViewModel product, string userId, string sessionId)
    //{
    //  TransactionInfo tVM = null;

    //  // создаем транзакцию
    //  try
    //  {
    //    var transaction = await m_TransactionService.OpenTransactionAsync(new TransactionOpen
    //    {
    //      UserId = userId,
    //      SessionId = sessionId
    //    });
    //    tVM = new TransactionInfo(transaction);
    //  }
    //  catch (Exception e)
    //  {
    //    m_Logger.LogError(LoggingEvents.ERROR, $"{nameof(PutProductInCheckAsync)}: {e.Message}");
    //    return BadRequest();
    //  }

    //  try
    //  {
    //    var modelId = await m_FileStorageService.UploadFileAsync(new UploadFileModel()
    //      {Comment = product.PartNumber, MultipartFormDataFile = product.CADModel.GetMultipartFormData()});
    //    var viewerModelId = await m_FileStorageService.UploadFileAsync(new UploadFileModel()
    //      { Comment = product.PartNumber, MultipartFormDataFile = product.ViewerModel.GetMultipartFormData() });

    //    //await m_ProductService.UpdateAsync(new TProductUpdateModel(),  new TProductUpdateModel()
    //    //{
    //    //  //ProductId = product.ProductId,
    //    //  Designation = product.Description,
    //    //  Weight = product.WeightKg,
    //    //  Name = product.PartNumber,
    //    //  //ModelId = modelId,
    //    //  //ModelIdView = viewerModelId,
    //    //  UserId = tVM.UserId,
    //    //  SessionId = tVM.SessionId,
    //    //  TransactionId = tVM.TransactionId
    //    //});

    //    await m_ProductService.PutInCheckAsync(product.ProductId, tVM);

    //    await m_ProductService.CommitTransactionAsync(tVM);
    //    await m_TransactionService.CloseTransactionByTransactionIdAsync(tVM.TransactionId);
    //    return Ok();
    //  }
    //  catch (Exception e)
    //  {
    //    try
    //    {
    //      await m_ProductService.CancelTransactionAsync(tVM);
    //    }
    //    catch (Exception e2)
    //    {
    //      m_Logger.LogError(LoggingEvents.ERROR, $"{nameof(PutProductInCheckAsync)}: {e2.Message}");
    //    }
    //    try
    //    {
    //      await m_TransactionService.CancelTransactionByTransactionIdAsync(tVM.TransactionId);
    //    }
    //    catch (Exception e2)
    //    {
    //      m_Logger.LogError(LoggingEvents.ERROR, $"{nameof(PutProductInCheckAsync)}: {e2.Message}");
    //    }

    //    m_Logger.LogError(LoggingEvents.ERROR, $"{nameof(PutProductInCheckAsync)}: {e.Message}");
    //    return BadRequest();
    //  }
    //}
    //#endregion

    //#region FilePart
    
    //[HttpGet]
    //[Route(Urls.CAD.GET_MODEL)]
    //[ProducesResponseType((int)HttpStatusCode.BadRequest)]
    //[ProducesResponseType((int)HttpStatusCode.NotFound)]
    //[ProducesResponseType((int)HttpStatusCode.OK)]
    //public async Task<IActionResult> GetCadModelByIdAsync(string fileId)
    //{
    //  try
    //  {
    //    var file = await m_FileStorageService.DownloadFileAsync(fileId);

    //    var stream = new MemoryStream(file.FileData) {Position = 0};
    //    return File(stream, "application/octet-stream; version=\"5\"", $"{file.Name}.{file.Type}");
    //  }
    //  catch (Exception e)
    //  {
    //    m_Logger.LogError(LoggingEvents.ERROR, $"{nameof(GetCadModelByIdAsync)}: {e.Message}");
    //    return BadRequest();
    //  }
    //}

    //[HttpGet]
    //[Route(Urls.CAD.GET_MODEL)]
    //[ProducesResponseType((int)HttpStatusCode.BadRequest)]
    //[ProducesResponseType((int)HttpStatusCode.NotFound)]
    //[ProducesResponseType((int)HttpStatusCode.OK)]
    //public async Task<IActionResult> GetModelNameByIdAsync(string fileId)
    //{
    //  try
    //  {
    //    var info = await m_FileStorageService.GetFileInfo(fileId);
    //    return Ok(info.Name);
    //  }
    //  catch (Exception e)
    //  {
    //    m_Logger.LogError(LoggingEvents.ERROR, $"{nameof(GetModelNameByIdAsync)}: {e.Message}");
    //    return BadRequest();
    //  }
    //}
    //#endregion
  }
}