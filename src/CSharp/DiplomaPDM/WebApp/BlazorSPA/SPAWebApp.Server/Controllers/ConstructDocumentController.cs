﻿using System.Collections.Generic;
using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Identity;
using System;
using SPAWebApp.Server.Services.Contracts;
using System.Net.Http;
using Microsoft.EntityFrameworkCore.Query.ExpressionVisitors.Internal;
using Microsoft.JSInterop;
using SPAWebApp.Server.Extensions;
using SPAWebApp.Server.Models.NonTransactable.Account;
using SPAWebApp.Server.Models.NonTransactable.TransactionHeaders;
using SPAWebApp.Server.Models.Transactable;
using SPAWebApp.Server.Models.Transactable.ConstructDocument;
using SPAWebApp.Server.Models.Transactable.Relationship;
using SPAWebApp.Shared.Models.Services.ConstructDocument;
using SPAWebApp.Shared.Models.Services.Product;
using SPAWebApp.Shared.Models.Services.Relationship;
using SPAWebApp.Shared.Models.Services.TransactionHeaders;
using SPAWebApp.Shared.Models.ViewModels.ConstructDocument;

namespace SPAWebApp.Server.Controllers
{
  [Route("constructDocument")]
  //[Route("[controller]/[action]")]
  [ApiController]
  [ApiExplorerSettings(IgnoreApi = true)]
  public class ConstructDocumentController : ControllerBase
  {
    private readonly IConstructDocumentService m_ConstructDocumentService;
    private readonly ITransactionHeadersService m_TransactionService;
    private readonly IRelationshipService m_RelationshipService;
    private readonly UserManager<ApplicationUser> m_UserManager;
    private readonly IProductService m_ProductService;


    public ConstructDocumentController(IConstructDocumentService constructDocumentService, 
      ITransactionHeadersService transactionService,
      UserManager<ApplicationUser> userManager,
      IRelationshipService RelationshipService,
      IProductService ProductService)
    {
      m_ConstructDocumentService = constructDocumentService;
      m_TransactionService = transactionService;
      m_UserManager = userManager;
      m_RelationshipService = RelationshipService;
      m_ProductService = ProductService;
    }

    [Route("getDocument/{id}")]
    [HttpGet]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(Document), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<Document>> GetDocumentByIdAsync(string id)
    {
      if (string.IsNullOrEmpty(id))
      {
        //return new Tuple<ControllerResult, Shared.Models.Document>(
        //  new ControllerResult { Succeed = false, Message = "Id не должен быть пустым" },
        //  null
        //  );
        return BadRequest("Id не должен быть пустым");
      }

      var document = await m_ConstructDocumentService.GetDocumentByIdAsync(id);

      if (document == null)
      {
        return NotFound("Документа с таким id нет в системе");
      }

      return Ok(document);
    }

    [Route("getAllDocuments")]
    [HttpGet]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(Document), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<Document>> GetAllDocumentsAsync()
    {
      var documents = await m_ConstructDocumentService.GetDocumentsAsync();

      if (documents.Count == 0)
      {
        return NotFound("Ничего не найдено");
      }

      return Ok(documents);
    }

    [Route("removeDocument")]
    [HttpPost]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(Document), (int)HttpStatusCode.OK)]
    public async Task<IActionResult> RemoveDocumentAsync([FromBody]string id)
    {
      TransactionInfo tVM = null;

      try
      {
        var transaction = await m_TransactionService.OpenTransactionAsync(new TransactionOpen
        {
          UserId = m_UserManager.GetUserId(HttpContext.User),
          SessionId = HttpContext.Session.Get<Guid>("SessionId").ToString()
        });
        tVM = new TransactionInfo(transaction);
      } catch
      {
        return BadRequest();
      }

      return BadRequest();

      //try
      //{
      //  // todo: можно ещё направить запрос в DocumentStructureService, дабы убедиться, что нет папок, привязанных к этому проекту
      //  await m_ProjectService.DeleteProjectByIdAsync(projectId, tVM);
      //  await m_ProjectService.CommitTransaction(tVM);
      //  await m_TransactionService.CloseTransactionByTransactionIdAsync(tVM.TransactionId);
      //  return Ok();
      //}
      //catch
      //{
      //  // todo: logger
      //  try
      //  {
      //    await m_ProjectService.CancelTransaction(tVM);
      //  }
      //  catch (Exception e)
      //  {
      //    // todo: logger
      //  }
      //  try
      //  {
      //    await m_TransactionService.CancelTransactionByTransactionIdAsync(tVM.TransactionId);
      //  }
      //  catch (Exception e)
      //  {
      //    // todo: logger
      //  }

      //  return BadRequest("Во время транзакции произошла ошибка");
      //}
      //await m_ConstructDocumentService.RemoveDocumentAsync(id);
      //return Ok();
    }

    [Route("change")]
    [HttpPost]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(Document), (int)HttpStatusCode.OK)]

    public async Task<IActionResult> ChangeDocumentAsync([FromBody] ModifyDocumentVM modifyDocumentViewModel)
    {
      if (!TryValidateModel(modifyDocumentViewModel))
      {
        return BadRequest("Параметры указаны неверно");
      }
      // todo транзакции сделать
      //await m_ConstructDocumentService.UpdateDocumentAsync( modifyDocumentViewModel);
      return Ok();
    }

    [Route("create")]
    [HttpPost]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType((int)HttpStatusCode.OK)]

    public async Task<string> CreateNewDocumentAsync([FromBody]CreateNewDocumentVM createNewDocumentViewModel)
    {
      if (!TryValidateModel(createNewDocumentViewModel)) return "";
      TransactionHeader theader = await m_TransactionService.OpenTransactionAsync(new TransactionOpen
      {
        UserId = m_UserManager.GetUserId(HttpContext.User),
        SessionId = HttpContext.Session.Get<Guid>("SessionId").ToString()
      });
      var result = await m_ConstructDocumentService.CreateDocumentAsync(new TDocumentCreateModel(createNewDocumentViewModel,
        new TransactionInfo(theader)));
      if (result != null)
      {
        TransactionHeader closeTransactionHeader = await m_TransactionService.GetTransactionByUserIdAsync(m_UserManager.GetUserId(HttpContext.User));
        await m_ConstructDocumentService.CommitTransactionAsync(new TransactionInfo
        {
          SessionId = closeTransactionHeader.SessionId,
          TransactionId = closeTransactionHeader.Id.ToString(),
          UserId = closeTransactionHeader.UserId
        });
        await m_TransactionService.CloseTransactionByUserIdAsync(m_UserManager.GetUserId(HttpContext.User));

      }

      return Json.Serialize(result);
      // todo транзакции сделать
      //return await m_ConstructDocumentService.CreateDocumentAsync(createNewDocumentViewModel);
      //return new ControllerResult { Succeed = true };
    }

    [Route("changeState")]
    [HttpPost]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(Document), (int)HttpStatusCode.OK)]

    public async Task<IActionResult> ChangeStateToDocument([FromBody]ChangeStateByDocumentVM changeStateByDocumentViewModel)
    {
      if (!TryValidateModel(changeStateByDocumentViewModel))
      {
        return BadRequest("Параметры указаны неверно" );
      }
      // todo транзакции сделать
     // await m_ConstructDocumentService.ChangeStateAsync(changeStateByDocumentViewModel);
      return Ok();
    }

    [Route("createNewRev/{id}")]
    [HttpGet]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(Document), (int)HttpStatusCode.OK)]
    public async Task<string> CreateNewVersionByDocument(string id)
    {
      if (string.IsNullOrEmpty(id))
      {
        return "Id is null";
      }

      string result = await m_ConstructDocumentService.CreateNewRevAsync(id);

      if (string.IsNullOrEmpty(result))
      {
        return "Result is null";
      }

      return result;
    }

    [Route("checkForCreateNewRev/{id}")]
    [HttpGet]
    public async Task<string> CheckDocumentForCreateNewRevision(string id)
    {
      if (string.IsNullOrEmpty(id))
      {
        return "Id is null";
      }

      string result = await m_ConstructDocumentService.CheckDocumentForCreateNewRevisionAsync(id);

      if (string.IsNullOrEmpty(result))
      {
        return "Result is null";
      }

      return result;
    }

    [Route("test")]
    [HttpGet]
    public async Task<IActionResult> test()
    {

      await m_ConstructDocumentService.test();
      return null;
    }

    private const int DOCUMENTS_PER_PAGE = 50;


    /// <summary>
    /// Возвращает количество страниц и изделия с первой страницы
    /// </summary>
    /// <returns></returns>
    [Route("GetDocumentsOfFirstPageAsync")]
    [HttpGet]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(DocumentAndPage), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<DocumentAndPage>> GetProductsOfFirstPageAsync()
    {
      List<Document> documents = await m_ConstructDocumentService.GetDocumentsAsync();

      if (documents.Count == 0)
      {
        return NotFound("Ничего не найдено");
      }

      int pages = documents.Count / DOCUMENTS_PER_PAGE;

      var result = new DocumentAndPage { Pages = pages, Documents = documents.GetRange(0, DOCUMENTS_PER_PAGE) };

      return Ok(result);
    }

    public class DocumentAndPage
    {
      public int Pages { get; set; }
      public List<Document> Documents { get; set; }
    }

    [Route("GetDocumentsOfPageAsync/{page}")]
    [HttpGet]
    [ProducesResponseType((int)HttpStatusCode.BadRequest)]
    [ProducesResponseType((int)HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(Tuple<int, List<Document>>), (int)HttpStatusCode.OK)]
    public async Task<ActionResult<List<Document>>> GetProductsOfPageAsync(int page)
    {
      List<Document> documents = await m_ConstructDocumentService.GetDocumentsAsync();

      if (documents.Count == 0)
      {
        return NotFound("Ничего не найдено");
      }

      int pages = documents.Count / DOCUMENTS_PER_PAGE;

      var result = documents.GetRange((pages > 0) ? pages * page : 0, DOCUMENTS_PER_PAGE);

      return Ok(result);
    }


    [Route("getRelatedProduct/{documentId}")]
    [HttpGet]
    [ProducesResponseType((int) HttpStatusCode.BadRequest)]
    [ProducesResponseType((int) HttpStatusCode.NotFound)]
    [ProducesResponseType(typeof(Tuple<int, List<Product>>), (int) HttpStatusCode.OK)]
    public async Task<ActionResult<List<Product>>> GetRelatedProducts(string documentId)
    {
      string[] ids = await m_RelationshipService.GetRelatedProductsByDocumentIdAsync(documentId);
      List<Product> tmp = await m_ProductService.GetProductListByIdsAsync(ids);
      return tmp;
    }

    [Route("connectProductToDocument")]
    [HttpPost]
    [ProducesResponseType((int) HttpStatusCode.BadRequest)]
    [ProducesResponseType((int) HttpStatusCode.NotFound)]
    [ProducesResponseType((int) HttpStatusCode.OK)]
    public async Task<IActionResult> ConnectProductToDocument([FromBody]Relationship relationship)
    {
      if (!TryValidateModel(relationship))
      {
        return BadRequest();
      }

      TransactionInfo tVM = null;

      try
      {
        var transaction = await m_TransactionService.OpenTransactionAsync(new TransactionOpen
        {
          UserId = m_UserManager.GetUserId(HttpContext.User),
          SessionId = HttpContext.Session.Get<Guid>("SessionId").ToString()
        });
        tVM = new TransactionInfo(transaction);
      }
      catch
      {
        return BadRequest();
      }

      try
      {
        await m_RelationshipService.CreateRelationshipAsync(
          new TRelationshipCreateModel(relationship, tVM));

        await m_RelationshipService.CommitTransactionAsync(tVM);
        await m_TransactionService.CloseTransactionByTransactionIdAsync(tVM.TransactionId);
        return Ok();
      }
      catch
      {
        try
        {
          await m_RelationshipService.CancelTransactionAsync(tVM);
        }
        catch
        {
        }
        try
        {
          await m_TransactionService.CancelTransactionByTransactionIdAsync(tVM.TransactionId);
        }
        catch
        {
        }
        return BadRequest();
      }
    }
  }
}