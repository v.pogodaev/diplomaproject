﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;
using SPAWebApp.Server.Models;
using SPAWebApp.Server.Models.NonTransactable.Account;
using SPAWebApp.Shared.Authentication.Models;

namespace SPAWebApp.Server.Infrastructure
{
  public class UserDbContext : IdentityDbContext<ApplicationUser, IdentityRole<Guid>, Guid>
  {
    public UserDbContext(DbContextOptions<UserDbContext> options) : base(options)
    {
      //Database.EnsureDeleted();
      //Database.EnsureCreated();
    }

    protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
    {
      base.OnConfiguring(optionsBuilder);
    }

    protected override void OnModelCreating(ModelBuilder builder)
    {
      base.OnModelCreating(builder);
    }

    //protected override void OnModelCreating(ModelBuilder builder)
    //{
    //  var roles = new List<Role>
    //  {
    //    new Role() {Id = 1, Name = "admin", Description = "Administrator"},
    //    new Role() {Id = 2, Name = "user", Description = "User with common rights"},
    //    new Role() {Id = 3, Name = "eng", Description = "User with engineer rights"}
    //  };

    //  var users = new List<User>
    //  {
    //    new User()
    //    {
    //      Id = Guid.NewGuid(),
    //      Login = "admin",
    //      Email = "admin@admin.admin",
    //      Password = "admin",
    //      FirstName = "AName",
    //      LastName = "ALast Name",
    //      MiddleName = "AMiddle Name",
    //      RoleId = 1
    //    },
    //    new User()
    //    {
    //      Id = Guid.NewGuid(),
    //      Login = "user",
    //      Email = "user@user.user",
    //      Password = "user",
    //      FirstName = "Иван",
    //      LastName = "Иванов",
    //      MiddleName = "Иванович",
    //      RoleId = 2
    //    },
    //    new User()
    //    {
    //      Id = Guid.NewGuid(),
    //      Login = "eng",
    //      Email = "eng@eng.eng",
    //      Password = "eng",
    //      FirstName = "Петр",
    //      LastName = "Петров",
    //      MiddleName = "Петрович",
    //      RoleId = 3
    //    },
    //  };

    //  builder.Entity<Role>().HasData(roles);
    //  builder.Entity<User>().HasData(users);
    //  base.OnModelCreating(builder);
    //}


    //public DbSet<User> Users { get; set; }

    //public DbSet<Role> Roles { get; set; }
  }
}
