﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using SPAWebApp.Server.Models;
using SPAWebApp.Server.Models.NonTransactable.Account;
using SPAWebApp.Server.Seed;

namespace SPAWebApp.Server
{
  public class Program
  {
    public static async Task Main(string[] args)
    {
      //BuildWebHost(args).Run();
      var host = BuildWebHost(args);

      using (var scope = host.Services.CreateScope())
      {
        var services = scope.ServiceProvider;
        try
        {
          var userManager = services.GetRequiredService<UserManager<ApplicationUser>>();
          var rolesManager = services.GetRequiredService<RoleManager<IdentityRole<Guid>>>();
          await RoleInitializer.InitializeAsync(userManager, rolesManager);
        }
        catch (Exception ex)
        {
          var logger = services.GetRequiredService<ILogger<Program>>();
          logger.LogError(ex, "An error occurred while seeding the database.");
        }
      }

      host.Run();
    }

    public static IWebHost BuildWebHost(string[] args) =>
        WebHost.CreateDefaultBuilder(args)
            .UseConfiguration(new ConfigurationBuilder()
                .AddCommandLine(args)
                .Build())
            .UseStartup<Startup>()
            .Build();
  }
}
