﻿using System;
using SPAWebApp.Server.Models.Transactable;
using SPAWebApp.Shared.Models.ViewModels.ConstructDocument;
using SPAWebApp.Shared.Models.ViewModels.FileService;
using TProductCreateModel = SPAWebApp.Server.Models.Transactable.Product.TProductCreateModel;

// ReSharper disable InconsistentNaming

namespace SPAWebApp.Server
{
  public static class UrlsConfig
  {
    public static class DocumentManagerOperations
    {
      public static string Test(Guid id) => $"/design_storage/design_document/test?documentId={id}";
      public static string PutCancelTransaction() => "/design_storage/designDocument/CancelTransaction";
      public static string PutCommitTransaction() => "/design_storage/designDocument/CommitTransaction";
      public static string GetDocumentById(string id) => $"/design_storage/design_document/GetDocument?documentId={id}";
      public static string ChangeDocument(string documentId) => $"/design_storage/design_document/change/{documentId}";
      public static string ChangeDocumentState(string documentId) => $"/design_storage/design_document/changeState/{documentId}";
      public static string CheckDocumentForCreateNewRevision(string id) => $"/design_storage/design_document/checkForCreateNewRev?documentId={id}";
      public static string CreateDocument() => "/design_storage/design_document/create";
      public static string CreateNewRevDocument(string id) => $"/design_storage/design_document/createNewRev?documentId={id}";
      public static string GetAllDocuments() => $"/design_storage/design_document/getAllDocuments";
      public static string GetAllDocumentsWithTransact() => $"/design_storage/design_document/getAllDocumentsWithTransact";
      public static string GetDocumentsByIds() => $"/design_storage/design_document/getDocumentsByIds";
      public static string RemoveDocument(string id) => $"/design_storage/design_document/removeDocument/{id}";
    }

    public class FilesManagerOperations
    {
      public static string DownloadFileById(string fileId) => $"/storage/download/{fileId}";
      public static string GetAllFile() => "/storage/getAllFilesData";
      public static string GetFileById(string id) => $"/storage/getData?documentId={id}";
      public static string RemoveFile(string id) => $"/storage/removeFile?fileId={id}";
      public static string UploadFile(string comment) => $"/storage/uploadOneFile?comment={comment}";
    }

    public class ProductManagerOperations
    {
      public static string CommitTransaction() => "/cards_storage/CommitTransaction";
      public static string CancelTransaction() => "/cards_storage/CancelTransaction";
      public static string CreateProduct() => "/cards_storage/create";
      public static string GetAllProducts() => "/cards_storage/getAllProducts";
      public static string GetProductById(string productId) => $"/cards_storage/getProduct/{productId}";
      public static string GetProductListByIds() => $"/cards_storage/getProductListByIds";
      public static string PutInCheck(string productId) => $"/cards_storage/postProductInCheck/{productId}";
      public static string GetInWork(string productId) => $"/cards_storage/postProductInWork/{productId}";
      public static string RemoveProduct() => "/cards_storage/removeProduct";
      public static string Update(string productId) => $"/cards_storage/update/{productId}";
      public static string UpdateState() => "/cards_storage/updateState";
    }
    
    public static class RelationshipManagerOperations
    {
      private const string DEFAULT_CONTROLLER_NAME = "relationship_storage";

      public static string CommitTransaction() => $"/{DEFAULT_CONTROLLER_NAME}/CommitTransaction";
      public static string CancelTransaction() => $"/{DEFAULT_CONTROLLER_NAME}/CancelTransaction";
      public static string CreateRelationship() => $"/{DEFAULT_CONTROLLER_NAME}/create";
      public static string GetRelatedDocuments(string productId) => $"/{DEFAULT_CONTROLLER_NAME}/getRelatedDocuments?productId={productId}";
      public static string GetRelatedProducts(string documentId) => $"/{DEFAULT_CONTROLLER_NAME}/getRelatedProducts?documentId={documentId}";
      public static string GetRelationshipById(string relationshipId) => $"/{DEFAULT_CONTROLLER_NAME}/getRelationship?relationshipId={relationshipId}";
      public static string RemoveRelationship(string relationshipId) => $"/{DEFAULT_CONTROLLER_NAME}/removeRelationship/{relationshipId}";
      public static string UpdateRelationship(string relationshipId) => $"/{DEFAULT_CONTROLLER_NAME}/update/{relationshipId}";

    }

    public static class ProjectApi
    {
      private const string CONTROLLER_PATH = "api/v1/Project";

      private const string CREATE = "create";
      private const string DELETE = "delete";
      private const string UPDATE = "update";
      private const string INCLUDE_ITEM = "includeItem";
      private const string GET_INCLUDED_ITEMS = "getIncludedItems";
      private const string EXCLUDE_ITEM = "excludeItem";
      private const string GET_INCLUDED_ITEMS_BY_TYPE = "getIncludedItemsByType";
      private const string ALLOWED_TYPES = "allowedTypes";
      private const string PROJECT = "project";
      private const string LAZY_PROJECTS = "lazyProjects";
      private const string COMMIT_TRANSACTION = "commitTransaction";
      private const string CANCEL_TRANSACTION = "cancelTransaction";

      public static string CreateProject() => $"{Project_API}/{CONTROLLER_PATH}/{CREATE}";
      public static string DeleteProjectById(Guid id) => $"{Project_API}/{CONTROLLER_PATH}/{DELETE}/{id.ToString()}";
      public static string UpdateProject() => $"{Project_API}/{CONTROLLER_PATH}/{UPDATE}";
      public static string IncludeItemToProject() => $"{Project_API}/{CONTROLLER_PATH}/{INCLUDE_ITEM}";
      public static string GetIncludedItemsByProjectId(Guid id) => $"{Project_API}/{CONTROLLER_PATH}/{GET_INCLUDED_ITEMS}/{id.ToString()}";
      public static string ExcludeItemFromProject(Guid projectId, string itemId) => $"{Project_API}/{CONTROLLER_PATH}/{EXCLUDE_ITEM}?projectId={projectId.ToString()}&itemId={itemId}";
      public static string GetIncludedItemsByType() => $"{Project_API}/{CONTROLLER_PATH}/{GET_INCLUDED_ITEMS_BY_TYPE}";
      public static string GetAllowedItemTypes() => $"{Project_API}/{CONTROLLER_PATH}/{ALLOWED_TYPES}";
      public static string GetProjectById(Guid id) => $"{Project_API}/{CONTROLLER_PATH}/{PROJECT}/{id.ToString()}";
      public static string GetAllProjects() => $"{Project_API}/{CONTROLLER_PATH}/{LAZY_PROJECTS}";
      public static string CommitTransaction() => $"{Project_API}/{CONTROLLER_PATH}/{COMMIT_TRANSACTION}";
      public static string CancelTransaction() => $"{Project_API}/{CONTROLLER_PATH}/{CANCEL_TRANSACTION}";
    }
    public static class TransactionHeadersApi
    {
      private const string CONTROLLER_PATH = "api/v1/Transaction";

      private const string OPEN_TRANSACTION = "open";
      private const string CLOSE_BY_TRANSACTION_ID = "closeByTransactionId";
      private const string CLOSE_BY_USER_ID = "closeByUserId";
      private const string CANCEL_BY_TRANSACTION_ID = "cancelByTransactionId";
      private const string CANCEL_BY_USER_ID = "cancelByUserId";
      private const string GET_BY_TRANSACTION_ID = "transaction";
      private const string GET_BY_USER_ID = "transactionByUser";

      public static string OpenTransaction() => $"{TransactionHeaders_API}/{CONTROLLER_PATH}/{OPEN_TRANSACTION}";
      public static string CloseByTransactionId(string tId) => $"{TransactionHeaders_API}/{CONTROLLER_PATH}/{CLOSE_BY_TRANSACTION_ID}/{tId}";
      public static string CloseByUserId(string uId) => $"{TransactionHeaders_API}/{CONTROLLER_PATH}/{CLOSE_BY_USER_ID}/{uId}";
      public static string CancelByTransactionId(string tId) => $"{TransactionHeaders_API}/{CONTROLLER_PATH}/{CANCEL_BY_TRANSACTION_ID}/{tId}";
      public static string CancelByUserId(string uId) => $"{TransactionHeaders_API}/{CONTROLLER_PATH}/{CANCEL_BY_USER_ID}/{uId}";
      public static string GetByTransactionId(string tId) => $"{TransactionHeaders_API}/{CONTROLLER_PATH}/{GET_BY_TRANSACTION_ID}/{tId}";
      public static string GetByUserId(string uId) => $"{TransactionHeaders_API}/{CONTROLLER_PATH}/{GET_BY_USER_ID}/{uId}";
    }
    public static class DocumentStructureApi
    {
      private const string CONTROLLER_PATH = "api/v1/Folder";

      private const string CREATE_FOLDER = "create";
      private const string INCLUDE_FOLDER_TO_FOLDER = "include";
      private const string EXCLUDE_FOLDER_FROM_FOLDER = "exclude";
      private const string INCLUDE_FOLDER_TO_PROJECT = "includeToProject";
      private const string EXCLUDE_FOLDER_FROM_PROJECT = "excludeFromProject";
      private const string INCLUDE_ITEM_TO_FOLDER = "includeItem";
      private const string EXCLUDE_ITEM_FROM_FOLDER = "excludeItem";
      private const string GET_INCLUDED_FOLDERS = "getIncludedFolders";
      private const string GET_INCLUDED_ITEMS = "getIncludedItems";
      private const string GET_INCLUDED_ITEMS_BY_TYPE = "getIncludedItemsByType";
      private const string GET_FOLDER_BY_ID = "folder";
      private const string GET_FOLDERS_BY_PROJECT_ID_LAZY = "foldersByProjectLazy";
      private const string GET_FOLDERS_BY_PROJECT_ID_GREEDY = "foldersByProjectGreedy";
      private const string GET_ALL_FOLDERS = "folders";
      private const string GET_ALLOWED_FOLDER_ITEM_TYPES = "allowedTypes";
      private const string DELETE_FOLDER = "delete";
      private const string COMMIT_TRANSACTION = "commitTransaction";
      private const string CANCEL_TRANSACTION = "cancelTransaction";

      public static string CreateFolder() => $"{DocumentStructure_API}/{CONTROLLER_PATH}/{CREATE_FOLDER}";
      public static string PutIncludeFolderToFolder(string superFolderId, string subFolderId) => $"{DocumentStructure_API}/{CONTROLLER_PATH}/{INCLUDE_FOLDER_TO_FOLDER}?superFolderId={superFolderId}&subFolderId={subFolderId}";
      public static string PutExcludeFolderFromFolder(string superFolderId, string subFolderId) => $"{DocumentStructure_API}/{CONTROLLER_PATH}/{EXCLUDE_FOLDER_FROM_FOLDER}?superFolderId={superFolderId}&subFolderId={subFolderId}";
      public static string PutIncludeFolderToProject(string folderId, string projectId) => $"{DocumentStructure_API}/{CONTROLLER_PATH}/{INCLUDE_FOLDER_TO_PROJECT}?folderId={folderId}&projectId={projectId}";
      public static string PutExcludeFolderFromProject(string folderId) => $"{DocumentStructure_API}/{CONTROLLER_PATH}/{EXCLUDE_FOLDER_FROM_PROJECT}/{folderId}";
      public static string PutIncludeItemToFolder(string folderId) => $"{DocumentStructure_API}/{CONTROLLER_PATH}/{INCLUDE_ITEM_TO_FOLDER}/{folderId}";
      public static string PutExcludeItemFromFolder(string folderId, string itemRealId) => $"{DocumentStructure_API}/{CONTROLLER_PATH}/{EXCLUDE_ITEM_FROM_FOLDER}?folderId={folderId}&itemRealId={itemRealId}";
      public static string GetIncludedFolders(string folderId) => $"{DocumentStructure_API}/{CONTROLLER_PATH}/{GET_INCLUDED_FOLDERS}/{folderId}";
      public static string GetIncludedItems(string folderId) => $"{DocumentStructure_API}/{CONTROLLER_PATH}/{GET_INCLUDED_ITEMS}/{folderId}";
      public static string GetIncludedItemsByType(string folderId, string type) => $"{DocumentStructure_API}/{CONTROLLER_PATH}/{GET_INCLUDED_ITEMS_BY_TYPE}?folderId={folderId}&type={type}";
      public static string GetFolderById(string id) => $"{DocumentStructure_API}/{CONTROLLER_PATH}/{GET_FOLDER_BY_ID}/{id}";
      public static string GetFoldersByProjectIdLazy(string id) => $"{DocumentStructure_API}/{CONTROLLER_PATH}/{GET_FOLDERS_BY_PROJECT_ID_LAZY}/{id}";
      public static string GetFoldersByProjectIdGreedy(string id) => $"{DocumentStructure_API}/{CONTROLLER_PATH}/{GET_FOLDERS_BY_PROJECT_ID_GREEDY}/{id}";
      public static string GetAllFolders() => $"{DocumentStructure_API}/{CONTROLLER_PATH}/{GET_ALL_FOLDERS}";
      public static string GetAllowedTypes() => $"{DocumentStructure_API}/{CONTROLLER_PATH}/{GET_ALLOWED_FOLDER_ITEM_TYPES}";
      public static string PutRemoveFolder(string folderId) => $"{DocumentStructure_API}/{CONTROLLER_PATH}/{DELETE_FOLDER}/{folderId}";
      public static string PutCommitTransaction() => $"{DocumentStructure_API}/{CONTROLLER_PATH}/{COMMIT_TRANSACTION}";
      public static string PutCancelTransaction() => $"{DocumentStructure_API}/{CONTROLLER_PATH}/{CANCEL_TRANSACTION}";

    }

    // BUG: почему-то через startup ни в какую не хочет заполняться, пока хардкод
    public static string DbManager { get; set; } = "http://localhost:8080";
    public static string DocumentManager { get; set; } = "http://localhost:58721";
    public static string FileManager { get; set; } = "http://localhost:58722";
    public static string ProductManager { get; set; } = "http://localhost:58723";
    public static string RelationshipManager { get; set; } = "http://localhost:58724";

    public static string Project_API { get; set; } = "https://localhost:44341";
    public static string DocumentStructure_API { get; set; } = "https://localhost:44342";
    public static string TransactionHeaders_API { get; set; } = "https://localhost:44343";
  }
}

