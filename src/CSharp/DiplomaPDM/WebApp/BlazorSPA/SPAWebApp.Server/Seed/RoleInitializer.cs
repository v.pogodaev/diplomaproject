﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Identity;
using SPAWebApp.Server.Models;
using SPAWebApp.Server.Models.NonTransactable.Account;

namespace SPAWebApp.Server.Seed
{
  public class RoleInitializer
  {
    public static async Task InitializeAsync(UserManager<ApplicationUser> userManager, RoleManager<IdentityRole<Guid>> roleManager)
    {
      string adminEmail = "admin@admin.com";
      string password = "admin123";
      if (await roleManager.FindByNameAsync("admin") == null)
      {
        await roleManager.CreateAsync(new IdentityRole<Guid>("admin"));
      }
      if (await roleManager.FindByNameAsync("user") == null)
      {
        await roleManager.CreateAsync(new IdentityRole<Guid>("user"));
      }
      if (await roleManager.FindByNameAsync("engineer") == null)
      {
        await roleManager.CreateAsync(new IdentityRole<Guid>("engineer"));
      }
      if (await userManager.FindByNameAsync("admin") == null)
      {
        ApplicationUser admin = new ApplicationUser { Email = adminEmail, UserName = "admin" };
        IdentityResult result = await userManager.CreateAsync(admin, password);
        if (result.Succeeded)
        {
          await userManager.AddToRoleAsync(admin, "admin");
        }
      }
    }
  }
}
