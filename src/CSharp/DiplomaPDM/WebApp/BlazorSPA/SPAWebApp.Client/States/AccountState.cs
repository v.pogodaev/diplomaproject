﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using SPAWebApp.Client.Services.Contracts;
using SPAWebApp.Shared;
using SPAWebApp.Shared.Authentication.Models;
using SPAWebApp.Shared.Models.ViewModels;
using SPAWebApp.Shared.Models.ViewModels.Account;

namespace SPAWebApp.Client.States
{
  public class AccountState
  {
    private readonly IAuthorizeApi m_AuthorizeApi;
    private UserInfo m_UserInfo;

    public async Task<bool> IsAdmin()
    {
      return await m_AuthorizeApi.IsAdmin();
    }

    public AccountState(IAuthorizeApi authorizeApi)
    {
      m_AuthorizeApi = authorizeApi;
    }

    public async Task<bool> IsLoggedIn()
    {
      try
      {
        var userInfo = await GetUserInfoAsync();
        return userInfo != null;
      }
      catch (HttpRequestException)
      {
        return false;
      }
    }

    public async Task Login(LoginVM loginParameters)
    {
      m_UserInfo = await m_AuthorizeApi.Login(loginParameters);
    }

    public async Task Register(RegisterVM registerParameters)
    {
      m_UserInfo = await m_AuthorizeApi.Register(registerParameters);
    }

    public async Task Logout()
    {
      await m_AuthorizeApi.Logout();
      m_UserInfo = null;
    }

    public async Task<UserInfo> GetUserInfoAsync()
    {
      if (m_UserInfo != null)
      {
        return m_UserInfo;
      }
      m_UserInfo = await m_AuthorizeApi.GetUserInfo();
      return m_UserInfo;
    }

    public async Task<List<string>> GetUserRolesAsync()
    {
      return await m_AuthorizeApi.GetUserRoles();
    }

    public async Task<List<AdminUserInfoVM>> GetUsersInfoAsync()
    {
      return await m_AuthorizeApi.GetUsersInfoAsync();
    }

    public async Task<List<UserRoleVM>> GetAllUserRolesAsync(Guid selectedUserId)
    {
      return await m_AuthorizeApi.GetAllUserRolesAsync(selectedUserId.ToString());
    }

    public async Task SaveUserRolesAsync(Guid selectedUserId, List<string> roles)
    {
      await m_AuthorizeApi.SaveUserRolesAsync(selectedUserId.ToString(), roles);
    }
  }
}
