﻿var container, stats;
var camera, controls, scene, renderer;

window.viewer = {
  setViewer: function (filePath) {

    scene = new THREE.Scene();
    scene.background = new THREE.Color(0x999999);
    scene.add(new THREE.AmbientLight(0x999999));

    // set up camera
    camera = new THREE.PerspectiveCamera(35, document.getElementById('VRMLViewer').clientWidth / document.getElementById('VRMLViewer').clientHeight, 1, 500);
    camera.up.set(0, 0, 1);
    camera.position.set(0, -9, 6);
    camera.add(new THREE.PointLight(0xffffff, 0.8));

    scene.add(camera);

    // light
    var dirLight = new THREE.DirectionalLight(0xffffff);
    dirLight.position.set(200, 200, 1000).normalize();
    camera.add(dirLight);
    camera.add(dirLight.target);

    // make grid
    var grid = new THREE.GridHelper(50, 50, 0xffffff, 0x555555);
    grid.rotateOnAxis(new THREE.Vector3(1, 0, 0), 90 * (Math.PI / 180));

    scene.add(grid);

    // set up renderer
    renderer = new THREE.WebGLRenderer({ antialias: true });
    renderer.setPixelRatio(window.devicePixelRatio);
    renderer.setSize(document.getElementById('VRMLViewer').clientWidth, document.getElementById('VRMLViewer').clientHeight);

    // set up controls
    var controls = new THREE.OrbitControls(camera, renderer.domElement);
    controls.addEventListener('change', render);
    controls.target.set(0, 1.2, 2);
    controls.update();

    // loader
    var loader = new THREE.VRMLLoader();
    loader.load(filePath, function (object) {
      scene.add(object);
      object.scale.set(10, 10, 10);
    });

    // connect to dom
    element.appendChild(renderer.domElement);
    renderer.render(scene, camera);
  }
}

function render() {
  renderer.render(scene, camera);
}

function animate() {
  requestAnimationFrame(animate);
  renderer.render(scene, camera);
  stats.update();
}
