﻿window.interop = {
  setProperty: function (name, value) {
    window.localStorage[name] = value;
    return value;
  },
  getProperty: function (name) {
    return window.localStorage[name];
  },
  setFocus: function (element) {
    element.focus();
  }
};