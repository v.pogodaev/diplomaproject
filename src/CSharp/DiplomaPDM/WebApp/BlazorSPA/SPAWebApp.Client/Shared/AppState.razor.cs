﻿using Microsoft.AspNetCore.Components;
using SPAWebApp.Client.States;

namespace SPAWebApp.Client.Shared
{
  public class AppStateViewModel : ComponentBase
  {
    [Inject]
    public AccountState AccountState { get; set; }

    [Parameter]
    protected RenderFragment ChildContent { get; set; }
  }
}
