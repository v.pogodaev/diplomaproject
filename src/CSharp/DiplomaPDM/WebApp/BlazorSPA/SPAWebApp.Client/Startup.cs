using Blazor.FileReader;
using Microsoft.AspNetCore.Components.Builder;
using Microsoft.Extensions.DependencyInjection;
using SPAWebApp.Client.Services;
using SPAWebApp.Client.Services.Contracts;
using SPAWebApp.Client.States;
using WebApp.ComponentsLib;

namespace SPAWebApp.Client
{
  public class Startup
  {
    public void ConfigureServices(IServiceCollection services)
    {
      services.AddServices();

      services.AddTransient<IFileStorageService, FileStorageService>();
      services.AddTransient<IProductCardService, ProductCardService>();
      services.AddScoped<IAuthorizeApi, AuthorizeApi>();
      services.AddScoped<AccountState>();
      services.AddModal();

      // ������� ���������� ��� �������� ������
      services.AddFileReaderService();
      
    }

    public void Configure(IComponentsApplicationBuilder app)
    {
      app.AddComponent<App>("app");
    }
  }

  public static class CustomServiceExtensionMethods
  {
    public static IServiceCollection AddServices(this IServiceCollection services)
    {
      // ������ ��������
      services.AddTransient<IProjectService, ProjectService>();
      // ������ ��������� �����
      services.AddTransient<IDocumentStructureService, DocumentStructureService>();
      // ������ ��������������� ����������
      services.AddTransient<IConstructDocumentService, ConstructDocumentService>();
      // ������ �������
      services.AddTransient<IProductCardService, ProductCardService>();

      return services;
    }
  }
}
