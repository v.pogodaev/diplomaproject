﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using SPAWebApp.Shared;
using SPAWebApp.Shared.Models;
using SPAWebApp.Shared.Models.Services.ConstructDocument;

namespace SPAWebApp.Client.Pages.ConstructDocument.Components
{
  public class ConstructDocumentsListViewModel : ComponentBase
  {

    [Parameter]
    public List<Document> Documents { get; set; }

    [Parameter]
    public Action<Document> SelectedDocument { get; set; }

  }
}
