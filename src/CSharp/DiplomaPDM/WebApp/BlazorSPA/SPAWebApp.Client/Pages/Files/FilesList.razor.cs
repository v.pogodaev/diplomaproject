﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using SPAWebApp.Shared;
using SPAWebApp.Shared.Models;
using SPAWebApp.Shared.Models.Services.FileStorage;

namespace SPAWebApp.Client.Pages.Files
{
  public class FilesListViewModel : ComponentBase
  {
    [Parameter]
    public List<File> Files { get; set; }

    [Parameter]
    public Action<File> SelectedFile { get; set; }
  }
}