﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using SPAWebApp.Client.Shared;
using SPAWebApp.Client.States;
using SPAWebApp.Shared;
using SPAWebApp.Shared.Authentication.Models;
using SPAWebApp.Shared.Models.ViewModels.Account;

namespace SPAWebApp.Client.Pages.Account
{
  public class LoginViewModel : ComponentBase
  {
    [Inject]
    protected IUriHelper UriHelper { get; set; }

    [CascadingParameter]
    protected AppState AppState { get; set; }

    protected LoginVM UserModel { get; set; } = new LoginVM();

    protected string Error { get; set; }

    protected async Task OnSubmitAsync()
    {
      try
      {
        await AppState.AccountState.Login(UserModel);
        UriHelper.NavigateTo("/");
      }
      catch (Exception e)
      {
        Error = e.Message;
      }
    }
  }
}
