﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using SPAWebApp.Client.Shared;
using SPAWebApp.Shared.Models.ViewModels.Account;

namespace SPAWebApp.Client.Pages.Account
{
  public class RegisterViewModel : ComponentBase
  {
    [Inject]
    protected IUriHelper UriHelper { get; set; }

    [CascadingParameter]
    protected AppState AppState { get; set; }

    protected RegisterVM RegisterModel { get; set; } = new RegisterVM();
    protected string Error { get; set; }

    protected async Task OnSubmit()
    {
      Error = null;
      try
      {
        await AppState.AccountState.Register(RegisterModel);
        UriHelper.NavigateTo("/");
      }
      catch (Exception e)
      {
        Error = e.Message;
      }
    }
  }
}
