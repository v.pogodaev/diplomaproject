﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using SPAWebApp.Client.Shared;
using SPAWebApp.Shared;

namespace SPAWebApp.Client.Pages.Account
{
  public class AccountPageViewModel : ComponentBase
  {
    [CascadingParameter]
    protected AppState AppState { get; set; }

    protected UserInfo User { get; set; }

    protected List<string> Roles { get; set; }

    protected override async Task OnInitAsync()
    {
      Console.WriteLine($"{nameof(AccountPageViewModel)}.{nameof(OnInitAsync)}");

      User = await AppState.AccountState.GetUserInfoAsync();
      Roles = await AppState.AccountState.GetUserRolesAsync();
    }
  }
}
