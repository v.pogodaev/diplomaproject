﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using SPAWebApp.Client.Shared;
using SPAWebApp.Shared.Authentication.Models;
using SPAWebApp.Shared.Models.ViewModels.Account;

namespace SPAWebApp.Client.Pages.Account
{
  public class AdminPageViewModel : ComponentBase
  {
    [CascadingParameter]
    protected AppState AppState { get; set; }

    protected bool IsAdmin { get; set; } = false;

    protected List<AdminUserInfoVM> Users { get; set; }

    protected AdminUserInfoVM SelectedUser { get; set; }
    protected List<UserRoleVM> SelectedUserRoles { get; set; }

    protected override async Task OnInitAsync()
    {
      Console.WriteLine($"{nameof(AdminPageViewModel)}.{nameof(OnInitAsync)}");
      
      IsAdmin = await AppState.AccountState.IsAdmin();
      Users = await AppState.AccountState.GetUsersInfoAsync();
    }

    protected async Task SelectUserAsync(AdminUserInfoVM user)
    {
      Console.WriteLine($"{nameof(AdminPageViewModel)}.{nameof(SelectUserAsync)}");

      SelectedUser = user;
      SelectedUserRoles = await AppState.AccountState.GetAllUserRolesAsync(SelectedUser.Id);
    }

    protected async Task SaveRolesAsync()
    {
      Console.WriteLine($"{nameof(AdminPageViewModel)}.{nameof(SaveRolesAsync)}");

      await AppState.AccountState.SaveUserRolesAsync(SelectedUser.Id,
        SelectedUserRoles.Where(r => r.IsAssigned).Select(r => r.RoleName).ToList());
     SelectedUser = null;
     SelectedUserRoles = null;
    }
  }
}
