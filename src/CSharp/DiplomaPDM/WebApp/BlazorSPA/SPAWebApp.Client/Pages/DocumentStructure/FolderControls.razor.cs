﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;

namespace SPAWebApp.Client.Pages.DocumentStructure
{
  public class FolderControlsViewModel : ComponentBase
  {
    [Parameter]
    public bool CanBeDeleted { get; set; }

    [Parameter]
    public Func<Task> OnDeleteAsync { get; set; }

    [Parameter]
    public Action OnAddDocument { get; set; }

    [Parameter]
    public Action OnAddProduct { get; set; }
  }
}
