﻿using Microsoft.AspNetCore.Components;
using SPAWebApp.Shared.Models;
using SPAWebApp.Shared.Models.Services.DocumentStructure;

namespace SPAWebApp.Client.Pages.DocumentStructure
{
  public class FolderInfoViewModel : ComponentBase
  {
    [Parameter]
    public Folder Folder { get; set; }
  }
}
