﻿using System;
using Microsoft.AspNetCore.Components;
using SPAWebApp.Shared.Models.ViewModels;
using SPAWebApp.Shared.Models.ViewModels.DocumentStructure;

namespace SPAWebApp.Client.Pages.DocumentStructure
{
  public class FolderEditFormViewModel : ComponentBase
  {
    [Parameter]
    public FolderModifyVM Folder { get; set; }

    [Parameter]
    public Action<FolderModifyVM> OnSaveFolder { get; set; }
    
    protected void SaveExecute()
    {
      Console.WriteLine($"{nameof(FolderEditFormViewModel)}.{nameof(SaveExecute)}");

      OnSaveFolder.Invoke(Folder);
    }
  }
}
