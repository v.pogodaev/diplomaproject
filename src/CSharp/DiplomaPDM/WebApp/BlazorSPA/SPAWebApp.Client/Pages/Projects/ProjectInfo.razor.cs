﻿using Microsoft.AspNetCore.Components;
using SPAWebApp.Shared.Models;
using SPAWebApp.Shared.Models.Services.Project;

namespace SPAWebApp.Client.Pages.Projects
{
  public class ProjectInfoViewModel : ComponentBase
  {
    [Parameter] public Project Project { get; set; }
  }
}
