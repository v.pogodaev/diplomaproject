﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using SPAWebApp.Shared.Models.ViewModels;
using SPAWebApp.Shared.Models.ViewModels.Project;

namespace SPAWebApp.Client.Pages.Projects
{
  public class ProjectEditFormViewModel : ComponentBase
  {
    [Parameter]
    public ProjectModifyVM Project { get; set; }

    [Parameter]
    public bool IsNew { get; set; }

    [Parameter]
    public Func<ProjectModifyVM, Task> OnSaveProjectAsync { get; set; }

    protected void SaveProjectExecute()
    {
      Console.WriteLine($"{nameof(ProjectEditFormViewModel)}.{nameof(SaveProjectExecute)}");

      OnSaveProjectAsync.Invoke(Project);
    }

    protected override void OnParametersSet()
    {
      Console.WriteLine($"{nameof(ProjectEditFormViewModel)}.{nameof(OnParametersSet)}");

      if (IsNew)
      {
        Project = new ProjectModifyVM();
      }
    }
  }
}
