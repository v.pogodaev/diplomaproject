﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using SPAWebApp.Shared.Models;
using SPAWebApp.Shared.Models.Services.Project;

namespace SPAWebApp.Client.Pages.Projects
{
  public class ProjectsListViewModel : ComponentBase
  {
    [Parameter]
    public List<Project> Projects { get; set; }

    [Parameter]
    public Func<Project, Task> OnSelectedProjectAsync { get; set; }

    protected void SelectProjectExecute(Project project)
    {
      Console.WriteLine($"{nameof(ProjectsListViewModel)}.{nameof(SelectProjectExecute)}");

      OnSelectedProjectAsync.Invoke(project);
    }
  }
}
