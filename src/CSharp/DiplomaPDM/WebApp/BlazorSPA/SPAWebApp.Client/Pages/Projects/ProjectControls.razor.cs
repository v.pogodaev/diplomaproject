﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;

namespace SPAWebApp.Client.Pages.Projects
{
  public class ProjectControlsViewModel : ComponentBase
  {
    [Parameter] public bool IsEditing { get; set; }

    [Parameter] public Action OnCancelEditing { get; set; }

    [Parameter] public Action OnEdit { get; set; }

    [Parameter] public Func<Task> OnDeleteAsync { get; set; }

    protected void CancelEditingExecute()
    {
      Console.WriteLine($"{nameof(ProjectControlsViewModel)}.{nameof(CancelEditingExecute)}");

      OnCancelEditing.Invoke();
      StateHasChanged();
    }

    protected void EditExecute()
    {
      Console.WriteLine($"{nameof(ProjectControlsViewModel)}.{nameof(EditExecute)}");

      OnEdit.Invoke();
    }

    protected void DeleteExecute()
    {
      Console.WriteLine($"{nameof(ProjectControlsViewModel)}.{nameof(DeleteExecute)}");

      OnDeleteAsync.Invoke();
    }
  }
}
