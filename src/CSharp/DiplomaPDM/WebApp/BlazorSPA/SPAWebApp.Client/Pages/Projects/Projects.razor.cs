﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using SPAWebApp.Client.Services.Contracts;
using SPAWebApp.Shared.Models;
using SPAWebApp.Shared.Models.Services.ConstructDocument;
using SPAWebApp.Shared.Models.Services.DocumentStructure;
using SPAWebApp.Shared.Models.Services.Project;
using SPAWebApp.Shared.Models.ViewModels;
using SPAWebApp.Shared.Models.ViewModels.DocumentStructure;
using SPAWebApp.Shared.Models.ViewModels.Project;
using WebApp.ComponentsLib.Tree;

namespace SPAWebApp.Client.Pages.Projects
{
  public class ProjectsViewModel : ComponentBase
  {
    [Inject]
    protected IDocumentStructureService DocumentStructureService { get; set; }

    [Inject]
    protected IProjectService ProjectService { get; set; }

    protected Project SelectedProject { get; set; }
    protected bool IsNewProject { get; set; }
    public List<Project> Projects { get; set; }
    protected bool IsProjectEditing { get; set; } = false;
    protected ProjectModifyVM ProjectToEdit { get; set; }
    protected Tree ProjectTree { get; set; }

    protected Folder SelectedFolder { get; set; }
    protected FolderModifyVM FolderToEdit { get; set; }
    protected bool IsFolderEditing { get; set; }
    protected bool IsNewFolder { get; set; }
    protected bool CanFolderBeDeleted { get; set; }

    protected override async Task OnInitAsync()
    {
      // при инициализации страницы сразу подгружаем проекты
      Console.WriteLine($"{nameof(ProjectsViewModel)}.{nameof(OnInitAsync)}");

      await RefreshProjectsAsync();
    }

    protected void CreateProject()
    {
      Console.WriteLine($"{nameof(ProjectsViewModel)}.{nameof(CreateProject)}");

      SelectedProject = new Project();
      ProjectToEdit = new ProjectModifyVM();
      IsNewProject = true;
      IsProjectEditing = true;

      StateHasChanged();
    }

    protected async Task SelectProjectAsync(Project project)
    {
      Console.WriteLine($"{nameof(ProjectsViewModel)}.{nameof(SelectProjectAsync)}");

      IsNewProject = false;
      SelectedProject = project;
      SelectedFolder = null;
      FolderToEdit = null;
      ProjectToEdit = null;

      await LoadProjectTreeAsync();

      StateHasChanged();
    }

    /// <summary>
    /// Обновляет список. В конце запускается StateHasChanged
    /// </summary>
    /// <returns></returns>
    protected async Task RefreshProjectsAsync()
    {
      Console.WriteLine($"{nameof(ProjectsViewModel)}.{nameof(RefreshProjectsAsync)}");

      try
      {
        Projects = (await ProjectService.GetAllProjectsLazyAsync()).ToList();
        if (SelectedProject != null)
        {
          // Если проект был удален, то надо почистить из Selected, а если был изменен, но изменить
          SelectedProject = Projects.Exists(p => p.Id == SelectedProject.Id)
            ? Projects.FirstOrDefault(p => p.Id == SelectedProject.Id)
            : null;
        }
        StateHasChanged();
      }
      catch (Exception e)
      {
        // todo: modal exception
      }
    }

    protected void CancelProjectEditing()
    {
      Console.WriteLine($"{nameof(ProjectsViewModel)}.{nameof(CancelProjectEditing)}");

      if (IsNewProject)
      {
        SelectedProject = null;
      }
      IsProjectEditing = false;
      ProjectToEdit = null;

      StateHasChanged();
    }

    protected void EditProject()
    {
      Console.WriteLine($"{nameof(ProjectsViewModel)}.{nameof(EditProject)}");

      ProjectToEdit = new ProjectModifyVM { Id = SelectedProject.Id, Sign = SelectedProject.Sign, Name = SelectedProject.Name };
      IsNewProject = false;
      IsProjectEditing = true;

      StateHasChanged();
    }

    protected async Task DeleteProjectAsync()
    {
      Console.WriteLine($"{nameof(ProjectsViewModel)}.{nameof(DeleteProjectAsync)}");

      await ProjectService.DeleteProjectByIdAsync(SelectedProject.Id);
      await RefreshProjectsAsync();
    }

    protected async Task SaveProjectAsync(ProjectModifyVM project)
    {
      Console.WriteLine($"{nameof(ProjectsViewModel)}.{nameof(SaveProjectAsync)}");

      if (IsNewProject)
      {
        var newProject = await ProjectService.CreateProjectAsync(project);
        SelectedProject = newProject;
        IsNewProject = false;
      }
      else
      {
        var editedProject = await ProjectService.ModifyProjectAsync(project);
        SelectedProject = editedProject;
      }
      IsProjectEditing = false;
      await RefreshProjectsAsync();
    }

    protected void CreateFolder()
    {
      Console.WriteLine($"{nameof(ProjectsViewModel)}.{nameof(CreateFolder)}");

      FolderToEdit = new FolderModifyVM();

      if (SelectedFolder == null)
      {
        FolderToEdit.ProjectId = SelectedProject.Id.ToString();
      }
      else
      {
        FolderToEdit.SuperiorFolderId = SelectedFolder.Id.ToString();
      }

      IsNewFolder = true;
      IsFolderEditing = true;
      SelectedFolder = new Folder();

      StateHasChanged();
    }

    protected async Task LoadProjectTreeAsync()
    {
      Console.WriteLine($"{nameof(ProjectsViewModel)}.{nameof(LoadProjectTreeAsync)}");

      var nodes = new List<TreeNode>();

      SelectedProject.ProjectItems = (await ProjectService.GetIncludedItemsAsync(SelectedProject.Id)).ToList();


      Console.WriteLine($"{nameof(ProjectsViewModel)}.{nameof(LoadProjectTreeAsync)}: items count = {SelectedProject.ProjectItems.Count}");

      foreach (var item in SelectedProject.ProjectItems)
      {
        Console.WriteLine($"{nameof(ProjectsViewModel)}.{nameof(LoadProjectTreeAsync)}: item ({item.ItemRealId}) type = {item.ItemType}");

        if (item.ItemType.ToLower() == "folder") // пока что по-другому быть и не может
        {
          nodes.Add(MakeNodeFromFolder(await GetFolderByStringIdAsync(item.ItemRealId), nodes.Count, null));
        }
      }

      ProjectTree = new Tree { Nodes = nodes };
    }

    protected async Task<Folder> GetFolderByStringIdAsync(string folderId)
    {
      Console.WriteLine($"{nameof(ProjectsViewModel)}.{nameof(GetFolderByStringIdAsync)}");

      return await DocumentStructureService.GetFolderByIdAsync(new Guid(folderId));
    }

    protected async Task<List<TreeNode>> ExpandNodeAsync(object node, int nodeId)
    {
      Console.WriteLine($"{nameof(ProjectsViewModel)}.{nameof(ExpandNodeAsync)}");

      if (node is Document) { return new List<TreeNode>(); }
      if (!(node is Folder folder)) { return new List<TreeNode>(); } // вообще, такого тоже быть не должно, но фиг его знает
      var folders = await GetFoldersOfFolderByIdAsync(folder.Id);
      var documents = await GetDocumentsOfFolderByIdAsync(folder.Id);

      Console.WriteLine($"{nameof(ProjectsViewModel)}.{nameof(ExpandNodeAsync)}: folder's count: {folders.Count}, document's count: {documents.Count}");

      var nodes = new List<TreeNode>();
      int id = ProjectTree.Nodes.Count;
      if (folders != null)
      {
        foreach (var f in folders)
        {
          var fWithChildren = await DocumentStructureService.GetFolderByIdAsync(f.Id);
          nodes.Add(MakeNodeFromFolder(fWithChildren, id++, nodeId));
        }
      }
      if (documents != null)
      {
        foreach (var d in documents)
        {
          nodes.Add(MakeNodeFromDocument(d, id++, nodeId));
        }
      }

      return nodes;
    }

    protected async Task<ICollection<Folder>> GetFoldersOfFolderByIdAsync(Guid folderId)
    {
      Console.WriteLine($"{nameof(ProjectsViewModel)}.{nameof(GetFoldersOfFolderByIdAsync)}");

      return await DocumentStructureService.GetFoldersByFolderIdAsync(folderId);
    }

    protected async Task<ICollection<Document>> GetDocumentsOfFolderByIdAsync(Guid folderId)
    {
      Console.WriteLine($"{nameof(ProjectsViewModel)}.{nameof(GetDocumentsOfFolderByIdAsync)}");

      return await DocumentStructureService.GetDocumentsByFolderIdAsync(folderId);
    }

    protected async Task SelectFolder(object node)
    {
      Console.WriteLine($"{nameof(ProjectsViewModel)}.{nameof(SelectFolder)}");

      if (!(node is Folder folder)) { return; }

      IsNewFolder = false;
      SelectedFolder = folder;

      CanFolderBeDeleted = await CanFolderBeDeletedAsync();

      StateHasChanged();
    }

    protected void CancelFolderEditing()
    {
      Console.WriteLine($"{nameof(ProjectsViewModel)}.{nameof(CancelFolderEditing)}");

      IsFolderEditing = false;
      FolderToEdit = null;

      StateHasChanged();
    }

    protected async Task DeleteFolderAsync()
    {
      Console.WriteLine($"{nameof(ProjectsViewModel)}.{nameof(DeleteFolderAsync)}");

      await DocumentStructureService.DeleteFolderByIdAsync(SelectedFolder.Id);
      await LoadProjectTreeAsync();

      StateHasChanged();
    }

    protected async Task SaveFolderAsync(FolderModifyVM folder)
    {
      Console.WriteLine($"{nameof(ProjectsViewModel)}.{nameof(SaveFolderAsync)}");

      var newFolder = await DocumentStructureService.CreateFolderAsync(folder);
      SelectedFolder = newFolder;
      IsNewFolder = false;
      IsFolderEditing = false;
      await LoadProjectTreeAsync();

      StateHasChanged();
    }

    protected async Task<bool> CanFolderBeDeletedAsync()
    {
      Console.WriteLine($"{nameof(ProjectsViewModel)}.{nameof(CanFolderBeDeletedAsync)}");

      return await DocumentStructureService.CanFolderBeDeletedByIdAsync(SelectedFolder.Id);
    }

    protected bool IsDocumentAdding { get; set; } = false;

    protected void StartAddingDocument()
    {
      Console.WriteLine($"{nameof(ProjectsViewModel)}.{nameof(StartAddingDocument)}");

      IsDocumentAdding = true;
    }

    protected async Task DocumentAddedAsync(string docId)
    {
      Console.WriteLine($"{nameof(ProjectsViewModel)}.{nameof(DocumentAddedAsync)}");

      await DocumentStructureService.IncludeItemToFolder(SelectedFolder.Id, docId, "Document");
    }

    protected bool IsProductAdding { get; set; } = false;


    protected void StartAddingProduct()
    {
      Console.WriteLine($"{nameof(ProjectsViewModel)}.{nameof(StartAddingProduct)}");

      IsProductAdding = true;
    }


    private TreeNode MakeNodeFromFolder(Folder folder, int id, int? parentId)
    {
      Console.WriteLine($"{nameof(ProjectsViewModel)}.{nameof(MakeNodeFromFolder)}: : items count = {(folder.Items?.Count + folder.SubFolderIds?.Count)}");

      return new TreeNode
      {
        Id = id,
        HasChildren = folder.Items?.Count > 0 || folder.SubFolderIds?.Count > 0,
        ParentId = parentId,
        RealObject = folder,
        Name = folder.Name,
        Deep = 0,
        IsExpandable = true,
        IsSelected = false,
        IsExpanded = false,
        IsVisible = false,
        ClosedIcon = "far fa-folder icon-correct",
        OpenedIcon = "far fa-folder-open icon-correct",
        IconToOpen = "fas fa-plus-circle icon-correct",
        IconToClose = "fas fa-minus-circle icon-correct"
      };
    }

    private TreeNode MakeNodeFromDocument(Document document, int id, int? parentId)
    {
      return new TreeNode
      {
        Id = id,
        ParentId = parentId,
        RealObject = document,
        Name = document.Name,
        Deep = 0,
        HasChildren = false,
        IsExpandable = false,
        IsSelected = false,
        IsExpanded = false,
        IsVisible = false,
        ClosedIcon = "far fa-file icon-correct"
      };
    }
  }
}
