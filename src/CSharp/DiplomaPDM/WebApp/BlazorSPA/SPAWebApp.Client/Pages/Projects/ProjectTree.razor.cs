﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using WebApp.ComponentsLib.Tree;

namespace SPAWebApp.Client.Pages.Projects
{
  public class ProjectTreeViewModel : ComponentBase
  {
    [Parameter]
    public Tree Tree { get; set; }

    [Parameter]
    public Func<object, int, Task<List<TreeNode>>> OnExpandFolderAsync { get; set; }

    [Parameter]
    public Action<object> OnFolderSelect { get; set; }

    [Parameter]
    public Action OnCreateFolder { get; set; }


    protected void CreateFolderExecute()
    {
      Console.WriteLine($"{nameof(ProjectTreeViewModel)}.{nameof(CreateFolderExecute)}");

      OnCreateFolder.Invoke();
    }
  }
}
