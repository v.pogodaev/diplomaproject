﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using SPAWebApp.Shared;
using SPAWebApp.Shared.Models;
using SPAWebApp.Shared.Models.Services.Product;

namespace SPAWebApp.Client.Pages.Products
{
  public class ProductsListViewModel : ComponentBase
  {
    [Parameter]
    public List<Product> Products { get; set; }

    [Parameter]
    public Action<Product> SelectedProducts { get; set; }
  }
}