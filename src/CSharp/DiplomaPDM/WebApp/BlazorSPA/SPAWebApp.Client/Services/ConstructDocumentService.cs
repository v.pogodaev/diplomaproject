﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using SPAWebApp.Client.Services.Contracts;
using SPAWebApp.Shared.Models.Services.ConstructDocument;
using SPAWebApp.Shared.Models.Services.Product;
using SPAWebApp.Shared.Models.Services.Relationship;
using SPAWebApp.Shared.Models.ViewModels.ConstructDocument;

namespace SPAWebApp.Client.Services
{
  public class ConstructDocumentService : IConstructDocumentService
  {
    private readonly HttpClient m_HttpClient;

    public ConstructDocumentService(HttpClient httpClient)
    {
      m_HttpClient = httpClient;
    }


    public Task<List<Document>> GetDocumentListByIdsAsync(string[] ids)
    {
      throw new NotImplementedException();
    }

    public Task<List<Document>> GetAllDocumentsAsync()
    {
      throw new NotImplementedException();
    }

    public async Task<Document> GetDataAsync(string id)
    {
      var document = await m_HttpClient.GetJsonAsync<Document>($"constructDocument/getDocument/{id}");
      return document;
    }

    public async Task<List<Document>> GetAllDataAsync()
    {
      return await m_HttpClient.GetJsonAsync<List<Document>>($"constructDocument/getAllDocuments");

    }

    public async Task<string> AddFilesAsync(string[] ids)
    {
      return await m_HttpClient.GetJsonAsync<string>($"constructDocument/addFiles");
    }

    public async Task ChangeDocumentAsync(ModifyDocumentVM modifyDocumentViewModel)
    {
      try
      {
        await m_HttpClient.PostJsonAsync($"constructDocument/change", modifyDocumentViewModel);
      } catch (Exception e)
      {
        // todo: обработка ошибки
        Console.WriteLine(e.Message);
      }
    }

    public async Task<string> CreateAsync(CreateNewDocumentVM createNewDocumentViewModel)
    {
      try
      {
        return await m_HttpClient.PostJsonAsync<string>($"constructDocument/create", createNewDocumentViewModel);
      } catch (Exception e)
      {
        // todo: обработка ошибки. Что возвращать? Выкидывать эксепшн, наверн?
        Console.WriteLine(e.Message);
        return "";
      }
    }

    public async Task ChangeStateAsync(ChangeStateByDocumentVM changeStateByDocumentViewModel)
    {
      try
      {
        await m_HttpClient.PostJsonAsync($"constructDocument/changeState", changeStateByDocumentViewModel);
      } catch (Exception e)
      {
        // todo: обработка ошибки
        Console.WriteLine(e.Message);
      }
    }

    public async Task<string> CreateNewRevAsync(string Id)
    {
      return await m_HttpClient.GetStringAsync($"constructDocument/createNewRev/{Id}");
    }

    public async Task<string> CheckDocumentForCreateNewRevision(string Id)
    {
      return await m_HttpClient.GetStringAsync($"constructDocument/checkForCreateNewRev/{Id}");
    }

    public async Task RemoveDocumentAsync(string id)
    {
      await m_HttpClient.PostJsonAsync($"constructDocument/removeDocument", id);
    }

    public async Task RemoveFiles(List<string> ids)
    {
      await m_HttpClient.GetJsonAsync<string>($"constructDocument/removeFiles");
    }

    public async Task<DocumentAndPage> GetDocumentsOfFirstPageAsync()
    {
      return await m_HttpClient.GetJsonAsync<DocumentAndPage>($"constructDocument/GetDocumentsOfFirstPageAsync");
    }

    public async Task<List<Document>> GetDocumentsOfPageAsync(int page)
    {
      return await m_HttpClient.GetJsonAsync<List<Document>>($"constructDocument/GetDocumentsOfPageAsync/{page}");
    }

    public async Task<List<Product>> GetRelatedProducts(string documentId)
    {
      return await m_HttpClient.GetJsonAsync<List<Product>>($"constructDocument/getRelatedProduct/{documentId}");
    }

    public async Task ConnectProductToDocument(Relationship relationship)
    {
      await m_HttpClient.PostJsonAsync($"constructDocument/connectProductToDocument", relationship);
      // return await m_HttpClient.GetJsonAsync<string>($"constructDocument/connectProductToDocument/{relationship}");
    }

  }
}
