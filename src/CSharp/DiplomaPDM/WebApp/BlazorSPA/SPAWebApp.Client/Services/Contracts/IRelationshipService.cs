﻿using System.Threading.Tasks;
using SPAWebApp.Shared.Models.Services.Relationship;

namespace SPAWebApp.Client.Services.Contracts
{
  public interface IRelationshipService
  {
    //Task<string> CreateRelationshipAsync(TRelationshipCreateModel relationship);

    Task<string[]> GetRelatedDocumentsByProductIdAsync(string productId);

    Task<string[]> GetRelatedProductsByDocumentIdAsync(string documentId);

    Task<Relationship> GetRelationshipByIdAsync(string relationshipId);

   // Task RemoveRelationshipAsync(string relationshipId, TransactionInfo transaction);

   // Task UpdateRelationshipTypeAsync(string relationshipId, TRelationshipUpdateModel relationship);
  }
}