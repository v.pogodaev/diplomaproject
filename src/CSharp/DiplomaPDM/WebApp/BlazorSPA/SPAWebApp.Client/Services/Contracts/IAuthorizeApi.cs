﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SPAWebApp.Shared;
using SPAWebApp.Shared.Authentication.Models;
using SPAWebApp.Shared.Models.ViewModels.Account;

namespace SPAWebApp.Client.Services.Contracts
{
  public interface IAuthorizeApi
  {
    Task<UserInfo> Login(LoginVM loginParameters);
    Task<UserInfo> Register(RegisterVM registerParameters);
    Task Logout();
    Task<UserInfo> GetUserInfo();
    Task<bool> IsAdmin();
    Task<List<string>> GetUserRoles();
    Task<List<AdminUserInfoVM>> GetUsersInfoAsync();
    Task<List<UserRoleVM>> GetAllUserRolesAsync(string id);
    Task SaveUserRolesAsync(string id, List<string> roles);
  }
}
