﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SPAWebApp.Shared.Models.Services.ConstructDocument;
using SPAWebApp.Shared.Models.Services.Product;
using SPAWebApp.Shared.Models.Services.Relationship;
using SPAWebApp.Shared.Models.ViewModels.ConstructDocument;

namespace SPAWebApp.Client.Services.Contracts
{
  public interface IConstructDocumentService : SPAWebApp.Shared.Services.IConstructDocumentService
  {
    Task<List<Document>> GetDocumentListByIdsAsync(string[] ids);

    Task<List<Document>> GetAllDocumentsAsync();

    Task<string> AddFilesAsync(string[] ids);

    Task ChangeDocumentAsync(ModifyDocumentVM modifyDocumentViewModel);

    Task ChangeStateAsync(ChangeStateByDocumentVM changeStateByDocumentViewModel);

    Task<string> CreateNewRevAsync(string Id);

    Task<Document> GetDataAsync(string id);

    Task<List<Document>> GetAllDataAsync();

    Task RemoveDocumentAsync(string id);

    Task RemoveFiles(List<string> ids);

    // v.pogodaev: поменял выходной параметр
    Task<string> CreateAsync(CreateNewDocumentVM createNewDocumentViewModel);

    Task<string> CheckDocumentForCreateNewRevision(string id);

    Task<DocumentAndPage> GetDocumentsOfFirstPageAsync();

    Task<List<Document>> GetDocumentsOfPageAsync(int page);

    Task<List<Product>> GetRelatedProducts(string documentId);

    Task ConnectProductToDocument(Relationship relationship);
  }

  public class DocumentAndPage
  {
    public int Pages { get; set; }
    public List<Document> Documents { get; set; }
  }
}
