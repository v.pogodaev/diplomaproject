﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SPAWebApp.Shared.Models.Services.Project;
using SPAWebApp.Shared.Models.ViewModels.Project;

namespace SPAWebApp.Client.Services.Contracts
{
  public interface IProjectService : SPAWebApp.Shared.Services.IProjectService
  {
    Task<Project> CreateProjectAsync(ProjectCreateVM projectModel);
    Task DeleteProjectByIdAsync(Guid projectId);
    Task<Project> ModifyProjectAsync(ProjectModifyVM updatedProject);
    Task IncludeItemToProjectAsync(Guid projectId, ProjectItemCreateVM item);
    Task<ICollection<ProjectItem>> GetIncludedItemsAsync(Guid projectId);
    Task ExcludeItemAsync(Guid projectId, string itemRealId);
    Task<ICollection<ProjectItem>> GetIncludedItemsByTypeAsync(Guid projectId, string type);
    Task<ICollection<string>> GetAllowedTypesAsync();
    Task<Project> GetProjectByIdAsync(Guid projectId);
    Task<ICollection<Project>> GetAllProjectsLazyAsync();
  }
}
