﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SPAWebApp.Shared.Models.Services.ConstructDocument;
using SPAWebApp.Shared.Models.Services.DocumentStructure;
using SPAWebApp.Shared.Models.ViewModels.DocumentStructure;

namespace SPAWebApp.Client.Services.Contracts
{
  public interface IDocumentStructureService : SPAWebApp.Shared.Services.IDocumentStructureService
  {
    Task<ICollection<Folder>> GetAllFoldersAsync();
    Task<ICollection<Folder>> GetFoldersByFolderIdAsync(Guid folderId);
    Task<ICollection<Document>> GetDocumentsByFolderIdAsync(Guid folderId);
    Task<Folder> GetFolderByIdAsync(Guid folderId);
    Task DeleteFolderByIdAsync(Guid folderId);
    Task<Folder> CreateFolderAsync(FolderModifyVM folderToCreate);
    //Task<Folder> ModifyFolderAsync(FolderModifyViewModel2 folderToEdit);
    Task<bool> CanFolderBeDeletedByIdAsync(Guid folderId);
    Task IncludeItemToFolder(Guid selectedFolderId, string docId, string type);
  }
}
