﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using SPAWebApp.Shared.Models.Services.ConstructDocument;
using SPAWebApp.Shared.Models.Services.Product;

namespace SPAWebApp.Client.Services.Contracts
{
  public interface IProductCardService { 
    Task<List<Product>> GetProductListByIdsAsync(string[] ids);
    Task<List<Product>> GetAllProduct();

    Task RemoveProductAsync(string id);

    Task<ProductAndPage> GetProductsOfFirstPageAsync();

    Task<List<Product>> GetProductsOfPageAsync(int page);

    Task<List<Document>> GetRelatedDocuments(string productId);
  }

  public class ProductAndPage
  {
    public int Pages { get; set; }
    public List<Product> Products { get; set; }
  }
}
