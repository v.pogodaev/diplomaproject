﻿using System.Collections.Generic;
using System.Threading.Tasks;
using SPAWebApp.Shared.Models.Services.FileStorage;
using SPAWebApp.Shared.Models.ViewModels.FileService;

namespace SPAWebApp.Client.Services.Contracts
{
  public interface IFileStorageService// : SPAWebApp.Shared.Services.IFileStorageService
  {
    Task<File> GetDataAsync(string id);

    Task<List<File>> GetAllFilesAsync();

    Task RemoveFileAsync(string id);

    Task UploadFileAsync(UploadFileVM file);

    Task DownloadFileAsync(string fileId);
  }
}
