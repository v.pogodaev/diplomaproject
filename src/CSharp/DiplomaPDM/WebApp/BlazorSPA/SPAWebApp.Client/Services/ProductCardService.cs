﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using SPAWebApp.Client.Services.Contracts;
using SPAWebApp.Shared.Models.Services.ConstructDocument;
using SPAWebApp.Shared.Models.Services.Product;

namespace SPAWebApp.Client.Services
{
  public class ProductCardService : IProductCardService
  {
    private readonly HttpClient m_HttpClient;

    public ProductCardService(HttpClient httpClient)
    {
      m_HttpClient = httpClient;
    }

    public async Task<List<Product>> GetAllProduct()
    {
      
      return await m_HttpClient.GetJsonAsync<List<Product>>($"cards_storage/getAllProducts");
    }

    public Task<List<Product>> GetAllProductsAsync()
    {
      throw new NotImplementedException();
    }

    public Task<List<Product>> GetProductListByIdsAsync(string[] ids)
    {
      throw new NotImplementedException();
    }

    public async Task RemoveProductAsync(string id)
    {
      await m_HttpClient.PostJsonAsync($"cards_storage/removeProduct", id);
    }

    public async Task<ProductAndPage> GetProductsOfFirstPageAsync()
    {
      return await m_HttpClient.GetJsonAsync<ProductAndPage>($"cards_storage/GetProductsOfFirstPageAsync");
    }

    public async Task<List<Product>> GetProductsOfPageAsync(int page)
    {
      return await m_HttpClient.GetJsonAsync<List<Product>>($"cards_storage/GetProductsOfPageAsync/{page}");
    }

    public async Task<List<Document>> GetRelatedDocuments(string productId)
    {
      return await m_HttpClient.GetJsonAsync<List<Document>>($"cards_storage/getRelatedDocument/{productId}");
    }
  }
}
