﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using SPAWebApp.Client.Services.Contracts;
using SPAWebApp.Shared.Models.Services.FileStorage;
using SPAWebApp.Shared.Models.ViewModels.FileService;

namespace SPAWebApp.Client.Services
{
  public class FileStorageService : IFileStorageService
  {
    private readonly HttpClient m_HttpClient;

    public FileStorageService(HttpClient httpClient)
    {
      m_HttpClient = httpClient;
    }
    public async Task<List<File>> GetAllFilesAsync()
    {
      var result = await m_HttpClient.GetJsonAsync<List<File>>($"storage/getAllFilesData");
      return result;
    }

    public async Task<File> GetDataAsync(string id)
    {
      return await m_HttpClient.GetJsonAsync<File>($"storage/getFile/{id}");
    }

    public async Task RemoveFileAsync(string id)
    {
      await m_HttpClient.DeleteAsync($"storage/removeFile/{id}");
    }

    public async Task UploadFileAsync(UploadFileVM uploadFileViewModel)
    {
      try
      {
        await m_HttpClient.PostJsonAsync($"storage/upload", uploadFileViewModel);
      } catch (HttpRequestException e)
      {
        // todo: обработка ошибки
        Console.WriteLine(e.Message);
      }
    }

    public async Task DownloadFileAsync(string id)
    {
      try
      {
        await m_HttpClient.PostJsonAsync($"storage/upload", id);
      } catch (HttpRequestException e)
      {
        // todo: обработка ошибки
        Console.WriteLine(e.Message);
      }
    }
  }
}
