﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using SPAWebApp.Client.Services.Contracts;
using SPAWebApp.Shared;
using SPAWebApp.Shared.Models;
using SPAWebApp.Shared.Models.Services.ConstructDocument;
using SPAWebApp.Shared.Models.Services.DocumentStructure;
using SPAWebApp.Shared.Models.ViewModels;
using SPAWebApp.Shared.Models.ViewModels.DocumentStructure;

namespace SPAWebApp.Client.Services
{
  public class DocumentStructureService : IDocumentStructureService
  {
    private readonly HttpClient m_HttpClient;


    public DocumentStructureService(HttpClient httpClient)
    {
      m_HttpClient = httpClient;
    }


    public async Task<ICollection<Folder>> GetAllFoldersAsync()
    {
      try
      {
        return await m_HttpClient.GetJsonAsync<ICollection<Folder>>(Urls.DocumentStructure.GetAllFolders());
      }
      catch (Exception e)
      {
        throw new Exception(e.Message);
      }
    }

    public async Task<ICollection<Folder>> GetFoldersByFolderIdAsync(Guid folderId)
    {
      try
      {
        var path = Urls.DocumentStructure.GetIncludedFolders(folderId);
        Console.WriteLine(path);
        return await m_HttpClient.GetJsonAsync<ICollection<Folder>>(Urls.DocumentStructure.GetIncludedFolders(folderId));
      }
      catch (Exception e)
      {
        throw new Exception(e.Message);
      }
    }

    public async Task<ICollection<Document>> GetDocumentsByFolderIdAsync(Guid folderId)
    {
      try
      {
        var path = Urls.DocumentStructure.GetIncludedDocuments(folderId);
        Console.WriteLine(path);
        return await m_HttpClient.GetJsonAsync<ICollection<Document>>(Urls.DocumentStructure.GetIncludedDocuments(folderId));
      }
      catch (Exception e)
      {
        throw new Exception(e.Message);
      }
    }

    public async Task<Folder> GetFolderByIdAsync(Guid folderId)
    {
      try
      {
        var path = Urls.DocumentStructure.GetFolder(folderId);
        Console.WriteLine(path);
        return await m_HttpClient.GetJsonAsync<Folder>(Urls.DocumentStructure.GetFolder(folderId));
      }
      catch (Exception e)
      {
        throw new Exception(e.Message);
      }
    }

    public async Task DeleteFolderByIdAsync(Guid folderId)
    {
      try
      {
        var path = Urls.DocumentStructure.DeleteFolder(folderId);
        Console.WriteLine(path);
        await m_HttpClient.DeleteAsync(Urls.DocumentStructure.DeleteFolder(folderId));
      }
      catch (Exception e)
      {
        throw new Exception(e.Message);
      }
    }

    public async Task<Folder> CreateFolderAsync(FolderModifyVM folderToCreate)
    {
      try
      {
        var path = Urls.DocumentStructure.CreateFolder();
        Console.WriteLine(path);
        return await m_HttpClient.PostJsonAsync<Folder>(Urls.DocumentStructure.CreateFolder(), folderToCreate);
      }
      catch (Exception e)
      {
        throw new Exception(e.Message);
      }
    }

    //public async Task<Folder> ModifyFolderAsync(FolderModifyViewModel2 folderToEdit)
    //{
    //  try
    //  {
    //    var path = Urls.DocumentStructure.ModifyFolder();
    //    Console.WriteLine(path);
    //    return await m_HttpClient.PutJsonAsync<Folder>(Urls.DocumentStructure.ModifyFolder(), folderToEdit);
    //  }
    //  catch (Exception e)
    //  {
    //    throw new Exception(e.Message);
    //  }
    //}

    public async Task<bool> CanFolderBeDeletedByIdAsync(Guid folderId)
    {
      try
      {
        var path = Urls.DocumentStructure.GetFolderDeleteAbility(folderId);
        Console.WriteLine(path);
        return await m_HttpClient.GetJsonAsync<bool>(Urls.DocumentStructure.GetFolderDeleteAbility(folderId));
      }
      catch (Exception e)
      {
        throw new Exception(e.Message);
      }
    }

    public async Task IncludeItemToFolder(Guid selectedFolderId, string docId, string type)
    {
      try
      {
        var path = Urls.DocumentStructure.IncludeItemToFolder(selectedFolderId.ToString());
        Console.WriteLine(path);
        var item = new FolderItem() {ItemRealId = docId, ItemType = type};
        await m_HttpClient.PostJsonAsync(Urls.DocumentStructure.IncludeItemToFolder(selectedFolderId.ToString()), item);
      }
      catch (Exception e)
      {
        throw new Exception(e.Message);
      }
    }
  }
}
