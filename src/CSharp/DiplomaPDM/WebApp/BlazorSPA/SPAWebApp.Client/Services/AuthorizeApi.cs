﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using Microsoft.JSInterop;
using SPAWebApp.Client.Services.Contracts;
using SPAWebApp.Shared;
using SPAWebApp.Shared.Authentication.Models;
using SPAWebApp.Shared.Models.ViewModels;
using SPAWebApp.Shared.Models.ViewModels.Account;

namespace SPAWebApp.Client.Services
{
  public class AuthorizeApi : IAuthorizeApi
  {
    private readonly HttpClient m_HttpClient;

    public AuthorizeApi(HttpClient httpClient)
    {
      m_HttpClient = httpClient;
    }



    public async Task<UserInfo> Login(LoginVM loginParameters)
    {
      var stringContent = new StringContent(Json.Serialize(loginParameters), Encoding.UTF8, "application/json");

      var result = await m_HttpClient.PostAsync("api/Account/Login", stringContent);

      if (result.StatusCode == System.Net.HttpStatusCode.BadRequest)
      {
        throw new Exception(await result.Content.ReadAsStringAsync());
      }
      result.EnsureSuccessStatusCode();

      return Json.Deserialize<UserInfo>(await result.Content.ReadAsStringAsync());
    }

    public async Task Logout()
    {
      var result = await m_HttpClient.PostAsync("api/Account/Logout", null);
      result.EnsureSuccessStatusCode();
    }

    public async Task<UserInfo> Register(RegisterVM registerParameters)
    {
      var stringContent = new StringContent(
        Json.Serialize(registerParameters), Encoding.UTF8, "application/json");
      var result = await m_HttpClient.PostAsync("api/Account/Register", stringContent);
      if (result.StatusCode == System.Net.HttpStatusCode.BadRequest)
      {
        throw new Exception(await result.Content.ReadAsStringAsync());
      }
      result.EnsureSuccessStatusCode();

      return Json.Deserialize<UserInfo>(await result.Content.ReadAsStringAsync());
    }

    public async Task<UserInfo> GetUserInfo()
    {
      var result = await m_HttpClient.GetJsonAsync<UserInfo>("api/Account/UserInfo");
      return result;
    }

    public async Task<bool> IsAdmin()
    {
      var result = await m_HttpClient.GetJsonAsync<bool>("api/Account/IsAdmin");
      return result;
    }

    public async Task<List<string>> GetUserRoles()
    {
      Console.WriteLine("GetUserRoles");
      var result = await m_HttpClient.GetJsonAsync<List<string>>("api/Account/GetAssignedUserRoles");
      return result;
    }

    public async Task<List<AdminUserInfoVM>> GetUsersInfoAsync()
    {
      var result = await m_HttpClient.GetJsonAsync<List<AdminUserInfoVM>>("api/Account/GetUsers");
      return result;
    }

    public async Task<List<UserRoleVM>> GetAllUserRolesAsync(string id)
    {
      var result = await m_HttpClient.GetJsonAsync<List<UserRoleVM>>($"api/Account/getAllUserRoles?userId={id}");
      return result;
    }

    public async Task SaveUserRolesAsync(string id, List<string> roles)
    {
      var stringContent = new StringContent(Json.Serialize(roles), Encoding.UTF8, "application/json");

      await m_HttpClient.PostAsync($"api/Account/ChangeUserRoles?userId={id}", stringContent);
    }
  }
}
