﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using SPAWebApp.Client.Services.Contracts;
using SPAWebApp.Shared;
using SPAWebApp.Shared.Models;
using SPAWebApp.Shared.Models.Services.Project;
using SPAWebApp.Shared.Models.ViewModels;
using SPAWebApp.Shared.Models.ViewModels.Project;

namespace SPAWebApp.Client.Services
{
  public class ProjectService : IProjectService
  {
    private readonly HttpClient m_HttpClient;

    public ProjectService(HttpClient httpClient)
    {
      m_HttpClient = httpClient;
    }


    public async Task<Project> CreateProjectAsync(ProjectCreateVM projectModel)
    {
      try
      {
        return await m_HttpClient.PostJsonAsync<Project>(Urls.Project.CreateProject(), projectModel);
      }
      catch (Exception e)
      {
        throw new Exception(e.Message);
      }
    }

    public async Task DeleteProjectByIdAsync(Guid projectId)
    {
      try
      {
        await m_HttpClient.DeleteAsync(Urls.Project.DeleteProject(projectId));
      }
      catch (Exception e)
      {
        throw new Exception(e.Message);
      }
    }

    public async Task<Project> ModifyProjectAsync(ProjectModifyVM updatedProject)
    {
      try
      {
        return await m_HttpClient.PutJsonAsync<Project>(Urls.Project.ModifyProject(), updatedProject);
      }
      catch (Exception e)
      {
        throw new Exception(e.Message);
      }
    }

    public Task IncludeItemToProjectAsync(Guid projectId, ProjectItemCreateVM item)
    {
      throw new NotImplementedException();
    }

    public async Task<ICollection<ProjectItem>> GetIncludedItemsAsync(Guid projectId)
    {
      try
      {
        return await m_HttpClient.GetJsonAsync<ICollection<ProjectItem>>(Urls.Project.GetIncludedItems(projectId));
      }
      catch (Exception e)
      {
        throw new Exception(e.Message);
      }
    }

    public Task ExcludeItemAsync(Guid projectId, string itemRealId)
    {
      throw new NotImplementedException();
    }

    public Task<ICollection<ProjectItem>> GetIncludedItemsByTypeAsync(Guid projectId, string type)
    {
      throw new NotImplementedException();
    }

    public Task<ICollection<string>> GetAllowedTypesAsync()
    {
      throw new NotImplementedException();
    }

    public Task<Project> GetProjectByIdAsync(Guid projectId)
    {
      throw new NotImplementedException();
    }

    public async Task<ICollection<Project>> GetAllProjectsLazyAsync()
    {
      try
      {
        Console.WriteLine(Urls.Project.GetLazyProjects());
        return await m_HttpClient.GetJsonAsync<ICollection<Project>>(Urls.Project.GetLazyProjects());
      }
      catch (Exception e)
      {
        throw new Exception(e.Message);
      }
    }

    //public Task CommitTransaction(TransactionViewModel transaction)
    //{
    //  throw new NotImplementedException();
    //}

    //public Task CancelTransaction(TransactionViewModel transaction)
    //{
    //  throw new NotImplementedException();
    //}
  }
}
