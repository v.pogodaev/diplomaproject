﻿using System;
using Microsoft.AspNetCore.Components;

namespace WebApp.ComponentsLib.Services
{
  public class ModalService
  {
    public event Action<string, RenderFragment> OnShow;
    public event Action OnClose;

    public void Show(string title, Type contentType)
    {
      if (contentType.BaseType != typeof(ComponentBase))
      {
        throw new ArgumentException($"{contentType.FullName} must be a Component Base");
      }

      var content = new RenderFragment(f =>
      {
        f.OpenComponent(1, contentType);
        f.CloseComponent();
      });

      OnShow?.Invoke(title, content);
    }

    public void Close()
    {
      OnClose?.Invoke();
    }
  }
}
