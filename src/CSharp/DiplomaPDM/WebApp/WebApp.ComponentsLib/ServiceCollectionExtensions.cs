﻿using Microsoft.Extensions.DependencyInjection;
using WebApp.ComponentsLib.Services;

namespace WebApp.ComponentsLib
{
  public static class ServiceCollectionExtensions
  {
    public static IServiceCollection AddModal(this IServiceCollection services)
    {
      services.AddScoped<ModalService>();
      return services;
    }
  }
}
