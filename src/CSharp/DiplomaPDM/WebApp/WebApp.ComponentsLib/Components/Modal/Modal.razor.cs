﻿using System;
using Microsoft.AspNetCore.Components;
using WebApp.ComponentsLib.Services;

namespace WebApp.ComponentsLib.Components.Modal
{
  public class ModalViewModel : ComponentBase, IDisposable
  {
    [Inject] public ModalService ModalService { get; set; }

    public bool IsVisible { get; set; }
    protected string Title { get; set; }
    protected RenderFragment Content { get; set; }

    protected override void OnInit()
    {
      ModalService.OnShow += ShowModal;
      ModalService.OnClose += CloseModal;
    }

    public void ShowModal(string title, RenderFragment content)
    {
      Title = title;
      Content = content;
      IsVisible = true;

      StateHasChanged();
    }

    public void CloseModal()
    {
      IsVisible = false;
      Title = "";
      Content = null;

      StateHasChanged();
    }

    public void Dispose()
    {
      ModalService.OnShow -= ShowModal;
      ModalService.OnClose -= CloseModal;
    }
  }
}
