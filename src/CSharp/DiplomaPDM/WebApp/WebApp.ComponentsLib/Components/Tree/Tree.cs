﻿using System.Collections.Generic;
using System.Linq;

// ReSharper disable once CheckNamespace
namespace WebApp.ComponentsLib.Tree
{
  /// <summary>
  /// Дерево
  /// </summary>
  public class Tree
  {
    /// <summary>
    /// Список узлов
    /// </summary>
    public List<TreeNode> Nodes { get; set; } = null;

    /// <summary>
    /// Сортировка узлов. Необходимо выполнить после первого заполнения
    /// </summary>
    public void Reorder()
    {
      var oldTree = Nodes.OrderBy(n => n.Deep).ToList();
      var newTree = new List<TreeNode>();
      foreach (var node in Nodes)
      {
        if (node.Deep != 0) { continue; }

        node.IsVisible = true;
        newTree.Add(node);
        InsertChildren(oldTree, newTree, node.Id);
      }

      Nodes = newTree;
    }


    private void InsertChildren(List<TreeNode> oldTree, List<TreeNode> newTree, int parentId)
    {
      foreach (var node in oldTree)
      {
        if (node.ParentId != parentId) continue;
        newTree.Add(node);
        InsertChildren(oldTree, newTree, node.Id);
      }
    }
  }
}
