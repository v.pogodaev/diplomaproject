﻿// ReSharper disable once CheckNamespace
namespace WebApp.ComponentsLib.Tree
{
  /// <summary>
  /// Узел дерева
  /// </summary>
  public class TreeNode
  {
    /// <summary>
    /// Настоящий объект
    /// Явно занимает больше памяти
    /// </summary>
    public object RealObject { get; set; }

    /// <summary>
    /// Id узла
    /// </summary>
    public int Id { get; set; }

    /// <summary>
    /// Название, которое отображаем пользователю
    /// </summary>
    public string Name { get; set; }

    /// <summary>
    /// Id родительского узла
    /// </summary>
    public int? ParentId { get; set; }

    /// <summary>
    /// Глубина текущего узла
    /// Необходимо для отступа
    /// </summary>
    public int Deep { get; set; }

    /// <summary>
    /// Может ли узел иметь потомков
    /// </summary>
    public bool HasChildren { get; set; }

    /// <summary>
    /// Отображается ли узел в данный момент
    /// </summary>
    public bool IsVisible { get; set; } = false;

    /// <summary>
    /// Отображаются ли потомки узла в данный момент
    /// </summary>
    public bool IsExpanded { get; set; } = false;

    /// <summary>
    /// Выбран ли узел в данный момент
    /// </summary>
    public bool IsSelected { get; set; } = false;

    /// <summary>
    /// Был ли узел уже открыт
    /// Необходимо, чтобы не загружать данные повторно
    /// </summary>
    public bool WasExpanded { get; set; } = false;

    /// <summary>
    /// Может ли узел быть раскрыт
    /// </summary>
    public bool IsExpandable { get; set; }

    /// <summary>
    /// Иконка для открытия узла (например, плюсик)
    /// </summary>
    public string IconToOpen { get; set; }

    /// <summary>
    /// Иконка для закрытия узла (например, минус)
    /// </summary>
    public string IconToClose { get; set; }

    /// <summary>
    /// Иконка открытого узла (например, открытая папка)
    /// </summary>
    public string OpenedIcon { get; set; }

    /// <summary>
    /// Иконка закрытого узла (например, закрытая папка)
    /// Используется как иконка для нераскрываемых узлов
    /// </summary>
    public string ClosedIcon { get; set; }

  }
}
