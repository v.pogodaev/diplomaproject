﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;
using WebApp.ComponentsLib.Tree;

namespace WebApp.ComponentsLib.Components.Tree
{
  public class TreeComponentViewModel : ComponentBase
  {
    [Parameter]
    public ComponentsLib.Tree.Tree Tree { get; set; }

    [Parameter]
    public Func<object, int, Task<List<TreeNode>>> OnExpandAsync { get; set; }

    [Parameter]
    public Action<object> OnNodeChoose { get; set; }

    protected override void OnParametersSet()
    {
      Tree.Reorder();
      this.StateHasChanged();
    }


    protected async Task ExpandNodeAsync(TreeNode node)
    {
      if (node.WasExpanded)
      {
        Tree.Nodes.ForEach(n =>
        {
          if (n.ParentId != null && n.ParentId == node.Id)
            n.IsVisible = true;
        });
        node.IsExpanded = true;
      }
      else
      {
        var newNodes = await OnExpandAsync(node.RealObject, node.Id);
        newNodes.ForEach(n =>
        {
          n.IsVisible = true;
          n.Deep = node.Deep + 1;
          n.WasExpanded = false;
          n.IsExpanded = false;
        });
        Tree.Nodes.InsertRange(Tree.Nodes.IndexOf(node) + 1, newNodes);
        //Console.WriteLine("Ноды вставлены");
        node.WasExpanded = true;
        node.IsExpanded = true;
      }

      StateHasChanged();
    }

    protected void CollapseNode(TreeNode node)
    {
      //Console.WriteLine(node.Name + " пытаемся закрыть");
      if (!node.IsExpanded) { return; }
      //Console.WriteLine(node.Name + " закрываем детей");
      foreach (var treeNode in Tree.Nodes)
      {
        //Console.WriteLine(node.Id + "-" + node.Name + " - " + treeNode.Id + "-" + treeNode.Name);
        if (treeNode.ParentId != null && treeNode.ParentId == node.Id && treeNode.IsVisible)
        {
          CollapseBranch(treeNode);
        }
      }
      node.IsExpanded = false;
      this.StateHasChanged();
    }

    protected void CollapseBranch(TreeNode node)
    {
      //Console.WriteLine(node.Name + " is not visible");
      node.IsVisible = false;
      node.IsExpanded = false;
      if (node.IsExpandable)
      {
        Tree.Nodes.ForEach(n =>
        {
          if (n.ParentId != null && n.ParentId == node.Id && n.IsVisible)
          {
            CollapseBranch(n);
          }
        });
      }
    }
  }
}
